<?php

use App\Events\TestNotifications;
use App\Models\Category;
use App\Models\Term;
use App\Models\Verification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Pusher\PushNotifications\PushNotifications;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('getcode', function (Request $request) {
    $data = Verification::orderBy('created_at', 'desc')->where('phone', $request->phone)->first();
    if ($data)
        return $data->code;
    return "الفون غلط";
});
Route::get('print',function (){
    return view('print');
});
Route::get('/1', function () {
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return "good";
});

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/test-pusher-function', function () {
    $options = array(
        'cluster' => 'eu',
        'useTLS' => true
    );
    $pusher = new Pusher\Pusher(
        '75b53060b1f415501d21',
        '651def2f37f73734fe45',
        '1290713',
        $options
    );

    $data['message'] = 'hello world';
    $pusher->trigger('test-channel', 'TestNotifications', $data);
    if (broadcast(new TestNotifications("emma")))
        $beamsClient = new PushNotifications(array(
            "instanceId" => "4b9224fd-ac88-4e17-86bf-65c347bc0fbd",
            "secretKey" => "9ADC639AFE236794A06B8927D5FA6B28FA2E8A4E968E13A8556A5C9F8CF9548B",
        ));
//
    $publishResponse = $beamsClient->publishToInterests(
        array("debug-user"),
        array(
        "apns" => array("aps" =>
            array(
                "alert" => array(
                    "title" => "Hello",
                    "body" => "Hello, World!",

                ),
                "sound" => "default",
                "badge" => 5,
                "driver_id" => 3,



            )),
        "fcm" => array("data" =>
                    array(
                        "title" => "Hello",
                        "body" => "Hello, World!",
                        "driver_id" => 1
                    )),




        )


    );
//    $publishResponse = $beamsClient->publishToInterests(
//        array("debug-user"),
//        array("fcm" =>
//            array("data" =>
//            array(
//            "title" => "Hello",
//            "body" => "Hello, World!",
//            "driver_id" => 1
//        )),
//
//
//        )
//    );


//        event(new TestNotifications("test"));
    $publishResponse = $beamsClient->publishToUsers(
        array("1"),
        array(
            "fcm" => array(
                "notification" => array(
                    "title" => "Hi!",
                    "body" => "This is my first Push Notification!"
                )
            ),
            "apns" => array("aps" => array(
                "alert" => array(
                    "title" => "Hi!",
                    "body" => "This is my first Push Notification!",



                ),
                "sound" => "default",
                "badge" => 5,


                "notification" => array(
                    "title" => "hello!",
                    "body" => "testtest!",
                    "sound" => "default"

                )

            )),
            "web" => array(
                "notification" => array(
                    "title" => "Hi!",
                    "body" => "This is my first Push Notification!"
                )
            )
        ));

    return "true";
//    broadcast(new TestNotifications());

});
Route::get('/pusher/beams-auth', function (Request $request) {
    $userID = "1"; // If you use a different auth system, do your checks here
    $userIDInQueryParam = Input::get('user_id');
    $beamsClient = new PushNotifications(array(
        "instanceId" => "4b9224fd-ac88-4e17-86bf-65c347bc0fbd",
        "secretKey" => "9ADC639AFE236794A06B8927D5FA6B28FA2E8A4E968E13A8556A5C9F8CF9548B",
    ));

    if ($userID != $userIDInQueryParam) {
        return response('Inconsistent request', 401);
    } else {
        $beamsToken = $beamsClient->generateToken($userID);
        return response()->json($beamsToken);
    }
});

Route::get('/test-pusher', function () {
    return view('test-pusher');
});

//-----------------------------
Route::get('/chat', function () {
    return view('chat');
});
Route::get('/test', function () {
    return view('success');
});
Route::get('/register-shop', function () {
    $categories = Category::whereActive(1)->get();
    return view('register_shop', compact('categories'));
})->name('shop-form');
Route::post('/register-new-shop', 'Controller@registerNewShop')
    ->name('registerNewShop');
//Route::post('/register-new-shop', function (Request $request) {
//    if(!isset($request->terms)){
//        return back();
//    }
//    $data = \App\Models\ShopRequest::create($request->all());
//    if($data)
//        return view('register_shop',compact('categories'));
//    return back();
//})->name('registerNewShop');
//----------------------------
Route::get('/shop-terms', function () {
    $term = Term::where('type', 4)->select('term_ar')->first()->term_ar;
    return view('shop_terms', compact('term'));
});
/////////////////////////////////
Route::get('/captin-form', function () {
    return view('captin-form');
});
Route::post('/signup/form', 'SiteController@formRequest')->name('delegate-form-submit');
Route::post('/signup/send_verification_code', 'SiteController@sendVerification');
Route::post('/signup/verify_user', 'SiteController@verifyUser');
Route::get('/signup/form', function () {
    $user_id = request()->user_id;
    $phone = request()->phone;
    //$car_types = \App\CarType::get();
    $car_types = [];
    return view('signup-form', compact('user_id', 'phone', 'car_types'));
});
///////////////////////
Route::get('/privacy/{type}/{lang}', 'HomeController@privacy')->name('privacy');
Route::get('/terms/{type}/{lang}', 'HomeController@terms')->name('terms');

//Auth::routes();
Route::group(['prefix' => '/admin'], function () {
    Route::get('/login', 'Admin\AuthController@login_view');
    Route::get('/forget-password', 'Admin\AuthController@forgetPassword')->name('password.forget');
    Route::post('/forget-password', 'Admin\AuthController@checkMail')->name('check-mail');
    Route::get('/change-password/{admin_id}', 'Admin\AuthController@changePassword')->name('change-password');
    Route::post('/change-password', 'Admin\AuthController@changePasswordSubmit')->name('change-password-submit');
    Route::post('/login', 'Admin\AuthController@login')->name('login');
    Route::get('/logout', 'Admin\AuthController@logout')->name('logout');


    Route::group(['middleware' => 'auth:admin'], function () {

        Route::get('/home', 'HomeController@index')->name('admin.home');

        Route::resource('/departments', 'Admin\DepartmentController');
        Route::post('/departments/edit', 'Admin\DepartmentController@editDepartment')->name('editDepartment');
        Route::get('/departments/editStatus/{id}', 'Admin\DepartmentController@editDepartmentStatus')
            ->name('editDepartmentStatus');

        Route::resource('/sub_departments', 'Admin\SubDepartmentController');
        Route::get('/get_sub_departments/{department_id}', 'Admin\SubDepartmentController@getSubDepartments');
        Route::post('/sub_departments/edit', 'Admin\SubDepartmentController@editSubDepartment')
            ->name('editSubDepartment');
        Route::get('/sub_departments/editStatus/{id}', 'Admin\SubDepartmentController@editSubDepartmentStatus')
            ->name('editSubDepartmentStatus');

        //chat between admin and users
        Route::get('chat', 'Admin\ChatController@index');
        Route::get('reply/{id}', 'Admin\ChatController@reply')->name('admin.reply');
        Route::post('send', 'Admin\ChatController@create_message')->name('admin.send');

        Route::get('/sliders', 'Admin\SliderController@index')->name('sliders');
        Route::post('/sliders', 'Admin\SliderController@store')->name('sliders-store');
        Route::post('/sliders/edit', 'Admin\SliderController@editSlider')->name('editSlider');
        Route::get('/sliders/delet/{id}', 'Admin\SliderController@deleteSlider')->name('deleteSlider');

        Route::resource('/clients', 'Admin\User2Controller');
        Route::get('/clients/editStatus/{id}', 'Admin\User2Controller@editClientStatus')->name('editClientStatus');
        Route::get('/clients/complains/{user_id}', 'Admin\User2Controller@complains')->name('users.complains');

        Route::get('/clients/orders/{user_id}/{department_id}', 'Admin\User2Controller@orders')->name('users.orders');
        Route::post('/clients/orders-search', 'Admin\User2Controller@searchOrders')->name('users.orders.search');
        Route::get('/clients/trips/{user_id}', 'Admin\User2Controller@trips')->name('users.trips');
        Route::get('/clients/wallets/{user_id}', 'Admin\User2Controller@wallets')->name('users.wallets');
        Route::get('/delegates', 'Admin\User2Controller@index3');
        Route::post('/edit-documents', 'Admin\User2Controller@editDocuments')->name('edit-documents');
        Route::get('/download-delegates/{accept}/{type}', 'Admin\User2Controller@downloadDelegates')->name('delegates.download');
        Route::get('/delegates-with-area', 'Admin\User2Controller@indexDelegatesWithAreas')->name('delegates.with-areas');
        Route::post('/delegates-with-area', 'Admin\User2Controller@delegatesAreasSearch')->name('delegates.areas.search');
        Route::get('/delegates/waiting', 'Admin\User2Controller@waitingDelegates');
        Route::get('/delegates-heavey', 'Admin\User2Controller@index3Heavey');
        Route::get('/delegates-shops/{delegate_id}', 'Admin\User2Controller@delegateShops')->name('delegate.shops');
        Route::get('/delegates-heavy-with-area', 'Admin\User2Controller@indexHeavyDelegatesWithAreas')->name('heavy-delegates.with-areas');
        Route::post('/delegates-heavy-with-area', 'Admin\User2Controller@heavyDelegatesAreasSearch')->name('heavy-delegates.areas.search');

        Route::get('/delegates-heavey/waiting', 'Admin\User2Controller@waitingDelegatesHeavey');
        Route::get('/delegate-orders/{delegate_id}', 'Admin\User2Controller@delegateOrders')->name('delegateOrders');
        Route::get('/delegates/editStatus/{id}', 'Admin\User2Controller@editDelegateStatus')->name('editDelegateStatus');
        Route::get('/delegates/editReasonStatus/{id}', 'Admin\User2Controller@editReasonStatus')
            ->name('editReasonStatus');
        Route::get('/drivers/editDriverReasonStatus/{id}', 'Admin\User2Controller@editDriverReasonStatus')
            ->name('editDriverReasonStatus');

        Route::get('/drivers', 'Admin\User2Controller@index2');
        Route::get('/drivers-trips-download/{driver_id}', 'Admin\User2Controller@downloadTrips')->name('download.trips');
        Route::get('/drivers-download\{active}', 'Admin\User2Controller@downloadDrivers')->name('drivers.download');
        Route::get('/drivers/waiting', 'Admin\User2Controller@waitingDrivers');
        Route::get('/all-drivers', 'Admin\User2Controller@allDrivers')->name('all-drivers');
        Route::get('/drivers-with-area', 'Admin\User2Controller@driversByArea')->name('drivers-by-area');
        Route::post('/drivers-with-area', 'Admin\User2Controller@driversByAreaSearch')->name('drivers-by-area-search');
        Route::get('/driver-trips/{driver_id}', 'Admin\User2Controller@driverTrips')->name('driverTrips');
        Route::post('/driver-trips/search', 'Admin\User2Controller@tripsSearch')->name('trips.search');
        Route::get('/user-trips/{user_id}', 'Admin\User2Controller@userTrips')->name('userTrips');
        Route::get('/drivers/accept/{id}', 'Admin\User2Controller@accept_driver')->name('acceptDriver');
        Route::get('/delegates/accept/{id}', 'Admin\User2Controller@accept_delegate')->name('acceptDelegate');
        Route::post('/drivers/editCarLevel', 'Admin\User2Controller@editCarLevel')->name('editCarLevel');

        Route::post('/drivers/editCarLevelFlag', 'Admin\User2Controller@editCarLevelFlag')
            ->name('editCarLevelFlag');
        Route::get('/drivers/editStatus/{id}', 'Admin\User2Controller@editDriverStatus')->name('editDriverStatus');

        Route::get('/drivers/calculations/{driver_id}', 'Admin\User2Controller@calculationsDrivers');
        Route::get('/delegates/calculations/{delegate_id}', 'Admin\User2Controller@calculationsDelegates');
        Route::post('/delegates/search-orders', 'Admin\User2Controller@searchDelegateOrders')->name('delegate.orders.search');
        Route::get('/delegates-heavey/calculations/{delegate_id}', 'Admin\User2Controller@calculationsDelegatesHeavey');


        Route::get('/admins', 'Admin\User2Controller@indexAdmin');
        Route::get('/download-admin', 'Admin\AdminController@downloadAdmins')->name('admins.download');
        Route::get('/admins/create', 'Admin\User2Controller@createAdminForm')->name('admin.create');
        Route::get('/admins-by-area', 'Admin\User2Controller@adminsByArea')->name('admin.admins-by-area');
        Route::post('/admins-by-area', 'Admin\User2Controller@adminsByAreaSearch')->name('admin.admins-by-area-search');
        Route::post('/createAdmin', 'Admin\User2Controller@createAdmin')->name('createAdmin');
        Route::post('/editAdmin', 'Admin\User2Controller@editAdmin')->name('editAdmin');
        Route::get('/admins/editStatus/{id}', 'Admin\User2Controller@editAdminStatus')->name('editAdminStatus');
        Route::get('/admins/delet/{id}', 'Admin\User2Controller@deleteAdmin')->name('deleteAdmin');
//
        Route::get('/finished-driver-trips/{driver_id}', 'Admin\User2Controller@finishedDriverTrips');
        Route::get('/finished-user-trips/{user_id}', 'Admin\User2Controller@finishedUserTrips');

        Route::resource('/offer_points', 'Admin\OfferPointController');
        Route::post('/offer_points/edit', 'Admin\OfferPointController@editOfferPoint')->name('editOfferPoint');
        Route::get('/user_offer_points', 'Admin\OfferPointController@user_offer_points');
        Route::get('/user_offer_points/editStatus/{id}', 'Admin\OfferPointController@editUserOfferPointStatus')
            ->name('editUserOfferPointStatus');
        Route::get('/delegate_offer_points', 'Admin\OfferPointController@delegate_offer_points');
        Route::get('/shop_rates/{type}', 'Admin\ShopRateController@index')->name('rates.index');
        Route::get('/download-rates/{department_id}', 'Admin\ShopRateController@downloadRates')->name('download.rates');

        Route::resource('/shop_rates', 'Admin\ShopRateController', ['except' => ['index']]);
        Route::get('/shop_rates/show/{shop_id}', 'Admin\ShopRateController@showRates')->name('shop_rates.show');

        Route::resource('/countriess', 'Admin\CountryController');
        Route::post('/countriess/edit', 'Admin\CountryController@edit_country')->name('editCountry');
        Route::get('/countriess/editStatus/{id}', 'Admin\CountryController@editCountryStatus')->name('editCountryStatus');

        Route::resource('/cities', 'Admin\CityController');
        Route::post('/cities/edit', 'Admin\CityController@edit_city')->name('editCity');
        Route::get('/cities/editStatus/{id}', 'Admin\CityController@editCityStatus')->name('editCityStatus');


        Route::resource('/nationals', 'Admin\NationalController');
        Route::post('/nationals/edit', 'Admin\NationalController@editNational')->name('editNational');
        Route::get('/national/editStatus/{id}', 'Admin\NationalController@editNationalStatus')->name('editNationalStatus');

        Route::get('/car_types/{type}', 'Admin\CarTypeController@index')->name('car_type.index');

        Route::resource('/car_types', 'Admin\CarTypeController', ['except' => ['index']]);
        Route::post('/car_types/edit', 'Admin\CarTypeController@editCarType')->name('editCarType');
        Route::get('/car_types/editStatus/{id}', 'Admin\CarTypeController@editCarTypeStatus')
            ->name('editCarTypeStatus');

        Route::resource('/rushhours', 'Admin\RushhourController');
        Route::post('/rushhours/edit', 'Admin\RushhourController@edit_rushhour')->name('editRushhour');
        Route::get('/rushhours/delet/{id}', 'Admin\RushhourController@delete_rushhour')->name('deleteRushhour');

        Route::resource('/reasons', 'Admin\ReasonController');
        Route::post('/reasons/edit', 'Admin\ReasonController@edit_reason')->name('editReason');
        Route::get('/reasons/delet/{id}', 'Admin\ReasonController@delete_reason')->name('deleteReason');

        Route::resource('/reasons', 'Admin\ReasonController');
        Route::post('/reasons/edit', 'Admin\ReasonController@edit_reason')->name('editReason');
        Route::get('/reasons/delet/{id}', 'Admin\ReasonController@delete_reason')->name('deleteReason');
//
        Route::resource('/issues', 'Admin\IssueController');
        Route::post('/issues/edit', 'Admin\IssueController@edit_issue')->name('editIssue');
        Route::get('/issues/delet/{id}', 'Admin\IssueController@delete_issue')->name('deleteIssue');
//
        Route::resource('/bank_account', 'Admin\BankAccountController');
        Route::post('/bank_account/edit', 'Admin\BankAccountController@edit_bank_account')->name('editBankAccount');
        Route::get('/bank_account/delet/{id}', 'Admin\BankAccountController@delete_bank_account')->name('deleteBankAccount');
//
        Route::resource('/losts', 'Admin\LostController');
        Route::get('/losts-text', 'Admin\LostController@lostText')->name('losts.text');

        Route::post('/losts/edit', 'Admin\LostController@edit_lost')->name('editLost');
        Route::get('/losts/delet/{id}', 'Admin\LostController@delete_lost')->name('deleteLost');
//
        Route::resource('/bank_transfers', 'Admin\BankingTransferController');
        Route::get('/bank_transfers_admin', 'Admin\BankingTransferController@from_admin')->name('bank_transfers.from_admin');
//
        Route::get('/promocodes/{department_id}', 'Admin\PromocodeController@index')->name('promocodes.index');

        Route::resource('/promocodes', 'Admin\PromocodeController', ['except' => ['index']]);
        Route::get('/promocodes/delet/{id}', 'Admin\PromocodeController@delete_promo')->name('deletePromo');

        Route::resource('/carlevelss', 'Admin\LevelController');
        Route::post('/carlevelss/edit', 'Admin\LevelController@edit_carlevels')->name('editCarlevel');
        Route::get('/carlevelss/editStatus/{id}', 'Admin\LevelController@editCarlevelStatus')
            ->name('editCarlevelStatus');

        Route::resource('/carprices', 'Admin\CarPriceController');
        Route::post('/carprices/edit', 'Admin\CarPriceController@edit_carprices')->name('editCarprice');

        Route::resource('/govs', 'Admin\GovController');
        Route::post('/govs/edit', 'Admin\GovController@edit_gov')->name('editGov');

        Route::resource('/cities', 'Admin\CityController');
        Route::post('/cities/edit', 'Admin\CityController@edit_city')->name('editCity');

        Route::resource('/cats', 'Admin\CategoryController');
        Route::post('/cats/edit', 'Admin\CategoryController@edit_cat')->name('editCat');
        Route::get('/cats/editStatus/{id}', 'Admin\CategoryController@editCatStatus')->name('editCatStatus');

        Route::resource('/periods', 'Admin\PeriodController');
        Route::post('/periods/edit', 'Admin\PeriodController@editPeriod')->name('editPeriod');
        Route::get('/periods/editStatus/{id}', 'Admin\PeriodController@editPeriodStatus')->name('editPeriodStatus');


        Route::resource('/trips', 'Admin\TripController');
        Route::get('/finished-trips', 'Admin\TripController@finishedTrips');
        Route::get('/unfinished-trips', 'Admin\TripController@unfinishedTrips');
        Route::get('/cancelled-trips', 'Admin\TripController@cancelledTrips');
        Route::post('/trips', 'Admin\TripController@checkPaymentSrtatus')->name('checkPaymentSrtatus');
//Route::get('/offs/editStatus/{id}', 'Admin\OfferController@editOfferStatus')->name('editOfferStatus');

        Route::resource('/notifications', 'Admin\NotificationController');
        Route::get('/notifications/delet/{id}', 'Admin\NotificationController@delete_not');

        Route::resource('/reviews', 'Admin\ReviewController');
        Route::get('/reviews/delet/{id}', 'Admin\ReviewController@delete_review');

        Route::resource('/terms', 'Admin\TermController');
        Route::post('/terms/edit', 'Admin\TermController@edit_terms')->name('editTerm');
        Route::get('/complains_suggestions/{type}', 'Admin\ComplainSuggestController@index')->name('complains_suggestions.index');
        Route::get('/finished-complains/{type}', 'Admin\ComplainSuggestController@finished')->name('complains_suggestions.finished.index');
        Route::get('/close-complain/{complain_id}', 'Admin\ComplainSuggestController@closeComplain')->name('complain.close');

        Route::resource('/complains_suggestions', 'Admin\ComplainSuggestController', ['except' => ['index']]);
        Route::get('/download-complains/{status}', 'Admin\ComplainSuggestController@downloadComplains')->name('complains.download');

        Route::get('/contact_us/{type}', 'Admin\ContactusController@index')->name('contact_us.index');
        Route::get('/closed-contact_us/{type}', 'Admin\ContactusController@closedContacts')->name('contact_us.closed.index');
        Route::get('/close/{contact_id}', 'Admin\ContactusController@close')->name('contact-us.close');
        Route::resource('/contact_us', 'Admin\ContactusController', ['except' => ['index']]);

        Route::resource('/abouts', 'Admin\AboutController');
        Route::post('/abouts/edit', 'Admin\AboutController@edit_abouts')->name('editAbout');
        Route::get('/settings/{department_id}', 'Admin\SettingController@index')->name('settings.index');

        Route::resource('/settings', 'Admin\SettingController', ['except' => ['index']]);
        Route::post('/settings/edit', 'Admin\SettingController@edit_settings')->name('edit_settings');
        Route::get('/app_explanations/{type}', 'Admin\AppExplanationController@index')->name('app_explanations.index');

        Route::resource('/app_explanations', 'Admin\AppExplanationController', ['except' => ['index']]);
        Route::post('/app_explanations/edit', 'Admin\AppExplanationController@edit_explains')->name('editExplain');
        Route::post('/app_explanations/delet', 'Admin\AppExplanationController@deleteAppExplanations')->name('deleteAppExplanations');

        Route::get('/app_privacies', 'Admin\AppPrivacyController@index');
        Route::post('/app_privacies', 'Admin\AppPrivacyController@createPrivacy')->name('createPrivacy');
        Route::get('/app_privacies/{id}', 'Admin\AppPrivacyController@editView')->name('editPrivacyView');
        Route::post('/app_privacies/edit', 'Admin\AppPrivacyController@edit_explains')->name('editPrivacy');
        Route::post('/app_privacies/delet', 'Admin\AppPrivacyController@deleteAppExplanations')->name('deletePrivacy');


        Route::resource('/categories', 'Admin\CategoryController');
        Route::post('/categories/mainCat', 'Admin\CategoryController@storeMainCat')->name('storeMainCat');
        Route::post('/categories/delet', 'Admin\CategoryController@delete_cat')->name('deleteCat');
        Route::get('/shops/branches/{shop_id}', 'Admin\ShopController@branches')->name('shop.branches');

        Route::resource('/shops', 'Admin\ShopController');
        Route::get('/download-shops/{active}', 'Admin\ShopController@downloadShops')->name('shops.download');
        Route::post('/shops/editStatus', 'Admin\ShopController@editShopStatus')->name('editShopStatus');
        Route::post('/shops/editVerified', 'Admin\ShopController@editShopVerified')->name('editShopVerified');
        Route::get('/shops/create', 'Admin\ShopController@createShop')->name('createShop');
        Route::get('/shops-by-area', 'Admin\ShopController@shopsByArea')->name('shopsByArea');
        Route::post('/shops-by-area', 'Admin\ShopController@shopsByAreaSearch')->name('shopsByAreaSearch');
        Route::get('/shops/edit/{shop_id}', 'Admin\ShopController@editShop')->name('editShop');
        Route::get('/shops/orders/{shop_id}', 'Admin\ShopController@orders')->name('shops.orders');
        Route::post('/shops/orders/search', 'Admin\ShopController@searchOrders')->name('shops.orders.search');
        Route::post('/shops/edit', 'Admin\ShopController@updateShop')->name('updateShop');
        Route::post('/shops/delet', 'Admin\ShopController@delete_shop')->name('deleteShop');
        Route::get('/active-shops', 'Admin\ShopController@activeShops');
        Route::get('/inactive-shops', 'Admin\ShopController@inactiveShops');
        Route::get('/shop_requests', 'Admin\ShopController@shopRequests')->name('shopRequests');
        Route::post('/shop_requests/delete', 'Admin\ShopController@destroyRequest')->name('shopRequests.delete');
        Route::get('/shop_requests/accept/{id}', 'Admin\User2Controller@acceptShop')->name('acceptShop');
        Route::post('/shop_requests/editStatus', 'Admin\ShopController@editRequestStatus')->name('editRequestStatus');


        Route::resource('/all-orders', 'Admin\OrderController');
        Route::post('/orders/search', 'Admin\OrderController@search')->name('orders.search');
        Route::get('/download-orders', 'Admin\OrderController@downloadOrders')->name('orders.download');
        Route::post('/orders/search-status', 'Admin\OrderController@searchStatus')->name('orders.search-status');
        Route::get('/orders-by-type/{type}', 'Admin\OrderController@ordersByType')->name('orders-by-type.index');
        Route::any('/search-orders', 'Admin\OrderController@search')->name('orders.search');
        Route::get('accept-orders', 'Admin\OrderController@acceptOrders')->name('admin-accept-orders');
        Route::get('onway-orders', 'Admin\OrderController@onwayOrders')->name('admin-onway-orders');
        Route::get('finished-orders', 'Admin\OrderController@finishedOrders')->name('admin-finished-orders');
        Route::get('cancelled-orders', 'Admin\OrderController@cancelledOrders')->name('admin-cancelled-orders');
        Route::get('confirm-accept-orders', 'Admin\OrderController@confirmiedAcceptOrders')
            ->name('admin-confirm-accept-orders');


//reports
        Route::get('reports', "Admin\ReportController@reports")->name('reports');
        Route::post('Report', "Admin\ReportController@makeReport")->name('makeReport');

        Route::post('usersReport', "Admin\ReportController@usersReport")->name('usersReport');
        Route::get('usersInvoice', "Admin\ReportController@usersInvoice")->name('usersInvoice');

        Route::post('delegatesReport', "Admin\ReportController@delegatesReport")->name('delegatesReport');
        Route::get('delegatesInvoice', "Admin\ReportController@delegatesInvoice")->name('delegatesInvoice');

        Route::post('delegatesHeaveyReport', "Admin\ReportController@delegatesHeaveyReport")->name('delegatesHeaveyReport');
        Route::get('delegatesHeaveyInvoice', "Admin\ReportController@delegatesHeaveyInvoice")->name('delegatesHeaveyInvoice');

        Route::post('driversReport', "Admin\ReportController@driversReport")->name('driversReport');
        Route::get('driversInvoice', "Admin\ReportController@driversInvoice")->name('driversInvoice');

        Route::post('shopsInvoice', "Admin\ReportController@shopsInvoice")->name('shopsInvoice');
        Route::get('shopsInvoice', "Admin\ReportController@shopsInvoice")->name('shopsInvoice');

        Route::post('tripsReport', "Admin\ReportController@tripsReport")->name('tripsReport');
        Route::get('tripsInvoice', "Admin\ReportController@tripsInvoice")->name('tripsInvoice');

        Route::post('ordersShopsReport', "Admin\ReportController@ordersShopsReport")->name('ordersShopsReport');
        Route::get('ordersShopsInvoice', "Admin\ReportController@ordersShopsInvoice")->name('ordersShopsInvoice');

        Route::post('ordersNormalReport', "Admin\ReportController@ordersNormalReport")->name('ordersNormalReport');
        Route::get('ordersNormalInvoice', "Admin\ReportController@ordersNormalInvoice")->name('ordersNormalInvoice');

    });
});


//////////shop
Route::group(['prefix' => '/shop'], function () {
    Route::get('/login', 'Shop\AuthController@login_view')->name('shops.login-page');
    Route::post('/login', 'Shop\AuthController@login')->name('login_shop');
    Route::get('/payment', 'Shop\AuthController@Payment')->name('shops.payment');
    Route::get('/check-payment', 'Shop\AuthController@checkPayment')->name('shops.check.payment');
    Route::get('/logout', 'Shop\AuthController@logout')->name('logout_shop');
    Route::get('/turn-notification', 'Shop\AuthController@turnNotification')->name('shops.notification');
    Route::get('/notifications', 'Shop\AuthController@notifications')->name('shops.notifications.page');

    Route::get('/forget-password', 'Shop\AuthController@forgetPassword')->name('shop.password.forget');
    Route::post('/forget-password', 'Shop\AuthController@checkMail')->name('shop.check-mail');
    Route::get('/change-password/{admin_id}', 'Shop\AuthController@changePassword')->name('change-password');
    Route::post('/change-password', 'Shop\AuthController@changePasswordSubmit')->name('change-password-submit');
    Route::get('invoice/{order_id}', 'Shop\OrderController@printOrder')->name('shop.orders.print');
    Route::get('orders/pdf/{order_id}', 'Shop\OrderController@pdfOrder')->name('shop.orders.pdf');
    Route::get('print-receipt', 'Shop\OrderController@printReceipt')->name('shop.printtt');

    Route::group(['middleware' => 'auth:shop'], function () {


        Route::get('/admins', 'Shop\User2Controller@indexAdmin');
        Route::get('/generate-auth-token', 'Shop\AuthController@generateAuthToken')->name('shops.generate-auth-token');
        Route::post('/createAdmin', 'Shop\User2Controller@createAdmin')->name('createAdminShop');
        Route::post('/editAdmin', 'Shop\User2Controller@editAdmin')->name('editAdminShop');
        Route::get('/admins/editStatus/{id}', 'Shop\User2Controller@editAdminStatus')->name('editAdminShopStatus');
        Route::get('/admins/delet/{id}', 'Shop\User2Controller@deleteAdmin')->name('deleteAdminShop');

        Route::get('/edit-profile', 'Shop\ShopController@editShopProfile')->name('editShopProfile');
        Route::post('/edit-profile', 'Shop\ShopController@updateShopProfile')->name('updateShopProfile');

        Route::get('/edit-dailyWork', 'Shop\ShopController@editDailyWork')->name('editDailyWork');
        Route::post('/edit-dailyWork', 'Shop\ShopController@updateDailyWork')->name('updateDailyWork');

        Route::get('/home', 'ShopHomeController@index')->name('shop_home');

        Route::resource('/clients', 'Shop\User2Controller');
        Route::get('/delegates', 'Shop\User2Controller@delegates');
        Route::get('/pending-delegates', 'Shop\User2Controller@pendingDelegate')->name('shops.pending-delegates');
        Route::get('/accept-delegates/{request_id}', 'Shop\User2Controller@acceptDelegate')->name('shops.accept-delegates');
        Route::get('/delegates', 'Shop\User2Controller@delegates');
        Route::get('/create-delegate', 'Shop\User2Controller@createDelegate')->name('delegates.create');
        Route::post('/create-delegate', 'Shop\User2Controller@storeDelegate')->name('delegates.store');

        Route::get('/download-delegates', 'Shop\User2Controller@downloadDelegates')->name('shops.delegates.download');
        Route::get('/sliders', 'Shop\User2Controller@sliders');


//
        Route::resource('/menus', 'Shop\MenuController');
        Route::post('/menus/edit', 'Shop\MenuController@editMenu')->name('editMenu');
        Route::post('/menus/delet', 'Shop\MenuController@deleteMenu')->name('deleteMenu');

        Route::resource('/branches', 'Shop\BranchController');
        Route::post('/branches/editStatus', 'Shop\BranchController@editBranchStatus')->name('editBranchStatus');
        Route::post('/branches/editVerified', 'Shop\BranchController@editBranchVerified')->name('editBranchVerified');
        Route::get('/branches/create', 'Shop\BranchController@createBranch')->name('createBranch');
        Route::get('/branches/edit/{shop_id}', 'Shop\BranchController@editBranch')->name('editBranch');
        Route::post('/branches/edit', 'Shop\BranchController@updateBranch')->name('updateBranch');
        Route::post('/branches/delet', 'Shop\BranchController@deleteBranch')->name('deleteBranch');
        Route::get('/active-branches', 'Shop\BranchController@activeBranchs');
        Route::get('/inactive-branches', 'Shop\BranchController@inactiveBranchs');

//        Route::resource('/branches', 'Shop\BranchController');
//        Route::post('/branches/edit', 'Shop\BranchController@editBranch')->name('editBranch');
//        Route::post('/branches/delet', 'Shop\BranchController@deleteBranch')->name('deleteBranch');

        Route::resource('/products', 'Shop\ProductController');
        Route::post('/products/editStatus', 'Shop\ProductController@editProductStatus')->name('editProductStatus');
        Route::post('/products/edit', 'Shop\ProductController@editProduct')->name('editProduct');
        Route::post('/products/delet', 'Shop\ProductController@deleteProduct')->name('deleteProduct');
        Route::post('/products/addVar', 'Shop\ProductController@addVariation')->name('addVariation');
        Route::post('/products/editVar', 'Shop\ProductController@editVariation')->name('editVariation');
        Route::post('/products/deleteVar', 'Shop\ProductController@deleteVariation')->name('deleteVariation');
        Route::post('/products/deleteOption', 'Shop\ProductController@deleteOption')->name('deleteOption');
        Route::post('/products/addOption', 'Shop\ProductController@addOption')->name('addOption');
        Route::post('/products/editOption', 'Shop\ProductController@editOption')->name('editOption');
        Route::get('/download-products', 'Shop\ProductController@downloadProducts')->name('download.products');
        Route::post('/import-products', 'Shop\ProductController@importProducts')->name('import.products');

        Route::resource('/orders', 'Shop\OrderController');
        Route::get('shops/download-orders', 'Shop\OrderController@downloadOrders')->name('shop.orders.download');
        Route::get('accept-orders', 'Shop\OrderController@acceptOrders')->name('accept-orders');
        Route::get('onway-orders', 'Shop\OrderController@onwayOrders')->name('onway-orders');
        Route::get('finished-orders', 'Shop\OrderController@finishedOrders')->name('finished-orders');
        Route::get('cancelled-orders', 'Shop\OrderController@cancelledOrders')->name('cancelled-orders');

        Route::resource('/shops-promocodes', 'Shop\PromocodeController');
        Route::get('/shops-promocodes/delete/{id}', 'Shop\PromocodeController@delete_promo')->name('deletePromo');

        Route::get('print-pos', 'Shop\OrderController@printESCPOS');


//reports
//        Route::get('reports', "Admin\ReportController@reports")->name('reports');
//        Route::post('Report', "Admin\ReportController@makeReport")->name('makeReport');
//
//        Route::post('usersReport', "Admin\ReportController@usersReport")->name('usersReport');
//        Route::get('usersInvoice', "Admin\ReportController@usersInvoice")->name('usersInvoice');
//
//        Route::post('driversReport', "Admin\ReportController@driversReport")->name('driversReport');
//        Route::get('driversInvoice', "Admin\ReportController@driversInvoice")->name('driversInvoice');
//
//        Route::post('tripsReport', "Admin\ReportController@tripsReport")->name('tripsReport');
//        Route::get('tripsInvoice', "Admin\ReportController@tripsInvoice")->name('tripsInvoice');

    });
});
Route::get('PrintESCPOSController', 'PrintESCPOSController@printCommands')->name('print.commands');
Route::any('WebClientPrintController', 'WebClientPrintController@processRequest')->name('print.process');


