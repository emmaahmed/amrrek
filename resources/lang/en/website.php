<?php
return[
    'project-title'=>'Amrrek',
    'home'=>'Home',
    'about-amrk'=>'About Amrrek',
    'services'=>'Services',
    'stores'=>'Stores',
    'help'=>'Help',
    'contact-us'=>'Contact Us',
    'be-a-shop'=>'Be A Shop',
    'social-contacts'=>'Social Contacts',
    'be-shop'=>'Be Shop',
    'be-delivery'=>'Be Delivery',
    'be-driver'=>'Be Driver',
    'be-heavy-driver'=>'Be Heavy Driver',
    'terms-and-conditions'=>'Terms & Conditions',
    'copy-right'=>'Copyright ©2021 Reserved',
    'amrk-services'=>'Amrrek Services',
    'services-desc'=>'These controls and conditions represent a formal "contract" agreement between (Amrrek as a first party) (and the    trader-seller -as a second party) within the Amrrek platform, applications, accessories, and accessories available as products on the Apple.LLc Store as well as on the Google Play Store.',
    'about'=>'About',
    'top-stores'=>'Top Stores',
    'top-stores-desc'=>'Find everything you need
groceries and more
Have your daily essentials delivered to you with
AmrrekMart. Whether it s ingredients for tonight s dinner
or if you ve run out of shampoo, order what you need in
just a few taps.',
    'show-all-stores'=>'Show All Stores',
    'success-partner'=>'Success Partner',
    'about-us'=>'About Us',
    'shops'=>'Shops',
    'users'=>'Users',
    'drivers'=>'Drivers',
    'cities'=>'Cities',
    'gallery'=>'Gallery',
    'quick-links'=>'Quick Links',
    'more-services'=>'More Services',
    'contact-us-desc'=>'Talk to us. Share your thoughts
Amrrek is listen to you..',
    'contact-information'=>'Contact Information',
    'please-enter-your-name'=>'Please Enter Your Name',
    'name'=>'Name',
    'please-enter-your-number'=>'Please Enter Your Number',
    'phone'=>'Phone',
    'email'=>'Email',
    'please-enter-your-email'=>'Please Enter Your Email',
    'write-your-message'=>'Write Your Message',
    'send-message'=>'Send Message',
    'contact-form'=>'Contact Form',
    'submitted-successfully'=>'Submitted successfully',
    'shops-service'=>'Shops Services',
    'shops-service-desc'=>'Find everything you need
groceries and more
Have your daily essentials delivered to you with
AmrrekMart. Whether it s ingredients for tonight s dinner
or if you ve run out of shampoo, order what you need in
just a few taps.',
    'all'=>'All',
    'search-now'=>'Search Now',
    'location'=>'location',
    'enter-shop-name'=>'Enter Shop Name',
    'all'=>'All',
    'message'=>'Message',
    'enter-your-email'=>'Enter your email',
    'off'=>'off',
    'already-sent'=>'email already sent',
    'select-subject'=>'Select Subject',
    'payment'=>'Payment',
    'delivery'=>'Delivery',
    'delivery-payment'=>'Delivery & Payment',
    'website'=>'Website',
    'application'=>'Application',
    'shop-login'=>'Shop Login',
];