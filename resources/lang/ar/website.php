<?php
return[
    'project-title'=>'أمرك',
    'home'=>'الصفحة الرئيسية',
    'about-amrk'=>'عن أمرك',
    'services'=>'الخدمات',
    'stores'=>'المتاجر',
    'help'=>'مساعدة',
    'contact-us'=>'اتصل بنا',
    'be-a-shop'=>'كن متجرا',
    'social-contacts'=>'روابط التواصل الاجتماعي',
    'be-shop'=>'كن متجرا',
    'be-delivery'=>'كن مندوب',
    'be-driver'=>'كن سائق',
    'be-heavy-driver'=>'كن مندوب ناقل',
    'terms-and-conditions'=>'الشروط والأحكام',
    'copy-right'=>'حقوق الطبع والنشر © 2021 محفوظة',
    'amrk-services'=>'خدمات أمرك',
    'services-desc'=>'أمرك هي منصة التوصيل العملاقة في المملكة. أمرك هي تجربة فريدة عند الطلب تحصل على أعلى تقييمات المستخدمين بين جميع تطبيقات التوصيل الأخرى. إنه ليس الأول ولكنه يعتبر أفضل تطبيق يقدم كل ما يمكنك تخيله من جميع أنواع المتاجر والمطاعم ويغطي جميع مجالات الأعمال في المملكة العربية السعودية ودول مجلس التعاون الخليجي.',
    'about'=>'معلومات عن',
    'top-stores'=>'أفضل المتاجر',
    'top-stores-desc'=>'جميع احتياجاتك في مكان واحد
مجموعة متنوعة من المتاجر الالكترونية، سواءاً أكانت احتياجاتك من السوبر
ماركت أو الصيدلية أو متجر للهدايا أو متاجر متنوعة، نجمع لك باقة متكاملة
من المتاجر المتنوعة لتحظى بتسوق آمن ومريح ومتطور..',
    'show-all-stores'=>'أعرض كل المتاجر',
    'success-partner'=>'شركاء النجاح',
    'about-us'=>'معلومات عنا',
    'shops'=>'متاجر',
    'users'=>'المستخدمون',
    'drivers'=>'السائقون',
    'cities'=>'المدن',
    'gallery'=>'المعرض',
    'quick-links'=>'روابط سريعة',
    'more-services'=>'المزيد من الخدمات',
    'contact-us-desc'=>'تحدث معنا، وشاركنا أفكارك
أمرك يستمع إليك.',
    'contact-information'=>'معلومات الأتصال',
    'please-enter-your-name'=>'يرجى إدخال اسمك',
    'name'=>'الأسم',
    'please-enter-your-number'=>'الرجاء إدخال رقمك',
    'phone'=>'رقم الهاتف',
    'email'=>'البريد الإلكتروني',
    'please-enter-your-email'=>'رجاءا أدخل بريدك الإلكتروني',
    'write-your-message'=>'اكتب رسالتك',
    'send-message'=>'أرسال الرسالة',
    'contact-form'=>'نموذج الاتصال',
    'submitted-successfully'=>'تم الإرسال بنجاح',
    'shops-service'=>'خدمات المتاجر',
    'shops-service-desc'=>'جميع احتياجاتك في مكان واحد
مجموعة متنوعة من المتاجر الالكترونية، سواءاً أكانت احتياجاتك من السوبر
ماركت أو الصيدلية أو متجر للهدايا أو متاجر متنوعة، نجمع لك باقة متكاملة
من المتاجر المتنوعة لتحظى بتسوق آمن .',
    'all'=>'الكل',
    'search-now'=>'ابحث الآن',
    'location'=>'الموقع',
    'enter-shop-name'=>'أدخل أسم المتجر',
    'all'=>'الكل',
    'message'=>'الرسالة',
    'enter-your-email'=>'إدخل بريدك الإلكتروني',
    'off'=>'خصم',
    'already-sent'=>'تم إرسال البريد الإلكتروني من قبل',
    'select-subject'=>'أختر العنوان',
    'payment'=>'الدفع',
    'delivery'=>'التوصيل',
    'delivery-payment'=>'الدفع و التوصيل',
    'website'=>'عن الموقع',
    'application'=>'عن التطبيق',
        'shop-login'=>'دخول المتجر',


];