<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <title>Amrk - امرك</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/flaticon/font/flaticon.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets-login/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('cp/endless/assets/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets-login/css/skins/default.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style-spider.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/animate.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<style>
    .card-body {
    padding: 2.25rem !important;
}

</style>

</head>
<body main-theme-layout="rtl" class="bg-img bg-img2">
<!-- Loader starts-->
<div class="loader-wrapper" >
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper login-11 "   style="margin: 20px">
    <div class="auth-bg" >
        <div class="authentication-box" >
            <div class="text-center logo"><img src="{{asset('assets-login/img/logos/logo.png')}}"  alt=""></div>
                                    <h3>تسجيل حساب متجر جديد مع تطبيق أمرك</h3>
            <div class="card mt-4">
                <div class="card-body">
                    <div class="text-center">
                        <b style="color: #C32F45;">
                            تم تسجيل طلبكم بنجاح و سيتم التواصل معكم قريبا.

                        </b>
                    </div>

                </div>
        </div>
    </div>

</div>
</div>
                        <img src="{{asset('assets-login/img/bg-image-51.png')}}" alt="bg" class="img-fluid wow fadeInUp " data-wow-duration="2s">

<!-- latest jquery-->
<script src="{{asset('cp/endless/assets/js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('cp/endless/assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('cp/endless/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('cp/endless/assets/js/script.js')}}"></script>
<!-- Plugin used-->
</body>
</html>
