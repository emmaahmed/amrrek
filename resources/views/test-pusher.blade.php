<!DOCTYPE html>
<head>
    <title>Pusher Test</title>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="js/app.js"></script>

    <script>

        Pusher.logToConsole = true;

        var pusher = new Pusher('75b53060b1f415501d21', {
            cluster: 'eu'
        });

        var channel = pusher.subscribe('order-chat-ZWG34d8sXnbMVZF7NZaM0htDi1636528674');
        channel.bind('App\\Events\\OrdersChatEvent', function(data) {

            console.log(JSON.stringify(data));
        });
        // window.Echo.channel(`test-channel`)
        //     .listen('.TestNotifications', (e) => {
        //         console.log('test real time');
        //
        //     });

    </script>
</head>
<body>
<h1>Pusher Test</h1>
<p>
    Try publishing an event to channel <code>my-channel</code>
    with event name <code>my-event</code>.
</p>
</body>
