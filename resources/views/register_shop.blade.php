<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <title>Amrk - امرك</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/flaticon/font/flaticon.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets-login/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('cp/endless/assets/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets-login/css/skins/default.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style-spider.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/animate.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<style>
    .card-body {
    padding: 2.25rem !important;
}

</style>

</head>
<body main-theme-layout="rtl" class="bg-img bg-img2" >
<!-- Loader starts-->
<div class="loader-wrapper" >
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper login-11 "   style="margin: 20px">
    <div class="auth-bg" >
        <div class="authentication-box" >
            <div class="text-center logo"><img src="{{asset('assets-login/img/logos/logo.png')}}"  alt=""></div>
                                    <h3>تسجيل حساب متجر جديد مع تطبيق أمرك</h3>
            <div class="card mt-4">
                <div class="card-body">
                    <div class="text-center">

                    </div>
                    <div class="text-center" style="color: red;"><strong>{{Session::get('error')}}</strong></div>
                    <form action="{{route('registerNewShop')}}" method="post" class="theme-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-form-label pt-0">الإسم التجاري للمتجر</label>
                            <input class="form-control" type="text" required="" name="name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">الكود الترويجي (في حال كان تسجيلك يتم من خلال وكالة تسويقية ادخل الكود هنا)</label>
                            <input class="form-control" type="number"  name="comercial_number">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">وصف للمتجر (عربي)</label>
                            <input class="form-control" type="text" required="" name="description_ar">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">وصف للمتجر (إنجليزي)</label>
                            <input class="form-control" type="text" required="" name="description_en">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة المتجر</label>
                            <input class="form-control" type="file" name="image" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة الخلفية للمتجر</label>
                            <input class="form-control" type="file" name="cover_image" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم السجل التجاري للمتجر</label>
                            <input class="form-control" type="text" required="" name="commercial_register">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">البريد الإلكتروني</label>
                            <input class="form-control" type="email" required name="email"
                                   placeholder="examole@example.com"
                                    {{--                                   oninvalid="this.setCustomValidity('example@example.com')"--}}
                            >
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">الهاتف</label>
                            <input class="form-control" type="number" required name="phone">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">التصنيف</label>
                            <select class="form-control btn-square" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم الضريبي للمتجر(إن وجد)</label>
                            <input class="form-control" type="text" name="tax_num">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة الرقم الضريبي للمتجر(إن وجد)</label>
                            <input class="form-control" type="file" name="tax_img">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label class="col-form-label pt-0">المدينة</label>--}}
{{--                            <input class="form-control" type="text" required="" name="city">--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <label class="col-form-label pt-0">إسم المسؤول</label>
                            <input class="form-control" type="text" required="" name="moderator_name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">البريد الإلكتروني للمسؤول</label>
                            <input class="form-control" type="email" required name="moderator_email"
                                   placeholder="examole@example.com"
{{--                                   oninvalid="this.setCustomValidity('example@example.com')"--}}
                            >
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">هاتف المسؤول</label>
                            <input class="form-control" type="number" required name="moderator_phone">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">كلمة المرور</label>
                            <input class="form-control" type="password" required name="password">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">منصب المسؤول</label>
                            <input class="form-control" type="text" required="" name="moderator_title">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">إسم البنك</label>
                            <input class="form-control" type="text" required="" name="bank_name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم الحساب</label>
                            <input class="form-control" type="text" required="" name="bank_account">
                        </div>

                        <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lat" value="" id="userlat" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lng" value="" id="userlng" placeholder="Enter long">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete" class="col-form-label pt-0">البحث عن عنوان</label>
                                        <input type="text" name="address" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <input class="" type="checkbox" required="" name="terms">
                            <label class="col-form-label pt-0">أوافق علي </label>
                            <a target="_blank" style="color: #C32F45;" href="{{asset('shop-terms')}}">الشروط و الأحكام</a>
                        </div>
{{--                        <div class="mg-t-60 tx-center">--}}
{{--                            <a target="_blank" href="http://2grand.net/">--}}
{{--                                <img src="{{asset('grand.png')}}" style="max-width: 20% !important;display: table;margin: 0 auto;">--}}
{{--                            </a>--}}
{{--                        </div>--}}
                        <div class="form-group form-row mt-3 mb-0">
                            <button class="btn btn-danger btn-block" style="background-color: #BC293E; color: #ffffff;" type="submit">تسجيل</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</div>
                        <img src="{{asset('assets-login/img/bg-image-51.png')}}" alt="bg" class="img-fluid wow fadeInUp " data-wow-duration="2s">

<!-- latest jquery-->
<!--<script src="{{asset('cp/endless/assets/js/jquery-3.2.1.min.js')}}"></script>-->
<!-- Bootstrap js-->
<!--<script src="{{asset('cp/endless/assets/js/bootstrap/popper.min.js')}}"></script>-->
<!--<script src="{{asset('cp/endless/assets/js/bootstrap/bootstrap.js')}}"></script>-->
<!-- feather icon js-->
<!--<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather.min.js')}}"></script>-->
<!--<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather-icon.js')}}"></script>-->
<!-- Sidebar jquery-->
<!--<script src="{{asset('cp/endless/assets/js/sidebar-menu.js')}}"></script>-->
<!--<script src="{{asset('cp/endless/assets/js/config.js')}}"></script>-->
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<!-- Plugin used-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script src="{{asset('assets-login/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('assets-login/js/popper.min.js')}}"></script>
<script src="{{asset('assets-login/js/bootstrap.min.js')}}"></script>
<!--<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> <!-- stats.js lib -->
<script src="{{asset('assets-login/js/script-spider.js')}}"></script>
<script src="{{asset('assets-login/js/wow.min.js')}}"></script>
<script>

    new WOW().init();

</script>

<script src="{{asset('cp/endless/assets/js/script.js')}}"></script>

<script>
    $(document).on('change', '#status', function (e) {
        alert('ww');
        var model_id = $(this).attr('model_id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{URL::route('editShopStatus')}}",
            data: {
                model_id: model_id,
                "_token": "{{ csrf_token() }}"
            },
            success: function (response) {
                location.reload();
                if (response.success) {
                    toastr.success(response.success);
                } else if (response.warning) {
                    toastr.warning(response.warning);
                } else {
                    toastr.error(response.error);
                }
            },
            error: function (jqXHR) {
                toastr.error(jqXHR.responseJSON.message);
            }
        });
    });

</script>

<script>
    var marker = null;
    var placeSearch, autocomplete;
    function initMap() {
        autocomplete =
            new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                {types: ['geocode']});
        var map = new google.maps.Map(document.getElementById('userlocation'), {
            zoom: 7,
            center: {lat: 23.8859, lng: 45.0792 }
        });
        var MaekerPos = new google.maps.LatLng(23.8859,45.0792);
        marker = new google.maps.Marker({
            position: MaekerPos,
            map: map
        });
        autocomplete.addListener('place_changed', function(){
            placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
            document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
            document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
        });
        map.addListener('click', function(e) {
            placeMarkerAndPanTo(e.latLng, map);
            document.getElementById("userlat").value=e.latLng.lat();
            document.getElementById("userlng").value=e.latLng.lng();
        });
    }
    function placeMarkerAndPanTo(latLng, map) {
        map.setZoom(15);
        marker.setPosition(latLng);
        map.panTo(latLng);
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnXtbPyAEiGsv0YBnR5eLE53ssWy4kiWk&libraries=places&callback=initMap">
</script>




</body>
</html>
