<!DOCTYPE html>
<html lang="en" style="margin: 0 !important; padding:0!important; ">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<style>
    @page { margin:0px; }
    body {
        font-family: 'examplefont', sans-serif;
        font-size: 12px;
    }
    hr{
        height:2px;
        border-width:0;
    }
</style>
</head>
<body style="margin: 0 !important; padding:0!important; text-align: center " >
<div id="invoice-POS" style="text-align: left;margin: 0 !important; padding:0!important;" >
    <div id="mid"  >
        <div class="info" style="margin-left:100px; ">

        <img src="{{public_path('Group 24801.png')}}" width="80">
        </div>
        <div class="info" style="margin-left:75px; ">

        <h3 style="margin: 0;">{{$order->shop->name}}</h3>
        <h3 style="margin: 0;">Order No. #{{$order->order_number}}</h3>
        </div>
    </div>
        <!--End Info-->
    <!--End InvoiceTop-->

    <div id="mid">
        <div class="info">
            <p>
                Customer info: <br>
                Name : {{$order->user->name}}<br>
                Phone : {{$order->user->phone}}<br>
                Address : {{str_replace('‬','',$order->out_address)}}

            </p>
        </div>
    </div>
    <hr>
    <div id="mid">
        <div class="info">
            <p>
                Delegate info:<br>
                Name : {{$delegate->f_name}} {{$delegate->l_name}} <br>
                Phone : {{$delegate->phone}}<br>
            </p>
        </div>
    </div>
    <hr>
    <!--End Invoice Mid-->

    <div id="bot">

        <div id="table">
            <table>
                <tr class="tabletitle">
                    <td class="item" >
                        <h4>Item</h4>
                    </td>
                    <td class="Hours" style="width:100px ;">
                        <h4>Qty</h4>
                    </td>
                    <td class="Rate">
                        <h4>Sub Total</h4>
                    </td>
                </tr>
                @if(count($order->order_products)>0)
                @foreach ($order->order_products as $product)



                <tr class="service">
                    <td class="tableitem">
                        <p class="itemtext">{{$product->product_name_en}}</p>
                    </td>
                    <td class="tableitem">
                        <p class="itemtext"> * {{$product->quantity}}</p>
                    </td>
                    <td class="tableitem">
                        <p class="itemtext">{{$product->price_after}}</p>
                    </td>
                </tr>
                @endforeach
                @endif

                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>Sub Total</h3>
                    </td>
                    <td class="payment">
                        <h4>{{$order->sub_total}}</h4>
                    </td>
                    <td></td>

                </tr>

                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>{{$order->tax}} % {{$order->sub_total}}</h3>
                    </td>
                    <td class="payment">
                        <h4>{{(($order->tax * $order->sub_total) / 100)}}</h4>
                    </td>
                    <td></td>

                </tr>
                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>delivery</h3>
                    </td>
                    <td class="payment">
                        <h4>{{$lastOffer->offer}}</h4>
                    </td>
                    <td></td>

                </tr>
                <tr class="tabletitle">
                    <td class="Rate">
                        <h3>Total Cost</h3>
                    </td>
                    <td class="payment">
                        <h4>{{$order->total_cost+$lastOffer->offer}}</h4>
                    </td>
                    <td></td>

                </tr>

            </table>
        </div>
        <!--End Table-->
        @if($order->notes!=null && $order->notes!='')
            <hr>

            <div id="legalcopy">
                <p class="legal">Order Notes:<strong>{{$order->notes}}</strong>
                </p>
            </div>
        @endif
<hr>
        <div id="legalcopy">
            <p class="legal">{{\Carbon\Carbon::now()->format('l jS \of F Y h:i:s A')}}</p>
        </div>
        <div class="col-md-12" style="margin-left: 65px;">
            <img src="{{$output}}" ><br>


        </div>
        <div id="legalcopy">
            <p class="legal"><strong> Thank you for shopping at {{$order->shop->name}}</strong>
            </p>
        </div>


    </div>
    <!--End InvoiceBot-->
</div>
<!--End Invoice-->
</body>
</html>