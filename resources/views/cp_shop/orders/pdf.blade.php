<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon">
    <title>Amrk - invoice</title>
    <!-- Google font-->
    <link rel="stylesheet" type="text/css" href="../assets/css/print.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/responsive.css">
    <style>
        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        p {
            margin: 0pt;
        }

        table.items {
            border: 0.1mm solid #e7e7e7;
        }

        td {
            vertical-align: top;
        }

        .items td {
            border-left: 0.1mm solid #e7e7e7;
            border-right: 0.1mm solid #e7e7e7;
        }

        table thead td {
            text-align: center;
            border: 0.1mm solid #e7e7e7;
        }

        .items td.blanktotal {
            background-color: #EEEEEE;
            border: 0.1mm solid #e7e7e7;
            background-color: #FFFFFF;
            border: 0mm none #e7e7e7;
            border-top: 0.1mm solid #e7e7e7;
            border-right: 0.1mm solid #e7e7e7;
        }

        .items td.totals {
            text-align: right;
            border: 0.1mm solid #e7e7e7;
        }

        .items td.cost {
            text-align: center;
        }
        @page {
            header: page-header;

            footer: page-footer;
        }

    </style>

</head>

<body>
<htmlpageheader name="page-header" >
    <p style="text-align: center">
    هذه الفاتورة صادرة من نظام امرك الالكتروني بدون أدني مسئولية ، تمكن العميل مع التعامل مع المندوب
    </p>
</htmlpageheader>

<table width="100%" style="font-family: sans-serif;" cellpadding="10">
    <tr>
        <td width="100%" style="padding: 0px; text-align: center;">
            <img class="media-object img-90 center"
                 src="{{asset('cp/endless/assets/images/other-images/logo-login.png')}}" alt="">
        </td>
    </tr>
    <tr>
        <td width="100%" style="text-align: center; font-size: 20px; font-weight: bold; padding: 0px;">
            <h3 class="center bolder">فاتورة أمرك</h3>
        </td>
    </tr>
</table>
<br>
<table width="100%" style="font-family: sans-serif;" cellpadding="10">
    <tr>
        <td width="49%" style="border: 0.1mm solid #eee;">Shop Name :{{$order->shop->name}}<br>Address:{{$order->shop->address}}<br>Shop Phone: {{$order->shop->phone}}</td>
        <td width="2%">&nbsp;</td>
        <td width="49%" style="border: 0.1mm solid #eee; text-align: right;"><strong>Invoice:</strong><br> #{{$order->order_number}}<br>Order Date: <br>{{$order->created_at}}</td>
    </tr>
</table>
<br>
<table width="100%" style="font-family: sans-serif;" cellpadding="10">
    <tr>
        <td width="49%" style="border: 0.1mm solid #eee;"><img src="{{$order->delegate->image}}" style="width: 60px !important; height: 60px; border-radius: 50% !important;" alt=""><br><strong>Delivery name:</strong>{{$order->delegate->f_name}} {{$order->delegate->l_name}}<br>Car Number: {{$order->delegate->car_num}}<br>Phone Number: {{$order->delegate->phone}}</td>
        <td width="2%">&nbsp;</td>
        <td width="49%" style="border: 0.1mm solid #eee; text-align: right;"> Customer Name : {{$order->user->name}} <br> Phone Number : {{$order->user->phone}} <br>  </td>
    </tr>
</table>
<br>
<br>
<table class="items" width="100%" style="font-size: 14px; border-collapse: collapse;" cellpadding="8">
    <thead>
    <tr>
        <td width="15%" style="text-align: left;"><strong>Item Description</strong></td>
        <td width="45%" style="text-align: left;"><strong>Quantity</strong></td>
        <td width="20%" style="text-align: left;"><strong>Price Without Vat</strong></td>
    </tr>
    </thead>
    <tbody>
    <!-- ITEMS HERE -->
    @if(count($order->order_products)>0)
        @foreach($order->order_products as $product)

            <tr>
        <td style="padding: 0px 7px; line-height: 20px;">{{$product->product_name}}</td>
        <td style="padding: 0px 7px; line-height: 20px;">{{$product->quantity}} *</td>
        <td style="padding: 0px 7px; line-height: 20px;">{{$product->price_after}} SAR</td>
    </tr>
        @endforeach
    @endif

    </tbody>
</table>
<br>
<table width="40%" align="right" style="font-family: sans-serif; font-size: 14px;" >
    <tr>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total without Vat</strong></td>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$order->total_cost - ($tax->val * $order->total_cost/100)}}  SAR</td>
    </tr>
    <tr>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total With Vat</strong></td>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$order->total_cost }} SAR</td>
    </tr>
    <tr>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total Delivery</strong></td>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$order->offer->offer}} </td>
    </tr>
    <tr>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;"><strong>Total</strong></td>
        <td style="border: 1px #eee solid; padding: 0px 8px; line-height: 20px;">{{$order->total_cost +$order->offer->offer }} SAR</td>
    </tr>
</table>

<br>
<htmlpagefooter name="page-footer">
    <div class="col-md-12 footer-copyright" style="text-align: center">
        <a href="https://amrk.my-staff.net/" target="_blank">
            <b class="mb-0" style="color: #C32F45;"> جميع حقوق النشر محفوظة - أمرك 2021
            </b>
        </a>

    </div>
</htmlpagefooter>

</body>
</html>