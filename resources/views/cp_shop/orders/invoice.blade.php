<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="  هذه الفاتورة صادرة من نظام امرك الالكتروني بدون أدني مسئولية ، تمكن العميل مع التعامل مع المندوب">
    <meta name="keywords"
          content="Amrrek Bill, Amrrek Bill template, Amrrek App, Amrrek App Bill, Amrrek Order Bill">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('default.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('default.png')}}" type="image/x-icon">
    <title>Amrrek - أمرك</title>
    <!-- Google font-->
    <!-- Font Awesome-->
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('cp/endless/assets/css/light-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/responsive.css')}}">
</head>

<body>
<!-- Loader starts-->
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <div class="page-body">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="invoice">
                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12 mb-5">
                                                    <img class="media-object img-90 center"
                                                         src="{{asset('cp/endless/assets/images/other-images/logo-login.png')}}" alt="">
                                                    <h3 class="center bolder">فاتورة أمرك</h3>
                                                </div>
                                                <div class="col-sm-6">

                                                    <div class="media">
                                                        <div class="media-left"></div>
                                                        <div class="media-body m-l-20">
                                                            <h4 class="media-heading" > {{$order->shop->name}} </h4>
                                                            <p>
                                                                {{$order->shop->address}}
                                                                <br>
                                                                <span class="digits"> {{$order->shop->phone}}</span></p>
                                                        </div>
                                                    </div>
                                                    <!-- End Info-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="text-md-right">
                                                        <h3>Invoice #<span class="digits counter"> {{$order->order_number}}</span></h3>
                                                        <p>Order Number <span class="digits"> #{{$order->order_number}}</span>
                                                            <br>
                                                            Order Date : <span class="digits"> {{$order->created_at}}</span>
                                                            <br>
                                                        </p>

                                                    </div>
                                                    <!-- End Title-->
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <!-- End InvoiceTop-->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="media">
                                                    <div class="media-left"><img class="media-object rounded-circle img-60"
                                                                                 src="{{$order->delegate?$order->delegate->image:''}}" alt=""></div>
                                                    <div class="media-body m-l-20">
                                                        <h6 class="media-heading">Delivery name : {{$order->delegate->f_name}} {{$order->delegate->l_name}}</h6>
                                                        <p>id number : {{$order->delegate->national_id}}<br><span class="digits">Car Number : {{$order->delegate->car_num}}</span>
                                                            <br><span class="digits">Phone Number  : {{$order->delegate->phone}}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="media text-md-right">

                                                    <div class="media-body m-l-20">
                                                        <h6 class="media-heading">Customer Name : {{$order->user->name}}</h6>
                                                        <p>Phone Number : {{$order->user->phone}}<br><span class="digits"> Address :  {{$order->out_address}}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Invoice Mid-->
                                        <div>
                                            <div class="table-responsive invoice-table" id="table">
                                                <div class="col-md-12">
                                                    <div>
                                                        <p class="legal text-right bolder mb-3">  هذه الفاتورة صادرة من نظام امرك الالكتروني بدون أدني مسئولية ، تمكن العميل مع التعامل مع المندوب </p>
                                                    </div>
                                                </div>
                                                <table class="table table-bordered table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td class="item">
                                                            <h6 class="p-2 mb-0">Item Description</h6>
                                                        </td>
                                                        <td class="Hours">
                                                            <h6 class="p-2 mb-0">Quantity</h6>
                                                        </td>

                                                        <td class="subtotal">
                                                            <h6 class="p-2 mb-0">Price Without Vat</h6>
                                                        </td>
                                                    </tr>
                                                    @if(count($order->order_products)>0)
                                                        @foreach($order->order_products as $product)
                                                    <tr>
                                                        <td>
                                                            <label>{{$product->product_name}}</label>

                                                        </td>
                                                        <td>
                                                            <p class="itemtext digits"> {{$product->quantity}} * </p>
                                                        </td>

                                                        <td>
                                                            <p class="itemtext digits">  {{$product->price_after}} SAR</p>
                                                        </td>
                                                    </tr>
                                                        @endforeach
                                                    @endif
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="Rate">
                                                            <h6 class="mb-0 p-2">Total without Vat :   {{$order->total_cost - ($tax->val * $order->total_cost/100)}}  SAR</h6>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="Rate">
                                                            <h6 class="mb-0 p-2">Total With Vat :   {{$order->total_cost }} SAR</h6>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="Rate">
                                                            <h6 class="mb-0 p-2">Total Delivery :   {{$order->offer->offer}}</h6>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="Rate">
                                                            <h6 class="mb-0 p-3 bolder">Total :   {{$order->total_cost +$order->offer->offer }}  SAR</h6>
                                                        </td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- End Table-->
                                            <div class="row">


                                                <div class="col-md-12 center">
                                                    {!! QrCode::style('dot')->eye('circle')->errorCorrection('H')->size(150)->merge('/public/default.png')->generate(route('shop.orders.pdf',$order->id)); !!}
                                                </div>

                                            </div>
                                        </div>
                                        <!-- End InvoiceBot-->
                                    </div>
                                    <!--                    <div class="col-sm-12 text-center mt-3">-->
                                    <!--                      <button class="btn btn btn-primary mr-2" type="button">Make a complaint</button>-->

                                    <!--                      <button class="btn btn btn-primary mr-2" type="button" onclick="myFunction()">Print</button>-->
                                    <!--                    </div>-->
                                    <!-- End Invoice-->
                                    <!-- End Invoice Holder-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <!--      <footer class="footer">-->
        <!--        <div class="container-fluid">-->
        <!--          <div class="row">-->
        <!--            <div class="col-md-6 footer-copyright">-->
        <!--              <p class="mb-0">Copyright 2018 © Endless All rights reserved.</p>-->
        <!--            </div>-->
        <!--            <div class="col-md-6">-->
        <!--              <p class="pull-right mb-0">Hand crafted & made with<i class="fa fa-heart"></i></p>-->
        <!--            </div>-->
        <!--          </div>-->
        <!--        </div>-->
        <!--      </footer>-->
    </div>
</div>
<!-- latest jquery-->
<!-- Plugin used-->
</body>

</html>