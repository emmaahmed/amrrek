@extends('cp_shop.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                المنتجات

                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الدول</li>
                            </ol>--}}
                        </div>
                        <div style="float: right">
                            @if(auth()->user()->hasPermissionTo('اضافة منتج'))
                                <a  class="btn btn-primary" href="{{route('products.create')}}">
                                    <i
                                            class="icon-plus"></i>
                                    إضافة منتج
                                </a>
                            @endif
                                <a href="{{route('download.products')}}"
                                   class="btn btn-success"><span>تحميل تقارير المنتجات</span></a>

                                <button title="رفع المنتجات" type="button"
                                        class="btn btn-primary"
                                        data-toggle="modal"
                                        data-target="#upload-excel"
                                        >
                                    رفع المنتجات
                                </button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">الحالة</th>
                                <th scope="col">الإسم</th>
                                <th scope="col">الوصف</th>
                                <th scope="col">الإختيارات</th>
                                <th scope="col">الصورة</th>
                                <th scope="col">السعر</th>
                                <th scope="col">القائمة</th>
                                <th scope="col">العمليات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($countries as $c)
                                <tr id="main_cat_{{$c->id}}">
                                    <td>{{$c->id}}</td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تفعيل منتج'))
                                            <div class="nav-right col p-0">
                                                <div class="media">
                                                    <div class="media-body text-right icon-state switch-outline">
                                                        <label class="switch">
                                                            <input type="checkbox" id="status" model_id="{{$c->id}}"
                                                                   @if($c->active == 1)  checked @endif><span
                                                                    class="switch-state bg-success"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                    <td>{{$c->name_ar}}</td>
                                    <td>{{$c->description_ar}}</td>
                                    <td>
                                        @if(sizeof($c->variations) > 0)
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td><b>الإسم</b></td>
                                                    <td><b>النوع</b></td>
                                                    <td><b>اجباري</b></td>
                                                    <td><b> . . . </b></td>
                                                </tr>
                                                @foreach($c->variations as $variation)
                                                    <tr>
                                                        <td>{{$variation->name_ar}}</td>
                                                        <td>{{$variation->type == 0 ? "اختيار واحد" : "اختيار من متعدد"}}</td>
                                                        <td>
                                                            @if($variation->required == 1)
                                                                <i class='font-success show icon-check'></i>
                                                            @else
                                                                <i class="font-danger show icon-close"></i>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if((Auth::guard('shop')->user()->type != 3 && Auth::guard('shop')->user()->type != 4) || (Auth::guard('shop')->user()->shop_id == $c->shop_id) )
                                                                @if(auth()->user()->hasPermissionTo('تعديل منتج'))
                                                                    <button title="تعديل الاختيار" type="button"
                                                                            class=" btn-warning"
                                                                            data-toggle="modal"
                                                                            data-target="#edit_var{{$variation->id}}"
                                                                            style="padding: 1px">
                                                                        <i class="fa fa-edit"></i>
                                                                    </button>
                                                                    <button title="حذف الاختيار" type="button"
                                                                            class=" btn-danger"
                                                                            data-toggle="modal"
                                                                            data-target="#delete_var{{$variation->id}}"
                                                                            style="padding: 1px 2px">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                @endif
                                                            @endif
                                                        </td>

                                                        <div class="modal fade" id="edit_var{{$variation->id}}"
                                                             tabindex="-1" role="dialog"
                                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                                            تعديل اختيار</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form class="form-horizontal needs-validation was-validated"
                                                                          method="post"
                                                                          action="{{route('editVariation')}}"
                                                                          enctype="multipart/form-data">
                                                                        {{csrf_field()}}
                                                                        <div class="modal-body">

                                                                            <input name="variation_id"
                                                                                   value="{{$variation->id}}" hidden>

                                                                            <div class="form-group row">
                                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                                       for="textinput">الإسم
                                                                                    بالعربي</label>
                                                                                <div class="col-lg-12">
                                                                                    <input name="name_ar" type="text"
                                                                                           placeholder="الإسم بالعربي"
                                                                                           value="{{$variation->name_ar}}"
                                                                                           class="form-control btn-square"
                                                                                           required
                                                                                           oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                    <div class="invalid-feedback">هذا
                                                                                        الحقل مطلوب إدخاله .
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                                       for="textinput">الإسم
                                                                                    بالانجليزي</label>
                                                                                <div class="col-lg-12">
                                                                                    <input name="name_en" type="text"
                                                                                           placeholder="الإسم بالانجليزي"
                                                                                           value="{{$variation->name_en}}"
                                                                                           class="form-control btn-square"
                                                                                           required
                                                                                           oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                    <div class="invalid-feedback">هذا
                                                                                        الحقل مطلوب إدخاله .
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div class="col-lg-1">
                                                                                    <input name="required"
                                                                                           type="checkbox"
                                                                                            {{$variation->required == 1 ? "checked" : "" }}>
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    <label class="col-lg-12 control-label text-lg-right"
                                                                                           for="textinput"> هذا الاختيار
                                                                                        اجباري ؟ </label>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                                       for="textinput">القائمة </label>
                                                                                <div class="col-lg-12">
                                                                                    <select name="type"
                                                                                            class="form-control btn-square">
                                                                                        <option value="0" {{$variation->type == 0 ? "selected" : "" }}>
                                                                                            اختيار واحد
                                                                                        </option>
                                                                                        <option value="1" {{$variation->type == 1 ? "selected" : "" }}>
                                                                                            اختيار من متعدد
                                                                                        </option>
                                                                                    </select>
                                                                                    <div class="invalid-feedback">هذا
                                                                                        الحقل مطلوب إدخاله .
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="reset" class="btn btn-dark"
                                                                                    data-dismiss="modal">
                                                                                إغلاق
                                                                            </button>
                                                                            <button class="btn btn-primary">حفظ</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal animated fadeIn"
                                                             id="delete_var{{$variation->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true"
                                                             style="text-align:right">
                                                            <div class="modal-dialog modal-dialog-centered"
                                                                 role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header btn-danger">
                                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                                            حذف الحقل</h5>
                                                                        {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                                        {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                                        {{--                                            </button>--}}
                                                                    </div>
                                                                    <form method="post"
                                                                          action="{{route('deleteVariation')}}"
                                                                          class="buttons">
                                                                        {{csrf_field()}}
                                                                        <div class="modal-body">
                                                                            <h4>هل انت متأكد ؟</h4>
                                                                            <h6>
                                                                                انت علي وشك حذف هذا الحقل
                                                                            </h6>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="model_id"
                                                                                   value="{{$variation->id}}">
                                                                            <button class="btn btn-dark" type="button"
                                                                                    data-dismiss="modal">
                                                                                إغلاق
                                                                            </button>
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">تأكيد
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>

                                                @endforeach
                                            </table>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if($c->image != "")
                                            <img src="{{$c->image}}" width="50px" height="50px">
                                            <br>
                                            <a class="badge btn btn-success btn-sm" target="_blank"
                                               href="{{$c->image}}"> عرض</a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <span class="badge badge-dark">{{$c->price_after}}</span>
                                    </td>
                                    <td>
                                        @if($c->menue)
                                            <span class="badge badge-danger">{{$c->menue->name_ar}}</span>
                                        @else
                                            <span class="badge badge-danger">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <button title="عرض المنتج" type="button" class="btn btn-info"
                                                data-toggle="modal" data-target="#show{{$c->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-eye" color="white" data-toggle="modal">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </button>
                                        @if((Auth::guard('shop')->user()->type != 3 && Auth::guard('shop')->user()->type != 4) || (Auth::guard('shop')->user()->shop_id == $c->shop_id) )
                                            @if(auth()->user()->hasPermissionTo('تعديل منتج'))
                                                <button title="إضافة اختيار" type="button" class="btn btn-success"
                                                        data-toggle="modal" data-target="#addVariation{{$c->id}}">
                                                    <i class="fa fa-plus-circle"></i>
                                                </button>

                                                {{--@if(admin()->hasPermissionTo('Edit City'))--}}
                                                <button title="تعديل" type="button" class="btn btn-warning"
                                                        data-toggle="modal"
                                                        data-target="#edit_{{$c->id}}">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            @endif
                                            <br>
                                            @if(auth()->user()->hasPermissionTo('حذف منتج'))
                                                <button title="حذف" type="button" class="btn btn-danger"
                                                        data-toggle="modal"
                                                        data-target="#delete_{{$c->id}}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            @endif
                                            {{--@endif--}}
                                        @endif
                                    </td>

                                    <div class="modal fade" id="show{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">تفاصيل المنتج</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <input type="hidden" name="model_id" value="{{$c->id}}">


                                                <div class="modal-body">

                                                    <div class="form-group row">
                                                        <div class="col-lg-1">
                                                            <b>الإسم:</b>
                                                        </div>
                                                        <div class="col-lg-3" style="text-align: right">
                                                            <span>{{$c->name_ar}}</span>
                                                        </div>


                                                        <div class="col-lg-1">
                                                            <b>السعر:</b>
                                                        </div>
                                                        <div class="col-lg-2" style="text-align: right">
                                                            <span class="badge badge-dark">{{$c->price_after}}</span>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <input name="has_sizes" readonly
                                                                   type="checkbox" {{$c->has_sizes == 1 ? "checked" : ""}} >
                                                        </div>
                                                        <div class="col-lg-2" style="text-align: right">
                                                            <span> المنتج له احجام ؟ </span>
                                                        </div>

                                                        <div class="col-lg-2" style="text-align: left">
                                                            @if($c->image != "")
                                                                <img src="{{$c->image}}" width="50px" height="50px">
                                                            @else
                                                                -
                                                            @endif
                                                        </div>


                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-lg-1">
                                                            <b>القائمة:</b>
                                                        </div>
                                                        <div class="col-lg-3" style="text-align: right">
                                                            <span class="badge badge-danger">{{isset($c->menue) ? $c->menue->name_ar :'-'}}</span>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <b>الوصف:</b>
                                                        </div>
                                                        <div class="col-lg-7" style="text-align: right">
                                                            <span>{{$c->description_ar}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-lg-1">
                                                            <b>الإختيارات:</b>
                                                            @if((Auth::guard('shop')->user()->type != 3 && Auth::guard('shop')->user()->type != 4) || (Auth::guard('shop')->user()->shop_id == $c->shop_id) )
                                                                @if(auth()->user()->hasPermissionTo('تعديل منتج'))
                                                                    <button title="إضافة اختيار" type="button"
                                                                            class="btn btn-success"
                                                                            style="padding: 5px 10px"
                                                                            data-toggle="modal"
                                                                            data-target="#addVariation{{$c->id}}">
                                                                        <i class="fa fa-plus-circle"></i>
                                                                    </button>
                                                                @endif
                                                            @endif
                                                            <br>
                                                        </div>
                                                        <div class="col-lg-11" style="text-align: right">
                                                            @if(isset($c->variations) && sizeof($c->variations) > 0)
                                                                <div class="row ">
                                                                    <div class="col-lg-8 alert alert-dark"><b>الإسم</b>
                                                                    </div>
                                                                    <div class="col-lg-1 alert alert-dark"><b>النوع</b>
                                                                    </div>
                                                                    <div class="col-lg-1 alert alert-dark"><b>اجباري</b>
                                                                    </div>

                                                                    <div class="col-lg-1 alert alert-dark">
                                                                        <b>العمليات</b></div>


                                                                </div>

                                                                @foreach($c->variations as $variation)
                                                                    <div class="row">
                                                                        <div class="col-lg-8 alert alert-light">
                                                                            {{$variation->name_ar}}
                                                                            <br>
                                                                            <br>
                                                                            <br>
                                                                            @if(isset($variation->options) && sizeof($variation->options) > 0)
                                                                                <div class="row ">
                                                                                    <div class="col-lg-2"></div>
                                                                                    <div class="col-lg-6">
                                                                                        <b>الاضافات: </b></div>
                                                                                </div>
                                                                                <div class="row ">
                                                                                    <div class="col-lg-2 "></div>
                                                                                    <div class="col-lg-7 alert alert-dark">
                                                                                        <b>الإسم</b></div>
                                                                                    <div class="col-lg-2 alert alert-dark">
                                                                                        <b>السعر</b></div>
                                                                                    <div class="col-lg-1 alert alert-dark">
                                                                                        <b>X</b></div>

                                                                                </div>
                                                                                @foreach($variation->options as $option)
                                                                                    <div class="row">
                                                                                        <div class="col-lg-2"></div>
                                                                                        <div class="col-lg-7 alert alert-light">
                                                                                            {{$option->name_ar}}
                                                                                        </div>
                                                                                        <div class="col-lg-2 alert alert-light">
                                                                                            {{$option->price}}
                                                                                        </div>
                                                                                        <div class="col-lg-1 alert alert-light">
                                                                                            @if((Auth::guard('shop')->user()->type != 3 && Auth::guard('shop')->user()->type != 4) || (Auth::guard('shop')->user()->shop_id == $c->shop_id) )
                                                                                                @if(auth()->user()->hasPermissionTo('تعديل منتج'))
                                                                                                    <button title="تعديل الإضافة"
                                                                                                            type="button"
                                                                                                            class=" btn-warning"
                                                                                                            data-toggle="modal"
                                                                                                            data-target="#edit_Option{{$option->id}}"
                                                                                                            style="padding: 1px">
                                                                                                        <i class="fa fa-edit"></i>
                                                                                                    </button>
                                                                                                    <button title="حذف الإضافة"
                                                                                                            type="button"
                                                                                                            class=" btn-danger"
                                                                                                            data-toggle="modal"
                                                                                                            data-target="#delete_option{{$option->id}}"
                                                                                                            style="padding: 2px">
                                                                                                        <i class="fa fa-trash"></i>
                                                                                                    </button>
                                                                                                @endif
                                                                                            @endif
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="modal fade"
                                                                                         id="edit_Option{{$option->id}}"
                                                                                         tabindex="-1" role="dialog"
                                                                                         aria-labelledby="exampleModalLabel"
                                                                                         aria-hidden="true">
                                                                                        <div class="modal-dialog"
                                                                                             role="document">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <h5 class="modal-title"
                                                                                                        id="exampleModalLabel">
                                                                                                        تعديل
                                                                                                        الإضافة</h5>
                                                                                                    <button type="button"
                                                                                                            class="close"
                                                                                                            data-dismiss="modal"
                                                                                                            aria-label="Close">
                                                                                                        <span aria-hidden="true">&times;</span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <form class="form-horizontal needs-validation was-validated"
                                                                                                      method="post"
                                                                                                      action="{{route('editOption')}}"
                                                                                                      enctype="multipart/form-data">
                                                                                                    {{csrf_field()}}
                                                                                                    <div class="modal-body">

                                                                                                        <input name="option_id"
                                                                                                               value="{{$option->id}}"
                                                                                                               hidden>

                                                                                                        <div class="form-group row">
                                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                                   for="textinput">الإسم
                                                                                                                بالعربي</label>
                                                                                                            <div class="col-lg-12">
                                                                                                                <input name="name_ar"
                                                                                                                       type="text"
                                                                                                                       placeholder="الإسم بالعربي"
                                                                                                                       value="{{$option->name_ar}}"
                                                                                                                       class="form-control btn-square"
                                                                                                                       required
                                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                                <div class="invalid-feedback">
                                                                                                                    هذا
                                                                                                                    الحقل
                                                                                                                    مطلوب
                                                                                                                    إدخاله
                                                                                                                    .
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group row">
                                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                                   for="textinput">الإسم
                                                                                                                بالانجليزي</label>
                                                                                                            <div class="col-lg-12">
                                                                                                                <input name="name_en"
                                                                                                                       type="text"
                                                                                                                       placeholder="الإسم بالانجليزي"
                                                                                                                       value="{{$option->name_en}}"
                                                                                                                       class="form-control btn-square"
                                                                                                                       required
                                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                                <div class="invalid-feedback">
                                                                                                                    هذا
                                                                                                                    الحقل
                                                                                                                    مطلوب
                                                                                                                    إدخاله
                                                                                                                    .
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="form-group row">
                                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                                   for="textinput">السعر </label>
                                                                                                            <div class="col-lg-12">
                                                                                                                <input name="price"
                                                                                                                       type="number"
                                                                                                                       placeholder="السعر "
                                                                                                                       value="{{$option->price}}"
                                                                                                                       class="form-control btn-square"
                                                                                                                       required
                                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                                <div class="invalid-feedback">
                                                                                                                    هذا
                                                                                                                    الحقل
                                                                                                                    مطلوب
                                                                                                                    إدخاله
                                                                                                                    .
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <button type="reset"
                                                                                                                class="btn btn-dark"
                                                                                                                data-dismiss="modal">
                                                                                                            إغلاق
                                                                                                        </button>
                                                                                                        <button class="btn btn-primary">
                                                                                                            حفظ
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="modal animated fadeIn"
                                                                                         id="delete_option{{$option->id}}"
                                                                                         tabindex="-1"
                                                                                         role="dialog"
                                                                                         aria-labelledby="exampleModalLabel"
                                                                                         aria-hidden="true"
                                                                                         style="text-align:right">
                                                                                        <div class="modal-dialog modal-dialog-centered"
                                                                                             role="document">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header btn-danger">
                                                                                                    <h5 class="modal-title"
                                                                                                        id="exampleModalLabel">
                                                                                                        حذف الحقل</h5>
                                                                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                                                                    {{--                                            </button>--}}
                                                                                                </div>
                                                                                                <form method="post"
                                                                                                      action="{{route('deleteOption')}}"
                                                                                                      class="buttons">
                                                                                                    {{csrf_field()}}
                                                                                                    <div class="modal-body">
                                                                                                        <h4>هل انت متأكد
                                                                                                            ؟</h4>
                                                                                                        <h6>
                                                                                                            انت علي وشك
                                                                                                            حذف هذا
                                                                                                            الحقل
                                                                                                        </h6>
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <input type="hidden"
                                                                                                               name="model_id"
                                                                                                               value="{{$option->id}}">
                                                                                                        <button class="btn btn-dark"
                                                                                                                type="button"
                                                                                                                data-dismiss="modal">
                                                                                                            إغلاق
                                                                                                        </button>
                                                                                                        <button type="submit"
                                                                                                                class="btn btn-primary">
                                                                                                            تأكيد
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                @endforeach
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-lg-1 alert alert-light">{{$variation->type == 0 ? "اختيار واحد" : "اختيار من متعدد"}}</div>
                                                                        <div class="col-lg-1 alert alert-light">
                                                                            @if($variation->required == 1)
                                                                                <i class='font-success show icon-check'></i>
                                                                            @else
                                                                                <i class="font-danger show icon-close"></i>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-lg-1 alert alert-light">
                                                                            @if((Auth::guard('shop')->user()->type != 3 && Auth::guard('shop')->user()->type != 4) || (Auth::guard('shop')->user()->shop_id == $c->shop_id) )
                                                                                @if(auth()->user()->hasPermissionTo('تعديل منتج'))
                                                                                    <button title="إضافة إضافة"
                                                                                            type="button"
                                                                                            class=" btn-primary"
                                                                                            data-toggle="modal"
                                                                                            data-target="#addOPtion{{$variation->id}}"
                                                                                            style="padding: 2px">
                                                                                        <i class="fa fa-plus-circle"></i>
                                                                                    </button>
                                                                                    <button title="تعديل الاختيار"
                                                                                            type="button"
                                                                                            class=" btn-warning"
                                                                                            data-toggle="modal"
                                                                                            data-target="#edit_var{{$variation->id}}"
                                                                                            style="padding: 1px">
                                                                                        <i class="fa fa-edit"></i>
                                                                                    </button>
                                                                                    <button title="حذف الاختيار"
                                                                                            type="button"
                                                                                            class=" btn-danger"
                                                                                            data-toggle="modal"
                                                                                            data-target="#delete_var{{$variation->id}}"
                                                                                            style="padding: 2px">
                                                                                        <i class="fa fa-trash"></i>
                                                                                    </button>
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal fade"
                                                                         id="addOPtion{{$variation->id}}" tabindex="-1"
                                                                         role="dialog"
                                                                         aria-labelledby="exampleModalLabel"
                                                                         aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title"
                                                                                        id="exampleModalLabel">إضافة
                                                                                        إضافة</h5>
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <form class="form-horizontal needs-validation was-validated"
                                                                                      method="post"
                                                                                      action="{{route('addOption')}}"
                                                                                      enctype="multipart/form-data">
                                                                                    {{csrf_field()}}
                                                                                    <div class="modal-body">

                                                                                        <input name="variation_id"
                                                                                               value="{{$variation->id}}"
                                                                                               hidden>

                                                                                        <div class="form-group row">
                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                   for="textinput">الإسم
                                                                                                بالعربي</label>
                                                                                            <div class="col-lg-12">
                                                                                                <input name="name_ar"
                                                                                                       type="text"
                                                                                                       placeholder="الإسم بالعربي"
                                                                                                       class="form-control btn-square"
                                                                                                       required
                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                <div class="invalid-feedback">
                                                                                                    هذا الحقل مطلوب
                                                                                                    إدخاله .
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group row">
                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                   for="textinput">الإسم
                                                                                                بالانجليزي</label>
                                                                                            <div class="col-lg-12">
                                                                                                <input name="name_en"
                                                                                                       type="text"
                                                                                                       placeholder="الإسم بالانجليزي"
                                                                                                       class="form-control btn-square"
                                                                                                       required
                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                <div class="invalid-feedback">
                                                                                                    هذا الحقل مطلوب
                                                                                                    إدخاله .
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group row">
                                                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                                                   for="textinput">السعر </label>
                                                                                            <div class="col-lg-12">
                                                                                                <input name="price"
                                                                                                       type="number"
                                                                                                       placeholder="السعر "
                                                                                                       class="form-control btn-square"
                                                                                                       required
                                                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                                                <div class="invalid-feedback">
                                                                                                    هذا الحقل مطلوب
                                                                                                    إدخاله .
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="reset"
                                                                                                class="btn btn-dark"
                                                                                                data-dismiss="modal">
                                                                                            إغلاق
                                                                                        </button>
                                                                                        <button class="btn btn-primary">
                                                                                            حفظ
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                @endforeach
                                                            @else
                                                                -
                                                            @endif
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="addVariation{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">إضافة اختيار</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>

                                                </div>
                                                <form class="form-horizontal needs-validation was-validated"
                                                      method="post" action="{{route('addVariation')}}"
                                                      enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">

                                                        <input name="product_id" value="{{$c->id}}" hidden>

                                                        <div class="form-group row">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput">الإسم بالعربي</label>
                                                            <div class="col-lg-12">
                                                                <input name="name_ar" type="text"
                                                                       placeholder="الإسم بالعربي"
                                                                       class="form-control btn-square" required
                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput">الإسم بالانجليزي</label>
                                                            <div class="col-lg-12">
                                                                <input name="name_en" type="text"
                                                                       placeholder="الإسم بالانجليزي"
                                                                       class="form-control btn-square" required
                                                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-lg-1">
                                                                <input name="required" type="checkbox" class=" ">
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput"> هذا الاختيار اجباري ؟ </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput">القائمة </label>
                                                            <div class="col-lg-12">
                                                                <select name="type" class="form-control btn-square">
                                                                    <option value="0">اختيار واحد</option>
                                                                    <option value="1">اختيار من متعدد</option>
                                                                </select>
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button class="btn btn-primary">حفظ</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">تعديل منتج</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal" method="post"
                                                      action="{{route('editProduct')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">


                                                        <div class="modal-body">

                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">القائمة </label>
                                                                <div class="col-lg-12">
                                                                    <select name="menu_id"
                                                                            class="form-control btn-square">
                                                                        @foreach($menus as $menu)
                                                                            <option
                                                                                    value="{{$menu->id}}" {{$menu->id == $c->menu_id ? 'selected' : ''}}>{{$menu->name_ar}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله
                                                                        .
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">الإسم بالعربي</label>
                                                                <div class="col-lg-12">
                                                                    <input id="name_ar" name="name_ar"
                                                                           value="{{$c->name_ar}}"
                                                                           type="text" placeholder="الإسم بالعربي"
                                                                           class="form-control btn-square" required
                                                                           oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله
                                                                        .
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">الإسم بالانجليزي</label>
                                                                <div class="col-lg-12">
                                                                    <input id="name_en" name="name_en"
                                                                           value="{{$c->name_en}}"
                                                                           type="text" placeholder="الإسم بالانجليزي"
                                                                           class="form-control btn-square" >

                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">الوصف بالعربي</label>
                                                                <div class="col-lg-12">
                                                                    <textarea name="description_ar"
                                                                              class="form-control btn-square"
                                                                              required>{{$c->description_ar}}</textarea>
                                                                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله
                                                                        .
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">الوصف بالانجليزي</label>
                                                                <div class="col-lg-12">
                                                                    <textarea name="description_en"
                                                                              class="form-control btn-square"
                                                                              required>{{$c->description_en}}</textarea>
                                                                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله
                                                                        .
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <div class="col-lg-1">
                                                                    <input name="has_sizes"
                                                                           type="checkbox" {{$c->has_sizes == 1 ? "checked" : ""}} >
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    <label class="col-lg-12 control-label text-lg-right"
                                                                           for="textinput"> المنتج له احجام ؟ </label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">السعر (ادخل سعر اقل حجم اذا كان
                                                                    المنتج له احجام) </label>
                                                                <div class="col-lg-12">
                                                                    <input name="price_after" type="number"
                                                                           value="{{$c->price_after}}"
                                                                           placeholder="السعر "
                                                                           class="form-control btn-square" required
                                                                           oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله
                                                                        .
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label class="col-lg-12 control-label text-lg-right"
                                                                       for="textinput">الصورة</label>
                                                                <div class="col-lg-12">
                                                                    <input name="image" type="file"
                                                                           accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                                                </div>
                                                                @if($c->image != "")
                                                                    <img src="{{$c->image}}" width="50px" height="50px">
                                                                @else
                                                                    -
                                                                @endif
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button class="btn btn-primary" type="submit">تعديل</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1"
                                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                                         style="text-align:right">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف الحقل</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="post" action="{{route('deleteProduct')}}" class="buttons">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف هذا الحقل
                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{$countries->links()}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>

    <div class="modal fade" id="subCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة منتج</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('products.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">القائمة </label>
                            <div class="col-lg-12">
                                <select name="menu_id" class="form-control btn-square" required>
                                    @foreach($menus as $menu)
                                        <option value="{{$menu->id}}">{{$menu->name_ar}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالعربي</label>
                            <div class="col-lg-12">
                                <input id="name_ar" name="name_ar" type="text" placeholder="الإسم بالعربي"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم
                                بالانجليزي</label>
                            <div class="col-lg-12">
                                <input id="name_en" name="name_en" type="text" placeholder="الإسم بالانجليزي"
                                       class="form-control btn-square"
                                       >

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالعربي</label>
                            <div class="col-lg-12">
                                <textarea name="description_ar" class="form-control btn-square" required></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالانجليزي</label>
                            <div class="col-lg-12">
                                <textarea name="description_en" class="form-control btn-square" required></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-1">
                                <input name="has_sizes" type="checkbox" class=" "
                                >
                            </div>
                            <div class="col-lg-9">
                                <label class="col-lg-12 control-label text-lg-right" for="textinput"> المنتج له احجام
                                    ؟ </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">السعر (ادخل سعر اقل حجم
                                اذا كان المنتج له احجام) </label>
                            <div class="col-lg-12">
                                <input name="price_after" type="number" placeholder="السعر "
                                       required class="form-control btn-square"
                                >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة</label>
                            <div class="col-lg-12">
                                <input name="image" type="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff" required>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="upload-excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">رفع المنتجات</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: right">
               <form method="post" action="{{route('import.products')}}" enctype="multipart/form-data">
                        @csrf
                   <p>لتحميل الملف أضغط <a href="/ملف المنتجات.xlsx" download=""> هنا </a></p>

                   <input type="file" required name="excel">
                        <button type="submit">رفع </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).on('change', '#status', function (e) {

            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editProductStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

    </script>

@endsection
