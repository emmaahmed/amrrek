   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">رقم المنتج</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الصورة</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الحالة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الإسم بالعربي</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الإسم بالإنجليزي</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الوصف بالعربي</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الوصف بالإنجليزي</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">السعر</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">رقم القائمة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">أسم القائمة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">عدد الإضافات</th>

    @foreach($products as $c)

            @if(count($c->variations) > 0)
                @foreach($c->variations as $key=>$variation)

                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}الإضافة بالعربي </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}الإضافة بالإنجليزية </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}نوع الأختيار </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}اجباري </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}الإختيارات بالعربي </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}الإختيارات بالإنجليزية </th>
                <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">{{($key+1)}}أسعار الإختيارات </th>
                @endforeach

            @endif
        @endforeach

    </tr>
    </thead>
    <tbody>

    @foreach($products as $c)
        <tr>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->id}}</td>

            <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{public_path()}}/uploads/products/{{ $c->getAttributes()['image'] }}" width="80px" height="80px" ></td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $c->active == 1?'مفعل':'غير مفعل' }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->name_ar}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->name_en}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->description_ar}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->description_en}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$c->price_after}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $c->menue?$c->menue->id:'-' }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $c->menue?$c->menue->name_ar:'-' }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{count($c->variations)}}</td>
            @if(count($c->variations) > 0)
                @foreach($c->variations as $key=>$variation)

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$variation->name_ar}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$variation->name_en}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$variation->type == 0 ? "اختيار واحد" : "اختيار من متعدد"}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$variation->required == 1 ? "إجباري" : "غير اجباري"}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
            @if(count($variation->options)>0)
            @foreach($variation->options as $option)
                {{$option->name_ar}},
                        @endforeach
                @endif
            </td>
                    <td style="text-align: center; font-family: 'Segoe UI Semilight';">
            @if(count($variation->options)>0)
            @foreach($variation->options as $option)
                {{$option->name_en}},
                        @endforeach
                @endif
            </td>           <td style="text-align: center; font-family: 'Segoe UI Semilight';">
            @if(count($variation->options)>0)
            @foreach($variation->options as $option)
                {{$option->price}},
                        @endforeach
                @endif
            </td>
                @endforeach

            @endif
        </tr>
    @endforeach
    </tbody>
</table>
   </html>