@extends('cp_shop.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                إضافة منتج



                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الدول</li>
                            </ol>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-4">
                            <div class="card-body">

      <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('products.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-lg-2 control-label text-lg-right" for="textinput">الصورة</label>
                            <div class="col-lg-3">
                                <input name="image" type="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff" required>
                                <p> يرجي ادخال صور حجم 300 * 300 </p>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">القائمة </label>
                            <div class="col-lg-12">
                                <select name="menu_id" class="form-control btn-square" required>
                                    @foreach($menus as $menu)
                                        <option value="{{$menu->id}}">{{$menu->name_ar}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالعربي</label>
                            <div class="col-lg-12">
                                <input id="name_ar" name="name_ar" type="text" placeholder="الإسم بالعربي"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم
                                بالانجليزي</label>
                            <div class="col-lg-12">
                                <input id="name_en" name="name_en" type="text" placeholder="الإسم بالانجليزي"
                                       class="form-control btn-square"
                                >

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالعربي</label>
                            <div class="col-lg-12">
                                <textarea name="description_ar" class="form-control btn-square" required></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالانجليزي</label>
                            <div class="col-lg-12">
                                <textarea name="description_en" class="form-control btn-square" required></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-1">
                                <input name="has_sizes" type="checkbox" class=" "
                                >
                            </div>
                            <div class="col-lg-9">
                                <label class="col-lg-12 control-label text-lg-right" for="textinput"> المنتج له احجام
                                    ؟ </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">السعر
                                الأساسي </label>
                            <div class="col-lg-12">
                                <input name="price_after" type="number" step="0.01" placeholder="السعر "
                                       required class="form-control btn-square"
                                >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">السعر بعد الخصم (إن وجد)
                                </label>
                            <div class="col-lg-12">
                                <input name="price_before" type="number" step="0.01" placeholder="السعر "
                                       required class="form-control btn-square"
                                >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group text-lg-right" >
                                    <button style="border-radius: 0;" type="button" id="VariationAdd" class="btn btn-success"> إضافة تفاصيل <span class="fa fa-plus-circle"></span></button>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div id="VariationsContainer">
                                </div>
                            </div>
                        </div>

                        {{-- Variations Shell --}}

                        <div id="VariationShell" class="form-group row add" style="display: none;">
                            <div class="col-md-3">
                                {{--                                    <input type="text" class="form-control variation_ar" name="variations[x][ar]" min="1" placeholder="إسم الإختيار باللغة العربية" required>--}}
                                <input type="text" class="form-control variation_ar" min="1" placeholder="إسم الإختيار باللغة العربية" >
                            </div>
                            <div class="col-md-3">
                                {{--                                    <input type="text" class="form-control variation_ar" name="variations[x][ar]" min="1" placeholder="إسم الإختيار باللغة العربية" required>--}}
                                <input type="text" class="form-control variation_en" min="1" placeholder="إسم الإختيار باللغة الإنجليزية" >
                            </div>
                            {{--<div class="col-md-5">--}}
                            {{--                                    <input type="text" class="form-control float-type variation_en" name="variations[x][en]" min="1" placeholder="إسم الإختيار باللغة الانجليزية" required>--}}
                            {{--<input type="text" class="form-control float-type variation_en" min="1" placeholder="إسم الإختيار باللغة الانجليزية" required>--}}
                            {{--</div>--}}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="type" class="custom-select form-control type">
                                        <option value="" disabled selected>اختر النوع</option>
                                        <option value="0">اختيار واحد</option>
                                        <option value="1">اختيار متعدد</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <label class="d-block" for="check-required">
                                    <input type="checkbox" class="checkbox_animated required" name="required" id="check-required" value="1">
                                    <label for="check-required">إجباري</label>
                                </label>
                            </div>
                            <!--<div class="col-md-12">
                                <label class="d-block" for="check-required">
                                    <input type="checkbox" class="checkbox_animated required" name="is_size" id="check-required2" value="1">
                                    <label for="check-required">هل هذا الاختيار هو حجم للمنتج ؟</label>
                                </label>
                            </div>-->
                            <div class="col-md-2">
                                <button type="button" class="btn btn-xs btn-danger variation-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <div class="col-md-2" style="margin-right: 35%;">
                                <button style="border-radius: 0; margin-bottom: 20px;" type="button" id="OptionAdd" class="btn btn-success option_btn" data-var-id="x"> إضافة إختيار <span class="fa fa-plus-circle"></span></button>
                            </div>

                            <br/>
                            <br/>
                            <br/>
                            <div class="col-md-12 row" style="margin-right: 1%;">
                                <div class="col">
                                    <div id="OptionsContainer">
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Options Shell --}}

                        <div id="OptionShell" class="form-group row col-md-12" style="display: none; ">
                            <div class="col-md-4">
                                {{--                                    <input type="text" class="form-control option_ar" name="variations[x][options][y][ar]" min="1" placeholder="إسم الإضافة باللغة العربية" required>--}}
                                <input type="text" class="form-control option_ar" step="0.01"  min="1" placeholder="إسم الإضافة باللغة العربية" >
                            </div>
                            <div class="col-md-4">
                                {{--                                    <input type="text" class="form-control option_ar" name="variations[x][options][y][ar]" min="1" placeholder="إسم الإضافة باللغة العربية" required>--}}
                                <input type="text" class="form-control option_en" step="0.01"  min="1" placeholder="إسم الإضافة باللغة الإنجليزية" >
                            </div>
                            {{--<div class="col-md-4">--}}
                            {{--                                    <input type="text" class="form-control float-type option_en" name="variations[x][options][y][en]" min="1" placeholder="إسم الإضافة باللغة الانجليزية" required>--}}
                            {{--<input type="text" class="form-control float-type option_en" min="1" placeholder="إسم الإضافة باللغة الانجليزية" required>--}}
                            {{--</div>--}}
                            <div class="col-md-3">
                                {{--                                    <input type="number" class="form-control float-type option_price" name="variations[x][options][y][price]" min="1" placeholder="سعر الإضافة" required>--}}
                                <input type="number" step="0.01" class="form-control float-type option_price" min="1" placeholder="سعر الإضافة" >
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-xs btn-danger option-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                            </div>
                            <br/>

                        </div>

                        {{-- Other Info Shell --}}

                        <div id="InfoShell" class="form-group row add" style="display: none;">
                            <div class="col-md-3">
                                <input type="text" class="form-control text1" placeholder="النص الأول" >
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control text2" placeholder="النص الثاني" >
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control text3" placeholder="النص الثالث" >
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-xs btn-danger info-remove-btn" style="margin:0; border-radius: 0;"><i data-feather="x"></i></button>
                            </div>
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
                </div>
                </div>
    </div>
@endsection
@section('extra-js')
    <script>


        var i = 0;

        $("#VariationAdd").click(function(){
            $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo('#VariationsContainer').fadeIn();
            // $("#VariationShell").clone(true).attr('id','VariationShell'+i).appendTo($(this).closest('.form-group')).fadeIn();


            $('#VariationShell'+i).find('input.variation_ar').attr('name', 'variations['+i+'][name_ar]');
            $('#VariationShell'+i).find('input.variation_en').attr('name', 'variations['+i+'][name_en]');
            // $('#VariationShell'+i).find('input.variation_en').attr('name', 'variations['+i+'][en]');
            $('#VariationShell'+i).find('select.type').attr('name', 'variations['+i+'][type]');
            $('#VariationShell'+i).find('input.required').attr('name', 'variations['+i+'][required]');

            // Is the variation is size of product ??
            //$('#VariationShell'+i).find('input.is_size').attr('name', 'variations['+i+'][is_size]');
            // end Is the variation is size of product ??

            $('#VariationShell'+i).find('button.option_btn').data('var-id', i);

            i++;
        });

        $('body').on('click', '.variation-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });

        $('body').on('click', '.variation-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });
        // Options
        var y = 0;
        $("#OptionAdd").on('click',function()
        {
            console.log('test');
            var x = $(this).data('var-id');
            $("#OptionShell").clone(true).attr('id','OptionShell'+y).appendTo($(this).closest('.form-group')).fadeIn();
            $('#OptionShell'+y).find('input.option_ar').attr('name', 'variations['+x+'][options]['+y+'][name_ar]');
            $('#OptionShell'+y).find('input.option_en').attr('name', 'variations['+x+'][options]['+y+'][name_en]');
            // $('#OptionShell'+y).find('input.option_en').attr('name', 'variations['+x+'][options]['+y+'][en]');
            $('#OptionShell'+y).find('input.option_price').attr('name', 'variations['+x+'][options]['+y+'][price]');
            if($('select[name="variations['+x+'][type]"]').val() == 2){
                // $('.option_color').css('display','');
                //alert('test');
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
            }else{
                $('#OptionShell'+y).find('input.option_color').attr('name', 'variations['+x+'][options]['+y+'][color]');
                $('#OptionShell'+y).find('input.option_color').css('display','none');
            }
            $('#OptionShell'+y).find('input.status').attr('name', 'variations['+x+'][options]['+y+'][status]');
            y++;
        });
        $('body').on('click', '.option-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });



        // InFo Shell Append

        $("#InfoAdd").click(function(){
            $("#InfoShell").clone(true).attr('id','InfoShell'+i).appendTo('#InfosContainer').fadeIn();
            $('#InfoShell'+i).find('input.text1').attr('name', 'info['+i+'][text1]');
            $('#InfoShell'+i).find('input.text2').attr('name', 'info['+i+'][text2]');
            $('#InfoShell'+i).find('input.text3').attr('name', 'info['+i+'][text3]');

            i++;
        });

        $('body').on('click', '.info-remove-btn', function(){
            $(this).closest('.form-group').remove();
        });


        // $('document').on('change','.type', function(){
        //     //$('select[name="' + name + '"]')
        //     console.log(this);
        //     if($('.type').attr("selected") == 2 ){
        //         console.log('aaaaaaaaaaa');
        //     }
        // });
        // $('body').on('change', '.type', function(){
        //     if($(this).val() == 2 ){ //type = 2 for color
        //         console.log('aaaaaaaaaaa');
        //         $(this).closest('.color').attr('display','block');
        //     }else{
        //         console.log('bbbbbbbbbbb');
        //         $(this).closest('.color').attr('display','none');
        //     }
        // });
    </script>
@endsection
