<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <title>Amrk - امرك</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/flaticon/font/flaticon.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets-login/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets-login/css/skins/default.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style-spider.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/animate.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">



</head>
<body id="top" >

<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<!-- <div class="loader-color">
<div class="cssload-dots">
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>

</div>
</div> -->
<!-- Login 11 start -->

<div class="login-11">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-12 col-pad-0 bg-color-11">
                <div class="form-section">
                    <div class="logo">
                        <a >
                            <img src="{{asset('assets-login/img/logos/logo.png')}}" alt="logo">
                        </a>
                    </div>
                    <h3>تحقق من بريدك الالكتروني</h3>
                    <div class="text-center" style="color: red;">
                        <strong>{{Session::get('error')}}</strong>
                    </div>
                    <div class="text-center" style="color: #fff;">
                        <strong>{{Session::get('success')}}</strong>
                    </div>

                    <div class="login-inner-form">
                        <form action="{{route('shop.check-mail')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group clearfix">
                                <label>البريد الإلكتروني</label>
                                <div class="form-box">
                                    <input type="email" required name="email" class="input-text" placeholder="البريد الإلكتروني">
                                    <i class="flaticon-mail-2"></i>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md btn-theme btn-block">ارسال</button>
                            </div>
                        </form>


                    </div>

                </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-12 col-pad-0 bg-img bg-img2 none-992">

                <div class="info wow fadeInLeft" data-wow-duration="2s">
                    <h1 class="black"><span>مرحبا بك في </span> أمرك</h1>
                    <p>أمرك هي منصة التوصيل العملاقة في المملكة. أمرك هي تجربة فريدة عند الطلب تحصل على أعلى تقييمات المستخدمين بين جميع تطبيقات التوصيل الأخرى. إنه ليس الأول ولكنه يعتبر أفضل تطبيق يقدم كل ما يمكنك تخيله من جميع أنواع المتاجر والمطاعم ويغطي جميع مجالات الأعمال في المملكة العربية السعودية ودول مجلس التعاون الخليجي</p>
                    <img src="{{asset('assets-login/img/mockup.png')}}" class="img-fluid  mockup">
                </div>
                <div id="particles-js">
                    <div class="bg-photo">
                        <img src="{{asset('assets-login/img/bg-image-51.png')}}" alt="bg" class="img-fluid wow fadeInUp " data-wow-duration="2s">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Login 11 end -->

<!-- External JS libraries -->

<script src="{{asset('assets-login/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('assets-login/js/popper.min.js')}}"></script>
<script src="{{asset('assets-login/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> <!-- stats.js lib -->
<script src="{{asset('assets-login/js/script-spider.js')}}"></script>
<script src="{{asset('assets-login/js/wow.min.js')}}"></script>
<script>

    new WOW().init();

</script>

<!-- Custom JS Script -->

<!-- <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <filter id="goo">
            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12">
            </feGaussianBlur>
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7" result="goo">
            </feColorMatrix>
        </filter>
    </defs>
</svg>
<script>



       $(".loader-color").show();
       setTimeout(function () {

          $('.loader-color').hide();
       }, 2000);


    </script> -->

</body>
</html>