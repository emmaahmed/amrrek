@extends('cp_shop.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                كود الخصم
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الدول</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            {{--@if(admin()->hasPermissionTo('Add country'))--}}
                            @if(!$code)
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#subCat"><i class="icon-plus"></i>
                                إضافة كود خصم
                            </button>
                            @endif
                            {{--@endif--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>

                                <th scope="col">الكود</th>
                                <th scope="col">القيمة</th>
                                <th scope="col">نوع الخصم</th>
                                <th scope="col">عدد مرات الاستخدام</th>
                                <th scope="col">تاريخ الانتهاء</th>
                                <th scope="col">الوصف بالعربية</th>
                                <th scope="col">الوصف بالإنجليزية</th>

                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($code)
                                <tr id="main_cat_{{$code->id}}" onclick="myFunction({{$code->id}})">
                                    <td>{{$code->id}}</td>

                                    <td>{{$code->code}}</td>
                                    <td>{{$code->value}}</td>
                                    <td>{{$code->type == 0 ? "خصم ثابت " : "خصم نسبة"}}</td>
                                    <td>{{$code->expire_times}}</td>
                                    <td>{{$code->expire_at}}</td>
                                    <td>{{$code->ar_desc}}</td>
                                    <td>{{$code->en_desc}}</td>

                                    <td>
                                        <button title="" class="btn btn-danger" data-target="#delete_{{$code->id}}" data-toggle="modal" data-original-title="حذف">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>

                                    <div class="modal  fade" id="delete_{{$code->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">هل انت متأكد ؟</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="get" action="{{route('shops.deletePromo',$code->id)}}" class="buttons">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف كود الخصم
                                                            <br>رقم الكود: ({{$code->id}})
                                                            <br>الكود: ({{$code->code}})

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {{--                                                        <input type="hidden" name="model_id" value="{{$result->id}}">--}}
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">إغلاق</button>
                                                        <button type="submit" class="btn btn-danger">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endif
                                {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$countries->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>

    <div class="modal fade" id="subCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة كود خصم جديد</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post" action="{{route('shops-promocodes.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" name="department_id" value="2">



                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">كود الخصم</label>
                            <div class="col-lg-12">
                                <input name="code" class="form-control digits" type="text" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد نوع الخصم</label>
                            <div class="col-lg-12">
                                <select name="type" class="form-control digits" id="code-type" required>
                                    <option value="0" >خصم ثابت</option>
                                    <option value="1" >خصم بالنسبة</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"><span id="code-text"> قيمة</span> الخصم</label>
                            <div class="col-lg-12">
                                <input name="value" class="form-control digits" type="text" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">عدد مرات الاستخدام للكود</label>
                            <div class="col-lg-12">
                                <input name="expire_times" class="form-control digits" type="number" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">تاريخ انتهاء صلاحية الكود</label>
                            <div class="col-lg-12">
                                <input name="expire_at" class="form-control digits" type="date" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        {{--                        <div class="form-group row ">--}}
                        {{--                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالإنجليزية</label>--}}
                        {{--                            <div class="col-lg-12">--}}
                        {{--                                <input name="en_desc" class="form-control digits" type="text" required>--}}
                        {{--                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالعربية</label>
                            <div class="col-lg-12">
                                <input name="ar_desc" class="form-control digits" type="text" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالإنجليزية</label>
                            <div class="col-lg-12">
                                <input name="en_desc" class="form-control digits" type="text" required>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                    </div>




                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('extra-js')
    <script>
        $('#code-type').on('change',function (){
            if($('#code-type').val()==0) {
                $('#code-text').text('قيمة');
            }
            else{
                $('#code-text').text('نسبة');

            }

        });
    </script>
@endsection