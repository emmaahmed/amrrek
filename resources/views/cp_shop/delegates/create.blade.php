@extends('cp_shop.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                            data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                تسجيل مندوب
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المتاجر</li>
                            </ol>--}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
        <div class="card">
        <div class="card-body">

        <form class="form-horizontal needs-validation was-validated" method="post" action="{{route('delegates.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="modal-body">

                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة الشخصية</label>
                    <input name="image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>


                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم الأول</label>
                    <div class="col-lg-12">
                        <input name="f_name" type="text" placeholder="الإسم الأول" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>


                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">الأسم الثاني</label>
                    <div class="col-lg-12">
                        <input id="inputName1" name="l_name" type="text" placeholder="الأسم الثاني" class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row " >
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">العمر</label>
                    <div class="col-lg-12">
                        <input id="inputName1" name="age" type="text" placeholder="العمر" class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">البريد الإلكتروني </label>
                    <div class="col-lg-12">
                        <input id="inputName1" name="email" type="text" placeholder="البريد الإلكتروني " class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">كلمة المرور </label>
                    <div class="col-lg-12">
                        <input id="inputName1" name="password" type="password" min="8" placeholder="كلمة المرور " class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">نوع الهوية</label>
                    <div class="col-lg-12">
                        <select class="form-control btn-square" name="national_id_type">
                            @foreach($nationals as $national)
                                <option value="{{$national->id}}">{{$national->name_ar}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">رقم الهوية الوطنية / الإقامة</label>
                    <div class="col-lg-12">
                        <input id="inputName1"  name="national_id" type="text" placeholder="رقم الهوية " class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">نوع الاشتراك</label>
                    <div class="col-lg-12">
                        <select class="form-control btn-square" name="subscription_type">
                            @foreach($subscriptionTypes as $type)
                                <option value="{{$type->id}}">{{$type->name_ar}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">نوع المركبة</label>
                    <div class="col-lg-12">
                        <select class="form-control btn-square" name="car_level">
                            @foreach($carTypes as $type)
                                <option value="{{$type->id}}">{{$type->name_ar}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">اسم المركبة </label>
                    <div class="col-lg-12">
                        <input name="car_name" type="text" placeholder="اسم المركبة" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">موديل المركبة </label>
                    <div class="col-lg-12">
                        <input name="car_model" type="text" placeholder="موديل المركبة" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">لوحة المركبة </label>
                    <div class="col-lg-12">
                        <input name="car_num" type="text" placeholder="لوحة المركبة" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>


                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة الأمامية للمركبة</label>
                    <input name="front_car_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>

                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة خلفية للمركبة</label>
                    <input name="back_car_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>
                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">صورة التأمين</label>
                    <input name="insurance_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>
                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">صورة التراخيص</label>
                    <input name="license_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>
                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">صورة رخصة القيادة</label>
                    <input name="civil_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>
                <div class="form-group " >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">صورة الهوية الوطنية أو الإقامة</label>
                    <input name="civil_image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">أدخل لون المركبة </label>
                    <div class="col-lg-12">
                        <input name="car_color" type="text" placeholder="أدخل لون المركبة" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">إختر لون المركبة </label>
                    <div class="col-lg-12">
                        <input name="car_color" type="color" placeholder="إختر لون المركبة" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">أسم البنك </label>
                    <div class="col-lg-12">
                        <input name="bank_name" type="text" placeholder="أسم البنك" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">رقم حساب البنك </label>
                    <div class="col-lg-12">
                        <input name="bank_account_num" type="text" placeholder="أسم البنك" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">رقم الأيبان </label>
                    <div class="col-lg-12">
                        <input name="bank_account_name" type="text" placeholder="رقم الأيبان" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                {{--                            //////////////////////////////////////////////--}}


                {{--                            //////////////////////////////////////////////--}}
                <br>


            </div>
            <div class="modal-footer">

                <button class="btn btn-primary">حفظ</button>
            </div>
        </form>
        <!-- Container-fluid Ends-->
        </div>
        </div>
        </div>
    </div>



@endsection
