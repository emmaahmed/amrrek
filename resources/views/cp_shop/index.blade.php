<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('default.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('default.png')}}" type="image/x-icon">
    <title>Amrk - امرك</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">
    <link
            href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/date-picker.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/prism.css')}}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('cp/endless/assets/css/light-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/responsive.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('cp/endless/assets/css/photoswipe.css') }}">

    <style>
        .customizer-links {
            display: none;
        }
    </style>
    <style>
        @font-face {
            font-family: 'din';
            src: url({{asset('din.otf')}}) format('opentype');
        }

        body, h1, h2, h3, h4, h5, h6, * {
            font-family: 'din';
            font-size: small;
        }

        th {
            text-align: center;
        }

        td {
            text-align: center;
        }

        element.style {
        }

        .selling-update svg path {
            color: #C32F45;
        }

        .selling-update svg path,
        .selling-update svg line,
        .selling-update svg polyline,
        .selling-update svg polygon,
        .selling-update svg rect,
        .selling-update svg circle,
        .mb-0,
        .f-18,
        .align-abjust,
        h6, h4, h3 {

            color: #C32F45;
        }

        .page-wrapper .page-body-wrapper .page-sidebar {
            background: #b5293c;
        }

        .loader, .whirly-loader {
            color: red !important;
        }

        button {
            margin: 2px;
        }

        .tt-menu {
            display: none !important;
        }
    </style>
    @yield('imgStylHedr')

</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"></div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-right row">
            <div class="main-header-left d-lg-none">
                <div class="logo-wrapper">
                    {{--<a class="pull-right mb-0" href="http://2grand.net" target="_blank"><img src="http://radareldad.my-staff.net/grand.png" style="width:100px;">
                    </a>--}}
                    {{--                    <a href="http://2grand.net" target="_blank">--}}
                    {{--                        <img src="{{asset('grand.png')}}" alt=""--}}
                    {{--                             style="width:230px; height:60px;background-color: white;padding: 10px;border-radius: 5px">--}}
                    {{--                    </a>--}}
                </div>
            </div>
            <div class="mobile-sidebar d-block">
                <div class="media-body text-right switch-sm">
                    <label class="switch"><a><i id="sidebar-toggle"
                                                data-feather="align-left"></i></a></label>
                </div>
            </div>
            <div class="nav-right col p-0">
                <ul class="nav-menus">
                    <li>
                        @if(!isset($orders_charts))
                            <form class="form-inline search-form" action="#" method="get" dir="rtl">
                                <div class="form-group">
                                    <div class="Typeahead Typeahead--twitterUsers">
                                        <div class="u-posRelative">
                                            <input class="Typeahead-input {{--form-control-plaintext--}}"
                                                   id="demo-input"
                                                   type="text" name="q" placeholder="بحث . . ." style="width:100%">
                                            <div class="spinner-border Typeahead-spinner" role="status"><span
                                                        class="sr-only">جاري التحميل . . .</span></div>
                                            <span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                                        </div>
                                        <div class="Typeahead-menu"></div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </li>
                    <li style="    top: 10px;">
                        <label class="switch">
                            <input type="checkbox" id="turn-notification"  @if(\Illuminate\Support\Facades\Auth::guard('shop')->user()->notification_flag == 1) checked @endif><span class="switch-state "></span>
                        </label>

                    </li>

                    <li><a class="text-dark" href="{{route('shop_home')}}#!" onclick="toggleFullScreen()"><i
                                    data-feather="maximize"></i></a></li>
                    {{--<li class="onhover-dropdown"><a class="txt-dark" href="index.html#">
                        <h6>EN</h6>
                        </a>
                      <ul class="language-dropdown onhover-show-div p-20">
                        <li><a href="index.html#" data-lng="en"><i class="flag-icon flag-icon-is"></i> English</a></li>
                        <li><a href="index.html#" data-lng="es"><i class="flag-icon flag-icon-um"></i> Spanish</a></li>
                        <li><a href="index.html#" data-lng="pt"><i class="flag-icon flag-icon-uy"></i> Portuguese</a></li>
                        <li><a href="index.html#" data-lng="fr"><i class="flag-icon flag-icon-nz"></i> French</a></li>
                      </ul>
                    </li>--}}
                    {{--<li class="onhover-dropdown"><i data-feather="bell"></i><span class="dot"></span>
                      <ul class="notification-dropdown onhover-show-div">
                        <li>Notification <span class="badge badge-pill badge-primary pull-right">3</span></li>
                        <li>
                          <div class="media">
                            <div class="media-body">
                              <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag"></i></span>Your order ready for Ship..!<small class="pull-right">9:00 AM</small></h6>
                              <p class="mb-0">Lorem ipsum dolor sit amet, consectetuer.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-body">
                              <h6 class="mt-0 txt-success"><span><i class="download-color font-success" data-feather="download"></i></span>Download Complete<small class="pull-right">2:30 PM</small></h6>
                              <p class="mb-0">Lorem ipsum dolor sit amet, consectetuer.</p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="media">
                            <div class="media-body">
                              <h6 class="mt-0 txt-danger"><span><i class="alert-color font-danger" data-feather="alert-circle"></i></span>250 MB trash files<small class="pull-right">5:00 PM</small></h6>
                              <p class="mb-0">Lorem ipsum dolor sit amet, consectetuer.</p>
                            </div>
                          </div>
                        </li>
                        <li class="bg-light txt-dark"><a href="index.html#">All</a> notification</li>
                      </ul>
                    </li>--}}
                    {{--
                  <li><a href="{{route('shop_home')}}#"><i class="right_side_toggle" data-feather="message-circle"></i><span class="dot"></span></a></li>
                  --}}
                    <li class="onhover-dropdown"><i data-feather="bell"></i><span class="dot"></span>
                        <ul class="notification-dropdown onhover-show-div" id="notifications-ul">
                            <li>الإشعارات <span class="badge badge-pill badge-primary pull-right"
                                                id="notify-count">{{count($notifications)}}</span></li>
                            @if(isset($notifications) && count($notifications)>0)
                                @foreach($notifications as $notification)
                                    @if($notification->order_type==2)
                                        <a href="{{route('orders.index')}}">
                                            <li>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h6 class="mt-0"><span><i class="shopping-color"
                                                                                  data-feather="shopping-bag"></i></span>{{$notification->title}}
                                                            <small class="pull-right">{{\Carbon\Carbon::parse($notification->created_at)->format('Y-m-d H:i')}}</small>
                                                        </h6>
                                                        <p class="mb-0">{{$notification->body}}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        </a>
                                    @else
                                        <li>
                                            <div class="media">
                                                <div class="media-body">
                                                    <h6 class="mt-0"><span><i class="shopping-color"
                                                                              data-feather="shopping-bag"></i></span>{{$notification->title}}
                                                        <small class="pull-right">{{\Carbon\Carbon::parse($notification->created_at)->format('Y-m-d H:i')}}</small>
                                                    </h6>
                                                    <p class="mb-0">{{$notification->body}}</p>
                                                </div>
                                            </div>
                                        </li>

                                    @endif
                                @endforeach
                                @if(count($notifications)>4)
                                        <li>
                                            <a href="{{route('shops.notifications.page')}}">
                                                كل الإشعارات
                                            </a>
                                        </li>

                                    @endif
                            @endif

                            {{--                            <li class="bg-light txt-dark"><a href="index.html#">All</a> notification</li>--}}
                        </ul>
                    </li>

                    <b>{{\App\Models\Shop::whereId(Auth::guard('shop')->user()->shop_id)->first()->name}}</b>
                    <li class="onhover-dropdown">
                        <div class="media align-items-center">
                            <img class="align-self-center pull-right img-50 rounded-circle"
                                 src="{{\App\Models\Shop::whereId(Auth::guard('shop')->user()->shop_id)->first()->image}}"
                                 alt="header-user">
                            <div class="dotted-animation"><span class="animate-circle"></span><span
                                        class="main-circle"></span></div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div p-20">
                            {{--<li><a href="index.html#"><i data-feather="user"></i>                                    Edit Profile</a></li>
                            <li><a href="index.html#"><i data-feather="mail"></i>                                    Inbox</a></li>
                            <li><a href="index.html#"><i data-feather="lock"></i>                                    Lock Screen</a></li>
                            <li><a href="index.html#"><i data-feather="settings"></i>                                    Settings</a></li>--}}

                            @if(Auth::guard('shop')->user()->type == 1 || Auth::guard('shop')->user()->type ==3)
                                <li><a href="{{ asset('/shop/edit-profile') }}">
                                        البروفايل
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-user">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                    </a>
                                </li>
                            @endif
                            <li><a href="{{ route('logout_shop') }}"

                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    خروج
                                    <i data-feather="log-out"></i>

                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout_shop') }}" method="GET"
                                  style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </li>

                </ul>
                <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
            </div>

{{--            <div class="nav-right col p-0">--}}
{{--                <div class="media">--}}
{{--                    <div class="media-body text-right icon-state switch-outline">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <script id="result-template" type="text/x-handlebars-template">
                <div class="ProfileCard u-cf">
                    <div class="ProfileCard-avatar">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-airplay m-0">
                            <path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path>
                            <polygon points="12 15 17 21 7 21 12 15"></polygon>
                        </svg>
                    </div>
                    <div class="ProfileCard-details">
                        <div class="ProfileCard-realName"></div>
                    </div>
                </div>
            </script>
            <script id="empty-template" type="text/x-handlebars-template">
                <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down,
                    yikes!
                </div>

            </script>
        </div>
    </div>
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <div class="page-sidebar">
            <div class="main-header-left d-none d-lg-block">
                <div class="logo-wrapper">
                    <a href="#" style="display: table;margin: 0 auto;">
                        <img src="{{asset('logo-svg.svg')}}" alt=""></a></div>
            </div>
            <div class="sidebar custom-scrollbar">
                <div class="sidebar-user text-center">
                    <div>
                        <img class="img-60 rounded-circle"
                             src="{{App\Models\Shop::whereId(Auth::guard('shop')->user()->shop_id)->first()->image}}" alt="#">
                        {{--<div class="profile-edit"><a href="edit-profile.html" target="_blank"><i data-feather="edit"></i></a></div>--}}
                    </div>
                    <h6 class="mt-3 f-14">{{Auth::guard('shop')->user()->name}}</h6>
                    {{--                    <p> مدير النظام</p>--}}
                </div>
                <ul class="sidebar-menu">
                    <li>
                        @if(auth()->guard('shop')->user()->hasPermissionTo('الرئيسية للمتجر'))
                            <a class="sidebar-header" href="{{route('shop_home')}}"><i data-feather="home"></i>
                                {{--@if (auth()->user()->hasPermissionTo('Show dashboard'))--}}

                                <span>الرئيسية</span>

                                {{--<span class="badge badge-pill badge-primary">6</span>--}}
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        @endif
                        {{--<ul class="sidebar-submenu">--}}
                        {{--<li><a href="index.html"><i class="fa fa-circle"></i><span>Default</span></a></li>
                        <li><a href="dashboard-ecommerce.html"><i class="fa fa-circle"></i><span>E-commerce</span></a></li>
                        <li><a href="dashboard-university.html"><i class="fa fa-circle"></i><span>University</span></a></li>
                        <li><a href="dashboard-bitcoin.html"><i class="fa fa-circle"></i><span>Crypto</span></a></li>
                        <li><a href="dashboard-server.html"><i class="fa fa-circle"></i><span>Server</span></a></li>
                        <li><a href="dashboard-project.html"><i class="fa fa-circle"></i><span>Project</span></a></li>--}}
                        {{--</ul>--}}
                    </li>


                    @if(auth()->guard('shop')->user()->hasPermissionTo('المشرفين للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/admins')}}"><i data-feather="user"></i>
                                <span>المشرفين</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('الفروع'))
                        @if(Auth::guard('shop')->user()->type == 1 || Auth::guard('shop')->user()->type == 2 )
                            <li>
                                <a class="sidebar-header" href="{{asset('/shop/branches')}}"><i
                                            data-feather="layers"></i>
                                    <span>الفروع</span>
                                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                                </a>
                            </li>
                        @endif
                    @endif

                    @if(auth()->guard('shop')->user()->hasPermissionTo('المناديب المسجلة للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/delegates')}}"><i
                                        data-feather="user-check"></i>
                                <span>المناديب المسجلين</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('المناديب المسجلة للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{route('shops.pending-delegates')}}"><i
                                        data-feather="user-check"></i>
                                <span>المناديب في انتظار القبول</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('الطلبات للمتجر'))
                        <li>

                            <a class="sidebar-header"><i data-feather="shopping-cart"></i>
                                <span> الطلبات</span>
                                <i class="fa fa-angle-right pull-right"></i></a>
                            <ul class="sidebar-submenu">
                                <li><a href="{{asset('/shop/orders')}}"><i class="fa fa-circle"></i>كل الطلبات</a></li>
                                <li><a href="{{route('accept-orders')}}"><i class="fa fa-circle"></i>الطلبات
                                        المقبولة</a></li>
                                <li><a href="{{route('onway-orders')}}"><i class="fa fa-circle"></i>الطلبات في
                                        الطريق</a></li>
                                <li><a href="{{route('finished-orders')}}"><i class="fa fa-circle"></i>الطلبات المنتهية</a>
                                </li>
                                <li><a href="{{route('cancelled-orders')}}"><i class="fa fa-circle"></i>الطلبات الملغية</a>
                                </li>

                            </ul>
                        </li>

                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('العروض للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/sliders')}}"><i data-feather="gift"></i>
                                <span>العروض</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('المنيو للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/menus')}}"><i data-feather="file-text"></i>
                                <span>المنيو</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    @if(auth()->guard('shop')->user()->hasPermissionTo('المنتجات للمتجر'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/products')}}"><i data-feather="sliders"></i>
                                <span>المنتجات</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif

                    {{--                    @if(\Auth::guard('shop')->user()->type == 3 || \Auth::guard('shop')->user()->type == 4 )--}}
                    {{--                        <li>--}}
                    {{--                            <a class="sidebar-header" href="{{asset('/shop/products_branches')}}"><i data-feather="home"></i>--}}
                    {{--                                <span>منتجات الفرع</span>--}}
                    {{--                                <i class="fa fa-angle-right pull-right"></i>--}}
                    {{--                            </a>--}}
                    {{--                        </li>--}}
                    {{--                    @endif--}}

                    @if(auth()->guard('shop')->user()->hasPermissionTo('مواعيد العمل'))
                        <li>
                            <a class="sidebar-header" href="{{asset('/shop/edit-dailyWork')}}"><i
                                        data-feather="watch"></i>
                                <span>مواعيد العمل</span>
                                {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                            </a>
                        </li>
                    @endif
                    {{--                    @if(auth()->user()->hasPermissionTo('اكواد الخصم'))--}}
                    <li>
                        <a class="sidebar-header" href="{{route('shops-promocodes.index')}}"><i
                                    data-feather="watch"></i>
                            <span>كود الخصم</span>
                            {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                        </a>
                    </li>
                    {{--                    @endif--}}



                    {{--@if(admin()->hasPermissionTo('View category'))--}}
                    {{--<li><a class="sidebar-header" href="{{asset('cats')}}"><i data-feather="home"></i>
                            <span>الاقسام</span>
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                    </li>--}}
                    {{--@endif--}}

                    {{--                    <li><a class="sidebar-header" href="{{asset('/admin/categories')}}"><i data-feather="home"></i>--}}
                    {{--                            <span>الاقسام</span>--}}
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}



                    {{--@if(admin()->hasPermissionTo('View notification'))--}}
                    {{--                    <li><a class="sidebar-header" href="{{asset('/admin/reports')}}"><i data-feather="home"></i>--}}
                    {{--                            <span>التقارير</span>--}}
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--@endif--}}

                    {{--@if(admin()->hasPermissionTo('View term'))--}}
                    {{--                    <li><a class="sidebar-header" href="{{asset('/admin/terms')}}"><i data-feather="home"></i>--}}
                    {{--                            <span>الشروط و الاحكام</span>--}}
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--@endif--}}





                    {{--@if(admin()->hasPermissionTo('View about'))--}}

                    {{--@endif--}}
                    {{--<li><a class="sidebar-header" href="index.html#"><i data-feather="airplay"></i><span>Widgets</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="general-widget.html"><i class="fa fa-circle"></i>General</a></li>
                        <li><a href="chart-widget.html"><i class="fa fa-circle"></i>Chart</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="../starter-kit/layout-light.html"><i data-feather="anchor"></i><span> Starter kit</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="disc"></i><span>Color Version</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="layout-light.html"><i class="fa fa-circle"></i>Layout Light</a></li>
                        <li><a href="layout-dark.html"><i class="fa fa-circle"></i>Layout Dark</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="sidebar"></i><span>Sidebar</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="compact.html"><i class="fa fa-circle"></i>Compact Sidebar</a></li>
                        <li><a href="compact-small.html"><i class="fa fa-circle"></i>Compact Icon Sidebar</a></li>
                        <li><a href="sidebar-hidden.html"><i class="fa fa-circle"></i>Sidebar Hidden</a></li>
                        <li><a href="sidebar-fixed.html"><i class="fa fa-circle"></i>Sidebar Fixed</a></li>
                        <li><a class="disabled" href="" onclick="return false;"><i class="fa fa-circle"></i>Disable</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="layout"></i><span>Page layout</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="box-layout.html"><i class="fa fa-circle"></i>Boxed</a></li>
                        <li><a href="layout-rtl.html"><i class="fa fa-circle"></i>RTL</a></li>
                        <li><a href="1-column.html"><i class="fa fa-circle"></i>1 Column</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="align-justify"></i><span>Menu Options</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="hide-on-scroll.html"><i class="fa fa-circle"></i>Hide menu on Scroll</a></li>
                        <li><a href="vertical.html"><i class="fa fa-circle"></i>Vertical Menu</a></li>
                        <li><a href="mega-menu.html"><i class="fa fa-circle"></i>Mega Menu</a></li>
                        <li><a href="fix-header.html"><i class="fa fa-circle"></i>Fix header</a></li>
                        <li><a href="fix-header-sidebar.html"><i class="fa fa-circle"></i>Fix Header & sidebar</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="cloud-lightning"></i><span>Footers</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="footer-light.html"><i class="fa fa-circle"></i>Footer Light</a></li>
                        <li><a href="footer-dark.html"><i class="fa fa-circle"></i>Footer Dark</a></li>
                        <li><a href="footer-fixed.html"><i class="fa fa-circle"></i>Footer Fixed</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="pagebuild.html"><i data-feather="clipboard"></i><span>Page Builder</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="edit-3"></i><span>Form Builders</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="form-builder-1.html"><i class="fa fa-circle"></i>Form Builder 1</a></li>
                        <li><a href="form-builder-2.html"><i class="fa fa-circle"></i>Form Builder 2</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="button-builder.html"><i data-feather="bookmark"></i><span>Button Builder</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="box"></i><span> Base</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="state-color.html"><i class="fa fa-circle"></i>State color</a></li>
                        <li><a href="typography.html"><i class="fa fa-circle"></i>Typography</a></li>
                        <li><a href="avatars.html"><i class="fa fa-circle"></i>Avatars</a></li>
                        <li><a href="helper-classes.html"><i class="fa fa-circle"></i>helper classes</a></li>
                        <li><a href="grid.html"><i class="fa fa-circle"></i>Grid</a></li>
                        <li><a href="tag-pills.html"><i class="fa fa-circle"></i>Tag & pills</a></li>
                        <li><a href="progress-bar.html"><i class="fa fa-circle"></i>Progress</a></li>
                        <li><a href="modal.html"><i class="fa fa-circle"></i>Modal</a></li>
                        <li><a href="alert.html"><i class="fa fa-circle"></i>Alert</a></li>
                        <li><a href="popover.html"><i class="fa fa-circle"></i>Popover</a></li>
                        <li><a href="tooltip.html"><i class="fa fa-circle"></i>Tooltip</a></li>
                        <li><a href="loader.html"><i class="fa fa-circle"></i>Spinners</a></li>
                        <li><a href="dropdown.html"><i class="fa fa-circle"></i>Dropdown</a></li>
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Tabs<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="tab-bootstrap.html"><i class="fa fa-circle"></i>Bootstrap Tabs</a></li>
                            <li><a href="tab-material.html"><i class="fa fa-circle"></i>Line Tabs</a></li>
                          </ul>
                        </li>
                        <li><a href="according.html"><i class="fa fa-circle"></i>Accordion</a></li>
                        <li><a href="navs.html"><i class="fa fa-circle"></i>Navs</a></li>
                        <li><a href="box-shadow.html"><i class="fa fa-circle"></i>Shadow</a></li>
                        <li><a href="list.html"><i class="fa fa-circle"></i>Lists</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="folder-plus"></i><span> Advance</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="scrollable.html"><i class="fa fa-circle"></i>Scrollable</a></li>
                        <li><a href="tree.html"><i class="fa fa-circle"></i>Tree view</a></li>
                        <li><a href="bootstrap-notify.html"><i class="fa fa-circle"></i>Bootstrap Notify</a></li>
                        <li><a href="rating.html"><i class="fa fa-circle"></i>Rating</a></li>
                        <li><a href="dropzone.html"><i class="fa fa-circle"></i>dropzone</a></li>
                        <li><a href="tour.html"><i class="fa fa-circle"></i>Tour</a></li>
                        <li><a href="sweet-alert2.html"><i class="fa fa-circle"></i>SweetAlert2</a></li>
                        <li><a href="modal-animated.html"><i class="fa fa-circle"></i>Animated Modal</a></li>
                        <li><a href="owl-carousel.html"><i class="fa fa-circle"></i>Owl Carousel</a></li>
                        <li><a href="ribbons.html"><i class="fa fa-circle"></i>Ribbons</a></li>
                        <li><a href="pagination.html"><i class="fa fa-circle"></i>Pagination</a></li>
                        <li><a href="steps.html"><i class="fa fa-circle"></i>Steps</a></li>
                        <li><a href="breadcrumb.html"><i class="fa fa-circle"></i>Breadcrumb</a></li>
                        <li><a href="range-slider.html"><i class="fa fa-circle"></i>Range Slider</a></li>
                        <li><a href="image-cropper.html"><i class="fa fa-circle"></i>Image cropper</a></li>
                        <li><a href="sticky.html"><i class="fa fa-circle"></i>Sticky</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="cloud-drizzle"></i><span>Animation<span class="badge badge-danger ml-3">Hot</span></span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="animate.html"><i class="fa fa-circle"></i>Animate</a></li>
                        <li><a href="scroll-reval.html"><i class="fa fa-circle"></i>Scroll Reveal</a></li>
                        <li><a href="AOS.html"><i class="fa fa-circle"></i>AOS animation</a></li>
                        <li><a href="tilt.html"><i class="fa fa-circle"></i>Tilt Animation</a></li>
                        <li><a href="wow.html"><i class="fa fa-circle"></i>Wow Animation</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="command"></i><span>Icons</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="flag-icon.html"><i class="fa fa-circle"></i>Flag icon</a></li>
                        <li><a href="font-awesome.html"><i class="fa fa-circle"></i>Fontawesome Icon</a></li>
                        <li><a href="ico-icon.html"><i class="fa fa-circle"></i>Ico Icon</a></li>
                        <li><a href="themify-icon.html"><i class="fa fa-circle"></i>Thimify Icon</a></li>
                        <li><a href="feather-icon.html"><i class="fa fa-circle"></i>Feather icon</a></li>
                        <li><a href="whether-icon.html"><i class="fa fa-circle"></i>Whether Icon</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="cloud"></i><span>Buttons</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="buttons.html"><i class="fa fa-circle"></i>Default Style</a></li>
                        <li><a href="buttons-flat.html"><i class="fa fa-circle"></i>Flat Style</a></li>
                        <li><a href="buttons-edge.html"><i class="fa fa-circle"></i>Edge Style</a></li>
                        <li><a href="raised-button.html"><i class="fa fa-circle"></i>Raised Style</a></li>
                        <li><a href="button-group.html"><i class="fa fa-circle"></i>Button Group</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="file-text"></i><span>Forms</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Form Controls<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="form-validation.html"><i class="fa fa-circle"></i>Form Validation</a></li>
                            <li><a href="base-input.html"><i class="fa fa-circle"></i>Base Inputs</a></li>
                            <li><a href="radio-checkbox-control.html"><i class="fa fa-circle"></i>Checkbox & Radio</a></li>
                            <li><a href="input-group.html"><i class="fa fa-circle"></i>Input Groups</a></li>
                            <li><a href="megaoptions.html"><i class="fa fa-circle"></i>Mega Options</a></li>
                          </ul>
                        </li>
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Form Widgets<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="datepicker.html"><i class="fa fa-circle"></i>Datepicker</a></li>
                            <li><a href="time-picker.html"><i class="fa fa-circle"></i>Timepicker</a></li>
                            <li><a href="datetimepicker.html"><i class="fa fa-circle"></i>Datetimepicker</a></li>
                            <li><a href="daterangepicker.html"><i class="fa fa-circle"></i>Daterangepicker</a></li>
                            <li><a href="touchspin.html"><i class="fa fa-circle"></i>Touchspin</a></li>
                            <li><a href="select2.html"><i class="fa fa-circle"></i>Select2</a></li>
                            <li><a href="switch.html"><i class="fa fa-circle"></i>Switch</a></li>
                            <li><a href="typeahead.html"><i class="fa fa-circle"></i>Typeahead</a></li>
                            <li><a href="clipboard.html"><i class="fa fa-circle"></i>Clipboard</a></li>
                          </ul>
                        </li>
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Form Layout<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="default-form.html"><i class="fa fa-circle"></i>Default Forms</a></li>
                            <li><a href="form-wizard.html"><i class="fa fa-circle"></i>Form Wizard 1</a></li>
                            <li><a href="form-wizard-two.html"><i class="fa fa-circle"></i>Form Wizard 2</a></li>
                            <li><a href="form-wizard-three.html"><i class="fa fa-circle"></i>Form Wizard 3</a></li>
                            <li><a href="form-wizard-four.html"><i class="fa fa-circle"></i>Form Wizard 4</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="server"></i><span>Tables</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Bootstrap Tables<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="bootstrap-basic-table.html"><i class="fa fa-circle"></i>Basic Tables</a></li>
                            <li><a href="bootstrap-sizing-table.html"><i class="fa fa-circle"></i>Sizing Tables</a></li>
                            <li><a href="bootstrap-border-table.html"><i class="fa fa-circle"></i>Border Tables</a></li>
                            <li><a href="bootstrap-styling-table.html"><i class="fa fa-circle"></i>Styling Tables</a></li>
                            <li><a href="table-components.html"><i class="fa fa-circle"></i>Table components</a></li>
                          </ul>
                        </li>
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Data Tables<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="datatable-basic-init.html"><i class="fa fa-circle"></i>Basic Init</a></li>
                            <li><a href="datatable-advance.html"><i class="fa fa-circle"></i>Advance Init</a></li>
                            <li><a href="datatable-styling.html"><i class="fa fa-circle"></i>Styling</a></li>
                            <li><a href="datatable-AJAX.html"><i class="fa fa-circle"></i>AJAX</a></li>
                            <li><a href="datatable-server-side.html"><i class="fa fa-circle"></i>Server Side</a></li>
                            <li><a href="datatable-plugin.html"><i class="fa fa-circle"></i>Plug-in</a></li>
                            <li><a href="datatable-API.html"><i class="fa fa-circle"></i>API</a></li>
                            <li><a href="datatable-data-source.html"><i class="fa fa-circle"></i>Data Sources</a></li>
                          </ul>
                        </li>
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Extension Data Tables<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="datatable-ext-autofill.html"><i class="fa fa-circle"></i>Auto Fill</a></li>
                            <li><a href="datatable-ext-basic-button.html"><i class="fa fa-circle"></i>Basic Button</a></li>
                            <li><a href="datatable-ext-col-reorder.html"><i class="fa fa-circle"></i>Column Reorder</a></li>
                            <li><a href="datatable-ext-fixed-header.html"><i class="fa fa-circle"></i>Fixed Header</a></li>
                            <li><a href="datatable-ext-html-5-data-export.html"><i class="fa fa-circle"></i>HTML 5 Export</a></li>
                            <li><a href="datatable-ext-key-table.html"><i class="fa fa-circle"></i>Key Table</a></li>
                            <li><a href="datatable-ext-responsive.html"><i class="fa fa-circle"></i>Responsive</a></li>
                            <li><a href="datatable-ext-row-reorder.html"><i class="fa fa-circle"></i>Row Reorder</a></li>
                            <li><a href="datatable-ext-scroller.html"><i class="fa fa-circle"></i>Scroller</a></li>
                          </ul>
                        </li>
                        <li><a href="jsgrid-table.html"><i class="fa fa-circle"></i>Js Grid Table</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="book"></i><span>Cards</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="basic-card.html"><i class="fa fa-circle"></i>Basic Card</a></li>
                        <li><a href="creative-card.html"><i class="fa fa-circle"></i>Creative Card</a></li>
                        <li><a href="tabbed-card.html"><i class="fa fa-circle"></i>Tabbed Card</a></li>
                        <li><a href="dragable-card.html"><i class="fa fa-circle"></i>Draggable Card</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="sliders"></i><span>Timeline</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="timeline-v-1.html"><i class="fa fa-circle"></i>Timeline 1</a></li>
                        <li><a href="timeline-v-2.html"><i class="fa fa-circle"></i>Timeline 2</a></li>
                        <li><a href="timeline-small.html"><i class="fa fa-circle"></i>Timeline 3</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="bar-chart"></i><span>Charts</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="chart-google.html"><i class="fa fa-circle"></i>Google Chart</a></li>
                        <li><a href="chart-sparkline.html"><i class="fa fa-circle"></i>sparkline chart</a></li>
                        <li><a href="chart-flot.html"><i class="fa fa-circle"></i>Flot Chart</a></li>
                        <li><a href="chart-radial.html"><i class="fa fa-circle"></i>Radial Chart</a></li>
                        <li><a href="chart-knob.html"><i class="fa fa-circle"></i>Knob Chart</a></li>
                        <li><a href="chart-morris.html"><i class="fa fa-circle"></i>Morris Chart</a></li>
                        <li><a href="chartjs.html"><i class="fa fa-circle"></i>chatjs Chart</a></li>
                        <li><a href="chartist.html"><i class="fa fa-circle"></i>chartist Chart</a></li>
                        <li><a href="chart-peity.html"><i class="fa fa-circle"></i>Peity Chart</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="map"></i><span>Maps</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="map-js.html"><i class="fa fa-circle"></i>Maps JS</a></li>
                        <li><a href="vector-map.html"><i class="fa fa-circle"></i>Vector Maps</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="printer"></i><span>Email Templates</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Basic<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="basic-template.html" target="_blank"><i class="fa fa-circle"></i>Basic Email</a></li>
                            <li><a href="email-header.html" target="_blank"><i class="fa fa-circle"></i>Basic With Header</a></li>
                          </ul>
                        </li>
                      </ul>
                      <ul class="sidebar-submenu">
                        <li><a href="index.html#"><i class="fa fa-circle"></i>Ecommerce<i class="fa fa-angle-down pull-right"></i></a>
                          <ul class="sidebar-submenu">
                            <li><a href="template-email.html" target="_blank"><i class="fa fa-circle"></i>Email Template</a></li>
                            <li><a href="template-email-2.html" target="_blank"><i class="fa fa-circle"></i>Email Template 2</a></li>
                            <li><a href="ecommerce-templates.html" target="_blank"><i class="fa fa-circle"></i>Ecommerce Email</a></li>
                            <li><a href="email-order-success.html" target="_blank"><i class="fa fa-circle"></i>Order Success </a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="git-pull-request"></i><span>Editors</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="summernote.html"><i class="fa fa-circle"></i>Summer Note</a></li>
                        <li><a href="ckeditor.html"><i class="fa fa-circle"></i>CK editor</a></li>
                        <li><a href="simple-MDE.html"><i class="fa fa-circle"></i>MDE editor</a></li>
                        <li><a href="ace-code-editor.html"><i class="fa fa-circle"></i>ACE code editor</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="users"></i><span>Users</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="user-profile.html"><i class="fa fa-circle"></i>Users Profile</a></li>
                        <li><a href="edit-profile.html"><i class="fa fa-circle"></i>Users Edit</a></li>
                        <li><a href="user-cards.html"><i class="fa fa-circle"></i>Users Cards</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="calendar"></i><span>Calender</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="calendar.html"><i class="fa fa-circle"></i>Full Calender Basic</a></li>
                        <li><a href="calendar-event.html"><i class="fa fa-circle"></i>Full Calender Events</a></li>
                        <li><a href="calendar-advanced.html"><i class="fa fa-circle"></i>Full Calender Advance</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="internationalization.html"><i data-feather="aperture"></i><span>Internationalization</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="image"></i><span>Gallery</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="gallery.html"><i class="fa fa-circle"></i>Gallery Grid</a></li>
                        <li><a href="gallery-with-description.html"><i class="fa fa-circle"></i>Gallery Grid with Desc</a></li>
                        <li><a href="gallery-masonry.html"><i class="fa fa-circle"></i>Masonry Gallery</a></li>
                        <li><a href="masonry-gallery-with-disc.html"><i class="fa fa-circle"></i>Masonry Gallery Desc</a></li>
                        <li><a href="gallery-hover.html"><i class="fa fa-circle"></i>Hover Effects</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="mail"></i><span>Email</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="email-application.html"><i class="fa fa-circle"></i>Email App</a></li>
                        <li><a href="email-compose.html"><i class="fa fa-circle"></i>Email Compose</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="edit"></i><span> Blog</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="blog.html"><i class="fa fa-circle"></i>Blog Details</a></li>
                        <li><a href="blog-single.html"><i class="fa fa-circle"></i>Blog Single</a></li>
                        <li><a href="add-post.html"><i class="fa fa-circle"></i>Add Post</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="message-square"></i><span>Chat</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="chat.html"><i class="fa fa-circle"></i>Chat App</a></li>
                        <li><a href="chat-video.html"><i class="fa fa-circle"></i>Video chat</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="social-app.html"><i data-feather="chrome"></i><span>Social App</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="user-check"></i><span>Job Search</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="job-cards-view.html"><i class="fa fa-circle"></i>Cards view</a></li>
                        <li><a href="job-list-view.html"><i class="fa fa-circle"></i>List View</a></li>
                        <li><a href="job-details.html"><i class="fa fa-circle"></i>Job Details</a></li>
                        <li><a href="job-apply.html"><i class="fa fa-circle"></i>Apply</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="layers"></i><span>Learning</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="learning-list-view.html"><i class="fa fa-circle"></i>Learning List</a></li>
                        <li><a href="learning-detailed.html"><i class="fa fa-circle"></i>Detailed Course </a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="faq.html"><i data-feather="help-circle"></i><span>FAQ</span></a></li>
                    <li><a class="sidebar-header" href="knowledgebase.html"><i data-feather="database"></i><span>Knowledgebase</span></a></li>
                    <li><a class="sidebar-header" href="support-ticket.html"><i data-feather="headphones"></i><span>Support Ticket</span></a></li>
                    <li><a class="sidebar-header" href="to-do.html"><i data-feather="mic"></i><span>To-Do</span></a></li>
                    <li><a class="sidebar-header" href="landing-page.html" target="_blank"><i data-feather="navigation-2"></i><span>Landing page</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="shopping-bag"></i><span>Ecommerce</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="product.html"><i class="fa fa-circle"></i>Product</a></li>
                        <li><a href="product-page.html"><i class="fa fa-circle"></i>Product page</a></li>
                        <li><a href="list-products.html"><i class="fa fa-circle"></i>Product list</a></li>
                        <li><a href="payment-details.html"><i class="fa fa-circle"></i>Payment Details</a></li>
                        <li><a href="order-history.html"><i class="fa fa-circle"></i>Order History</a></li>
                        <li><a href="invoice-template.html"><i class="fa fa-circle"></i>Invoice</a></li>
                        <li><a href="cart.html"><i class="fa fa-circle"></i>Cart</a></li>
                        <li><a href="list-wish.html"><i class="fa fa-circle"></i>Wishlist</a></li>
                        <li><a href="checkout.html"><i class="fa fa-circle"></i>Checkout</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="pricing.html"><i data-feather="dollar-sign"></i><span> Pricing</span></a></li>
                    <li><a class="sidebar-header" href="sample-page.html"><i data-feather="file"></i><span> Sample page</span></a></li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="search"></i><span>Search Pages</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="search.html"><i class="fa fa-circle"></i>Search Website</a></li>
                        <li><a href="search-images.html"><i class="fa fa-circle"></i>Search Images</a></li>
                        <li><a href="search-video.html"><i class="fa fa-circle"></i>Search Video</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="alert-octagon"></i><span> Error Page</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="error-400.html" target="_blank"><i class="fa fa-circle"></i>Error 400</a></li>
                        <li><a href="error-401.html" target="_blank"><i class="fa fa-circle"></i>Error 401</a></li>
                        <li><a href="error-403.html" target="_blank"><i class="fa fa-circle"></i>Error 403</a></li>
                        <li><a href="error-404.html" target="_blank"><i class="fa fa-circle"></i>Error 404</a></li>
                        <li><a href="error-500.html" target="_blank"><i class="fa fa-circle"></i>Error 500</a></li>
                        <li><a href="error-503.html" target="_blank"><i class="fa fa-circle"></i>Error 503</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="unlock"></i><span> Authentication</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="login.html" target="_blank"><i class="fa fa-circle"></i>Login Simple</a></li>
                        <li><a href="login-image.html" target="_blank"><i class="fa fa-circle"></i>Login with Bg Image</a></li>
                        <li><a href="login-video.html" target="_blank"><i class="fa fa-circle"></i>Login with Bg video</a></li>
                        <li><a href="signup.html" target="_blank"><i class="fa fa-circle"></i>Register Simple</a></li>
                        <li><a href="signup-image.html" target="_blank"><i class="fa fa-circle"></i>Register with Bg Image</a></li>
                        <li><a href="signup-video.html" target="_blank"><i class="fa fa-circle"></i>Register with Bg video</a></li>
                        <li><a href="unlock.html" target="_blank"><i class="fa fa-circle"></i>Unlock User</a></li>
                        <li><a href="forget-password.html" target="_blank"><i class="fa fa-circle"></i>Forget Password</a></li>
                        <li><a href="reset-password.html" target="_blank"><i class="fa fa-circle"></i>Reset Password</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="index.html#"><i data-feather="briefcase"></i><span>Coming Soon</span><i class="fa fa-angle-right pull-right"></i></a>
                      <ul class="sidebar-submenu">
                        <li><a href="comingsoon.html" target="_blank"><i class="fa fa-circle"></i>Coming Simple</a></li>
                        <li><a href="comingsoon-bg-video.html" target="_blank"><i class="fa fa-circle"></i>Coming with Bg video</a></li>
                        <li><a href="comingsoon-bg-img.html" target="_blank"><i class="fa fa-circle"></i>Coming with Bg Image</a></li>
                      </ul>
                    </li>
                    <li><a class="sidebar-header" href="maintenance.html" target="_blank"><i data-feather="settings"></i><span> Maintenance</span></a></li>--}}
                </ul>
            </div>
        </div>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <div class="right-sidebar" id="right_side_bar">
            <div class="container p-0">
                <div class="modal-header p-l-20 p-r-20">
                    <div class="col-sm-8 p-0">
                        <h6 class="modal-title font-weight-bold">FRIEND LIST</h6>
                    </div>
                    <div class="col-sm-4 text-right p-0"><i class="mr-2" data-feather="settings"></i></div>
                </div>
            </div>
            <div class="friend-list-search mt-0">
                <input type="text" placeholder="search friend"><i class="fa fa-search"></i>
            </div>
            <div class="chat-box">
                <div class="people-list friend-list">
                    <ul class="list">
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/1.jpg')}}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Vincent Porter</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/2.png')}}" alt="">
                            <div class="status-circle away"></div>
                            <div class="about">
                                <div class="name">Ain Chavez</div>
                                <div class="status"> 28 minutes ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/8.jpg')}}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Kori Thomas</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/4.jpg')}}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Erica Hughes</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/5.jpg')}}" alt="">
                            <div class="status-circle offline"></div>
                            <div class="about">
                                <div class="name">Ginger Johnston</div>
                                <div class="status"> 2 minutes ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/6.jpg')}}" alt="">
                            <div class="status-circle away"></div>
                            <div class="about">
                                <div class="name">Prasanth Anand</div>
                                <div class="status"> 2 hour ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image"
                                                  src="{{asset('cp/endless/assets/images/user/7.jpg')}}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Hileri Jecno</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Right sidebar Ends-->


    <?php /*if(session()->has('insert_message')): */?><!--

          <div class="alert alert-success dark">
              <?php /*echo e(session()->get('insert_message')); */?>

            </div>

--><?php /*endif; */?>




        @yield('content')


        +
        <!-- footer start-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="col-md-12 footer-copyright" style="text-align: center">
                    <a href="https://amrk.my-staff.net/" target="_blank">
                        <b class="mb-0" style="color: #C32F45;"> جميع حقوق النشر محفوظة - أمرك 2021
                        </b>
                    </a>

                </div>
                {{--                <div class="row">--}}

                {{--                    <a class="pull-right mb-0" href="http://2grand.net" target="_blank" style="margin: auto">--}}
                {{--                        <img src="{{asset('grand.png')}}" style="width:100px;">--}}
                {{--                    </a>--}}
                {{--                    --}}{{--<div class="col-md-6 footer-copyright">--}}
                {{--                      <p class="mb-0">Copyright 2018 © Endless All rights reserved.</p>--}}
                {{--                    </div>--}}
                {{--                    <div class="col-md-6">--}}
                {{--                      <p class="pull-right mb-0">Hand crafted & made with<i class="fa fa-heart"></i></p>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </footer>
    </div>
</div>
<!-- latest jquery-->
<script src="{{asset('cp/endless/assets/js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('cp/endless/assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('cp/endless/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
{{--<script src="{{asset('cp/endless/assets/js/chart/chartist/chartist.js')}}"></script>--}}
<script src="{{asset('cp/endless/assets/js/chart/knob/knob.min.js')}}"></script>
{{--<script src="{{asset('cp/endless/assets/js/chart/knob/knob-chart.js')}}"></script>--}}
<script src="{{asset('cp/endless/assets/js/prism/prism.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>

<script src="{{asset('cp/endless/assets/js/notify/bootstrap-notify.min.js')}}"></script>
{{--<script src="{{asset('cp/endless/assets/js/dashboard/default.js')}}"></script>--}}
<script src="{{asset('cp/endless/assets/js/notify/index.js')}}"></script>
<!--    <script src="../assets/js/typeahead/handlebars.js"></script>-->
<!--    <script src="../assets/js/typeahead/typeahead.bundle.js"></script>-->
<!--    <script src="../assets/js/typeahead/typeahead.custom.js"></script>-->
<script src="{{asset('cp/endless/assets/js/chat-menu.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/height-equal.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/tooltip-init.js')}}"></script>
<!--    <script src="../assets/js/typeahead-search/handlebars.js"></script>-->
<!--    <script src="../assets/js/typeahead-search/typeahead-custom.js"></script>-->
<!-- Plugins JS Ends-->
<!-- Theme js-->

<script src="{{asset('cp/endless/assets/js/script.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/theme-customizer/customizer.js')}}"></script>
<!-- Plugin used-->

<!-- photos-->
<script src="{{asset('cp/endless/assets/js/photoswipe/photoswipe.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/photoswipe/photoswipe-ui-default.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/photoswipe/photoswipe.js')}}"></script>
<!-- photos-->

<!-- map-->

{{--<script src="{{asset('cp/endless/assets/js/map-js/mapsjs-core.js')}}"></script>--}}
{{--<script src="{{asset('cp/endless/assets/js/map-js/mapsjs-service.js')}}"></script>--}}
{{--<script src="{{asset('cp/endless/assets/js/map-js/mapsjs-ui.js')}}"></script>--}}
{{--<script src="{{asset('cp/endless/assets/js/map-js/mapsjs-mapevents.js')}}"></script>--}}
{{--<script src="{{asset('cp/endless/assets/js/map-js/custom.js')}}"></script><!-- map-->--}}

</body>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">google.load('visualization', '1.0', {'packages': ['corechart']});</script>


<script>
    $(document).ready(function () {

        function drawChart() {
            /*users charts*/
            @if(isset($orders_charts))
            if ($("#_orders_charts").length > 0) {
                var data = google.visualization.arrayToDataTable([
                    ["Element", " Orders", {role: "style"}],
                        @if(isset($orders_charts))
                        @foreach($orders_charts as $test)
                    [" {{$test->date  }} ", {{ $test->count }} , "#C32F45"],
                    @endforeach
                    @endif
                ]);
                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {
                        calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"
                    }, 2]);
                var options = {
                    width: '20%',
                    height: 300,
                    bar: {groupWidth: "10%"},
                    legend: {position: "none"},
                };
                var chart = new google.visualization.ColumnChart(document.getElementById("_orders_charts"));
                chart.draw(view, options);
            }
            @endif

        }

        google.charts.load("current", {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

    });

</script>
<script>
    $(document).ready(function () {
        $("#demo-input").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

@yield('mapLocation')
<script src="https://js.pusher.com/beams/1.0/push-notifications-cdn.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="/js/app.js"></script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
    // var wcppGetPrintersTimeout_ms = 60000; //60 sec
    // var wcppGetPrintersTimeoutStep_ms = 500; //0.5 sec
    //
    // function wcpGetPrintersOnSuccess(){
    //     // Display client installed printers
    //     if(arguments[0].length > 0){
    //         var p=arguments[0].split("|");
    //         var options = '';
    //         for (var i = 0; i < p.length; i++) {
    //             options += '<option>' + p[i] + '</option>';
    //         }
    //         $('#installedPrinters').css('visibility','visible');
    //         $('#installedPrinterName').html(options);
    //         $('#installedPrinterName').focus();
    //         $('#loadPrinters').hide();
    //     }else{
    //         alert("No printers are installed in your system.");
    //     }
    // }
    //
    // function wcpGetPrintersOnFailure() {
    //     // Do something if printers cannot be got from the client
    //     alert("No printers are installed in your system.");
    // }
</script>


{!!

// Register the WebClientPrint script code
// The $wcpScript was generated by PrintESCPOSController@index

$wcpScript;

!!}

<script>
    @if(Auth::guard('shop')->user()->notification_flag==1)
    const beamsClient = new PusherPushNotifications.Client({
        instanceId: '4b9224fd-ac88-4e17-86bf-65c347bc0fbd',
    });

    beamsClient.start()
        .then(() => beamsClient.addDeviceInterest('shops'))
        .then(() => console.log('Successfully registered and subscribed!'))
        .catch(console.error);

    const beamsTokenProvider = new PusherPushNotifications.TokenProvider({
        url: '{{route('shops.generate-auth-token')}}',
    });
    // console.log(beamsTokenProvider.token());
    beamsClient
        .start()
        .then(() => beamsClient.setUserId('shops-{{Auth::guard('shop')->user()->jwt}}', beamsTokenProvider))
        .catch(console.error);

    Pusher.logToConsole = true;

    var pusher = new Pusher('75b53060b1f415501d21', {
        cluster: 'eu'
    });

    var channel = pusher.subscribe('order-' + '{{Auth::guard('shop')->user()->jwt}}');
    channel.bind('App\\Events\\OrdersEvent', function (data) {
        var val = $('#notify-count').text();

        console.log(JSON.stringify(data));
        // console.log(JSON.stringify(data.order));
        // var dataJson = JSON.stringify(data);
        // console.log('order'+data.order);
        // console.log('index'+data.order.created_at);
        if(parseInt(val)+1 <5) {

        $('#notifications-ul').append('<a href="{{route('orders.index')}}">' +
            '                            <li>' +
            '                                <div class="media">' +
            '                                    <div class="media-body">' +
            '                                        <h6 class="mt-0"><span><i class="shopping-color" data-feather="shopping-bag">' +
            '</i></span>لديك طلب جديد !<small class="pull-right">'+data.order.created_at+'</small></h6>' +
            '                                        <p class="mb-0">أضغط هنا لتصفح الطلب</p>\n' +
            '                                    </div>' +
            '                                </div>' +
            '                            </li>'
            +
            '</a>'
        );
       }
        {{--else{--}}
        {{--    $('#notifications-ul').append( '<li>'+--}}
        {{--        '<a href="{{route('shops.notifications.page')}}">'+--}}
        {{--            'كل الاشعارات'+--}}
        {{--        '</a>'+--}}
        {{--    '</li>');--}}

        {{--}--}}
        $('#notify-count').text(parseInt(val) + 1)
        javascript:jsWebClientPrint.print('useDefaultPrinter=' + $('#useDefaultPrinter').attr('checked') + '&printerName=' + $('#installedPrinterName').val() +'&id='+data.order.id);
        // console.log(parseInt(val)+1);
    });
    @endif
    $('#turn-notification').on('change',function (){

        axios.get("{{route('shops.notification')}}")
            .then(function (response) {
                location.reload();

                // console.log(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    });

</script>
@yield('extra-js')

</html>

