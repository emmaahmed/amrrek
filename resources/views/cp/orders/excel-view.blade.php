<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            رقم الطلب
        </th>
        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            العميل
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            المندوب
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            القسم
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            موقع البدأ
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            موقع الانتهاء
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            وقت التوصيل
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            تكلفة الطلب
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            تكلفة التوصيل
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            كود الخصم المستخدم
        </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">
            تاريخ الطلب
        </th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">القبول:</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">في الطريق لاستلام الشحنة :</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">تم استلام الشحنة :</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">في الطريق لمكان المعالجة :</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';"> تمت المعالجة:</th>


        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">في الطريق لتسليم الشحن:</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">تم تسليم الشحنة:</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">إلغاء:</th>

    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->id}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->user?$order->user->name:''}}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->delegate?$order->delegate->name:''}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                @if($order->department_id == 2)
                    <b class="badge badge-danger">القسم: متاجر</b>
                @elseif($order->department_id == 3)
                    <b class="badge badge-danger">القسم: توصيل طلبات</b>
                @elseif($order->department_id == 5)
                    <b class="badge badge-danger"> القسم: النقل الثقيل</b>
                @endif
                    -{{$order->sub_department?$order->sub_department->name_ar:''}}
            </td>



            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$order->in_address}}{{$order->in_city_name}}
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$order->end_address}}{{$order->out_city_name}}
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->delivery_time}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->total_cost}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"> <span class="badge badge-danger">
           {{isset($order->offer->offer) ? $order->offer->offer : "-"}}
                                        </span>
                {{isset($order->country->currency) ? $order->country->currency : "-"}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->promo?$order->promo->code:''}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$order->created_at}}</td>
            <td style=" text-align: center;height: 80px; width: 80px;">{{$order->order_status->accept == null ? '-' : $order->order_status->accept}}</td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                        {{$order->order_status->on_first_way == null ? '-' : $order->order_status->on_first_way}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                        {{$order->order_status->received == null ? '-' : $order->order_status->received}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                       {{$order->order_status->on_way == null ? '-' : $order->order_status->on_way}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                      {{$order->order_status->processed == null ? '-' : $order->order_status->processed}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                      {{$order->order_status->on_last_way == null ? '-' : $order->order_status->on_last_way}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                      {{$order->order_status->finished == null ? '-' : $order->order_status->finished}}
            </td>
            <td style=" text-align: center;height: 80px; width: 80px;">                                                      {{$order->order_status->cancelled == null ? '---' : $order->order_status->cancelled}}
                <br>

                @if($order->order_status->cancel_by != null)
                    @if($order->order_status->cancel_by === 0)
                        <b class="badge badge-default">ملغي بواسطة العميل</b>
                    @elseif($order->order_status->cancel_by === 1)
                        <b class="badge badge-default">ملغي بواسطة المندوب</b>
                    @endif
                @endif

            </td>

        </tr>
    @endforeach
    </tbody>
</table>
</html>