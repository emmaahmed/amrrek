@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                الاعدادات
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">عن التطبيق</li>
                            </ol>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                {{--<th scope="col">#</th>--}}
                                <th scope="col">العنوان</th>
                                <th scope="col">القيمة</th>
                                <th scope="col">الاختيارات</th>

                            </tr>

                            </thead>
                            <tbody>
                            @foreach($data as $d)
                                <tr>
                                    <th scope="col">{{$d->name_ar}}</th>
                                    <th>
                                        <b class="badge badge-default" style="font-size: large">
                                            @if($d->key_name=='lost_something')
                                            {{substr($d->val,0,50)}}
                                                @else
                                                {{$d->val}}
                                            @endif
                                        </b>
                                    </th>
                                    @if(auth()->user()->hasPermissionTo('تعديل الاعدادات'))

                                    <td>
                                            <button title="تعديل" type="button" class="btn btn-warning" data-toggle="modal"
                                                    data-target="#edit_setting{{$d->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>


                                    </td>
                                    @endif
                                </tr>
                                <div class="modal fade" id="edit_setting{{$d->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">تعديل عن التطبيق</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="form-horizontal" method="post"
                                                  action="{{route('edit_settings')}}" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <div class="modal-body">

                                                        <div class="form-group row ">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput"> {{$d->name_ar}}</label>
                                                            <div class="col-lg-12">
                                                                <input type="hidden" value="{{$d->id}}" name="id">
                                                                @if($d->key_name=='lost_something')
                                                                    <textarea class="form-control" name="val"
                                                                            placeholder=" "
                                                                              required>{{$d->val}}</textarea>

                                                                @else
                                                                    

                                                                <input class="form-control" name="val"
                                                                       value="{{$d->val}}" placeholder=" "
                                                                       required>
                                                                    @endif
                                                            </div>
                                                        </div>



                                                    {{--<div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                        <label class="col-lg-12 control-label text-lg-right" for="textinput">النص بالنجليزية</label>
                                                        <div class="col-lg-12">
                                                            <textarea class="form-control" name="body_en" rows="10" placeholder="النص بالنجليزية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">{{$c->body_en}}</textarea>
                                                        </div>
                                                    </div>
                                                    @include('cp.layouts.error', ['input' => 'en_name'])--}}


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                        إغلاق
                                                    </button>
                                                    <button class="btn btn-primary" type="submit">تعديل</button>
                                                </div>
                                                                                             </form>--}}
                                        </div>
                                    </div>
                                </div>

                            @endforeach


                            </tbody>


                        </table>

                    </div>


                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>

@endsection
