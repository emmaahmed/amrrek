   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البريد الالكتروني</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">العنوان</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الرسالة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">حالة الطلب </th>
    </tr>
    </thead>
    <tbody>
    @foreach($complains as $complain)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{isset($complain->user->id) ? $complain->user->name : '-'}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{isset($complain->user->id) ? $complain->user->email : '-'}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$complain->title}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$complain->description}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $complain->status==0?'نشطة':'منتهية' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
   </html>