@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')

                            <h3>
                                <i data-feather="home"></i>
                                الأقسام الفرعية
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الاقسام</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            @if(auth()->user()->hasPermissionTo('اضافة قسم'))
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#subCat"><i class="icon-plus"></i>
                                إضافة قسم
                            </button>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">النوع</th>
                                <th scope="col">الإسم بالعربية</th>
                                <th scope="col">الإسم بالإنجليزية</th>
                                <th scope="col"> سيارات الخدمة</th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sub_departments as $key=>$c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})" class="{{$c->active == 0 ? 'table-danger' :''}}">
                                    <td>{{$key+1}}</td>
                                    <td>
                                        @if($c->department_id == 3)
                                            <span style="color: #C32F45;">طلبات أي خدمة</span>
                                        @elseif($c->department_id == 5)
                                            <span style="color: #C32F45;">طلبات نقل ثقيل</span>
                                        @endif
                                    </td>
                                    <td>{{$c->name_ar}}</td>
                                    <td>{{$c->name_en}}</td>
                                    <td>
                                        @if(isset($c['cars']) && count($c['cars'])>0)
                                        @foreach( $c['cars'] as $car) {{$car->name}} <br>
                                        @endforeach
                                            @endif
                                    </td>
{{--                                    @if($c->image != NULL)--}}
{{--                                        <th><img src="{{$c->image}}"  width="50px" height="50px"></th>--}}
{{--                                    @else--}}
{{--                                        <th> - </th>--}}
{{--                                    @endif--}}
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تعديل قسم'))
                                        <button title="تعديل" type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{$c->id}}">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        @if($c->orders_count==0)
                                            <button title="حذف" type="button" class="btn btn-danger" data-toggle="modal"
                                                    style="margin: 1px"
                                                    data-target="#delete_{{$c->id}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            @endif

                                        @endif
                                        @if(auth()->user()->hasPermissionTo('تفعيل قسم'))
                                        @if($c->active == 1)
                                            <a href="{{route('editSubDepartmentStatus',$c->id)}}" >
                                                <button title="إلغاء تفعيل" class="btn btn-danger">
                                                    <i class="fa fa-minus-circle"></i>
                                                </button>
                                            </a>
                                        @else
                                            <a href="{{route('editSubDepartmentStatus',$c->id)}}"  >
                                                <button title="تفعيل" class="btn btn-success">
                                                    <i class="fa fa-plus-circle"></i>
                                                </button>
                                            </a>
                                        @endif

                                        @endif
                                    </td>

                                    <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">تعديل قسم</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal needs-validation was-validated" method="post" action="{{route('editSubDepartment')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">


{{--                                                        <div class="form-group row {{ $errors->has('ar_name') ? ' has-error' : '' }}">--}}
{{--                                                            <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">الإسم بالعربية</label>--}}
{{--                                                            <div class="col-lg-12">--}}
{{--                                                                <input id="inputName1" name="name_ar" type="text" placeholder="الإسم بالعربية" class="form-control btn-square" for="textinput" value="{{$c->name_ar}}" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">--}}
{{--                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        @include('cp.layouts.error', ['input' => 'ar_name'])--}}

                                                        <div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالعربي</label>
                                                            <div class="col-lg-12">
                                                                <input name="name_ar" type="text" placeholder="الإسم بالعربي" class="form-control btn-square" for="textinput" value="{{$c->name_ar}}" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                                                            </div>
                                                        </div>
                                                        @include('cp.layouts.error', ['input' => 'en_name'])
                                                        <div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالانجليزي</label>
                                                            <div class="col-lg-12">
                                                                <input name="name_en" type="text" placeholder="الإسم بالانجليزي" class="form-control btn-square" for="textinput" value="{{$c->name_en}}" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                                                            </div>
                                                        </div>
                                                        @include('cp.layouts.error', ['input' => 'en_name'])

                                                        <div class="form-group row">
                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">النوع </label>
                                                            <div class="col-lg-12">
                                                                <select name="department_id" type="text" placeholder="النوع " class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                                                    @foreach($departments as $department)
                                                                        <option value="{{$department->id}}" {{$department->id == $c->department_id ? "selected" : ""}} >{{$department->name}} </option>
                                                                    @endforeach
                                                                </select>
                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="row">
                                                            <br>
                                                            <div class="col-md-4 mb-4">
                                                                <div class="" style="float: right;">
                                                                    <input class="checkbox_animated checkall"
                                                                           type="checkbox" id="all" >
                                                                    <label class="mb-0" for="all">الكل</label>

                                                                </div>
                                                            </div>
                                                            @if(count($carTypes)>0)
                                                            @foreach($carTypes as $type)
                                                                <div class="col-md-4 mb-4">
                                                                    <div class="" style="float: right;">
                                                                        <input class="checkbox_animated checkhour" id="{{$type->id}}" value="{{$type->id}}"
                                                                               type="checkbox" @if(in_array($type->id,$c['carTypesIds'])) checked @endif name="car_types[]">
                                                                        <label class="mb-0" for="{{$type->id}}">{{$type->name}}</label>

                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                                @endif
                                                        </div>

                                                        <br>
                                                        <br>

                                                        <div class="form-group row" style="text-align: right">
                                                            {{--                            <label class="col-lg-12 control-label text-lg-right" for="textinput">القسم الرئيسي </label>--}}
                                                            <div class="col-lg-12">
                                                                <input type="checkbox" name="has_processing" class="" {{$c->has_processing == 1 ? "checked" : ""}}>&nbsp;
                                                                <lable>يوجد موقع للمعالجة</lable>
                                                            </div>
                                                        </div>
{{--                                                        <div class="form-group ">--}}
{{--                                                            <label  class="col-lg-12 control-label text-lg-right" for="textinput">الصورة</label>--}}
{{--                                                            <input name="image" type="file" class="form-control btn-square" >--}}
{{--                                                        </div>--}}
{{--                                                        @if($c->image != NULL)--}}
{{--                                                            <div class="form-group ">--}}
{{--                                                                <img src="{{  $c->image }}" width="60px" height="60px">--}}
{{--                                                            </div>--}}
{{--                                                        --}}{{--@else--}}
{{--                                                            <div class="form-group ">--}}
{{--                                                                <span>لا توجد صورة</span>--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                                                        <button class="btn btn-primary" type="submit">تعديل</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1"
                                         style="text-align: right"
                                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف القسم الفرعي</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="post" action="{{route('sub_departments.destroy',$c->id)}}" class="buttons">
                                                    {{csrf_field()}}
                                                    @method('DELETE')
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف القسم الفرعي
                                                            <br>رقم القسم: ({{$c->id}})
                                                            <br>الإسم: ({{$c->name_ar}})

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$cats->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <div class="modal fade" id="subCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة قسم</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post" action="{{route('sub_departments.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالعربي</label>
                            <div class="col-lg-12">
                                <input name="name_ar" type="text" placeholder="الإسم بالعربي" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالانجليزي</label>
                            <div class="col-lg-12">
                                <input name="name_en" type="text" placeholder="الإسم بالانجليزي" class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput">القسم الرئيسي </label>
                            <div class="col-lg-12">
                                <select name="department_id" type="text" placeholder="النوع " class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    @foreach($departments as $department)
                                    <option value="{{$department->id}}" >{{$department->name}} </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>
<br>
<br>
                        <div class="row">
                            <br>
                            <div class="col-md-4 mb-4">
                                <div class="" style="float: right;">
                                    <input class="checkbox_animated checkall"
                                           type="checkbox" id="all" >
                                    <label class="mb-0" for="all">الكل</label>

                                </div>
                            </div>
                            @if(count($carTypes)>0)

                            @foreach($carTypes as $type)
                                <div class="col-md-4 mb-4">
                                    <div class="" style="float: right;">
                                        <input class="checkbox_animated checkhour" id="{{$type->id}}" value="{{$type->id}}"
                                               type="checkbox" name="car_types[]">
                                        <label class="mb-0" for="{{$type->id}}">{{$type->name}}</label>

                                    </div>
                                </div>

                            @endforeach
                                @endif
                        </div>

                        <br>
<br>
                        <div class="form-group row" style="text-align: right">
{{--                            <label class="col-lg-12 control-label text-lg-right" for="textinput">القسم الرئيسي </label>--}}
                            <div class="col-lg-12">
                                <input type="checkbox" name="has_processing" class="">&nbsp;
                                <lable>يوجد موقع للمعالجة</lable>
                            </div>
                        </div>

{{--                        <div class="form-group ">--}}
{{--                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الإسم بالإنجليزية</label>--}}
{{--                            <div class="col-lg-12" >--}}
{{--                                <input name="en_name" type="text" placeholder="الإسم بالإنجليزية" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">--}}
{{--                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group " >--}}
{{--                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة</label>--}}
{{--                            <input name="image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">--}}
{{--                            <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>--}}
{{--                        </div>--}}

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('extra-js')
    <script>
        var clicked = false;
        $(".checkall").on("click", function() {
            $(".checkhour").prop("checked", !clicked);
            clicked = !clicked;
            this.innerHTML = clicked ? 'Deselect' : 'Select';
        });

    </script>
@endsection