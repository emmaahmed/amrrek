<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('cp/endless/assets/images/logo.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('cp/endless/assets/images/logo.png')}}" type="image/x-icon">
    <title>Amrk - امرك</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('cp/endless/assets/css/light-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/responsive.css')}}">
    <style>
        {{--body {--}}
        {{--    /* The image used */--}}
        {{--    background-image: url({{asset('background.jpg')}});--}}
        {{--    /* Center and scale the image nicely */--}}
        {{--    background-position: center;--}}
        {{--    background-repeat: no-repeat;--}}
        {{--    background-size: cover;--}}
        {{--}--}}
    </style>
</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper" >
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper"  style="margin: 20px">
    <div class="auth-bg" >
        <div class="authentication-box" >
            <div class="text-center"><img src="{{asset('default.png')}}" height="100px" width="120px" alt=""></div>
            <div class="card mt-4">
                <div class="card-body">
                    <div class="text-center">
                        <h4>تسجيل حساب متجر جديد مع تطبيق أمرك</h4>
                        {{--                        <h6>Enter your Email and Password </h6>--}}
                    </div>
                    <div class="text-center" style="color: red;"><strong>{{Session::get('error')}}</strong></div>
                    <form action="{{route('registerNewShop')}}" method="post" class="theme-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-form-label pt-0">الإسم التجاري للمتجر</label>
                            <input class="form-control" type="text" required="" name="name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">الرقم التجاري التجاري للمتجر</label>
                            <input class="form-control" type="number" required="" name="comercial_number">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">وصف للمتجر (عربي)</label>
                            <input class="form-control" type="text" required="" name="description_ar">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">وصف للمتجر (إنجليزي)</label>
                            <input class="form-control" type="text" required="" name="description_en">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة المتجر</label>
                            <input class="form-control" type="file" name="image" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة الخلفية للمتجر</label>
                            <input class="form-control" type="file" name="cover_image" required>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم السجل التجاري للمتجر</label>
                            <input class="form-control" type="text" required="" name="commercial_register">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">البريد الإلكتروني</label>
                            <input class="form-control" type="email" required name="email"
                                   placeholder="examole@example.com"
                                    {{--                                   oninvalid="this.setCustomValidity('example@example.com')"--}}
                            >
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">الهاتف</label>
                            <input class="form-control" type="number" required name="phone">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">التصنيف</label>
                            <select class="form-control btn-square" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_ar}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم البطاقة الضريبية للمتجر(إن وجد)</label>
                            <input class="form-control" type="text" name="tax_num">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">صورة البطاقة الضريبية للمتجر(إن وجد)</label>
                            <input class="form-control" type="file" name="tax_img">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">المدينة</label>
                            <input class="form-control" type="text" required="" name="city">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">إسم المسؤل</label>
                            <input class="form-control" type="text" required="" name="moderator_name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">البريد الإلكتروني للمسؤل</label>
                            <input class="form-control" type="email" required name="moderator_email"
                                   placeholder="examole@example.com"
                                    {{--                                   oninvalid="this.setCustomValidity('example@example.com')"--}}
                            >
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">هاتف المسؤل</label>
                            <input class="form-control" type="number" required name="moderator_phone">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">كلمة المرور</label>
                            <input class="form-control" type="password" required name="password">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">منصب المسؤل</label>
                            <input class="form-control" type="text" required="" name="moderator_title">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">إسم البنك</label>
                            <input class="form-control" type="text" required="" name="bank_name">
                        </div>
                        <div class="form-group">
                            <label class="col-form-label pt-0">رقم الحساب</label>
                            <input class="form-control" type="text" required="" name="bank_account">
                        </div>

                        <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lat" value="" id="userlat" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lng" value="" id="userlng" placeholder="Enter long">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete">البحث عن عنوان</label>
                                        <input type="text" name="address" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <input class="" type="checkbox" required="" name="terms">
                            <label class="col-form-label pt-0">أوافق علي </label>
                            <a target="_blank" style="color: #C32F45;" href="{{asset('shop-terms')}}">الشروط و الأحكام</a>
                        </div>
                        {{--                        <div class="mg-t-60 tx-center">--}}
                        {{--                            <a target="_blank" href="http://2grand.net/">--}}
                        {{--                                <img src="{{asset('grand.png')}}" style="max-width: 20% !important;display: table;margin: 0 auto;">--}}
                        {{--                            </a>--}}
                        {{--                        </div>--}}
                        <div class="form-group form-row mt-3 mb-0">
                            <button class="btn btn-danger btn-block" type="submit">تسجيل</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- latest jquery-->
<script src="{{asset('cp/endless/assets/js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('cp/endless/assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('cp/endless/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('cp/endless/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('cp/endless/assets/js/script.js')}}"></script>
<!-- Plugin used-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).on('change', '#status', function (e) {
        alert('ww');
        var model_id = $(this).attr('model_id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{{URL::route('editShopStatus')}}",
            data: {
                model_id: model_id,
                "_token": "{{ csrf_token() }}"
            },
            success: function (response) {
                location.reload();
                if (response.success) {
                    toastr.success(response.success);
                } else if (response.warning) {
                    toastr.warning(response.warning);
                } else {
                    toastr.error(response.error);
                }
            },
            error: function (jqXHR) {
                toastr.error(jqXHR.responseJSON.message);
            }
        });
    });

</script>

<script>
    var marker = null;
    var placeSearch, autocomplete;
    function initMap() {
        autocomplete =
            new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                {types: ['geocode']});
        var map = new google.maps.Map(document.getElementById('userlocation'), {
            zoom: 12,
            center: {lat: 26.719517, lng: 29.2161655 }
        });
        var MaekerPos = new google.maps.LatLng(0 , 0);
        marker = new google.maps.Marker({
            position: MaekerPos,
            map: map
        });
        autocomplete.addListener('place_changed', function(){
            placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
            document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
            document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
        });
        map.addListener('click', function(e) {
            placeMarkerAndPanTo(e.latLng, map);
            document.getElementById("userlat").value=e.latLng.lat();
            document.getElementById("userlng").value=e.latLng.lng();
        });
    }
    function placeMarkerAndPanTo(latLng, map) {
        map.setZoom(15);
        marker.setPosition(latLng);
        map.panTo(latLng);
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHQj-nrlglAjt4sQgpRNcOdbRGNm7eGMc&libraries=places&callback=initMap">
</script>




</body>
</html>
