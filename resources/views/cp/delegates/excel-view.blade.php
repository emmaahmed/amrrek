   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة المندوب</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأول</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأخير</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البريد الإلكتروني</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">رقم الهاتف</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المنطقة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البلد</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">عدد الطلبات</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">نوع السيارة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة السيارة الأمامية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة السيارة الخلفية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة التأمين</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة الرخصة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة القيادة المدنية</th>
    </tr>
    </thead>
    <tbody>
    @foreach($delegates as $delegate)
        <tr>
            <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{is_file(public_path('/uploads/delegates/images/'.$delegate->getAttributes()['image']))?public_path('/uploads/delegates/images/'.$delegate->getAttributes()['image']):public_path('/uploads/no-image.png')}}" width="80px" height="80px" ></td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->f_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->l_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->email }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->phone }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->city }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->country?$delegate->country->name_ar:'' }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->orders_count }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $delegate->car_type?$delegate->car_type->name_ar:''}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $delegate->delegate_documents?$delegate->delegate_documents->front_car_image:'' }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $delegate->delegate_documents?$delegate->delegate_documents->back_car_image:'' }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $delegate->delegate_documents?$delegate->delegate_documents->insurance_image:'' }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $delegate->delegate_documents?$delegate->delegate_documents->license_image:'' }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $delegate->delegate_documents?$delegate->delegate_documents->civil_image:'' }}">أضغط هنا</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
   </html>