@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                {{__('main-admin.about-app')}}
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">عن التطبيق</li>
                            </ol>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                {{--<th scope="col">#</th>--}}
                                <th scope="col"> {{__('main-admin.type')}}</th>
                                <th scope="col">{{__('main-admin.text-in-arabic')}}</th>
                                <th scope="col">{{__('main-admin.text-in-english')}}</th>
                                <th scope="col">{{__('main-admin.options')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($abouts as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})">
                                    {{--<td>{{$c->id}}</td>--}}
                                    <td width="10px">
                                            <span class="badge badge-default" style="color:#C32F45">
                                                @if($c->type == 0)
                                                    <span>{{__('main-admin.for-user')}}</span>
                                                @elseif($c->type == 1)
                                                    <span>{{__('main-admin.for-delegate')}}</span>
                                                @elseif($c->type == 2)
                                                    <span>{{__('main-admin.for-driver')}}</span>
                                                @elseif($c->type == 3)
                                                    <span>{{__('main-admin.for-heavy-delegate')}}</span>
                                                @elseif($c->type == 4)
                                                    <span>{{__('main-admin.for-shop')}}</span>
                                                @endif
                                            </span>
                                    </td>
                                    <td>
                                        <textarea class="form-control" name="body_ar" rows="15"
                                                  placeholder="{{__('main-admin.text-in-arabic')}}">{{$c->body_ar}}</textarea>
                                    </td>
                                    <td>
                                        <textarea class="form-control" name="body_en" rows="15"
                                                  placeholder="{{__('main-admin.text-in-english')}}" dir="ltr">{{$c->body_en}}</textarea>
                                    </td>


                                    <td width="10px">
                                        @if(auth()->user()->hasPermissionTo('تعديل عن التطبيق'))
                                            <button title="{{__('main-admin.edit')}}" type="button" class="btn btn-warning"
                                                    data-toggle="modal" data-target="#edit_{{$c->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        @endif
                                    </td>

                                    <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{__('main-admin.edit-app')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal" method="post"
                                                      action="{{route('editAbout')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="term_id" value="{{$c->id}}">


                                                        <div class="form-group row {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput"> {{__('main-admin.text-in-arabic')}}</label>
                                                            <div class="col-lg-12">
                                                                <textarea class="form-control" name="body_ar" rows="10"
                                                                          placeholder="{{__('main-admin.text-in-arabic')}}" required
                                                                          oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')">{{$c->body_ar}}</textarea>
                                                            </div>
                                                        </div>
                                                        @include('cp.layouts.error', ['input' => 'ar_name'])

                                                        <div class="form-group row {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                            <label class="col-lg-12 control-label text-lg-right"
                                                                   for="textinput">{{__('main-admin.text-in-english')}}</label>
                                                            <div class="col-lg-12">
                                                                <textarea class="form-control" name="body_en" rows="10"
                                                                          placeholder="{{__('main-admin.text-in-english')}}" required
                                                                          oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')">{{$c->body_en}}</textarea>
                                                            </div>
                                                        </div>
                                                        @include('cp.layouts.error', ['input' => 'en_name'])


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button class="btn btn-primary" type="submit">{{__('main-admin.edit')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>


@endsection
