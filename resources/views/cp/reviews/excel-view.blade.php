   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">التقييم</th>
        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">التعليق</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المستخدم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المتجر</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الإختيارات</th>

    </tr>
    </thead>
    <tbody>
    @foreach($rates as $rate)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$rate->rate}}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$rate->comment}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$rate->user_name}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$rate->shop_name}} </td>

        </tr>
    @endforeach
    </tbody>
</table>
   </html>