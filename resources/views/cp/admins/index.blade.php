@extends('cp.index')
@section('content')
<style>
    .dataTables_info, .dataTables_paginate{
        display:block !important;
    }
</style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                المشرفين
                                <a href="{{route('admins.download')}}"
                                   class="btn btn-success"><span>تحميل تقارير المشرفين</span></a>

                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المستخدمين</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            @if(auth()->user()->hasPermissionTo('اضافة مشرف'))
                                <a href="{{route('admin.create')}}" class="btn btn-primary" >
                                    <i class="icon-plus"></i>
                                    إضافة مشرف
                                </a>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table display dataTable" id="basic-1">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col"> الإسم</th>
                                <th scope="col"> الصورة</th>
                                <th scope="col">الهاتف</th>
                                <th scope="col"> البريد الإلكتروني</th>
                                <th scope="col"> المنطقة</th>
                                <th scope="col"> الحالة</th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})"
                                    class="{{$c->active == 0 ? 'table-danger' :''}}">
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->name}}</td>
                                    @if($c->image)
                                        <td>
                                            <button title="عرض" type="button" class="btn btn-danger"
                                                    data-toggle="modal" data-target="#image{{$c->id}}"
                                                    style="padding: 1px">
                                                <img src="{{$c->image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif

                                    <td>{{$c->phone}}</td>
                                    <td>{{$c->email}}</td>
                                    <td>{{$c->city}}</td>
                                    {{--<td>{{$c->name_en}} / {{$c->name_ar}}</td>--}}
                                    <td>
                                        @if($c->active == 1)
                                            <i class="font-danger show icon-check"></i>
                                        @else
                                            <i class="font-danger show icon-close"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تفعيل مشرف'))
                                            @if($c->active == 1)
                                                <a href="{{route('editAdminStatus',$c->id)}}">
                                                    <button title="إيقاف " class="btn btn-danger">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </button>
                                                </a>
                                            @else
                                                <a href="{{route('editAdminStatus',$c->id)}}">
                                                    <button title="اعادة تشغيل " class="btn btn-success">
                                                        <i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </a>
                                            @endif
                                        @endif
                                        @if(auth()->user()->hasPermissionTo('تعديل مشرف'))
                                            <button title="تعديل" type="button" class="btn btn-warning"
                                                    data-toggle="modal" data-target="#edit_{{$c->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        @endif
                                        @if(auth()->user()->hasPermissionTo('حذف مشرف'))
                                           <button title="حذف" type="button" class="btn btn-danger"
                                                    data-toggle="modal" data-target="#delete_{{$c->id}}">
                                                <i class="fa fa-trash"></i>
                                            </button>

                                            <!--<a href="{{route('deleteAdmin',$c->id)}}" data-original-title="" title="">-->
                                            <!--    <button title="" class="btn btn-danger" data-original-title="حذف">-->
                                            <!--        <i class="fa fa-trash"></i>-->
                                            <!--    </button>-->
                                            <!--</a>-->
                                        @endif
                                    </td>

                                    <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">تعديل المشرف </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal" method="post"
                                                      action="{{route('editAdmin')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">


                                                        <div class="form-group ">
                                                            <label style="float: right">إسم المشرف</label>
                                                            <input id="name" type="text"
                                                                   class="form-control @error('name') is-invalid @enderror"
                                                                   name="name" required
                                                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"
                                                                   autocomplete="name"
                                                                   value="{{$c->name}}"
                                                                   autofocus placeholder="إسم المشرف">
                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userName" type="text" class="form-control" placeholder="إسم العميل">--}}
                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">البريد الإلكتروني</label>
                                                            <input id="email" type="email"
                                                                   class="form-control @error('email') is-invalid @enderror"
                                                                   name="email"  required
                                                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"
                                                                   autocomplete="email"
                                                                   value="{{$c->email}}"
                                                                   placeholder="البريد الإلكتروني">
                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userEmail" type="email" class="form-control" placeholder="البريد الإلكتروني">--}}
                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">الهاتف</label>
                                                            <input id="phone" type="text" class="form-control"
                                                                   name="phone"
                                                                   required
                                                                   value="{{$c->phone}}"
                                                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"
                                                                   autocomplete="phone" placeholder="الهاتف">

                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">كلمة المرور</label>
                                                            <input id="password" type="password"
                                                                   class="form-control @error('password') is-invalid @enderror"
                                                                   name="password"


                                                                   placeholder="كلمة المرور">
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userPassword" type="password" class="form-control" placeholder="كلمة المرور">--}}
                                                        </div>


                                                        <div class="form-group">
                                                            <label style="float: right">الصورة</label>
                                                            <input name="image" type="file" class="form-control">
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <hr>
                                                        <h3 class="" style="float: right;">الصلاحيات</h3>
                                                        <div class="clearfix"></div>
                                                        <div class="row">
                                                            <br>
                                                            <div class="col-md-4 mb-4">
                                                                <div class="" style="float: right;">
                                                                    <input class="checkbox_animated checkall"
                                                                           type="checkbox" id="all" >
                                                                    <label class="mb-0" for="all">الكل</label>

                                                                </div>
                                                            </div>

                                                        @foreach($permissions as $permission)
                                                                <div class="col-md-4 mb-4">
                                                                    <div class="" style="float: right;">
                                                                        <input class="checkbox_animated checkhour" id="{{$permission->id}}" value="{{$permission->name}}"
                                                                               @if($c->hasPermissionTo($permission->name)) checked @endif
                                                                               type="checkbox" name="permissions[]">
                                                                        <label class="mb-0" for="{{$permission->id}}">{{$permission->name}}</label>

                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button class="btn btn-primary" type="submit">تعديل</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1"
                                         style="text-align: right"
                                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف المشرف</h5>
                                                </div>
                                                <form method="get" action="{{route('deleteAdmin',$c->id)}}" class="buttons">
                                                    @csrf
                                                    <!--@method('delete')-->
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف المشرف
                                                            <br>رقم المشرف: ({{$c->id}})
                                                            <br>الإسم: ({{$c->name}})

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    <div class="modal fade" id="subCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة مشرف</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{ route('createAdmin') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">


                        <div class="form-group ">
                            <label style="float: right">إسم المشرف</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}" required
                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')" autocomplete="name"
                                   autofocus placeholder="إسم المشرف">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            {{--<input name="userName" type="text" class="form-control" placeholder="إسم العميل">--}}
                        </div>

                        <div class="form-group ">
                            <label style="float: right">البريد الإلكتروني</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required
                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')" autocomplete="email"
                                   placeholder="البريد الإلكتروني">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            {{--<input name="userEmail" type="email" class="form-control" placeholder="البريد الإلكتروني">--}}
                        </div>

                        <div class="form-group ">
                            <label style="float: right">الهاتف</label>
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                                   required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"
                                   autocomplete="phone" placeholder="الهاتف">

                        </div>

                        <div class="form-group ">
                            <label style="float: right">كلمة المرور</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"
                                   autocomplete="new-password" placeholder="كلمة المرور">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            {{--<input name="userPassword" type="password" class="form-control" placeholder="كلمة المرور">--}}
                        </div>

                        <div class="form-group">
                            <label style="float: right">الصورة</label>
                            <input name="image" type="file" class="form-control">
                        </div>

                        <div class="clearfix"></div>
                        <hr>
                        <h3 class="" style="float: right;">الصلاحيات</h3>
                        <div class="clearfix"></div>
                        <div class="row">
                            <br>
                            <div class="col-md-4 mb-4">
                                <div class="" style="float: right;">
                                    <input class="checkbox_animated checkall"
                                           type="checkbox" id="all" >
                                    <label class="mb-0" for="all">الكل</label>

                                </div>
                            </div>

                        @foreach($permissions as $perm)
                                <div class="col-md-4 mb-4">
                                    <div class="" style="float: right;">
                                        <input class="checkbox_animated checkhour" id="{{$perm->id}}" value="{{$perm->name}}"
                                               type="checkbox" name="permissions[]">
                                        <label class="mb-0" for="{{$perm->id}}">{{$perm->name}}</label>

                                    </div>
                                </div>

                            @endforeach
                        </div>
                        <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lng" value="30.1259003" id="userlat" placeholder="Enter lat ">
                            </div>
                            <div class="form-group">
                                <input  type="hidden" class="form-control"  name="lat" value="31.3728573" id="userlng" placeholder="Enter long">
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div id="userlocation" style="width:100%;height:350px"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="autocomplete">البحث عن عنوان</label>
                                        <input type="text" class="form-control" id="autocomplete" placeholder="البحث عن عنوان"  name="address" required >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="box-footer form-group">
                            {{--<button type="submit" class="btn btn-primary">حفظ</button>--}}
                            <button type="submit" class="btn btn-primary">
                            <!--{{ __('Register') }}-->
                                حفظ
                            </button>
                        </div>

                    </div>
                    <!-- /.box-body -->


                </form>
            </div>
        </div>
    </div>

@endsection
@section('extra-js')
    <script>
        var clicked = false;
        $(".checkall").on("click", function() {
            $(".checkhour").prop("checked", !clicked);
            clicked = !clicked;
            this.innerHTML = clicked ? 'Deselect' : 'Select';
        });

    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).on('change', '#status', function (e) {
            alert('ww');
            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editShopStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

    </script>

    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHQj-nrlglAjt4sQgpRNcOdbRGNm7eGMc&libraries=places&callback=initMap">
    </script>

@endsection