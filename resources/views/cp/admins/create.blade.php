@extends('cp.index')
@section('content')

    <div class="page-body" dir="rtl">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-right">


                        <?php if(session()->has('insert_message')): ?>
                        <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                            <i class="icon-thumb-up"></i>
                            <b>
                                <?php echo e(session()->get('insert_message')); ?>
                            </b>
                            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        @if($errors->any())
                            <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-down"></i>
                                <b>
                                    @if ($errors)
                                        {{__('main-admin.please-complete-info')}}

                                    @endif
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                        data-original-title="" title="">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif


                        <h3>
                            <i data-feather="home"></i>
                            {{__('main-admin.add-admin')}}

                        </h3>
                        {{--<ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">المتاجر</li>
                        </ol>--}}
                    </div>


                </div>
            </div>
        </div>
    </div>
        <form class="form-horizontal needs-validation was-validated" method="post"
              action="{{ route('createAdmin') }}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="modal-body">


                <div class="form-group ">
                    <label style="float: right">{{__('main-admin.admin-name')}}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                           name="name" value="{{ old('name') }}" required
                           oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')" autocomplete="name"
                           autofocus placeholder="{{__('main-admin.admin-name')}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    {{--<input name="userName" type="text" class="form-control" placeholder="إسم العميل">--}}
                </div>

                <div class="form-group ">
                    <label style="float: right">{{__('main-admin.email')}}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" required
                           oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')" autocomplete="email"
                           placeholder="{{__('main-admin.email')}}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    {{--<input name="userEmail" type="email" class="form-control" placeholder="البريد الإلكتروني">--}}
                </div>

                <div class="form-group ">
                    <label style="float: right">{{__('main-admin.phone')}}</label>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                           required oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')"
                           autocomplete="phone" placeholder="{{__('main-admin.phone')}}">

                </div>

                <div class="form-group ">
                    <label style="float: right">{{__('main-admin.password')}}</label>
                    <input id="password" type="password"
                           class="form-control @error('password') is-invalid @enderror" name="password" required
                           oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')"
                           autocomplete="new-password" placeholder="{{__('main-admin.password')}}                 ">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    {{--<input name="userPassword" type="password" class="form-control" placeholder="كلمة المرور">--}}
                </div>

                <div class="form-group">
                    <label style="float: right">{{__('main-admin.image')}}</label>
                    <input name="image" type="file" class="form-control">
                </div>

                <div class="clearfix"></div>
                <hr>
                <h3 class="" style="float: right;">{{__('main-admin.permissions')}}</h3>
                <div class="clearfix"></div>
                <div class="row">
                    <br>
                    <div class="col-md-4 mb-4">
                        <div class="" style="float: right;">
                            <input class="checkbox_animated checkall"
                                   type="checkbox" id="all" >
                            <label class="mb-0" for="all">{{__('main-admin.all')}}</label>

                        </div>
                    </div>

                    @foreach($permissions as $perm)
                        <div class="col-md-4 mb-4">
                            <div class="" style="float: right;">
                                <input class="checkbox_animated checkhour" id="{{$perm->id}}" value="{{$perm->name}}"
                                       type="checkbox" name="permissions[]">
                                <label class="mb-0" for="{{$perm->id}}">{{$perm->name}}</label>

                            </div>
                        </div>

                    @endforeach
                </div>
                <div class="pure-u-1 pure-u-md-1 pure-u-lg-1 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input  type="hidden" class="form-control"  name="lat" value="" id="userlat" placeholder="{{__('main-admin.enter-lat')}} ">
                    </div>
                    <div class="form-group">
                        <input  type="hidden" class="form-control"  name="lng" value="" id="userlng" placeholder="{{__('main-admin.enter-lng')}}">
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <div id="userlocation" style="width:100%;height:350px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="autocomplete">{{__('main-admin.search-for-address')}}</label>
                                <input type="text" name="address" class="form-control" id="autocomplete" placeholder="البحث عن عنوان" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="box-footer form-group">
                    {{--<button type="submit" class="btn btn-primary">حفظ</button>--}}
                    <button type="submit" class="btn btn-primary">
                    <!--{{ __('Register') }}-->
                        حفظ
                    </button>
                </div>

            </div>
            <!-- /.box-body -->


        </form>
        <!-- Container-fluid Ends-->
    </div>
@endsection

@section('extra-js')
    <script>
        var clicked = false;
        $(".checkall").on("click", function() {
            $(".checkhour").prop("checked", !clicked);
            clicked = !clicked;
            this.innerHTML = clicked ? 'Deselect' : 'Select';
        });

    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).on('change', '#status', function (e) {
            alert('ww');
            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editShopStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

    </script>

    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 12,
                center: {lat: 23.8859, lng: 45.0792 }
            });
            var MaekerPos = new google.maps.LatLng(23.8859,45.0792);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(15);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnXtbPyAEiGsv0YBnR5eLE53ssWy4kiWk&libraries=places&callback=initMap">
    </script>

@endsection