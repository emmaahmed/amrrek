@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                {{__('main-admin.admins-by-area')}}

                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المستخدمين</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            @if(auth()->user()->hasPermissionTo('اضافة مشرف'))
                                <a href="{{route('admin.create')}}" class="btn btn-primary" >
                                    <i class="icon-plus"></i>
                                    {{__('main-admin.add-admin')}}

                                </a>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form class="form-horizontal" style="padding-top: 50px;" action="{{route('admin.admins-by-area-search')}}" method="post">
                    @csrf

                    <!-- Form Name -->

                        <!-- Text input-->
                        <div class="form-group row">
                            <div class="col-lg-4 col-sm-12">
                                <select name="city"   class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="all">{{__('main-admin.all-areas')}}</option>
                                    @if(count($areas)>0)
                                        @foreach($areas as $area)
                                            <option value="{{$area->city}}">{{$area->city}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-lg-2 col-sm-6">
                                <button type="submit" class="btn btn-danger" >
                                    <i class="fa fa-search"></i>

                                </button>
                            </div>

                        </div>
                        <!-- Text input-->

                    </form>

                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">{{__('main-admin.id-number')}}</th>
                                <th scope="col">{{__('main-admin.name')}} </th>
                                <th scope="col">{{__('main-admin.image')}} </th>
                                <th scope="col">{{__('main-admin.phone')}}</th>
                                <th scope="col"> {{__('main-admin.email')}}</th>
                                <th scope="col">{{__('main-admin.area')}} </th>
                                <th scope="col">{{__('main-admin.status')}} </th>
                                <th scope="col">{{__('main-admin.options')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})"
                                    class="{{$c->active == 0 ? 'table-danger' :''}}">
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->name}}</td>
                                    @if($c->image)
                                        <td>
                                            <button title="{{__('main-admin.view')}}" type="button" class="btn btn-danger"
                                                    data-toggle="modal" data-target="#image{{$c->id}}"
                                                    style="padding: 1px">
                                                <img src="{{$c->image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif

                                    <td>{{$c->phone}}</td>
                                    <td>{{$c->email}}</td>
                                    <td>{{$c->city}}</td>
                                    {{--<td>{{$c->name_en}} / {{$c->name_ar}}</td>--}}
                                    <td>
                                        @if($c->active == 1)
                                            <i class="font-danger show icon-check"></i>
                                        @else
                                            <i class="font-danger show icon-close"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تفعيل مشرف'))
                                            @if($c->active == 1)
                                                <a href="{{route('editAdminStatus',$c->id)}}">
                                                    <button title="{{__('main-admin.stop')}} " class="btn btn-danger">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </button>
                                                </a>
                                            @else
                                                <a href="{{route('editAdminStatus',$c->id)}}">
                                                    <button title="{{__('main-admin.replay')}} " class="btn btn-success">
                                                        <i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </a>
                                            @endif
                                        @endif
                                        @if(auth()->user()->hasPermissionTo('تعديل مشرف'))
                                            <button title="{{__('main-admin.edit')}}" type="button" class="btn btn-warning"
                                                    data-toggle="modal" data-target="#edit_{{$c->id}}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        @endif
                                        @if(auth()->user()->hasPermissionTo('حذف مشرف'))
                                            <a href="{{route('deleteAdmin',$c->id)}}" data-original-title="" title="">
                                                <button title="" class="btn btn-danger" data-original-title="{{__('main-admin.delete')}}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </a>
                                        @endif
                                    </td>

                                    <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"> {{__('main-admin.edit-admin')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal" method="post"
                                                      action="{{route('editAdmin')}}" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">


                                                        <div class="form-group ">
                                                            <label style="float: right">{{__('main-admin.admin-name')}}</label>
                                                            <input id="name" type="text"
                                                                   class="form-control @error('name') is-invalid @enderror"
                                                                   name="name" required
                                                                   oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')"
                                                                   autocomplete="name"
                                                                   value="{{$c->name}}"
                                                                   autofocus placeholder="{{__('main-admin.admin-name')}}">
                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userName" type="text" class="form-control" placeholder="إسم العميل">--}}
                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">{{__('main-admin.email')}}</label>
                                                            <input id="email" type="email"
                                                                   class="form-control @error('email') is-invalid @enderror"
                                                                   name="email"  required
                                                                   oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')"
                                                                   autocomplete="email"
                                                                   value="{{$c->email}}"
                                                                   placeholder="{{__('main-admin.email')}}">
                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userEmail" type="email" class="form-control" placeholder="البريد الإلكتروني">--}}
                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">{{__('main-admin.phone')}}</label>
                                                            <input id="phone" type="text" class="form-control"
                                                                   name="phone"
                                                                   required
                                                                   value="{{$c->phone}}"
                                                                   oninvalid="this.setCustomValidity('{{__('main-admin.this-field-is-required')}}')"
                                                                   autocomplete="phone" placeholder="{{__('main-admin.phone')}}">

                                                        </div>

                                                        <div class="form-group ">
                                                            <label style="float: right">{{__('main-admin.password')}}</label>
                                                            <input id="password" type="password"
                                                                   class="form-control @error('password') is-invalid @enderror"
                                                                   name="password"


                                                                   placeholder="{{__('main-admin.password')}}">
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                            {{--<input name="userPassword" type="password" class="form-control" placeholder="كلمة المرور">--}}
                                                        </div>


                                                        <div class="form-group">
                                                            <label style="float: right">{{__('main-admin.image')}}</label>
                                                            <input name="image" type="file" class="form-control">
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <hr>
                                                        <h3 class="" style="float: right;">{{__('main-admin.permissions')}}</h3>
                                                        <div class="clearfix"></div>
                                                        <div class="row">
                                                            <br>
                                                            <div class="col-md-4 mb-4">
                                                                <div class="" style="float: right;">
                                                                    <input class="checkbox_animated checkall"
                                                                           type="checkbox" id="all" >
                                                                    <label class="mb-0" for="all">{{__('main-admin.all')}}</label>

                                                                </div>
                                                            </div>

                                                        @foreach($permissions as $permission)
                                                                <div class="col-md-4 mb-4">
                                                                    <div class="" style="float: right;">
                                                                        <input class="checkbox_animated checkhour" id="{{$permission->id}}" value="{{$permission->name}}"
                                                                               @if($c->hasPermissionTo($permission->name)) checked @endif
                                                                               type="checkbox" name="permissions[]">
                                                                        <label class="mb-0" for="{{$permission->id}}">{{$permission->name}}</label>

                                                                    </div>
                                                                </div>

                                                            @endforeach
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-dark" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button class="btn btn-primary" type="submit">{{__('main-admin.edit')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
@section('extra-js')
    <script>
        var clicked = false;
        $(".checkall").on("click", function() {
            $(".checkhour").prop("checked", !clicked);
            clicked = !clicked;
            this.innerHTML = clicked ? 'Deselect' : 'Select';
        });

    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).on('change', '#status', function (e) {
            alert('ww');
            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editShopStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

    </script>

    <script>
        var marker = null;
        var placeSearch, autocomplete;
        function initMap() {
            autocomplete =
                new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                    {types: ['geocode']});
            var map = new google.maps.Map(document.getElementById('userlocation'), {
                zoom: 7,
                center: {lat: 26.719517, lng: 29.2161655 }
            });
            var MaekerPos = new google.maps.LatLng(0 , 0);
            marker = new google.maps.Marker({
                position: MaekerPos,
                map: map
            });
            autocomplete.addListener('place_changed', function(){
                placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                document.getElementById("userlat").value=autocomplete.getPlace().geometry.location.lat();
                document.getElementById("userlng").value=autocomplete.getPlace().geometry.location.lng();
            });
            map.addListener('click', function(e) {
                placeMarkerAndPanTo(e.latLng, map);
                document.getElementById("userlat").value=e.latLng.lat();
                document.getElementById("userlng").value=e.latLng.lng();
            });
        }
        function placeMarkerAndPanTo(latLng, map) {
            map.setZoom(9);
            marker.setPosition(latLng);
            map.panTo(latLng);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHQj-nrlglAjt4sQgpRNcOdbRGNm7eGMc&libraries=places&callback=initMap">
    </script>

@endsection