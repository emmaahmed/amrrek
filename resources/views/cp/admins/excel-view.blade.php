   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم</th>
{{--        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الصورة</th>--}}
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البريد الالكتروني</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">العنوان</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المنطقة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الحالة </th>
        @foreach($permissions as $permission)

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight'; width: 20px;">{{$permission->name}} </th>
        @endforeach

    </tr>
    </thead>
    <tbody>
    @foreach($admins as $admin)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$admin->name}}</td>
{{--            <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{$admin->image}}" width="80px" height="80px" ></td>--}}

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$admin->email}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$admin->address}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$admin->city}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $admin->status==0?'مفعل':'غير مفعل' }}</td>
            @foreach($permissions as $permission)
                @if($admin->hasPermissionTo($permission->name))
                <td style="text-align: center; font-family: 'Segoe UI Semilight';"><span>&#10004;</span>
                </td>
                @else
                    <td style="text-align: center; font-family: 'Segoe UI Semilight';"><span>&#10006;</span></td>
            @endif
            @endforeach

        </tr>
    @endforeach
    </tbody>
</table>
   </html>