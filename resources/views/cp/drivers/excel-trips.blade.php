   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأول</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأخير</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">نوع الرحلة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">حالة الرحلة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">العميل</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">موقع البدء</th>
{{--        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">مواصفات السيارة</th>--}}

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">موقع الإنهاء</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">طريقة الدفع</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">كود الخصم المستخدم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">تكلفة الرحلة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">توقيت الطلب</th>
    </tr>
    </thead>
    <tbody>
    @foreach($trips as $trip)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $trip->driver->f_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $trip->driver->l_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">  @if($trip->type == "urgent")
                    عاجلة
                @else
                    مؤجلة
                @endif
            </td>
            <td>
                @if($trip->status == 1)
                    في انتظار السائق
                @elseif($trip->status == 2)
                    تم بدئ الرحلة
                @elseif($trip->status == 3)
                    تم انهاء الرحلة
                @elseif($trip->status == 4)
                    تم إلغاء الرحلة
                @endif
            </td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $trip->user->name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$trip->start_address}}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';"> {{$trip->lastPath[0]->address}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                @if($trip->payment ==0)
                    نقدا
                @else
                    فيزا
                @endif
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$trip->promo?$c->promo->code:''}}            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$trip->trip_total}}            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$trip->created_at}}           </td>

        </tr>
    @endforeach
    </tbody>
</table>
   </html>