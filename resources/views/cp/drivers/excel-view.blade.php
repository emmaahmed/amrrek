<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة السائق</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأول</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم الأخير</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البريد الإلكتروني</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">رقم الهاتف</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المنطقة</th>
        {{--        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">مواصفات السيارة</th>--}}

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">نوع المعاملة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">عدد الرحلات</th>

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة السيارة الأمامية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة السيارة الخلفية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة التأمين</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة الرخصة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة القيادة المدنية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">لون السيارة</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">أسم لون السيارة</th>
    </tr>
    </thead>
    <tbody>
    @foreach($drivers as $driver)
        <tr>
            <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{is_file(public_path('/uploads/drivers/images/'.$driver->getAttributes()['image']))?public_path('/uploads/drivers/images/'.$driver->getAttributes()['image']):public_path('/uploads/drivers/no-image.png')}}" width="80px" height="80px" ></td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->f_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->l_name }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->email }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->phone }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->city }}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$driver->subscription_type == 1 ? "عمولة" : "اشتراك شهري"}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ count($driver->trips)}} </td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $driver->driver_documents?$driver->driver_documents->front_car_image:public_path('/uploads/drivers/no-image.png') }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $driver->driver_documents?$driver->driver_documents->back_car_image:public_path('/uploads/drivers/no-image.png') }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $driver->driver_documents?$driver->driver_documents->insurance_image:public_path('/uploads/drivers/no-image.png') }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $driver->driver_documents?$driver->driver_documents->license_image:public_path('/uploads/drivers/no-image.png') }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';"><a href="{{ $driver->driver_documents?$driver->driver_documents->civil_image:public_path('/uploads/drivers/no-image.png') }}">أضغط هنا</a></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight'; background: {{ $driver->car_color}};"> </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $driver->color_name}} </td>

        </tr>
    @endforeach
    </tbody>
</table>
</html>