@extends('cp.index')
@section('content')
    <style>
        .btn {
            font-size: 12px !important;
            padding: 0.375rem 0.75rem !important;
            letter-spacing: 0.7px;
        }
    </style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                المستخدمين
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المستخدمين</li>
                            </ol>--}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" style="text-align: right">م</th>
                                <th scope="col"> الإسم </th>
                                <th scope="col"> الصورة </th>
                                <th scope="col">الهاتف </th>
                                <th scope="col"> البريد الإلكتروني</th>
{{--                                <th scope="col"> الدولة </th>--}}
                                <th scope="col"> المحفظة </th>
                                <th scope="col"> النقاط </th>
                                <th scope="col"> المنطقة </th>
                                <th scope="col"> الحالة </th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})" class="{{$c->suspend == 1 ? 'table-danger' :''}}">
                                    <td style="text-align: right">
                                        {{$c->id}} <br>

                                    </td>
                                    <td>{{$c->name}}</td>
                                    @if($c->image)
                                        <td>
                                            <button title="عرض" type="button" class="btn-danger" data-toggle="modal" data-target="#image{{$c->id}}" style="padding: 1px">
                                                <img src="{{$c->image}}" width="30px" height="30px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif
                                    <td>{{$c->phone}}</td>
                                    <td>{{$c->email}}</td>
{{--                                    <td>{{isset($c->country->name) ? $c->country->name : "-"}}</td>--}}
                                    <td>{{isset($c->wallet) ? $c->wallet : "-"}}</td>
                                    <td>{{isset($c->points) ? $c->points : "-"}}</td>
                                    <td>{{isset($c->city) ? $c->city : "-"}}</td>
                                    <td>
                                        @if($c->active == 1)
                                            <i class="font-danger show icon-check"></i>
                                        @else
                                            <i class="font-danger show icon-close"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <ul style="display: flex">
                                        @if(auth()->user()->hasPermissionTo('المستخدمين'))
                                            <li>
                                                <a href="{{route('users.wallets',$c->id)}}" >
                                                    <button title="تفاصيل المحفظة" class="btn btn-warning">
                                                        <i class="fa fa-money"></i>

                                                    </button>
                                                </a>
                                            </li>
                                        @if($c->suspend == 0)
                                            <li>
                                            <a href="{{route('editClientStatus',$c->id)}}" >
                                                <button title="إيقاف " class="btn btn-danger btn-sm">
                                                    <i class="fa fa-minus-circle"></i>
                                                </button>
                                            </a>
                                            </li>
                                        @else
                                                    <li>
                                            <a href="{{route('editClientStatus',$c->id)}}" >
                                                <button title="اعادة تشغيل " class="btn btn-success">
                                                    <i class="fa fa-plus-circle"></i>
                                                </button>
                                            </a>
                                                    </li>
                                        @endif
                                            <li>
                                            <a href="{{route('users.complains',$c->id)}}" >
                                                <button title=" طلبات الاسترداد النقدي" class="btn btn-success">
                                                    <i class="fa fa-comment-o"></i>
                                                </button>
                                            </a>
                                            </li>
                                            <li>
                                            <a href="{{route('users.orders',['user_id'=>$c->id,'department_id'=>2])}}" >
                                                <button title="طلبات المتاجر" class="btn btn-primary">
                                                    <i class="fa fa-shopping-basket"></i>
                                                </button>
                                            </a>
                                            </li>
                                            <li>
                                            <a href="{{route('users.orders',['user_id'=>$c->id,'department_id'=>3])}}" >
                                                <button title="طلبات التوصيل" class="btn btn-secondary">
                                                    <i class="fa fa-car"></i>

                                                </button>
                                            </a>
                                            </li>
                                            <li>
                                            <a href="{{route('users.orders',['user_id'=>$c->id,'department_id'=>5])}}" >
                                                <button title="طلبات النقل الثقيل" class="btn btn-warning">
                                                    <i class="fa fa-truck"></i>

                                                </button>
                                            </a>
                                            </li>
                                            <li>
                                            <a href="{{route('users.trips',$c->id)}}" >
                                                <button title="الرحلات" class="btn btn-info">
                                                    <i class="fa fa-map"></i>

                                                </button>
                                            </a>
                                            </li>

                                        @endif
                                        </ul>
                                    </td>

                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
