@extends('cp.index')
@section('content')
    <style>
        .btn {
            font-size: 12px !important;
            padding: 0.375rem 0.75rem !important;
            letter-spacing: 0.7px;
        }
    </style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                تفاصيل المحفظة
                                -
                                ( {{$user->name}} - {{$user->phone}} - {{$user->email}} )
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المستخدمين</li>
                            </ol>--}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col" style="text-align: right">م</th>
                                <th scope="col"> القيمة القديمة للمحفظة </th>
                                <th scope="col"> القيمة المضافة </th>
                                <th scope="col"> القيمة الجديدة للمحفظة </th>
                                <th scope="col"> الوصف </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $key => $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})" class="{{$c->suspend == 1 ? 'table-danger' :''}}">
                                    <td style="text-align: right">
                                        {{$key+1}} <br>

                                    </td>
                                    <td>{{$c->old_value}}</td>
                                    <td>{{$c->new_value-$c->old_value}}</td>
                                    <td>{{$c->new_value}}</td>
                                    <td>{{$c->description}}</td>


                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
