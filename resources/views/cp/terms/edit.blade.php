@extends('cp.index')
@section('content')
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                            data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                تعديل الشروط و الاحكام
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المتاجر</li>
                            </ol>--}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->

            <form class="form-horizontal" method="post" action="{{route('editTerm')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="term_id" value="{{$term->id}}">


{{--                    <div class="form-group row ">--}}
{{--                        <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان بالعربية</label>--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <textarea class="form-control" name="ar_title" placeholder="العنوان بالعربية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">{{$result->ar_title}}</textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @include('cp.layouts.error', ['input' => 'ar_name'])--}}

{{--                    <div class="form-group row ">--}}
{{--                        <label class="col-lg-12 control-label text-lg-right" for="textinput">العنوان بالنجليزية</label>--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <textarea class="form-control" name="en_title" placeholder="العنوان بالنجليزية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">{{$result->en_title}}</textarea>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @include('cp.layouts.error', ['input' => 'en_name'])--}}

                    <div class="form-group row ">
                        <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص بالعربية</label>
                        <div class="col-lg-12">
                            <textarea class="form-control" name="term_ar" id="editor1" rows="5" placeholder="النص بالعربية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">{{$term->term_ar}}</textarea>
                        </div>
                    </div>
                    @include('cp.layouts.error', ['input' => 'ar_name'])

                    <div class="form-group row ">
                        <label class="col-lg-12 control-label text-lg-right" for="textinput">النص بالنجليزية</label>
                        <div class="col-lg-12">
                            <textarea class="form-control" name="term_en" id="editor2" rows="5" placeholder="النص بالنجليزية" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">{{$term->term_en}}</textarea>
                        </div>
                    </div>
                    @include('cp.layouts.error', ['input' => 'en_name'])

{{--                    <div class="form-group ">--}}
{{--                        <label  class="col-lg-12 control-label text-lg-right" for="textinput">الصورة</label>--}}
{{--                        <input name="image" type="file" class="form-control btn-square" >--}}
{{--                    </div>--}}
{{--                    @if($result->image != NULL)--}}
{{--                        <div class="form-group ">--}}
{{--                            <img src="{{$result->image}}" width="60px" height="60px">--}}
{{--                        </div>--}}
{{--                        --}}{{--@else--}}
{{--                            <div class="form-group ">--}}
{{--                                <span>لا توجد صورة</span>--}}
{{--                            </div>--}}
{{--                    @endif--}}


                </div>
                <div class="modal-footer">

                    <button class="btn btn-primary" type="submit">تعديل</button>
                </div>
            </form>



        <!-- Container-fluid Ends-->
    </div>



    <script>
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
    </script>


@endsection
