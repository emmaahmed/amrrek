@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close" >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                تواصل معنا
                                <a href="{{route('contact_us.index',$type)}}" class="btn btn-success"><span>النشطة</span></a>
                                <a href="{{route('contact_us.closed.index',$type)}}" class="btn btn-danger"><span>المغلقة</span></a>


                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الاقسام</li>
                            </ol>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">النوع</th>
                                <th scope="col">الحالة</th>
                                <th scope="col">الإسم </th>
                                <th scope="col">البريد الإلكتروني</th>
                                <th scope="col">الهاتف </th>
                                <th scope="col"> الرسالة</th>
                                <th scope="col"> حالة الطلب</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cats as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})" class="">
                                    <td>{{$c->id}}</td>
                                    <td>
                                        @if($c->type == 0)
                                            <b class="badge badge-default" style="color: #C32F45;">خاص بالمستخدم</b>
                                        @elseif($c->type == 1)
                                            <b class="badge badge-default" style="color: #C32F45;">خاص بالمندوب</b>
                                        @elseif($c->type == 2)
                                            <b class="badge badge-default" style="color: #C32F45;">خاص بالسائق</b>
                                        @elseif($c->type == 3)
                                            <b class="badge badge-default" style="color: #C32F45;">مندوب النقل الثقيل</b>
                                        @elseif($c->type == 4)
                                            <b class="badge badge-default" style="color: #C32F45;">خاص بالمتجر</b>
                                        @endif
                                    </td>
                                    <td>
                                        @if($c->status == 1)
                                            <i class="font-success show icon-check"></i>
                                        @else
                                            <i class="font-danger show icon-close"></i>
                                        @endif
                                    </td>
                                    <td>{{$c->name}}</td>
                                    <td>{{$c->email}}</td>
                                    <th> {{$c->phone}} </th>
                                    <th> {{$c->message}} </th>
                                    <th>
                                        @if($c->status == 0)

                                        <a href="{{route('contact-us.close',$c->id)}}" title="إغلاق الطلب"  class="btn btn-danger">
                                            <i class="fa fa-lock"></i>
                                        </a>
                                        @else
                                            <a href="{{route('contact-us.close',$c->id)}}" title="فتح الطلب"  class="btn btn-danger">
                                                <i class="fa fa-unlock"></i>
                                            </a>

                                        @endif


                                    </th>

                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$cats->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
