@extends('cp.index')
@section('content')
    <style>
        button{
            margin:  2px;
        }
        .select2-container--default .select2-selection--single {
            float: right;
            width: 100%;
        }
        .select2-container[dir="rtl"] .select2-selection--single .select2-selection__rendered{
            float: right;
        }
        .select2-results__option {
            text-align: right;
        }

    </style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                الإشعارات
                                ({{sizeof($nots)}})
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الاشعارات</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            @if(auth()->user()->hasPermissionTo('اضافة اشعار'))
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#subCat">
                                    <i class="icon-plus"></i>
                                    إضافة اشعار
                                </button>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#subCatUser"><i class="icon-plus"></i>
                                    إضافة اشعار لمستخدم
                                </button>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#subCatDelegate"><i class="icon-plus"></i>
                                    إضافة اشعار لمندوب
                                </button>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#subCatDelegateHeavey"><i class="icon-plus"></i>
                                    إضافة اشعار لمندوب نقل ثقيل
                                </button>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#subCatDriver"><i class="icon-plus"></i>
                                    إضافة اشعار لسائق
                                </button>

                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#subCatShop"><i class="icon-plus"></i>
                                    إضافة اشعار لمتجر
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">

                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">العنوان</th>
                                <th scope="col"> النص</th>
                                <th scope="col"> اسم المستخدم</th>
                                <th scope="col"> نوع المستخدم</th>
                                <th scope="col"> تاريخ الانشاء</th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nots as $c)
                                <tr>
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->title}}</td>
                                    <td>{{$c->body}}</td>
                                    <td>
                                        @if($c->type == 0)
                                            @if($c->user($c->user_id))
                                                {{--                                                <span class="badge badge-dark">{{$c->user($c->user_id)->id}}</span> <br>--}}
                                                {{$c->user($c->user_id)->name}} <br>
                                                {{$c->user($c->user_id)->phone}} <br>
                                                {{--                                                {{$c->user($c->user_id)->email}}--}}
                                            @endif
                                        @elseif($c->type == 1)
                                            @if($c->delegate($c->user_id))
                                                {{--                                                <span class="badge badge-dark">{{$c->delegate($c->user_id)->id}}</span> <br>--}}
                                                {{$c->delegate($c->user_id)->f_name}} {{$c->delegate($c->user_id)->l_name}} <br>
                                                {{$c->delegate($c->user_id)->phone}} <br>
                                                {{--                                                {{$c->delegate($c->user_id)->email}}--}}
                                            @endif
                                        @elseif($c->type == 2)
                                            @if($c->driver($c->user_id))
                                                {{--                                                <span class="badge badge-dark">{{$c->driver($c->user_id)->id}}</span> <br>--}}
                                                {{$c->driver($c->user_id)->f_name}} {{$c->driver($c->user_id)->l_name}}<br>
                                                {{$c->driver($c->user_id)->phone}} <br>
                                                {{--                                                {{$c->driver($c->user_id)->email}}--}}
                                            @endif
                                        @elseif($c->type == 3)
                                            all
                                        @elseif($c->type == 4)
                                            @if($c->delegate($c->user_id))
                                                {{--                                                <span class="badge badge-dark">{{$c->delegate($c->user_id)->id}}</span> <br>--}}
                                                {{$c->delegate($c->user_id)->f_name}} {{$c->delegate($c->user_id)->l_name}} <br>
                                                {{$c->delegate($c->user_id)->phone}} <br>
                                                {{--                                                {{$c->delegate($c->user_id)->email}}--}}
                                            @endif
                                        @elseif($c->type == 5)
                                            @if($c->shop($c->user_id))
                                                {{--                                                <span class="badge badge-dark">{{$c->shop($c->user_id)->id}}</span> <br>--}}
                                                {{$c->shop($c->user_id)->name}} <br>
                                                {{$c->shop($c->user_id)->phone}} <br>
                                                {{--                                                {{$c->shop($c->user_id)->email}}--}}
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if($c->type == 0)
                                            <b class="badge badge-danger">عميل</b>
                                        @elseif($c->type == 1)
                                            <b class="badge badge-danger">مندوب</b>
                                        @elseif($c->type == 2)
                                            <b class="badge badge-danger">سائق</b>
                                        @elseif($c->type == 3)
                                            <b class="badge badge-danger">الكل</b>
                                        @elseif($c->type == 4)
                                            <b class="badge badge-danger">مندوب نقل ثقيل</b>
                                        @elseif($c->type == 5)
                                            <b class="badge badge-danger">متجر</b>
                                        @else
                                            <b class="badge badge-danger">-</b>
                                        @endif
                                    </td>
                                    <td>{{$c->created_at}}</td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('حذف اشعار'))
                                            {{--                                            <a href="{{asset('/admin/notifications/delet/'.$c->id)}}">--}}
                                            <button title="حذف" type="button" class="btn btn-danger"
                                                    data-toggle="modal" data-target="#delete_{{$c->id}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            {{--                                            </a>--}}
                                        @endif
                                    </td>

                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">هل انت متأكد ؟</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="get" action="{{asset('/admin/notifications/delet/'.$c->id)}}" class="buttons">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <h6>
                                                            انت علي وشك حذف الحقل

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {{--                                                        <input type="hidden" name="model_id" value="{{$result->id}}">--}}
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">إغلاق</button>
                                                        <button type="submit" class="btn btn-danger">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>
                    {{$nots->links()}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <div class="modal fade" id="subCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد الارسال</label>
                            <div class="col-lg-12">
                                <select name="send_to" class="btn form-control b-light digits" required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="" selected disabled>اختر</option>
                                    <option value="users">ارسال للمستخدمين فقط</option>
                                    <option value="drivers">ارسال للسائقين فقط</option>
                                    <option value="delegates">ارسال للمناديب فقط</option>
                                    <option value="delegatesHeavey">ارسال لمناديب النقل الثقيل فقط</option>
                                    <option value="shops">ارسال للمتاجر فقط</option>
                                    <option value="all">ارسال للكل</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subCatUser" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لعميل</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input type="hidden" name="type" value="0">
                        <input type="hidden" name="send_to" value="user">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> المستخدمين</label>
                            <div class="col-lg-12">
                                <select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required>
                                    <option value="" selected disabled>اختر</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}} / {{$user->phone}}
                                            {{--                                            / {{$user->email}}--}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>

                        </div>

                        <div class="form-group row" style="display: none">
                            <div class="col-lg-12">
                                <select name="send_to" class="btn form-control b-light digits" required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="user" selected>اختر</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--    <div class="modal fade" id="subCatUser" role="dialog" aria-labelledby="exampleModalLabel"-->
    <!--         aria-hidden="true">-->
    <!--        <div class="modal-dialog" role="document">-->
    <!--            <div class="modal-content">-->
    <!--                <div class="modal-header">-->
    <!--                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لعميل</h5>-->
    <!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
    <!--                        <span aria-hidden="true">&times;</span>-->
    <!--                    </button>-->
    <!--                </div>-->
    <!--                <form class="form-horizontal needs-validation was-validated" method="post"-->
    <!--                      action="{{route('notifications.store')}}" enctype="multipart/form-data">-->
    <!--                    {{csrf_field()}}-->
    <!--                    <div class="modal-body">-->

    <!--                        <input type="hidden" name="type" value="0">-->
    <!--                        <div class="form-group row">-->
    <!--                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>-->
    <!--                            <div class="col-lg-12">-->
    <!--                                <input id="name" name="title" type="text" placeholder="العنوان"-->
    <!--                                       class="form-control btn-square" required-->
    <!--                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">-->
    <!--                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>-->
    <!--                            </div>-->
    <!--                        </div>-->

    <!--                        <div class="form-group row">-->
    <!--                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>-->
    <!--                            <div class="col-lg-12">-->
    <!--                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required-->
    <!--                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>-->
    <!--                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>-->
    <!--                            </div>-->
    <!--                        </div>-->

    <!--                        <div class="form-group row">-->
    <!--                                <div class="col">-->
    <!--                                    <div class="form-group">-->


    <!--                                  <label class="col-form-label float-left" for="product_id">المستخدمين</label>-->
    <!--<select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required>-->
    <!--                                    <option value="" selected disabled>اختر</option>-->
    <!--                                    @foreach($users as $user)-->
    <!--                                        <option value="{{$user->id}}">{{$user->name}} / {{$user->phone}}-->
    <!--{{--                                            / {{$user->email}}--}}-->
    <!--                                        </option>-->
    <!--                                    @endforeach-->
    <!--                                </select>-->
    <!--                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>-->

    <!--                                </div>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </div>-->

    <!--                        <div class="form-group row" style="display: none">-->
    <!--                            <div class="col-lg-12">-->
    <!--                                <select name="send_to" class="btn form-control b-light digits" required-->
    <!--                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">-->
    <!--                                    <option value="user" selected>اختر</option>-->
    <!--                                </select>-->
    <!--                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>-->
    <!--                            </div>-->
    <!--                        </div>-->


    <!--                    </div>-->
    <!--                    <div class="modal-footer">-->
    <!--                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>-->
    <!--                        <button class="btn btn-primary">حفظ</button>-->
    <!--                    </div>-->
    <!--                </form>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="modal fade" id="subCatDelegate"  role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لمندوب</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input type="hidden" name="type" value="1">
                        <input type="hidden" name="send_to" value="delegate">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد الارسال</label>
                            <div class="col-lg-12">
                                <select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="" selected disabled>اختر</option>
                                    @foreach($delegates as $delegate)
                                        <option value="{{$delegate->id}}">{{$delegate->f_name}} / {{$delegate->phone}}
                                            {{--                                            / {{$delegate->email}}--}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row" style="display: none">
                            <div class="col-lg-12">
                                <select name="send_to" class="btn form-control b-light digits" required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="delegate" selected>اختر</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subCatDelegateHeavey"  role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لمندوب نقل ثقيل</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input type="hidden" name="type" value="4">
                        <input type="hidden" name="send_to" value="delegate">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد الارسال</label>
                            <div class="col-lg-12">
                                <select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="" selected disabled>اختر</option>
                                    @foreach($delegatesHeavey as $delegateHeavey)
                                        <option value="{{$delegateHeavey->id}}">
                                            {{$delegateHeavey->f_name}} / {{$delegateHeavey->phone}}
                                            {{--                                            / {{$delegateHeavey->email}}--}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row" style="display: none">
                            <div class="col-lg-12">
                                <select name="send_to" class="btn form-control b-light digits" required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="delegate" selected>اختر</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subCatDriver"  role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لسائق</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input type="hidden" name="type" value="2">
                        <input type="hidden" name="send_to" value="driver">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد الارسال</label>
                            <div class="col-lg-12">
                                <select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="" selected disabled>اختر</option>
                                    @foreach($drivers as $driver)
                                        <option value="{{$driver->id}}">{{$driver->f_name}} / {{$driver->phone}}
                                            {{--                                            / {{$driver->email}}--}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="subCatShop" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">إضافة اشعار لمتجر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal needs-validation was-validated" method="post"
                      action="{{route('notifications.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input type="hidden" name="type" value="5">
                        <input type="hidden" name="send_to" value="shop">

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> العنوان</label>
                            <div class="col-lg-12">
                                <input id="name" name="title" type="text" placeholder="العنوان"
                                       class="form-control btn-square" required
                                       oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> النص</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" name="body" placeholder="النص " dir="rtl" required
                                          oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')"></textarea>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-12 control-label text-lg-right" for="textinput"> تحديد الارسال</label>
                            <div class="col-lg-12">
                                <select name="user_id" class="custom-select form-control js-example-basic-multiple" style="width: 100% !important; "required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="" selected disabled>اختر</option>
                                    @foreach($shops as $shop)
                                        <option value="{{$shop->id}}">{{$shop->name}} / {{$shop->phone}}
                                            {{--                                            / {{$shop->email}}--}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>

                        <div class="form-group row" style="display: none">
                            <div class="col-lg-12">
                                <select name="send_to" class="btn form-control b-light digits" required
                                        oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="shop" selected>اختر</option>
                                </select>
                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-dark" data-dismiss="modal">إغلاق</button>
                        <button class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
