
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{asset('cp/endless/assets/css/bootstrap.css')}}">

<style>
    @font-face {
        font-family: HsDream;
        src: url('/ArbFONTS-HSDream-Medium-1.otf');
    }
    * {
        font-family: HsDream !important;
    }
    .custom-class{
        padding: 50px;
        box-shadow: 0px 0px 30px 0px rgb(9 17 74 / 10%);
        margin-top: 50px;
        width: 90% !important;
    }
    .custom-div{
        text-align: center;
        margin: 10px;
    }
</style>
</head>
<body >
<div class="container-fluid custom-class">
    <div class="custom-div" ><img src="{{asset($type.'-image.svg')}}"></div>

@if(count($data)>0)
    @foreach($data as $privacy)
        {!!$privacy->body  !!}
    @endforeach
@endif
</div>
</body>
</html>

