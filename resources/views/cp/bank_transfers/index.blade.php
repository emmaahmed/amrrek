@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            @include('cp.layouts.messages')


                            <h3>
                                <i data-feather="home"></i>
                                @if(isset($from_admin) && $from_admin!=1)
                                التحويلات البنكية
                                @else
                                    تحويلات السائقين
                                @endif
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">الدول</li>
                            </ol>--}}
                        </div>
                        <div style="float: left">
                            @if(Request::is('admin/bank_transfers_admin'))
                                <a  class="btn btn-primary" href="{{route('bank_transfers.create')}}"><i class="icon-plus"></i>
                                    إضافة تحويل بنكي
                                </a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">الصورة</th>
                                <th scope="col">إسم السائق</th>
                                <th scope="col">هاتف السائق</th>
                                {{--                                <th scope="col">إيميل السائق</th>--}}
                                <th scope="col"> اسم البنك </th>
                                <th scope="col"> رقم التحويل </th>
                                <th scope="col">قيمة التحويل</th>



                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $c)
                                <tr id="main_cat_{{$c->id}}" onclick="myFunction({{$c->id}})">
                                    <td>{{$c->id}}</td>

                                    <td>
                                        @if($c->image)
                                            <img src="{{$c->image}}"  width="60px" height="60px">
                                            <br>
                                            <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#edit_{{$c->id}}">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        @else
                                            -
                                        @endif

                                    </td>
                                    <td >
                                        {{$c->name}}
                                    </td>
                                    <td >
                                        {{$c->phone}}
                                    </td>
                                    {{--                                    <td >--}}
                                    {{--                                        {{$c->email}}--}}
                                    {{--                                    </td>--}}
                                    <td>{{$c->bank_name}}</td>
                                    <td>{{$c->transfer_no}}</td>
                                    <td>{{$c->transfer_value}}</td>



                                    <td>
                                        @if(admin()->hasPermissionTo('التحويلات البنكية'))
                                            <a title="تعديل" href="{{route('bank_transfers.edit',$c->id)}}" class="btn btn-warning" >
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endif

                                    </td>

                                </tr>
                                <div class="modal fade" id="edit_{{$c->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"> الصور</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" style="text-align: center">

                                                @if($c->image)
                                                        <img src="{{$c->image}}" width="100px" height="100px">
                                                        <a target="_blank" class="btn btn-success" href="{{$c->image}}">عرض</a>
                                                        <hr>
                                                @else
                                                    <h3>لا يوجد صور مرفقة</h3>
                                                @endif




                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>{{--{{$countries->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->

    </div>

@endsection
