@extends('cp.index')
@section('content')
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                            data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                إضافة تحويل بنكي
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المتاجر</li>
                            </ol>--}}
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="card">
                <div class="tab-content" >

            <form class="form-horizontal needs-validation was-validated" method="post" action="{{route('bank_transfers.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}

                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">السائقين </label>
                    <div class="col-lg-6">
                        <select class="form-control btn-square" name="user_id" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                            @foreach($drivers as $driver)
                                <option  value="{{$driver->id}}">{{$driver->f_name}} {{$driver->l_name}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>
                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">الحساب البنكي </label>
                    <div class="col-lg-6">
                        <select class="form-control btn-square" name="bank_account_id" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                            @foreach($bankAccounts as $account)
                                <option  value="{{$account->id}}">{{$account->bank_name}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>




                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">رقم التحويل </label>
                    <div class="col-lg-6">
                        <input id="inputName1" name="transfer_no" type="text" placeholder="رقم التحويل " class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>


                <div class="form-group row ">
                    <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">قيمة التحويل </label>
                    <div class="col-lg-6">
                        <input id="inputName1" name="transfer_value" type="text" placeholder="قيمة التحويل " class="form-control btn-square" for="textinput"  required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                        <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>

                <div class="form-group row" >
                    <label class="col-lg-12 control-label text-lg-right" for="textinput">الصورة</label>
                    <div class="col-lg-6">

                    <input name="image" type="file" class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                    <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>
                    </div>
                </div>


                <button class="btn btn-primary" type="submit">حفظ</button>
        </form>
                </div>
        <!-- Container-fluid Ends-->
    </div>
        </div>
    </div>





@endsection
