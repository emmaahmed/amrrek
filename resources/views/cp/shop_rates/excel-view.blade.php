   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">القسم</th>
        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المستخدم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">هاتف المستخدم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المتجر</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">التقييم</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">التعليق</th>

    </tr>
    </thead>
    <tbody>
    @foreach($rates as $rate)
        <tr>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">                                        @if($rate->department_id == 2)
                    <b class='badge badge-danger'>طلبات متاجر</b>
                @elseif($rate->department_id == 3)
                    <b class='badge badge-danger'>طلبات عادية</b>
                @else
                    -
                @endif
            </td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">                                        @if($rate->user)

                    {{$rate->user->name}}
                    {{--                                            <img src="{{$rate->user->image}}" width="50px" height="50px"> <br>--}}
                @else
                    -
                @endif
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">                                        @if($rate->user)
                    {{$rate->user->phone}}
                @else
                    -
                @endif
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">                                        @if($rate->shop)
                    {{--                                            {{$rate->shop->id}} <br>--}}
                    {{$rate->shop->name}} <br>
                    {{--                                            <img src="{{$rate->shop->image}}" width="50px" height="50px"> <br>--}}
                @else
                    -
                @endif
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$rate->rate}}
            </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">
                {{$rate->comment}}
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
   </html>