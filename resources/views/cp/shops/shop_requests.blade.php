@extends('cp.index')
@section('content')
    <style>
        button{
            margin:  2px;
        }
        h1{
            color : #C32F45;
        }
    </style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                            data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                المتاجر
                                ({{$type}})

                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المتاجر</li>
                            </ol>--}}
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-xl-12">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">تفعيل/إيقاف</th>
{{--                                <th scope="col">مضمون</th>--}}
                                <th scope="col"> الإسم</th>
                                <th scope="col"> الصورة</th>
                                <th scope="col">صورة الخلفية</th>
                                <th scope="col">الهاتف</th>
                                <th scope="col"> البريد الإلكتروني</th>
                                <th scope="col"> الكود الترويجي</th>
                                <th scope="col"> المدينة</th>
                                <th scope="col"> الإحداثيات</th>
                                <th scope="col"> القسم </th>
                                <th scope="col"> صورة البطاقة الضريبية </th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $c)
                                @if($c->accept == 0)
                                    <tr id="main_cat_{{$c->id}}"
                                    class="{{$c->suspend == 1 ? 'table-danger' :''}}">
                                    <td>{{$c->id}}</td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تفعيل متجر'))
                                            <div class="nav-right col p-0">
                                                <div class="media">
                                                    <div class="media-body text-right switch-md icon-state">
                                                        <label class="switch">
                                                            <input type="checkbox" id="status" model_id="{{$c->id}}"
                                                                   @if($c->active == 1)  checked @endif><span
                                                                    class="switch-state bg-success"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>

                                    <td>{{$c->name}}</td>
                                    @if($c->image)
                                        <td>
                                            <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#image{{$c->id}}" style="padding: 1px">
                                                <img src="{{$c->image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif
                                    @if($c->cover_image)
                                        <td>
                                            <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#cover_image{{$c->id}}" style="padding: 1px">
                                                <img src="{{$c->cover_image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="cover_image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->cover_image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif
                                    <td>{{$c->phone}}</td>
                                    <td>
                                        {{$c->email}} <br>
                                    </td>
                                        <td>{{$c->comercial_number?$c->comercial_number:''}}</td>
                                    <td>
                                        {{$c->city}} <br>
                                    </td>
                                    <td>
                                        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$c->lat}},{{$c->lng}}">
                                            <i class="icon-location-pin " style="font-size: x-large"></i>
                                        </a>
                                    </td>

                                    <td>
                                        <b class="badge badge-dark">
                                            {{isset($c->category) ? $c->category->name_ar ." - ".$c->category->name_en : "-"}}
                                        </b>
                                    </td>
                                    <td>
                                        <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#image{{$c->id}}" style="padding: 1px">
                                            <img src="{{$c->tax_img}}" width="50px" height="50px"></img>
                                        </button>

                                        {{--==image==--}}
                                        <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <img src="{{$c->tax_img}}"/>
                                                </div>
                                            </div>
                                        </div>
                                        {{--==image==--}}
                                    </td>

                                    <td>
                                        @if($c->accept == 0)
                                            <a href="{{route('acceptShop',$c->id)}}">
                                                <button title="قبول المتجر" class="btn btn-success">
                                                    <i class="fa fa-plus-square"></i>
                                                </button>
                                            </a>
                                        @endif
                                        <button title="عرض التفاصيل" type="button" class="btn btn-danger"
                                                data-toggle="modal" data-target="#show{{$c->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-eye" color="white" data-toggle="modal">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </button>
{{--                                        @if(auth()->user()->hasPermissionTo('تعديل متجر'))--}}
{{--                                        <a title="تعديل" type="button" class="btn btn-warning"--}}
{{--                                                style="margin: 1px"--}}
{{--                                                href="{{ asset('admin/shops/edit/'.$c->id) }}"--}}
{{--                                                data-toggle="modal"--}}
{{--                                                data-target="#edit_{{$c->id}}"--}}
{{--                                        >--}}
{{--                                            <i class="fa fa-edit"></i>--}}
{{--                                        </a>--}}
{{--                                        @endif--}}
                                        @if(auth()->user()->hasPermissionTo('حذف متجر'))
                                        <button title="حذف" type="button" class="btn btn-danger" data-toggle="modal"
                                                style="margin: 1px"
                                                data-target="#delete_{{$c->id}}">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endif
                                    </td>


                                    <div class="modal fade" id="show{{$c->id}}" tabindex="-1" role="dialog"
                                         style="text-align: right"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title" id="exampleModalLabel">تفاصيل الطلب</h1>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <input type="hidden" name="model_id" value="{{$c->id}}">

                                                <div class="form-horizontal needs-validation was-validated">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">


                                                        {{--                                                        <div class="form-group row {{ $errors->has('ar_name') ? ' has-error' : '' }}">--}}
                                                        {{--                                                            <label class="col-lg-12 control-label text-lg-right"  for="validationCustom04">الإسم بالعربية</label>--}}
                                                        {{--                                                            <div class="col-lg-12">--}}
                                                        {{--                                                                <input id="inputName1" name="name_ar" type="text" placeholder="الإسم بالعربية" class="form-control btn-square" for="textinput" value="{{$c->name_ar}}" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">--}}
                                                        {{--                                                                <div class="invalid-feedback">هذا الحقل مطلوب إدخاله .</div>--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                        @include('cp.layouts.error', ['input' => 'ar_name'])--}}

                                                        <div class="form-group row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالعربي</label>
                                                                <input name="description_ar" type="text" placeholder="الوصف بالعربي" disabled class="form-control btn-square" for="textinput" value="{{$c->description_ar}}">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">الوصف بالانجليزي</label>

                                                                <input name="description_en" type="text" placeholder="الوصف الانجليزي" disabled class="form-control btn-square" for="textinput" value="{{$c->description_en}}">
                                                            </div>

                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">العنوان</label>

                                                                <input name="address" type="text" placeholder="العنوان" disabled class="form-control btn-square" for="textinput" value="{{$c->address}}">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">السجل التجاري</label>
                                                                <input name="commercial_register" type="text" placeholder="السجل التجاري" disabled class="form-control btn-square" for="textinput" value="{{$c->commercial_register}}">
                                                            </div>

                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">البطاقة الضريبية</label>
                                                                <input name="tax_num" type="text" placeholder="البطاقة الضريبية" disabled class="form-control btn-square" for="textinput" value="{{$c->tax_num}}">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">اسم البنك و رقم الحساب</label>

                                                                <input name="tax_num" type="text" placeholder="البطاقة الضريبية" disabled class="form-control btn-square" for="textinput" value="{{$c->bank_name}} - {{$c->bank_account}} ">
                                                            </div>

                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12" >
                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">اسم المشرف</label>

                                                                <input name="moderator_name" type="text" placeholder="اسم المشرف" disabled class="form-control btn-square" for="textinput" value="{{$c->moderator_name}} ">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">إيميل المشرف</label>
                                                                <input name="moderator_name" type="text" placeholder="إيميل المشرف" disabled class="form-control btn-square" for="textinput" value="{{$c->moderator_email}} ">
                                                            </div>

                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                            <label class="col-lg-12 control-label text-lg-right" for="textinput">هاتف المشرف</label>
                                                                <input name="moderator_phone" type="text" placeholder="هاتف المشرف" disabled class="form-control btn-square" for="textinput" value="{{$c->moderator_phone}} ">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12">

                                                                <label class="col-lg-12 control-label text-lg-right" for="textinput">منصب المسؤول</label>
                                                                <input name="moderator_title" type="text" placeholder="منصب المسؤل" disabled class="form-control btn-square" for="textinput" value="{{$c->moderator_title}} ">
                                                            </div>

                                                        </div>


                                                </div>

                                               </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--///////////////////////////--}}
                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1"
                                         style="text-align: right"
                                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف المتجر</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="post" action="{{route('shopRequests.delete')}}" class="buttons">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف المتجر
                                                            <br>رقم المتجر: ({{$c->id}})
                                                            <br>الإسم: ({{$c->name}})

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </tr>
                                @endif
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>
{{--                    {{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>

    <script>
        $(document).on('change', '#status', function (e) {

            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editRequestStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });


    </script>


@endsection
