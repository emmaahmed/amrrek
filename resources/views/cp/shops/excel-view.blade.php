   <html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  </head>
    <table>
    <thead>
    <tr>

        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة المتجر </th>
        <th style="text-align: center;color: #b5293c;background:#e9edf1 ; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">صورة الخلفية </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الأسم </th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البريد الإلكتروني</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">رقم الهاتف</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">المنطقة</th>
{{--        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">مواصفات السيارة</th>--}}

        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">القسم التابع له	</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">العنوان</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">الوصف </th>


        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';"> السجل التجاري</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">البطاقة الضريبية</th>
        <th style="text-align: center;color: #b5293c;background: #e9edf1; font-size: 12px; font-weight: 800; font-family: 'Segoe UI Semilight';">اسم البنك و رقم الحساب</th>
    </tr>
    </thead>
    <tbody>
    @foreach($shops as $shop)
        <tr>
                        <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{is_file(public_path('/uploads/shops/images/'.$shop->getAttributes()['image']))?public_path('/uploads/shops/images/'.$shop->getAttributes()['image']):public_path('/uploads/no-image.png')}}" width="80px" height="80px" ></td>


            <td style=" text-align: center;height: 80px; width: 80px;"><img src="{{public_path()}}/uploads/shops/coverimages/{{ $shop->getAttributes()['cover_image'] }}" width="80px" height="80px" ></td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->name }}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->email }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->phone }}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->city_name }}</td>

            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{$shop->category  ? $shop->category->name_ar : ""}}</td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->address}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->description_ar}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->commercial_register}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->tax_num}} </td>
            <td style="text-align: center; font-family: 'Segoe UI Semilight';">{{ $shop->bank_name}}-{{ $shop->bank_account}} </td>

                  </tr>
    @endforeach
    </tbody>
</table>
   </html>