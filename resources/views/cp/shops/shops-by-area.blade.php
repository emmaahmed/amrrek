@extends('cp.index')
@section('content')
    <style>
        button{
            margin:  2px;
        }
    </style>
    <div class="page-body" dir="rtl">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-right">


                            <?php if(session()->has('insert_message')): ?>
                            <div class="alert alert-success dark alert-dismissible fade show col-lg-3" role="alert">
                                <i class="icon-thumb-up"></i>
                                <b>
                                    <?php echo e(session()->get('insert_message')); ?>
                                </b>
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <?php endif; ?>

                            @if($errors->any())
                                <div class="alert alert-danger dark alert-dismissible fade show col-lg-3" role="alert">
                                    <i class="icon-thumb-down"></i>
                                    <b>
                                        @if ($errors)
                                            <?php echo "من فضلك اكمل ادخال البيانات المطلوبة !"; ?>
                                        @endif
                                    </b>
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"
                                            data-original-title="" title="">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif


                            <h3>
                                <i data-feather="home"></i>
                                المتاجر
                                ({{$type}})
{{--                                @if(auth()->user()->hasPermissionTo('المتاجر'))--}}
{{--                                    <a href="{{asset('/shopss')}}"--}}
{{--                                       class="btn btn-primary"><span>المتاجر قيد التنفيذ</span></a>--}}
{{--                                @if(auth()->user()->hasPermissionTo('المتاجر'))--}}
{{--                                    <a href="{{asset('/admin/active-shops')}}"--}}
{{--                                       class="btn btn-success"><span>المتاجر المفعلة</span></a>--}}
{{--                                    <a href="{{asset('/admin/inactive-shops')}}" class="btn btn-danger"><span>المتاجر الغير المفعلة</span></a>--}}
{{--                                @endif--}}
                            </h3>
                            {{--<ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">المتاجر</li>
                            </ol>--}}
                        </div>
{{--                        <div style="float: left">--}}
{{--                            @if(auth()->user()->hasPermissionTo('اضافة متجر'))--}}
{{--                            <a type="button" class="btn btn-primary"--}}
{{--                                    data-toggle="modal"--}}
{{--                                    data-target="#subCat"--}}
{{--                                    href="{{route('createShop')}}"--}}
{{--                            >--}}
{{--                                <i class="icon-plus"></i>--}}
{{--                                إضافة متجر--}}
{{--                            </a>--}}
{{--                            @endif--}}
{{--                        </div>--}}


                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form class="form-horizontal" style="padding-top: 50px;" action="{{route('shopsByAreaSearch')}}" method="post">
                    @csrf

                    <!-- Form Name -->

                        <!-- Text input-->
                        <div class="form-group row">
                            <div class="col-lg-4 col-sm-12">
                                <select name="city"   class="form-control btn-square" required oninvalid="this.setCustomValidity('هذا الحقل مطلوب إدخاله')">
                                    <option value="all">كل المناطق</option>
                                    @if(count($areas)>0)
                                        @foreach($areas as $area)
                                            <option value="{{$area->city_name}}">{{$area->city_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-lg-2 col-sm-6">
                                <button type="submit" class="btn btn-danger" >
                                    <i class="fa fa-search"></i>

                                </button>
                            </div>

                        </div>
                        <!-- Text input-->

                    </form>

                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">م</th>
                                <th scope="col">تفعيل/إيقاف</th>
{{--                                <th scope="col">مضمون</th>--}}
                                <th scope="col"> الإسم</th>
                                <th scope="col"> الصورة</th>
                                <th scope="col">صورة الخلفية</th>
                                <th scope="col">الهاتف</th>
                                <th scope="col"> البريد الإلكتروني</th>
                                <th scope="col"> الكود الترويجي</th>
                                <th scope="col"> الإحداثيات</th>
                                <th scope="col">المنطقة</th>
{{--                                <th scope="col"> الوصف</th>--}}
{{--                                <th scope="col"> العضوية</th>--}}
{{--                                <th scope="col"> website</th>--}}
{{--                                <th scope="col"> السجل التجاري</th>--}}
{{--                                <th scope="col"> الرقم الضريبي</th>--}}
{{--                                <th scope="col"> رخصة التسجيل</th>--}}
{{--                                <th scope="col"> الصورة الضريبية</th>--}}
{{--                                <th scope="col">صورة السجل التجاري</th>--}}
{{--                                <th scope="col"> المواعيد</th>--}}
                                <th scope="col"> القسم التابع له</th>
                                <th scope="col">الإختيارات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $c)
                                <tr id="main_cat_{{$c->id}}"
                                    class="{{$c->suspend == 1 ? 'table-danger' :''}}">
                                    <td>{{$c->id}}</td>
                                    <td>
                                        @if(auth()->user()->hasPermissionTo('تفعيل متجر'))
                                            <div class="nav-right col p-0">
                                                <div class="media">
                                                    <div class="media-body text-right icon-state ">
                                                        <label class="switch">
                                                            <input type="checkbox" id="status" model_id="{{$c->id}}"
                                                                   @if($c->suspend == 0)  checked @endif><span
                                                                    class="switch-state bg-success"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
{{--                                    <td>--}}
{{--                                        @if(auth()->user()->hasPermissionTo('تفعيل متجر'))--}}
{{--                                        <div class="nav-right col p-0">--}}
{{--                                            <div class="media">--}}
{{--                                                <div class="media-body text-right icon-state">--}}
{{--                                                    <label class="switch">--}}
{{--                                                        <input type="checkbox" id="verified" model_id="{{$c->id}}"--}}
{{--                                                               @if($c->verified == 1)  checked @endif><span--}}
{{--                                                                class="switch-state bg-success"></span>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        @endif--}}
{{--                                    </td>--}}
                                    <td>{{$c->name}}</td>
                                    @if($c->image)
                                        <td>
                                            <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#image{{$c->id}}" style="padding: 1px">
                                                <img src="{{$c->image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif
                                    @if($c->cover_image)

                                        <td>
                                            <button title="عرض" type="button" class="btn btn-danger" data-toggle="modal" data-target="#cover_image{{$c->id}}" style="padding: 1px">
                                                <img src="{{$c->cover_image}}" width="50px" height="50px"></img>
                                            </button>

                                            {{--==image==--}}
                                            <div class="modal fade" id="cover_image{{$c->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <img src="{{$c->cover_image}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--==image==--}}
                                        </td>
                                    @else
                                        <th> -</th>
                                    @endif
                                    <td>{{$c->phone}}</td>
                                    <td>
                                        {{$c->email}} <br>
{{--                                        <b class="badge badge-danger">{{$c->country->name}}</b>--}}
                                    </td>
                                    <td>
                                        {{$c->comercial_number}} <br>
{{--                                        <b class="badge badge-danger">{{$c->country->name}}</b>--}}
                                    </td>
                                    <td>
                                        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$c->lat}},{{$c->lng}}">
                                            <i class="icon-location-pin " style="font-size: x-large"></i>
                                        </a>
                                    </td>
                                    <td>{{$c->city_name}}</td>

                                    <td>
                                        <b class="badge badge-dark">
                                            {{isset($c->category) ? $c->category->name_ar ." - ". $c->category->name_en : "-"}}
                                        </b>
                                    </td>

                                    <td>
                                        <button title="عرض التفاصيل" type="button" class="btn btn-danger"
                                                data-toggle="modal" data-target="#show{{$c->id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-eye" color="white" data-toggle="modal">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </button>
                                        @if(auth()->user()->hasPermissionTo('تعديل متجر'))
                                        <a title="تعديل" type="button" class="btn btn-warning"
                                                style="margin: 1px"
                                                href="{{ asset('admin/shops/edit/'.$c->id) }}"
{{--                                                data-toggle="modal"--}}
{{--                                                data-target="#edit_{{$c->id}}"--}}
                                        >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @endif
                                        @if(auth()->user()->hasPermissionTo('حذف متجر'))
                                        <button title="حذف" type="button" class="btn btn-danger" data-toggle="modal"
                                                style="margin: 1px"
                                                data-target="#delete_{{$c->id}}">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endif
                                        <a href="{{route('shop.branches',$c->id)}}" >
                                            <button title="الفروع" class="btn btn-secondary">
                                                <i class="fa fa-home"> {{$c->branches_count}}</i>



                                            </button>
                                        </a>

                                        <a href="{{route('shops.orders',$c->id)}}" >
                                            <button title="الطلبات" class="btn btn-secondary">
                                                <i class="fa fa-shopping-basket"> {{$c->all_orders_count}}</i>



                                            </button>
                                        </a>

                                    </td>


                                    {{--///////////////////////////--}}

                                    <div class="modal fade" id="show{{$c->id}}" tabindex="-1" role="dialog"
                                         style="text-align: right"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title" id="exampleModalLabel">تفاصيل المتجر</h1>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <input type="hidden" name="model_id" value="{{$c->id}}">


                                                <div class="modal-body">

                                                    @if($c)
                                                        <h1><i data-feather="menu"></i>
                                                            الوصف بالعربي:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->description_ar}}
                                                        <hr>
                                                        <h1><i data-feather="menu"></i>
                                                            الوصف بالإنجليزي:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->description_en}}
                                                        <hr>

                                                        <h1><i data-feather="menu"></i>
                                                            العنوان:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->address}}
                                                        <hr>

                                                        <h1><i data-feather="user"></i>
                                                            السجل التجاري:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->commercial_register}}
                                                        <hr>

                                                        <h1><i data-feather="user"></i>
                                                            البطاقة الضريبية:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->tax_num}}
                                                        <hr>

                                                        <h1><i data-feather="star"></i>
                                                            اسم البنك و رقم الحساب:
                                                            &nbsp;&nbsp;
                                                        </h1>

                                                        {{$c->bank_name}}
                                                        &nbsp;&nbsp; - &nbsp;&nbsp;
                                                        {{$c->bank_account}}
                                                        <hr>


                                                    @if($c->moderator)
                                                        <h1><i data-feather="user"></i>
                                                            بيانات المشرف:
                                                            &nbsp;&nbsp;
                                                        </h1>
                                                        إسم المشرف:
                                                        &nbsp;&nbsp;
                                                        {{$c->moderator->name}}
                                                        <br>
                                                        إيميل المشرف:
                                                        &nbsp;&nbsp;
                                                        {{$c->moderator->email}}
                                                        <br>

                                                        هاتف المشرف:
                                                        &nbsp;&nbsp;
                                                        {{$c->moderator->phone}}
                                                        <br>

                                                        منصب المشرف:
                                                        &nbsp;&nbsp;
                                                        {{$c->moderator->title}}
                                                        <br>
                                                        <hr>
                                                    @endif
                                                    @endif


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--///////////////////////////--}}

                                    <div class="modal animated fadeIn" id="delete_{{$c->id}}" tabindex="-1"
                                         style="text-align: right"
                                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header btn-danger">
                                                    <h5 class="modal-title" id="exampleModalLabel">حذف المتجر</h5>
                                                    {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                    {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                    {{--                                            </button>--}}
                                                </div>
                                                <form method="post" action="{{route('deleteShop')}}" class="buttons">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <h4>هل انت متأكد ؟</h4>
                                                        <h6>
                                                            انت علي وشك حذف المتجر
                                                            <br>رقم المتجر: ({{$c->id}})
                                                            <br>الإسم: ({{$c->name}})

                                                        </h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="model_id" value="{{$c->id}}">
                                                        <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                            إغلاق
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">تأكيد</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </tr>
                            @endforeach
                            {{--<tbody id="sub_cats_{{$category->id}}"></tbody>--}}
                            </tbody>
                        </table>
                    </div>
{{--                    {{$users->links()}}--}}
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>

    <script>
        $(document).on('change', '#status', function (e) {

            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editShopStatus')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        $(document).on('change', '#verified', function (e) {

            var model_id = $(this).attr('model_id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{URL::route('editShopVerified')}}",
                data: {
                    model_id: model_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (response) {
                    location.reload();
                    if (response.success) {
                        toastr.success(response.success);
                    } else if (response.warning) {
                        toastr.warning(response.warning);
                    } else {
                        toastr.error(response.error);
                    }
                },
                error: function (jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

    </script>


@endsection
