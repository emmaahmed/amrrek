<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/style.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/responsive.css">
    <title>أمرك</title>
    <link rel="icon" type="image/png" href="{{asset('landing')}}/assets/img/favicon.png">
</head>

<body>
    <div class="preloader">
        <div class="loader">
            <div class="loader-circle"></div> <span>  Amrrek</span>
        </div>
    </div>
    <div class="coming-soon-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="coming-soon-content wow fadeInUp" data-wow-delay="0s">
                        <div class="logo"> <a href="index.html"><img src="{{asset('landing')}}/assets/img/logo.png" alt="image"></a></div>
                        <h5> Coming SOON ..........</h5>
                        <h1>  Amrrek application </h1>
                        <p> 
                            
                            Amrrek is the giant delivery platform in the kingdom. Amrrek is a unique on-demand experience earning the highest user ratings among all the other largest delivery apps. It's not  the first but it consider to be the best App that delivers everything you can imagine from all types of stores, restaurants and, covers all areas of business in Saudi Arabia & GCC country . Amrrek services expansion reached to Middle East and north of Africa, and soon it will be in other countries in the region.

                        </p>
                        <div class="btn-box"> <button class="btn btn-primary get-more-info-btn">contact us <i
                                    class="fas fa-chevron-right"></i></button></div>
                    </div>
                    <div id="timer" class="wow fadeIn">
                        <div id="days"></div>
                        <div id="hours"></div>
                        <div id="minutes"></div>
                        <div id="seconds"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="shape1"><img src="{{asset('landing')}}/assets/img/circle1.png" alt="image"></div>
        <div class="shape2"><img src="{{asset('landing')}}/assets/img/circle2.png" alt="image"></div>
    </div>
    <div class="sidebar-modal">
        <div class="sidebar-modal-inner wow fadeInRight">
            <div class="about-area wow fadeInRight">
                <div class="title">
                    <h2 class="primary-color">About Amrrek </h2>
                    <p> 
                        Amrrek is the giant delivery platform in the kingdom. Amrrek is a unique on-demand experience earning the highest user ratings among all the other largest delivery apps. It's not  the first but it consider to be the best App that delivers everything you can imagine from all types of stores, restaurants and, covers all areas of business in Saudi Arabia & GCC country . Amrrek services expansion reached to Middle East and north of Africa, and soon it will be in other countries in the region.

Amrrek is not only a delivery app. It is your commander that delivers everything you order. It is close to you all time and covers all you needs “Restaurants, Stores, Groceries, Water, Auto parts, Laundry, Clothes, Home Services, Accessories, Technicians, Mechaniary , Business support , and even if you look for something somewhere you can command Amrrek to bring it to your place.
 
                        
                    </p>
                </div>
            </div>
            <div class="about-area wow fadeInRight">
                <div class="title">
                    <h2 class="primary-color">Amrrek Advantages </h2>
                    <p> 
                        Amrrek is the giant delivery platform in the kingdom. Amrrek is a unique on-demand experience earning the highest user ratings among all the other largest delivery apps. It's not  the first but it consider to be the best App that delivers everything you can imagine from all types of stores, restaurants and, covers all areas of business in Saudi Arabia & GCC country . Amrrek services expansion reached to Middle East and north of Africa, and soon it will be in other countries in the region.

Amrrek is not only a delivery app. It is your commander that delivers everything you order. It is close to you all time and covers all you needs “Restaurants, Stores, Groceries, Water, Auto parts, Laundry, Clothes, Home Services, Accessories, Technicians, Mechaniary , Business support , and even if you look for something somewhere you can command Amrrek to bring it to your place.
 
                        
                    </p>
                </div>
            </div>
            <div class="contact-area wow fadeInRight">
                <div class="title">
                    <h2 class="primary-color">Contact us </h2>
                </div>
                <div class="contact-info">
                    <div class="contact-info-content">
                        <h3>  Contact us by phone or mail </h3>
                        <h2> <a href="tel:964498894848484+">9665003211599167+</a> <span>OR</span> <a
                                href="mailto:info@Amrrek.com.sa">Info@Amrrek.com.sa</a></h2>
                        <ul class="social">
                            <li><a href="#" target="_blank"><i
                                        class="fab fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank"><i
                                        class="fab fa-facebook-f"></i></a></li>
                        </ul>
                       
                    </div>
                </div>
            </div><span class="close-btn sidebar-modal-close-btn"><i class="fas fa-times"></i></span>
        </div>
    </div>
    <script src="{{asset('landing')}}/assets/js/jquery.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/wow.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/popper.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/form-validator.min.js"></script>
    <script src="{{asset('landing')}}/assets/js/contact-form-script.js"></script>
    <script src="{{asset('landing')}}/assets/js/main.js"></script>

{{--    <!--Start of Tawk.to Script-->--}}
{{--    <script type="text/javascript">--}}
{{--    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
{{--    (function(){--}}
{{--        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
{{--        s1.async=true;--}}
{{--        s1.src='https://embed.tawk.to/60a389ab185beb22b30e4987/1f5vd3i2l';--}}
{{--        s1.charset='UTF-8';--}}
{{--        s1.setAttribute('crossorigin','*');--}}
{{--        s0.parentNode.insertBefore(s1,s0);--}}
{{--    })();--}}

{{--    <!--End of Tawk.to Script-->--}}
{{--</script>--}}
</body>

</html>