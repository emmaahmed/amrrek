<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/style.css">
    <link rel="stylesheet" href="{{asset('landing')}}/assets/css/responsive.css">
    <title>أمرك</title>
    <link rel="icon" type="image/png" href="{{asset('landing')}}/assets/img/favicon.png">
</head>

<body>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/60a389ab185beb22b30e4987/1f5vd3i2l';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();

    <!--End of Tawk.to Script-->
</script>
</body>

</html>