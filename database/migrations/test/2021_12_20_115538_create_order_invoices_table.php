<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable()->unsigned();
            $table->double('total')->nullable()->default(0);
            $table->double('sub_total')->nullable()->default(0);
            $table->double('tax')->nullable()->default(0);
            $table->double('offer')->nullable()->default(0);
            $table->double('tax_percentage')->nullable()->default(0.1);
            $table->tinyInteger('confirmed')->nullable()->default(0)->comment('0=>not confirmed yet ,1=>confirmed');
            $table->timestamps();
            if (Schema::hasTable('orders')) {
                $table->foreign('order_id')->references('id')->on('orders')->cascadeOnDelete()->cascadeOnUpdate();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_invoices');
    }
}
