<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('attribute_id')->nullable();
            $table->string('attribute_type')->nullable();
            $table->string('transaction_id')->nullable();
            $table->tinyInteger('status')->comment('0=>failed,1=>accept')->nullable();
            $table->tinyInteger('type')->comment('0=>wallet,1=>subscription,2=>pay debit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
