<?php

namespace App\Providers;

use App\Models\Department;
use App\Models\Favorite;
use App\Models\Notification;
use App\Modules\Front\Models\ContactInfo;
use App\Modules\Website\Models\Section;
use App\Modules\Website\Models\WebsiteSetting;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');

use Neodynamic\SDK\Web\WebClientPrint;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // App::setLocale('ar');
        ini_set('serialize_precision', 14);
        ini_set('precision', 14);
        Schema::defaultStringLength(191);
        view()->composer('*', function($view){
            $wcpScript = WebClientPrint::createScript(route('print.process'), route('print.commands'), Session::getId());
            view()->share('wcpScript', $wcpScript);

            $settings = WebsiteSetting::first();
                view()->share('settings', $settings);
                $about = Section::whereId(8)->first();

                view()->share('about', $about);
                $allServices = Department::select('id','name as slug','name_'.app()->getLocale().' as name')->get();
                if(Auth::guard('shop')->user()){
                    $notifications=Notification::where(['type'=>5,'user_id'=>Auth::guard('shop')->user()->shop_id])->orderBy('created_at','desc')->take(5)->get();
                    view()->share('notifications', $notifications);

                }
                view()->share('allServices', $allServices);

        });

        $raw_locale = session()->get('locale');
        // dd($raw_locale);
        if (in_array($raw_locale, Config::get('app.locales'))) {
            $locale = $raw_locale;
        }
        else $locale = Config::get('app.locale');
        App::setLocale($locale);
    }
}
