<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class ProductsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Product
     */
    public function collection(Collection $rows)
    {
//        dd($rows);
//        Validator::make($rows->toArray(), [
//            '*.rkm_alkaem' => 'required',
//            '*.alsaar' => 'required',
//            '*.alesm_balaarby' => 'required',
//            '*.alesm_balenjlyzy' => 'required',
//        ])->validate();

        $spreadsheet = IOFactory::load(request()->file('excel'));
        $i = 0;
        $rowIndex = 0;

        foreach ($spreadsheet->getActiveSheet()->getDrawingCollection() as $drawing) {
            if ($drawing instanceof MemoryDrawing) {
                ob_start();
                call_user_func(
                    $drawing->getRenderingFunction(),
                    $drawing->getImageResource()
                );
                $imageContents = ob_get_contents();
                ob_end_clean();
                switch ($drawing->getMimeType()) {
                    case MemoryDrawing::MIMETYPE_PNG :
                        $extension = 'png';
                        break;
                    case MemoryDrawing::MIMETYPE_GIF:
                        $extension = 'gif';
                        break;
                    case MemoryDrawing::MIMETYPE_JPEG :
                        $extension = 'jpg';
                        break;
                }
            }
            else {
                $zipReader = fopen($drawing->getPath(), 'r');
                $imageContents = '';
                while (!feof($zipReader)) {
                    $imageContents .= fread($zipReader, 1024);
                }
                fclose($zipReader);
                $extension = $drawing->getExtension();
            }

            $myFileName = time() .$rowIndex. '.' . $extension;
            file_put_contents('public/uploads/products/' . $myFileName, $imageContents);

            $product = Product::create([
                'menu_id' => $rows[$rowIndex]['rkm_alkaem'],
                'shop_id' => Auth::guard('shop')->user()->shop_id,
                'price_before' => $rows[$rowIndex]['alsaar'],
                'price_after' => $rows[$rowIndex]['alsaar'],
//                'has_sizes' => Str::slug($row['has_sizes']),
                'name_ar' => $rows[$rowIndex]['alesm_balaarby'],
                'name_en' => $rows[$rowIndex]['alesm_balenjlyzy'],
                'description_ar' => $rows[$rowIndex]['alosf_balaarby'],
                'description_en' => $rows[$rowIndex]['alosf_balenjlyzy'],
                'image' => $myFileName,
            ]);
            if ($rows[$rowIndex]['aadd_aledafat']>0){
                for($i=1; $i<=$rows[$rowIndex]['aadd_aledafat']; $i++) {
                    $variation = $product->variations()->create([
                        'type' => $rows[$rowIndex][$i.'noaa_alakhtyar'],
                        'required' => $rows[$rowIndex][$i.'ajbary']=='إجباري' ? 1 : 0,
                        'name_ar' => $rows[$rowIndex][$i.'aledaf_balaarby'],
                        'name_en' => $rows[$rowIndex][$i.'aledaf_balenjlyzy'],
                    ]);
                    if (isset($rows[$rowIndex][$i.'alekhtyarat_balaarby'])) {
                        $optionsAr=explode(',',$rows[$rowIndex][$i.'alekhtyarat_balaarby']);
                        $optionsEn=explode(',',$rows[$rowIndex][$i.'alekhtyarat_balenjlyzy']);
                        $optionsPrices=explode(',',$rows[$rowIndex][$i.'asaaar_alekhtyarat']);
                           for($j=1;$j<count($optionsAr); $j++){
                            $variation->options()->create([
                                'name_ar' => isset($optionsAr[$j]),
                                'name_en' => isset($optionsEn[$j]),
                                'price' => isset($optionsPrices[$j]),
                                'type' => 0,

                            ]);
                        }
                    }
                }
            }
            $rowIndex++;
        }

        return $product;
    }
}
