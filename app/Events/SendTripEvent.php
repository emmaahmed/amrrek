<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendTripEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $trip;
    private $jwt;
    private $status;
    public $driver;

    public function __construct($trip, $jwt, $driver,$status=0)
    {
        $this->trip = $trip;
        $this->jwt = $jwt;
        $this->driver = $driver;
        $this->status = $status;

        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //.$this->data['jwt']
        return new Channel('trip-' . $this->jwt);
    }

    public function broadcastWith()
    {
        $data['trip']=$this->trip;
        if($this->status!=0) {
            $data['trip']['status'] = $this->status;
        }
        $data['trip']['driver']=$this->driver;
        $data['trip']['car_info']=[
                'car_image' => $this->driver->driver_documents ? $this->driver->driver_documents->front_car_image : null,
                'car_color' => $this->driver->car_color,
                'color_name' => $this->driver->color_name,
                'car_num' => $this->driver->car_num,
                'car_model' => $this->driver->car_model,];

      return $data;

    }
}
