<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrdersChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $message;
    private $jwt;
    public $delegateImage;
    public $userImage;
    public $departmentId;
    public $confirmAcceptOrder;
    public $messages_image;
    public $invoice;
    public function __construct($message,$jwt,$delegateImage,$userImage,$departmentId,$confirmAcceptOrder,$messages_image=null,$invoice=null)
    {
        $this->message=$message;
        $this->jwt=$jwt;
        $this->delegateImage=$delegateImage;
        $this->userImage=$userImage;
        $this->departmentId=$departmentId;
        $this->confirmAcceptOrder=$confirmAcceptOrder;
        $this->messages_image=$messages_image;
        $this->invoice=$invoice;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('order-chat-'.$this->jwt);
    }
    public function broadcastWith()
    {
//        $data=array();
        $data['message']=$this->message;
        $data['message']['delegate_image']=$this->delegateImage;
        $data['message']['user_image']=$this->userImage;
        $data['message']['department_id']=$this->departmentId;
        $data['message']['confirm_accept_order']=$this->confirmAcceptOrder;
        $data['message']['messages_image']= $this->messages_image;
        $data['message']['invoice']= $this->invoice;
        ;
        return $data;
    }

}
