<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrdersEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $order;
    public $user;
    private $jwt;
    private $confirm_order;
    public function __construct($order,$jwt,$user,$confirm_order=0)
    {
        $this->order=$order;
        $this->user=$user;
        $this->jwt=$jwt;
        $this->confirm_order=$confirm_order;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('order-'.$this->jwt);
    }
//    public function broadcastWith()
//    {
//        $data=$this->order;
//        $data['user']=$this->user;
//         return $data;
//    }

}
