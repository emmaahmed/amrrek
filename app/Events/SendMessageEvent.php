<?php

namespace App\Events;

use App\Models\Driver;
use App\Models\MessageImage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $message;
    private $jwt;
    public function __construct($message,$jwt)
    {
        $this->message=$message;
        $this->jwt=$jwt;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('message-'.$this->jwt);
    }
    public function broadcastWith()
    {

        $message['message']=$this->message;
        if ($this->message->sender_type == 0) {
            $message['message']['sender_image'] =
                User::where("id", $this->message->sender_id)->first()?User::where("id", $this->message->sender_id)->first()->image:'';
            $message['message']['receiver_image'] =
                Driver::where("id", $this->message->sender_id)->first()?Driver::where("id", $this->message->sender_id)->first()->image:'';
        } else {
            $message['message']['sender_image'] =
                Driver::where("id", $this->message->sender_id)->first()?Driver::where("id", $this->message->sender_id)->first()->image:'';
            $message['message']['receiver_image'] =
                User::where("id", $this->message->sender_id)->first()?User::where("id", $this->message->sender_id)->first()->image:'';
        }


        if ($this->message->message == null) {
            $image = MessageImage::where('message_id', $message->id)->first();
            $message['message']['image'] = isset($image) ? $image->image : '';
        } else {
            $message['message']['image'] = "";
        }

        return $message;
    }

}
