<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OffersEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $offer;
    public $delegate;
    public $lower_offers_counter;
    public $delivery_time;
    public $orders_count;
    public $comments_count;
    private $jwt;
    public function __construct($offer,$jwt,$delegate,$lower_offers_counter,$delivery_time,$orders_count=0,$comments_count=0)
    {
        $this->offer=$offer;
        $this->jwt=$jwt;
        $this->delegate=$delegate;
        $this->lower_offers_counter=$lower_offers_counter;
        $this->delivery_time=$delivery_time;
        $this->orders_count=$orders_count;
        $this->comments_count=$comments_count;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('offers-'.$this->jwt);
    }
    public function broadcastWith()
    {
//        $data=array();
        $data['offer']=$this->offer;
        $data['offer']['delegate']=$this->delegate;
        $data['offer']['delegate']['orders_count']=$this->orders_count;
        $data['offer']['delegate']['comments_count']=$this->comments_count;
        $data['offer']['lower_offers_counter']=$this->lower_offers_counter;
        $data['offer']['delivery_time']=$this->delivery_time;
//       dd($data);
        return $data;
    }
}
