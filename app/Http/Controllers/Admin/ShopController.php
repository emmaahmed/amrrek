<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ShopsExport;
use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\AboutUs;
use App\Models\Admin;
use App\Models\AppExplanation;
use App\Models\Category;
use App\Models\Country;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Shop;
use App\Models\ShopModerator;
use App\Models\ShopRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $type='المتاجر المفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->with('country')
            ->withCount('branches')
            ->withCount('allOrders')
            ->where('suspend',0)
            ->paginate(10);
        return view('cp.shops.index',['users'=>$users,'categories'=>$categories,'type'=>$type,'accept'=>0]);

    }
    public function branches($shop_id){
        $users=Shop::orderBy('id','desc')->where('parent_id',$shop_id)->with('country')
            ->where('suspend',0)
            ->withCount('allOrders')

            ->paginate(10);
        return view('cp.shops.branches',['users'=>$users]);

    }
    public function createShop(Request $request){
        $categories  = Category::get();
        $countries  = Country::where('active',1)->get();
        return view('cp.shops.create',[
            'categories'=>$categories,
            'countries'=>$countries,
        ]);
    }

    public function store(Request $request){

        $this->validate($request,[
            'email' => 'unique:shops,email',
            'moderator_email' => 'unique:admins,email'
        ]);
        $city=getCityLocation($request->lat,$request->lng);
        $request['city_name']=$city;

        $shop = Shop::create($request->all());
        $shopModerator = Admin::create([
            'shop_id' => $shop->id,
            'jwt' => generateJWT(),
            'active' => 1,
            'type' => 1,
            'name' => $request->moderator_name,
            'email' => $request->moderator_email,
            'phone' => $request->moderator_phone,
            'password' => $request->password,
        ]);
        $permissions = Permission::where('id','>', 103)->pluck('id')->toArray();
        $shopModerator->givePermissionTo($permissions);
//        $role3 = Role::create(['name' => 'Shop Admin','guard_name' => 'shop']);
//        $shopModerator->assignRole($role3);
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect(asset('admin/shops'));
        //return back()->with('success','Data stored successfully');
    }

    public function editShop(Request $request,$shop_id){
        $shop=Shop::where('id', $shop_id)->first();
        $categories  = Category::get();
        $countries  = Country::where('active',1)->get();
        return view('cp.shops.edit',[
            'categories'=>$categories,
            'countries'=>$countries,
            'shop'=>$shop
        ]);

    }

    public function updateShop(Request $request){
        $c=Shop::where('id', $request->model_id)->first();
        $c->update([
            'image' => $request->image,
            'cover_image' => $request->cover_image,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'address' => $request->address,
            'name' => $request->name
        ]);
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect()->back();
//        return back()->with('success','Data updated successfully');
    }


    public function activeShops(){
        $type='المتاجر المفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('suspend',0)
            ->withCount('branches')
            ->withCount('allOrders')

            ->paginate(10);
        return view('cp.shops.index',['users'=>$users,'categories'=>$categories,'type'=>$type,'accept'=>0]);
    }

    public function inactiveShops(){
        $type='المتاجر الغير مفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('suspend',1)
            ->paginate(10);
        return view('cp.shops.index',['users'=>$users,'categories'=>$categories,'type'=>$type,'accept'=>1]);
    }

    public function editShopStatus(Request $request)
    {

        $cat=Shop::where("id",$request->model_id)->first();
        if($cat->suspend == 1){
            Shop::where("id",$request->model_id)
                ->update(["suspend" => 0 ]);
        }else{
            Shop::where("id",$request->model_id)
                ->update(["suspend" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function editShopVerified(Request $request)
    {

        $cat=Shop::where("id",$request->model_id)->first();
        if($cat->verified == 1){
            Shop::where("id",$request->model_id)
                ->update(["verified" => 0 ]);
        }else{
            Shop::where("id",$request->model_id)
                ->update(["verified" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function delete_shop(Request $request)
    {
        Shop::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function shopRequests(){
        $type='طلبات إنضمام المتاجر';
        $users = ShopRequest::orderBy('id','desc')
            ->with('category')
            ->get();
        return view('cp.shops.shop_requests',['users'=>$users,'type'=>$type]);

    }

    public function editRequestStatus(Request $request)
    {

        $cat=ShopRequest::where("id",$request->model_id)->first();
        if($cat->active == 1){
            ShopRequest::where("id",$request->model_id)
                ->update(["active" => 0 ]);
        }else{
            ShopRequest::where("id",$request->model_id)
                ->update(["active" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }
    protected function orders($shop_id)
    {
        $type="كل الطلبات";

        $orders = Order::orderBy('id','desc')
            ->where('shop_id',$shop_id)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess")
            ->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
//            $order->order_products = OrderProduct::where('order_id',$order->id)->get();
//            if($order->order_products){
//                foreach ($order->order_products as $order_product){
//                    $order_product->order_product_variations = OrderProductVariation::where('order_product_id',$order_product->id)->get();
//                    if($order_product->order_product_variations){
//                        foreach ($order_product->order_product_variations as $order_product_variation){
//                            $order_product_variation->order_product_variation_options = OrderProductVariationOption::where('order_product_var_id',$order_product_variation->id)->get();
//                        }
//                    }
//                }
//            }
        }
//        dd($orders);
        return view('cp.shops.orders',compact('orders','type','shop_id'));
    }
    protected function searchOrders(Request $request){
        $type = "كل الطلبات";
        $shop_id=$request->shop_id;
        $orders = Order::where('shop_id',$request->shop_id)
            ->where('department_id',$request->department_id)

            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess");
        if($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $orders->where('created_at', '>=', $from);



        }
        if($request->to){
            $to = date('Y-m-d', strtotime($request->to));
            $orders->where('created_at', '<=', $to);

        }
        if($request->status){

            if($request->status!='all') {
                $orders->whereHas('order_status', function ($query) use ($request) {
                    $query->where($request->status, '!=', null);
                });
            }

        }



        $orders=$orders->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        return   $orders;
        return view('cp.shops.orders',compact('orders','type','shop_id'));

    }
    public function destroyRequest(Request $request){
        ShopRequest::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();

    }
    public function shopsByArea(){
        $type='المتاجر حسب المناطق';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('suspend',0)
            ->withCount('branches')
            ->withCount('allOrders')

            ->get();
        $areas = Shop::where('suspend',0)->where('city_name','!=',null)->select('city_name')->distinct()->get();

        return view('cp.shops.shops-by-area',['users'=>$users,'categories'=>$categories,'type'=>$type,'areas'=>$areas]);
    }
    public function shopsByAreaSearch(Request $request){
        $type='المتاجر حسب المناطق';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('suspend',0)
            ->withCount('branches')
            ->withCount('allOrders')
            ->where('city_name',$request->city)

            ->get();
        $areas = Shop::select('city_name')->distinct()->get();

        return view('cp.shops.shops-by-area',['users'=>$users,'categories'=>$categories,'type'=>$type,'areas'=>$areas]);
    }
    public function downloadShops($accept){
        return Excel::download(new ShopsExport($accept), 'المتاجر.xlsx');

    }

}
