<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gov;
use App\Models\NationalType;
use Illuminate\Http\Request;
use App\Models\Country;
use DB;
use Route;
use Session;


class NationalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $countries = NationalType::orderBy('id','desc')
            ->get();
        return view('cp.nationals.index',[
            'countries'=>$countries,
        ]);

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name_ar' => 'required|unique:national_types,name_ar',
            'name_en' => 'required|unique:national_types,name_en',
            'num' => 'required|int',
            'type' => 'required|in:0,1,2',

        ]);
        $add            = new NationalType();
        $add->name_ar   = $request->name_ar;
        $add->name_en   = $request->name_en;
        $add->type   = $request->type;
        $add->num   = $request->num;

        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country added successfully');
    }



    public function editNational(Request $request){
//        $this->validate($request,[
//            'name_ar' => 'required|unique:national_types,name_ar',
//            'name_en' => 'required|unique:national_types,name_en',
//        ]);
        $c=NationalType::where('id', $request->country_id)->first();
        $c->update($request->all());

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function editNationalStatus(Request $request,$id)
    {

        $cat=NationalType::where("id",$id)->first();
        if($cat->active == 1){
            NationalType::where("id",$id)
                ->update(["active" => 0 ]);
        }else{
            NationalType::where("id",$id)
                ->update(["active" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

}
