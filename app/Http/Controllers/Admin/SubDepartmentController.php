<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\CarType;
use App\Models\Category;
use App\Models\City;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\SubDepartmentCarType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class SubDepartmentController extends Controller
{
    //protected $categoryRepository;
//    public function __construct(CategoryRepositoryInterface $categoryRepository)
//    {
//        $this->categoryRepository = $categoryRepository;
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::orderBy('id','asc')
            ->where('id',3)
            ->orWhere('id',5)
            ->get();
        $carTypes = CarType::select('id','name_ar as name')->get();

        $sub_departments = SubDepartment::withCount('orders')->get();
        foreach ($sub_departments as $department){
            $department['carTypesIds']=$department->carTypes()->select('car_type_id')->pluck('car_type_id')->toArray();
            $department['cars']=CarType::whereIn('id',$department['carTypesIds'])->select('name_ar as name')->get();
        }

        return view('cp.sub_departments.index',compact('departments','sub_departments','carTypes'));

    }

    public function getSubDepartments($department_id)
    {
        $departments = Department::orderBy('id','asc')
            ->where('id',$department_id)
            ->get();

        $sub_departments = SubDepartment::where('department_id',$department_id)->withCount('orders')->with('carTypes')
            ->get();
        foreach ($sub_departments as $department){
            $department['carTypesIds']=$department->carTypes()->select('car_type_id')->pluck('car_type_id')->toArray();
            $department['cars']=CarType::whereIn('id',$department['carTypesIds'])->select('name_ar as name')->get();
        }

        if($department_id==5) {
            $carTypes = CarType::select('id','name_ar as name')->where('type', 1)->get();
        }
        else{
            $carTypes = CarType::select('id','name_ar as name')->where('type', 0)->get();

        }

        return view('cp.sub_departments.index',compact('departments','sub_departments','carTypes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

//    public function storeMainCat(Request $request)
//    {
//        $this->validate($request,[
//            'ar_name' => 'required|unique:categories',
//            'en_name' => 'required|unique:categories',
//            'image' => 'required'
//        ]);
//        $this->categoryRepository->storeMainCat($request);
//        $this->index();
//    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
//            'image' => 'required',

        ]);
//        return $request->car_types;
        $add                    = new SubDepartment();
        $add->name_ar              = $request->name_ar;
        $add->name_en              = $request->name_en;
        $add->has_processing       = isset($request->has_processing) ? 1 :0;
//        $add->image             = $request->image;
        $add->department_id     = $request->department_id;

        $add->save();
        if(isset($request->car_types) && count($request->car_types)>0){
            for ($i=0; $i<count($request->car_types); $i++) {
                SubDepartmentCarType::create([
                    'car_type_id' =>$request->car_types[$i],
                    'sub_department_id' => $add->id,
                ]);
            }
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','City added successfully');

    }


    public function editSubDepartment(Request $request)
    {
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
        ],[
            'name_ar.required' => 'Arabic text is required',
            'name_en.required' => 'English text is required',
        ]);
        $c=SubDepartment::where('id', $request->model_id)->with('carTypes')->first();
        $c->update($request->except('has_processing'));
        $c->update([
            "has_processing" => isset($request->has_processing) ? 1 :0
        ]);
        if(isset($request->car_types) && count($request->car_types)>0){
            for ($i=0; $i<count($request->car_types); $i++) {
               $c->carTypes()->delete();
                for ($i=0; $i<count($request->car_types); $i++) {
                    SubDepartmentCarType::create([
                        'car_type_id' =>$request->car_types[$i],
                        'sub_department_id' => $c->id,
                    ]);
                }

            }
        }

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function editSubDepartmentStatus(Request $request,$id)
    {
        $cat=SubDepartment::where("id",$id)->first();
        if($cat->active == 1){
            SubDepartment::where("id",$id)
                ->update(["active" => 0 ]);
        }else{
            SubDepartment::where("id",$id)
                ->update(["active" => 1 ]);
        }
        return back();
    }
    public function destroy($id){
        SubDepartment::whereId($id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();

    }

}
