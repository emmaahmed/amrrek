<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\OrderRepositoryInterface;
use App\Http\Controllers\Interfaces\Admin\WorkerRepositoryInterface;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductVariation;
use App\Models\OrderProductVariationOption;
use App\Models\OrderStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\OrdersExport;
use App\Exports\CostsExport;
use App\Exports\CostsUserExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class OrderController extends Controller
{

    public function __construct()
    {
    }

    protected function index()
    {
        $type = "كل الطلبات";
        $orders = Order::orderBy('id','desc')
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess")
            ->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
//            $order->order_products = OrderProduct::where('order_id',$order->id)->get();
//            if($order->order_products){
//                foreach ($order->order_products as $order_product){
//                    $order_product->order_product_variations = OrderProductVariation::where('order_product_id',$order_product->id)->get();
//                    if($order_product->order_product_variations){
//                        foreach ($order_product->order_product_variations as $order_product_variation){
//                            $order_product_variation->order_product_variation_options = OrderProductVariationOption::where('order_product_var_id',$order_product_variation->id)->get();
//                        }
//                    }
//                }
//            }
        }
//return $orders;
        return view('cp.orders.all-orders',compact('orders','type'));
    }
    protected function ordersByType($department_id)
    {
        $status=0;
        if($department_id==2) {
            $type = "طلبات المتاجر";
        }
        elseif($department_id==3){
            $type = "طلبات التوصيل";

        }
        else{
            $type = "طلبات النقل الثقيل";

        }
        $orders = Order::orderBy('id','desc')
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess")
            ->where('department_id',$department_id)
            ->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
//            $order->order_products = OrderProduct::where('order_id',$order->id)->get();
//            if($order->order_products){
//                foreach ($order->order_products as $order_product){
//                    $order_product->order_product_variations = OrderProductVariation::where('order_product_id',$order_product->id)->get();
//                    if($order_product->order_product_variations){
//                        foreach ($order_product->order_product_variations as $order_product_variation){
//                            $order_product_variation->order_product_variation_options = OrderProductVariationOption::where('order_product_var_id',$order_product_variation->id)->get();
//                        }
//                    }
//                }
//            }
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }
    protected function edit($id){
       return $order = Order::orderBy('id','desc')
            ->whereId($id)
            ->with(['user','shop','delegate','order_imagess','order_products' => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])->first();
        return view('cp.orders.show',compact('order'));
    }

    protected function acceptOrders()
    {
        $type = "الطلبات المقبولة";
        $status = 1;
        $orderStatusArray = OrderStatus::where('accept','!=',null)
            ->where('on_way','=',null)
            ->where('finished','=',null)
            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->whereIn('id',$orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }

    protected function onwayOrders()
    {
        $type = "طلبات في الطريق";
        $status = 2;

        $orderStatusArray = OrderStatus::where('accept','!=',null)
            ->where('on_way','!=',null)
            ->where('finished','=',null)
            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->whereIn('id',$orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }

    protected function finishedOrders()
    {
        $type = "الطلبات المنتهية";
        $status = 3;

        $orderStatusArray = OrderStatus::where('accept','!=',null)
            ->where('on_way','!=',null)
            ->where('finished','!=',null)
            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->whereIn('id',$orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }

    protected function confirmiedAcceptOrders()
    {
        $type = "طلبات تم تأكيدها";
        $status = 4;

        $orderStatusArray = OrderStatus::
            where('cancelled',null)
            ->pluck('order_id');
        $orderStatusAcceptArray = OrderStatus::where('accept','!=',null)
            ->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->whereIn('id',$orderStatusArray)
            ->whereIn('id',$orderStatusAcceptArray)
            ->where('confirm_accept','>',1)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }
    protected function cancelledOrders()
    {
        $type = "طلبات تم الغائها";
        $status = 5;

        $orderStatusArray = OrderStatus::
            where('cancelled','!=',null)
            ->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->whereIn('id',$orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        dd($orders);
        return view('cp.orders.index',compact('orders','type','status'));
    }
    protected function search(Request $request){
        $type = "كل الطلبات";

        $orders = Order::with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess");
        if($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $orders->where('created_at', '>=', $from);



        }
        if($request->to){
          $to = date('Y-m-d', strtotime($request->to));
            $orders->where('created_at', '<=', $to);

        }
        if($request->status){

            if($request->status!='all') {
                $orders->whereHas('order_status', function ($query) use ($request) {
                    $query->where($request->status, '!=', null);
                });
            }

        }


            $orders=$orders->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        return   $orders;
        return view('cp.orders.all-orders',compact('orders','type'));

        }
    protected function searchStatus(Request $request){
        $type = " الطلبات";
        $status=$request->status;
        // return $request->all();

        $orders = Order::with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess");
        if($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $orders->where('created_at', '>=', $from);



        }
        if($request->to){
          $to = date('Y-m-d', strtotime($request->to));
            $orders->where('created_at', '<=', $to);

        }
        if($request->status){

            if($request->status!='all') {
                $orders->whereHas('order_status', function ($query) use ($request) {
                    $query->where($request->status, '!=', null);
                });
            }

        }


            $orders=$orders->paginate(5);
        foreach($orders as $order){
            $order->order_status = OrderStatus::where('order_id',$order->id)->first();
        }
//        return   $orders;
        return view('cp.orders.index',compact('orders','type','status'));

        }
        public function closeOrder($order_id){
        $orderStatus=OrderStatus::where('order_id',$order_id)->first();
        $orderStatus->update([
            'closed_by'=>Carbon::now()
        ]);
            session()->flash('insert_message','تمت العملية بنجاح');
            return redirect()->back()->with('success','Record added successfully');


        }

    public function downloadOrders(Request $request){
        return Excel::download(new OrdersExport(0,$request), 'الطلبات.xlsx');

    }



}
