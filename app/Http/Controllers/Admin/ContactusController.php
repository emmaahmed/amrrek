<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\ContactUs;
use DB;
use Route;
use Session;


class ContactusController extends Controller
{
    public function index($type){
        $cats = ContactUs::where('type',$type)->where('status',0)->orderBy('id','desc')->get();
        return view('cp.contact_us.index',['cats'=>$cats,'type'=>$type]);

    }
    public function closedContacts($type){
        $cats = ContactUs::where('type',$type)->where('status',1)->orderBy('id','desc')->get();
        return view('cp.contact_us.index',['cats'=>$cats,'type'=>$type]);

    }

    public function deleteContact(Request $request,$id){
        ContactUs::where('id', $id)->forcedelete();
        return back();
    }
    public function close($id){
        $contact=ContactUs::where('id', $id)->first();
        if($contact->status==1){
            $contact->update(['status'=>0]);
        }
        else{
            $contact->update(['status'=>1]);

        }
        return back();
    }

}
