<?php

namespace App\Http\Controllers\Admin;

use App\Exports\DelegatesExport;
use App\Exports\DriversExport;
use App\Exports\TripsExport;
use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\CarLevel;
use App\Models\Category;
use App\Models\ComplainSuggests;
use App\Models\Driver;
use App\Models\Delegate;
use App\Models\DriverCarLevel;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\OrderStatus;
use App\Models\Shop;
use App\Models\ShopDelegate;
use App\Models\ShopRequest;
use App\Models\Trip;
use App\Models\User;
use App\Models\UserWallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Permission;
use DB;
use GuzzleHttp\Client;

class User2Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function acceptShop(Request $request, $id)
    {
        $shop_request = DB::table('shop_requests')
            ->whereId($id)->first();

        //dd($shop_request);
        $shop = new Shop();
        $shop->name = $shop_request->name;
        $shop->email = $shop_request->email;
        $shop->phone = $shop_request->phone;
        $shop->category_id = $shop_request->category_id;
        $shop->tax_num = $shop_request->tax_num;
        $shop->tax_img = $shop_request->tax_img;
//        $shop->image = $shop_request->image;
//        $shop->cover_image = $shop_request->cover_image;
        $shop->city_name = $shop_request->city;
        $shop->lat = $shop_request->lat;
        $shop->lng = $shop_request->lng;
        $shop->address = $shop_request->address;
        $shop->description_ar = $shop_request->description_ar;
        $shop->description_en = $shop_request->description_en;
        $shop->commercial_register = $shop_request->commercial_register;
        $shop->bank_name = $shop_request->bank_name;
        $shop->bank_account = $shop_request->bank_account;
        $shop->password = $shop_request->password ? Hash::make($shop_request->password) : Hash::make('123456');
        $shop->save();
        DB::table('shops')->whereId($shop->id)->update([
            'image' => $shop_request->image,
            'cover_image' => $shop_request->cover_image
        ]);


        //
        $shopModerator = new Admin();
        $shopModerator->shop_id = $shop->id;
        $shopModerator->jwt = generateJWT();
        $shopModerator->active = 1;
        $shopModerator->type = 1;
        $shopModerator->name = $shop_request->moderator_name;
        $shopModerator->email = $shop_request->moderator_email;
        $shopModerator->phone = $shop_request->moderator_phone;
        $shopModerator->title = $shop_request->moderator_title;
        //$shopModerator->password = $shop_request->password ? Hash::make($shop_request->password) : Hash::make('123456');
        $shopModerator->save();
        DB::table('admins')->whereId($shopModerator->id)->update([
            'password' => $shop_request->password
        ]);
//        $shopModerator = Admin::create([
//            'shop_id' => $shop->id,
//            'jwt' => generateJWT(),
//            'active' => 1,
//            'type' => 1,
//            'name' => $shop_request->moderator_name,
//            'email' => $shop_request->moderator_email,
//            'phone' => $shop_request->moderator_phone,
//            'password' => $shop_request->password,
//        ]);
        $permissions = Permission::where('id', '>', 103)->pluck('id')->toArray();
        $shopModerator->givePermissionTo($permissions);
        if ($shop && $shopModerator) {
            DB::table('shop_requests')
                ->whereId($id)->update(['accept' => 1]);
//            $shop_request->accept = 1;
//            $shop_request->save();
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return redirect(asset('admin/shops'));
    }

    public function index()
    {
        $users = User::orderBy('id', 'desc')
            ->get();

        return view('cp.users.index', ['users' => $users]);

    }

    public function index2()
    {

        $users = Driver::orderBy('id', 'desc')
            ->where('accept', 1)
            ->with('country')
            ->with('driver_car_levels')
            ->paginate(10);
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.drivers.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 1

        ]);

    }

    public function driversByArea()
    {

        $users = Driver::orderBy('id', 'desc')
//            ->where('accept',1)
            ->with('country')
            ->with('driver_car_levels')
            ->paginate(10);
        $carLevels = CarLevel::where('active', 1)->get();
        $areas = Driver::select('city')->distinct()->get();

        return view('cp.drivers.drivers-by-area', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas
        ]);

    }

    public function driversByAreaSearch(Request $request)
    {

        $users = Driver::orderBy('id', 'desc')
//            ->where('accept',1)
            ->with('country')
            ->where('city', $request->city)
            ->with('driver_car_levels')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        $areas = Driver::select('city')->distinct()->get();

        return view('cp.drivers.drivers-by-area', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas
        ]);

    }

    public function index3()
    {

        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 0)
            ->where('accept', 1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 1
        ]);
    }

    public function index3Heavey()
    {

        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 1)
            ->where('accept', 1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->with('car_type')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates_heavey.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 1

        ]);
    }

    public function editCarLevel(Request $request)
    {
        Driver::whereId($request->user_id)
            ->update(["car_level" => $request->car_level]);

        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function accept_driver(Request $request, $id)
    {
        Driver::where('id', $id)
            ->update([
                "accept" => 1,
                "files_completed" => 1,
            ]);
        $driver = Driver::where('id', $id)->first();
        $title = "تم قبول طلبكم كسائق في تطبيق أمرك";
        $message = "تم قبول طلبكم كسائق في تطبيق أمرك";
        Notification::sendNotifyTo($driver->jwt, $title, $message, 4, 0, 0, 'drivers');

//        Notification::send($driver->token, $title,
//            $message, 0, 1,
//            null, NULL, NULL, null);
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back()->with('success', 'Record updated successfully');
    }

    public function accept_delegate(Request $request, $id)
    {
        Delegate::where('id', $id)
            ->update([
                "accept" => 1,
                "files_completed" => 1,
            ]);

        $delegate = Delegate::where('id', $id)->first();
        $title = "تم قبول طلبكم كمندوب في تطبيق أمرك";
        $message = "تم قبول طلبكم كمندوب في تطبيق أمرك";
        $headers = [
            "Authorization" => "Bearer KLJew3bKpkx9DxixsosqzZY4QZI9mB7pEnKZkAo6auT7C1b7KBqLq29aimpDB-R0EvjhDa-aXttwyk7s0kkenahjFPP6E2sZLvlODeqxRNyxmVdf-JCrws6t-K_sxu6ksAQimg9bkEMimV_8m-ek2o68helzvSy0cROKbcM7vKajou8W6TlK5hspviqrEZoBWGyeuCPXTkfl004HUwj9diWudQlkaG7X_17KFUJLeypgwXN86NTz8tF14v4F4QDG1xv0Xe13Gm5z35Xk-iIABVeeQ6tWfN4gAJtTdBACl5EYmlT42Uxjv9-BZkhp5t0T9_QqpR9A9yXg3YEOXRVuYRZYnmn3h0plyzHytlQ-uUNqIvw_Cy6I3azY2Runyqalsb3-iyowZaXJWdjzesNnkiSd7ThrUViYVdXaA8JXXpqDZJLjHzY9Zx_Iflrb5KAqDA-uhD5C8TfiwB_t864KXNJx1RPqL3srYvZVLRzs-tbByo_anK7LWK7vSbwWEhEUahwEo3P3YtbEUhCA2lw5io3fh0WSFbYh77ERAt5fYAfME3u6iegI_JLq6EaU6_CcL-nYTV4-PHz6-v8ATaIxGT0NZMA7XIWNPC2RZV9GXWFE0MsjBYA_f9Gg8TXIgHSPTNyRyZ2H-ZZpqQx42w3r01nk6_E-lH9a1SrwPZNZLYX5ObZnPwle95BwBKOPgVHcW5-St1XpKOHrb90k1CcT0qBvh84",
            "Content-Type" => "application/json"
        ];
        $body= [
            "SupplierName" => $delegate->f_name . " " . $delegate->l_name,
            "Mobile" => $delegate->phone,
            "Email" => $delegate->email,
            //  "CommissionValue"=> 0,
            //  "CommissionPercentage"=> 0,
            //  "DepositTerms"=> "Daily",
            "BankId" => 1,
            "BankAccountHolderName" => $delegate->f_name . " " . $delegate->l_name,
            "BankAccount" => $delegate->bank_account_name?$delegate->bank_account_name:"test",
//            "Iban" => $delegate->bank_account_num,
//            "IsActive" => true,
        ];
//        return $body;

        $client = new Client([
            'headers' => $headers
        ]);
        $url = "https://api-sa.myfatoorah.com/v2/CreateSupplier";

        $res = $client->post($url,['form_params'=>$body]);
//        dd($res->getStatusCode());
        if($res->getStatusCode()==200){
            $response=json_decode($res->getBody()->getContents());
            if($response->Data)
            $delegate->update([
                'supplier_code'=>$response->Data->SupplierCode
            ]);
        }
       Notification::sendNotifyTo($delegate->jwt, $title, $message, 4, 0, 0, 'delegates');

//        Notification::send($driver->token, $title,
//            $message, 0, 1,
//            null, NULL, NULL, null);
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back()->with('success', 'Record updated successfully');
    }

    public function indexAdmin()
    {
        $permissions = Permission::where('id', '<', 104)->get();
        $users = Admin::orderBy('id', 'desc')
            ->where('id', '>', 1)
            ->where('type', 0)
            ->with('permissions')
            ->get();

        return view('cp.admins.index', [
            'users' => $users,
            'permissions' => $permissions,
        ]);
    }

    public function adminsByArea()
    {
        $permissions = Permission::where('id', '<', 104)->get();

        $users = Admin::orderBy('id', 'desc')
            ->where('id', '>', 1)
            ->where('type', 0)
            ->with('permissions')
            ->get();
        $areas = Admin::select('city')->distinct()->get();

        return view('cp.admins.admins-by-areas', [
            'users' => $users,
            'permissions' => $permissions,
            'areas' => $areas

        ]);
    }

    public function adminsByAreaSearch(Request $request)
    {
        $permissions = Permission::where('id', '<', 104)->get();

        $users = Admin::orderBy('id', 'desc')
            ->where('id', '>', 1)
            ->where('type', 0)
            ->where('city', $request->city)
            ->with('permissions')
            ->get();
        $areas = Admin::select('city')->distinct()->get();

        return view('cp.admins.admins-by-areas', [
            'users' => $users,
            'permissions' => $permissions,
            'areas' => $areas

        ]);
    }

    public function createAdminForm()
    {
        $permissions = Permission::where('id', '<', 104)->get();


        return view('cp.admins.create', [
            'permissions' => $permissions,
        ]);

    }

    public function createAdmin(Request $request)
    {
        // dd($request->all());
        $city = getCityLocation($request->lat, $request->lng);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins,email',
            'password' => 'required',
            'phone' => 'required|unique:admins,phone',
            'lat' => 'required',
            'lng' => 'required',
            'permissions' => 'required|array|min:1',
        ]);

        $user = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'jwt' => uniqid(),
            'phone' => $request->phone,
            'active' => 1,
            'token' => uniqid(),
            'lat' => $request->lat,
            'lng' => $request->lng,
            'image' => $request->image,
            'city' => $city,
        ]);
        $user->givePermissionTo($request->permissions);

        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function editAdmin(Request $request)
    {
        $admin = Admin::whereId($request->model_id)->first();
        $admin->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => isset($request->password) ? $request->password : '123456',
            'phone' => $request->phone,
        ]);
        if (isset($request->image))
            $admin->update(['image' => $request->image]);
        $admin->syncPermissions($request->permissions);
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function editAdminStatus(Request $request, $id)
    {
        $cat = Admin::where("id", $id)->first();
        if ($cat->active == 1) {
            Admin::where("id", $id)
                ->update(["active" => 0]);
        } else {
            Admin::where("id", $id)
                ->update(["active" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function deleteAdmin(Request $request, $id)
    {

        Admin::destroy($id);

        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back()->with('success', 'Record deleted successfully');
    }

    public function editClientStatus(Request $request, $id)
    {
        $cat = User::where("id", $id)->first();
        if ($cat->suspend == 1) {
            User::where("id", $id)
                ->update(["suspend" => 0]);
        } else {
            User::where("id", $id)
                ->update(["suspend" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function editDelegateStatus(Request $request, $id)
    {
        $cat = Delegate::where("id", $id)->first();
        if ($cat->suspend == 1) {
            Delegate::where("id", $id)
                ->update(["suspend" => 0]);
        } else {
            Delegate::where("id", $id)
                ->update([
                    "suspend" => 1,
                    "reason_ar" => $request->reason_ar,
                    "reason_en" => $request->reason_en,
                ]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function editReasonStatus(Request $request, $id)
    {
        $cat = Delegate::where("id", $id)->first();
        if ($cat->files_completed == 1) {
            Delegate::where("id", $id)
                ->update([
                    "files_completed" => 0,
                    "reason_ar" => $request->reason_ar,
                    "reason_en" => $request->reason_en,
                ]);
        } else {
            Delegate::where("id", $id)
                ->update(["files_completed" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function editDriverReasonStatus(Request $request, $id)
    {
        $cat = Driver::where("id", $id)->first();
        if ($cat->files_completed == 1) {
            Driver::where("id", $id)
                ->update([
                    "files_completed" => 0,
                    "reason_ar" => $request->reason_ar,
                    "reason_en" => $request->reason_en,
                ]);
        } else {
            Driver::where("id", $id)
                ->update(["files_completed" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }


    public function editDriverStatus(Request $request, $id)
    {
        $cat = Driver::where("id", $id)->first();
        if ($cat->suspend == 1) {
            Driver::where("id", $id)
                ->update(["suspend" => 0]);
        } else {
            Driver::where("id", $id)
                ->update(["suspend" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function driverTrips($driver_id)
    {
        $trips = Trip::where("driver_id", $driver_id)
            ->with('trip_paths')
            ->with('lastPath')
            ->with('promo')
            ->get();
        return view('cp.drivers.show', [
            'trips' => $trips,
            'driver_id' => $driver_id
        ]);
    }

    public function tripsSearch(Request $request)
    {
        $trips = Trip::where("driver_id", $request->driver_id)
            ->with('promo')
            ->with('trip_paths');

        if ($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $trips->where('created_at', '>=', $from);


        }
        if ($request->to) {
            $to = date('Y-m-d', strtotime($request->to));
            $trips->where('created_at', '<=', $to);

        }
        if ($request->status) {

            if ($request->status != 'all') {
                $trips->where('status', $request->status);
            }

        }


        $trips = $trips->paginate(5);
        return view('cp.drivers.show', [
            'trips' => $trips,
            'driver_id' => $request->driver_id

        ]);


    }

    public function finishedDriverTrips($driver_id)
    {
        $trips = Trip::where("driver_id", $driver_id)
            ->where('status', 3)
            ->with('lastPath')
            ->with('promo')
            ->get();
        return view('cp.drivers.show', [
            'trips' => $trips,
            'driver_id' => $driver_id
        ]);
    }

    public function delegateOrders($delegate_id)
    {
        $type = "كل الطلبات";
        $orders = Order::where('delegate_id', $delegate_id)
            ->with('user')
            ->with('promo')
            ->with('delegate')
            ->with('offer')
            ->with('country')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }

        return view('cp.delegates.show', compact('orders', 'type', 'delegate_id'));
    }

    public function editCarLevelFlag(Request $request)
    {
        $cat = DriverCarLevel::where("id", $request->driver_car_level_id)->first();
        if ($cat->flag == 1) {
            DriverCarLevel::where("id", $request->driver_car_level_id)
                ->update(["flag" => 0]);
        } else {
            DriverCarLevel::where("id", $request->driver_car_level_id)
                ->update(["flag" => 1]);
        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();
    }

    public function waitingDrivers()
    {

        $users = Driver::orderBy('id', 'desc')
            ->where('accept', 0)
            ->with('country')
            ->with('driver_car_levels')
            ->paginate(10);
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.drivers.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 0

        ]);

    }

    public function allDrivers()
    {
        $type = 1;
        $users = Driver::orderBy('id', 'desc')
            ->with('country')
            ->with('driver_car_levels')
            ->paginate(10);
        $totalTrips = Trip::where('status', 3)->count();
        $drivers = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers1 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonth()->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers2 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonths(2)->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers3 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonths(3)->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers4 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonths(4)->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers5 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonths(5)->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);
        $drivers6 = Driver::whereHas('trips', function ($query) {
            $query->where('status', 3)
                ->whereMonth('created_at', Carbon::now()->subMonths(6)->month);
        })->withCount('trips')->get()->sortByDesc('trips_count')->take(2);

        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.drivers.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'drivers' => $drivers,
            'drivers1' => $drivers1,
            'drivers2' => $drivers2,
            'drivers3' => $drivers3,
            'drivers4' => $drivers4,
            'drivers5' => $drivers5,
            'drivers6' => $drivers6,
            'totalTrips' => $totalTrips,
            'type' => $type,
            'accept' => 2


        ]);

    }

    public function waitingDelegates()
    {

        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 0)
            ->where('accept', 0)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 0
        ]);
    }

    public function waitingDelegatesHeavey()
    {
        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 1)
            ->where('accept', 0)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates_heavey.index', [
            'users' => $users,
            'carLevels' => $carLevels,
            'accept' => 0

        ]);
    }

    public function calculationsDrivers($driver_id)
    {
        $app_needs = 0;
        $drive_needs = 0;
        $app_percent = AppSetting::where('key_name', 'driver_monthly_subscribtion')->first()->val;
//        $app_percent = Admin::where('email','admin@admin.com')->first()->app_percent;
        $driver = Driver::whereId($driver_id)->first();
        $trips = Trip::where("driver_id", $driver_id)
            ->where('paid', 0)
            ->whereNull('cancel_id')
            ->with('trip_paths')
            ->get();
        //payment >> 0=>cash, 1=>online, 2=>wallet

        $total = Trip::where("driver_id", $driver_id)
            ->where('paid', 0)
            ->whereNull('cancel_id')
            ->sum('trip_total');
        $total_cash = Trip::where("driver_id", $driver_id)
            ->where('paid', 0)
            ->where('payment', 0)
            ->whereNull('cancel_id')
            ->sum('trip_total');
        $total_visa = Trip::where("driver_id", $driver_id)
            ->where('paid', 0)
            ->where('payment', 1)
            ->whereNull('cancel_id')
            ->sum('trip_total');
        $total_wallet = Trip::where("driver_id", $driver_id)
            ->where('paid', 0)
            ->where('payment', 2)
            ->whereNull('cancel_id')
            ->sum('trip_total');
        $total_visa_wallet = $total_visa + $total_wallet;
        //subscription_type  > 1 = عمولة
        //subscription_type  > 2 = اشتراك شهري
        $total_visa_wallet_cash = $total_visa + $total_wallet + $total_cash;
        $app_money = $total_visa_wallet_cash * ($app_percent / 100);
        $driver_money = $total_visa_wallet_cash * ((100 - $app_percent) / 100);
        if ($driver->subscription_type == 1) {
            if ($app_money > $total_visa_wallet) {
                $app_needs = $app_money - $total_visa_wallet;
            }
            if ($driver_money > $total_cash) {
                $drive_needs = $driver_money - $total_cash;
            }
        } else {
            $app_money = 0;
            $driver_money = 0;
        }

        return view('cp.drivers.accounts', [
            'driver' => $driver,
            'trips' => $trips,
            'total' => $total,
            'total_cash' => $total_cash,
            'total_visa' => $total_visa,
            'total_wallet' => $total_wallet,
            'app_money' => $app_money,
            'driver_money' => $driver_money,
            'app_needs' => $app_needs,
            'drive_needs' => $drive_needs,
        ]);

    }

    public function calculationsDelegates($delegate_id)
    {
        $delegate_needs = 0;
        $app_percent = AppSetting::where('key_name','app_percent_delegate')->first();
        $delegate = Delegate::whereId($delegate_id)->first();
        $order_ids = Order::where('delegate_id', $delegate_id)->whereNotNull('offer_id')->pluck('id');
        $order_ids_status = OrderStatus::whereIn('order_id', $order_ids)
            ->whereNull('cancelled')->whereNotNull('finished')->pluck('order_id');
        $orders = Order::where('delegate_id', $delegate_id)
            ->whereIn('id', $order_ids_status)
            ->whereIn('id', $order_ids)
            ->with('user')
            ->with('delegate')
            ->with('offer')
            ->with('country')
            ->with('country')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();

         $total = OrderOffer::where('delegate_id',$delegate_id)->whereIn('order_id', $order_ids_status)
            ->sum('offer');

        //subscription_type  > 1 = عمولة
        //subscription_type  > 2 = اشتراك شهري
        $app_money = $total * ($app_percent->val / 100);
        $app_needs = $app_money;
        if ($delegate->subscription_type == 1) {
            $delegate_money = $total-($total * (($app_percent->val) / 100));

            $app_needs = $app_money;
        }
        else{
            $delegate_money = $total;

        }
//return $app_money;
        return view('cp.delegates.accounts', [
            'delegate' => $delegate,
            'orders' => $orders,
            'total' => $total,
            'app_money' => $app_money,
            'delegate_money' => $delegate_money,
            'app_needs' => $app_needs,
            'delegate_needs' => $delegate_needs,
        ]);

    }

    public function calculationsDelegatesHeavey($delegate_id)
    {
        $delegate_needs = 0;
        $app_percent = Admin::where('email', 'Amrrek@amrrek.com')->first()->app_percent;
        $delegate = Delegate::whereId($delegate_id)->first();
        $order_ids = Order::where('delegate_id', $delegate_id)->whereNotNull('offer_id')->pluck('id');
        $order_ids_status = OrderStatus::whereIn('order_id', $order_ids)
            ->whereNull('cancelled')->whereNotNull('finished')->pluck('order_id');
        $orders = Order::where('delegate_id', $delegate_id)
            ->whereIn('id', $order_ids_status)
            ->whereIn('id', $order_ids)
            ->with('user')
            ->with('delegate')
            ->with('offer')
            ->with('country')
            ->with('country')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();

        $total = OrderOffer::whereIn('order_id', $order_ids)
            ->sum('offer');

        //subscription_type  > 1 = عمولة
        //subscription_type  > 2 = اشتراك شهري
        $app_money = $total * ($app_percent / 100);
        $delegate_money = $total * ((100 - $app_percent) / 100);
        $app_needs = $app_money;
        if ($delegate->subscription_type == 1) {
            $app_needs = $app_money;
        }

        return view('cp.delegates.accounts', [
            'delegate' => $delegate,
            'orders' => $orders,
            'total' => $total,
            'app_money' => $app_money,
            'delegate_money' => $delegate_money,
            'app_needs' => $app_needs,
            'delegate_needs' => $delegate_needs,
        ]);
    }

    public function complains($user_id)
    {
        $cats = ComplainSuggests::orderBy('id', 'desc')
            ->with('user')
//            ->with('issue')
//            ->with('lost')
//            ->with('trip')
            ->where(['type' => 0, 'user_id' => $user_id])
            ->get();
        foreach ($cats as $c) {
            if (isset($c->trip)) {
                if (isset($c->trip->driver_id)) {
                    $c->driver = User::whereId($c->trip->driver_id)
                        ->select('id', 'name', 'phone', 'email')->first();
                }
            }
        }

        return view('cp.users.complains', [
            'cats' => $cats,
        ]);

    }

    public function trips($user_id)
    {
        $trips = Trip::orderBy('id', 'desc')
            ->where('user_id', $user_id)
            ->with('promo')
            ->with('user')
            ->with('driver')
            ->with('trip_paths')
            ->get();
        return view('cp.users.trips', [
            'trips' => $trips,
            'user_id' => $user_id
        ]);

    }

    public function wallets($user_id)
    {
        $user = User::whereId($user_id)->first();
        $results = UserWallet::where('user_id', $user_id)->get();
        return view('cp.users.wallets', compact('results', 'user'));
    }

    public function indexDelegatesWithAreas()
    {

        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 0)
//            ->where('accept',1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        $areas = Delegate::where('type', 0)->select('city')->distinct()->get();
        return view('cp.delegates.index-areas', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas
        ]);
    }

    public function delegatesAreasSearch(Request $request)
    {
        $users = Delegate::orderBy('id', 'desc')
            ->where('city', $request->city)
            ->where('type', 0)
//            ->where('accept',1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $areas = Delegate::where('type', 0)->select('city')->distinct()->get();

        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates.index-areas', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas

        ]);

    }

    public function indexHeavyDelegatesWithAreas()
    {

        $users = Delegate::orderBy('id', 'desc')
            ->where('type', 1)
//            ->where('accept',1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $carLevels = CarLevel::where('active', 1)->get();
        $areas = Delegate::where('type', 1)->select('city')->distinct()->get();
        return view('cp.delegates_heavey.index-areas', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas
        ]);
    }

    public function heavyDelegatesAreasSearch(Request $request)
    {
        $users = Delegate::orderBy('id', 'desc')
            ->where('city', $request->city)
            ->where('type', 1)
//            ->where('accept',1)
            ->with('country')
            ->with('delegate_documents')
            ->with('orders')
            ->get();
        $areas = Delegate::where('type', 1)->select('city')->distinct()->get();

        $carLevels = CarLevel::where('active', 1)->get();
        return view('cp.delegates_heavey.index-areas', [
            'users' => $users,
            'carLevels' => $carLevels,
            'areas' => $areas

        ]);

    }

    public function delegateShops($delegate_id)
    {
        $delegate = Delegate::whereId($delegate_id)->select('f_name', 'l_name')->first();
        $ids = ShopDelegate::where('delegate_id', $delegate_id)->where('status',1)->select('shop_id')->pluck('shop_id');
        $type = 'المتاجر المفعلة';
        $categories = Category::get();
        $users = Shop::whereIn('id', $ids)
            ->orderBy('id', 'desc')
            ->with('country')
            ->withCount('branches')
            ->withCount('allOrders')
            ->where('suspend', 0)
            ->paginate(1000);
        return view('cp.delegates.shops', ['users' => $users, 'categories' => $categories, 'type' => $type, 'delegate' => $delegate]);
    }

    public function downloadDelegates($accept, $type)
    {
        return Excel::download(new DelegatesExport($accept, $type), 'المناديب.xlsx');

    }

    public function downloadDrivers($accept)
    {
        return Excel::download(new DriversExport($accept), 'السائقين.xlsx');

    }

    public function downloadTrips($driver_id)
    {

        return Excel::download(new TripsExport($driver_id), 'الرحلات.xlsx');

    }

    public function editDocuments(Request $request)
    {
        $delegate = Delegate::whereId($request->delegate_id)->first();

        if (!$request->front_car_accept || !$request->back_car_accept || !$request->insurance_accept || !$request->license_accept || !$request->civil_accept) {
            $delegate->update([
                'files_completed' => 0,
            ]);
            $delegate->delegate_documents()->update([
                'front_car_accept' => $request->front_car_accept ? 1 : 0,
                'back_car_accept' => $request->back_car_accept ? 1 : 0,
                'insurance_accept' => $request->insurance_accept ? 1 : 0,
                'license_accept' => $request->license_accept ? 1 : 0,
                'civil_accept' => $request->civil_accept ? 1 : 0,

            ]);
        } else {
            $delegate->update([
                'files_completed' => 1
            ]);
            $delegate->delegate_documents()->update([
                'front_car_accept' => $request->front_car_accept ? 1 : 0,
                'back_car_accept' => $request->back_car_accept ? 1 : 0,
                'insurance_accept' => $request->insurance_accept ? 1 : 0,
                'license_accept' => $request->license_accept ? 1 : 0,
                'civil_accept' => $request->civil_accept ? 1 : 0,

            ]);

        }
        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back();

    }

    protected function orders($user_id, $department_id)
    {
        if ($department_id == 2) {
            $type = "طلبات المتاجر";
        } elseif ($department_id == 3) {
            $type = "طلبات التوصيل";

        } else {
            $type = "طلبات النقل الثقيل";

        }

        $orders = Order::orderBy('id', 'desc')
            ->where('user_id', $user_id)
            ->with('user')
            ->with('promo')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess")
            ->where('department_id', $department_id)
            ->paginate(5);
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
//            $order->order_products = OrderProduct::where('order_id',$order->id)->get();
//            if($order->order_products){
//                foreach ($order->order_products as $order_product){
//                    $order_product->order_product_variations = OrderProductVariation::where('order_product_id',$order_product->id)->get();
//                    if($order_product->order_product_variations){
//                        foreach ($order_product->order_product_variations as $order_product_variation){
//                            $order_product_variation->order_product_variation_options = OrderProductVariationOption::where('order_product_var_id',$order_product_variation->id)->get();
//                        }
//                    }
//                }
//            }
        }
//        dd($orders);
        return view('cp.users.orders', compact('orders', 'type', 'user_id', 'department_id'));
    }

    protected function searchOrders(Request $request)
    {
        $type = "كل الطلبات";
        $user_id = $request->user_id;
        $department_id = $request->department_id;
        $orders = Order::where('user_id', $request->user_id)
            ->where('department_id', $request->department_id)
            ->with('promo')
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_imagess");
        if ($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $orders->where('created_at', '>=', $from);


        }
        if ($request->to) {
            $to = date('Y-m-d', strtotime($request->to));
            $orders->where('created_at', '<=', $to);

        }
        if ($request->status) {

            if ($request->status != 'all') {
                $orders->whereHas('order_status', function ($query) use ($request) {
                    $query->where($request->status, '!=', null);
                });
            }

        }


        $orders = $orders->paginate(5);
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        return   $orders;
        return view('cp.users.orders', compact('orders', 'type', 'user_id', 'department_id'));

    }

    protected function searchDelegateOrders(Request $request)
    {
        $type = "كل الطلبات";
        $delegate_id = $request->delegate_id;
        $orders = Order::where('delegate_id', $request->delegate_id)
            ->with('user')
            ->with('delegate')
            ->with('offer')
            ->with('promo')
            ->with('country')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images");
        if ($request->from) {
            $from = date('Y-m-d', strtotime($request->from));
            $orders->where('created_at', '>=', $from);


        }
        if ($request->to) {
            $to = date('Y-m-d', strtotime($request->to));
            $orders->where('created_at', '<=', $to);

        }
        if ($request->status) {

            if ($request->status != 'all') {
                $orders->whereHas('order_status', function ($query) use ($request) {
                    $query->where($request->status, '!=', null);
                });
            }

        }


        $orders = $orders->paginate(5);
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        return   $orders;
        return view('cp.delegates.show', compact('orders', 'type', 'delegate_id'));

    }


}
