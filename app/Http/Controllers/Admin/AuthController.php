<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login_view()
    {
                    // dd(Auth::guard('admin')->user());

        if(Auth::guard('admin')->user())
        {
            return redirect('/admin/home');
        }

        return view('cp.login.login');
    }

    public function login(Request $request)
    {
//        return $request->all();
//        dd(Auth::guard('admin')->attempt([
//            'email' => $request->email,
//            'password' => $request->password,
//            'active' => 1,
//            'shop_id' => null,
//            'type' => 0,
//        ]));
        if(Auth::guard('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password,
            'active' => 1,
            'shop_id' => null,
            'type' => 0,
        ]))
        {
//dd(Auth::guard('admin')->attempt([
//    'email' => $request->email,
//    'password' => $request->password,
//    'active' => 1,
//    'shop_id' => null,
//    'type' => [0,3,4],
//]));
            return redirect(route('admin.home'));
        }else{
            return back()->with('error', 'Invalid Credentials');
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
    public function forgetPassword(){
        return view('cp.login.forget-password');
    }
    public function checkMail(Request $request){
        $admin=Admin::whereEmail($request->email)->where('type',0)->first();
        if($admin){
            $link=route('change-password',$admin->id);
                        $data = array('name'=>$admin->name,'link'=>$link);

            Mail::send('cp.login.mail', $data, function($message)use($admin) {
                $message->to($admin->email)->subject
                ('Forget Admin Password');
                $message->from('info@amrrek.net	','Amrak Admin');
            });

            return back()->with('success', 'تم ارسال رسالة لبريدك الالكتروني');

        }
        else{
            return back()->with('error', 'البريد الإلكتروني غير موجود');

        }

    }
    public function changePassword($admin_id){
        return view('cp.login.update-password',compact('admin_id'));


    }
    public function changePasswordSubmit(Request $request){
        $admin=Admin::whereId($request->admin_id)->where('type',0)->first();
        if($admin) {
            $admin->password = $request->password;
            $admin->save();
            return redirect()->back()->with('success', 'تم تغيير كلمة السر بنجاح');
        }
        else{
            return redirect()->back()->with('error', 'مدير غير موجود');

        }
    }
}
