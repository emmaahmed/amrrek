<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarLevel;
use Illuminate\Http\Request;
use App\Models\Country;
use DB;
use Route;
use Session;


class LevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $countries = CarLevel::orderBy('id','desc')
            ->get();
        return view('cp.levels.index',[
            'countries'=>$countries,
        ]);

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name_ar' => 'required|unique:car_levels,name_ar',
            'image' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
        ]);
        $add            = new CarLevel();
        $add->name_ar      = $request->name_ar;
        $add->name_en      = $request->name_en;
        $add->image     = $request->image;
        $add->description_ar = $request->description_ar;
        $add->description_en = $request->description_en;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Level added successfully');
    }



    public function edit_carlevels(Request $request){
//        $this->validate($request,[
//            'name' => 'required',
//            'description' => 'required',
//        ]);
        /*Country::where('id', $request->country_id)
            ->update([
                'name_ar'      => $request->name_ar,
                'name_en'      => $request->name_en,
                'image'      => $request->image
            ]);*/
        $c=CarLevel::where('id', $request->level_id)->first();
        $c->update($request->all());

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function editCarlevelStatus(Request $request,$id)
    {
        $cat=CarLevel::where("id",$id)->first();
        if($cat->active == 1){
            CarLevel::where("id",$id)
                ->update(["active" => 0 ]);
        }else{
            CarLevel::where("id",$id)
                ->update(["active" => 1 ]);
        }
        return back();
    }

}
