<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Models\Period;
use DB;
use Route;
use Session;


class PeriodController extends Controller
{
    public function index(){
        $cats = Period::orderBy('id','desc')->get();
        return view('cp.periods.index',[
            'cats'=>$cats
        ]);

    }

    public function store(Request $request)
    {
        $add            = new Period();
        $add->name_ar   = $request->name_ar;
        $add->name_en   = $request->name_en;
        $add->save();
        return back()->with('success','Period added successfully');
    }



    public function editPeriod(Request $request){

        Period::where('id', $request->model_id)
            ->update([
                'name_ar'      => $request->name_ar,
                'name_en'      => $request->name_en,
            ]);
        return back()->with('success','Period updated successfully');
    }
    
    public function editPeriodStatus(Request $request,$id)
    {
        $cat=Period::where("id",$id)->first();
        if($cat->active == 1){
            Period::where("id",$id)
                ->update(["active" => 0 ]);
        }else{
            Period::where("id",$id)
                ->update(["active" => 1 ]);
        }
        return back();
    }

}
