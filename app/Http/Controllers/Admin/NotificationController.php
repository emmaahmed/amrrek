<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\Admin;
use App\Models\Delegate;
use App\Models\Driver;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $nots = Notification::orderBy('id','desc')->paginate(50);
        $users = User::orderBy('id','desc')->select('id','name','email','phone')->get();
        $delegates = Delegate::orderBy('id','desc')->where('type',0)->select('id','f_name','email','phone')->get();
        $delegatesHeavey = Delegate::orderBy('id','desc')->where('type',1)->select('id','f_name','email','phone')->get();
        $drivers = Driver::orderBy('id','desc')->select('id','f_name','email','phone')->get();
        $shops = Shop::orderBy('id','desc')->select('id','name','email','phone')->get();
        return view('cp.notifications.index',[
            'nots'=>$nots,
            'users'=>$users,
            'delegates'=>$delegates,
            'drivers'=>$drivers,
            'delegatesHeavey'=>$delegatesHeavey,
            'shops'=>$shops,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|',
            'body' => 'required|',
        ]);
        // return $request->all();
        global $type;
        if($request->send_to == "users"){
            $type = 0;
        }elseif($request->send_to == "delegates"){
            $type = 1;
        }elseif($request->send_to == "drivers"){
            $type = 2;
        }
        elseif($request->send_to == "delegatesHeavey"){
            $type = 4;
        }elseif($request->send_to == "shops"){
            $type = 5;
        }
        elseif($request->send_to == "all"){
            $type = 3;
        }elseif($request->send_to == "user"){
            $type = 0;
        }elseif($request->send_to == "delegate"){
            $users = Delegate::whereId($request->user_id)->get();
            $type = $users[0]->type == 0 ? 1 : 4 ;
        }elseif($request->send_to == "driver"){
            $type = 2;
        }elseif($request->send_to == "shop"){
            $type = 5;
        }

//type =>>>0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
//        dd($request->send_to);
        if($request->send_to == "all"){
            Notification::sendNotifyFor($request->title , $request->body , 4 ,'users');
            Notification::sendNotifyFor($request->title , $request->body , 4 ,'drivers');
            Notification::sendNotifyFor($request->title , $request->body , 4 ,'delegates');
            Notification::sendNotifyFor($request->title , $request->body , 4 ,'delegatesHeavey');
            Notification::sendNotifyFor($request->title , $request->body , 4 ,'shops');

        }
        elseif($request->send_to == "user"){
            $user=User::whereId($request->user_id)->first();
            Notification::sendNotifyTo($user->jwt,$request->title , $request->body , 4 ,0,0,'users');

        }
        elseif($request->send_to == "driver"){
            $driver=Driver::whereId($request->user_id)->first();
            Notification::sendNotifyTo($driver->jwt,$request->title , $request->body , 4 ,0,0,'drivers');

        }
        elseif($request->send_to == "shop"){
            $shop=Shop::whereId($request->user_id)->first();
            Notification::sendNotifyTo($shop->jwt,$request->title , $request->body , 4 ,0,0,'shops');

        }
        elseif($request->send_to == "delegate"){
            $delegate=Delegate::whereId($request->user_id)->first();
            Notification::sendNotifyTo($delegate->jwt,$request->title , $request->body , 4 ,0,0,'delegates');

        }
        else{
            Notification::sendNotifyFor($request->title , $request->body , 4 ,$request->send_to);

//                Notification::send("$user->token", $request->title , $request->body , "" ,0,null);
        }
        $add            = new Notification();
        $add->title     = $request->title;
        $add->body      = $request->body;
        $add->user_id   = isset($request->user_id) ? $request->user_id : 0;
        $add->type      = $type;
        $add->order_type      = 0;//without order
        $add->save();

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','data added successfully');
    }

    public function delete_not(Request $request,$id){
        /*Notification::where("id",$id)->forcedelete();*/
        Notification::whereId($id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Notification deleted successfully');
    }


}
