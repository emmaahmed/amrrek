<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankAccount;
use App\Models\BankingTransfer;
use App\Models\Country;
use App\Models\Driver;
use App\Models\Gov;
use Illuminate\Http\Request;
use App\Models\Rushhour;
use DB;
use Route;
use Session;


class BankingTransferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $results = BankingTransfer::orderBy('id','desc')
            ->where('from_admin','!=',1)
            ->join('drivers','drivers.id','banking_transfers.user_id')
            ->select('banking_transfers.id','banking_transfers.bank_name','transfer_no','transfer_value','banking_transfers.image',
                'drivers.id as user_id','drivers.f_name as name','email','phone')
            ->get();
        return view('cp.bank_transfers.index',[
            'results'=>$results,
            'from_admin'=>2
        ]);

    }

    public function from_admin(){
        $results = BankingTransfer::orderBy('id','desc')
            ->join('drivers','drivers.id','banking_transfers.user_id')
            ->where('from_admin',1)
            ->select('banking_transfers.id','banking_transfers.bank_name','transfer_no','transfer_value','banking_transfers.image',
                'drivers.id as user_id','drivers.f_name as name','email','phone')
            ->get();
        return view('cp.bank_transfers.index',[
            'results'=>$results,
            'from_admin'=>1

        ]);

    }
    public function create(){
        $drivers=Driver::all();
        $bankAccounts=BankAccount::all();
        return view('cp.bank_transfers.create',[
            'drivers'=>$drivers,
            'bankAccounts'=>$bankAccounts,
        ]);

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required|exists:drivers,id',
            'bank_account_id' => 'required|exists:bank_accounts,id',
            'image' => 'required',
            'transfer_value' => 'required',
            'transfer_no' => 'required',
        ]);
        $bankAccount=BankAccount::whereId($request->bank_account_id)->first();
        $add                = new BankingTransfer();
        $add->user_id        = $request->user_id;
        $add->bank_account_id        = $request->bank_account_id;
        $add->bank_name        = $bankAccount->bank_name;
        $add->image        = $request->image;
        $add->transfer_value     = $request->transfer_value;
        $add->transfer_no     = $request->transfer_no;
        $add->from_admin     = 1;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect()->route('bank_transfers.from_admin')->with('success','Record added successfully');
    }

    public function edit($id){
        $bankTransfer=BankingTransfer::whereId($id)->first();
        $drivers=Driver::all();
        $bankAccounts=BankAccount::all();
        return view('cp.bank_transfers.edit',[
            'drivers'=>$drivers,
            'bankAccounts'=>$bankAccounts,
            'bankTransfer'=>$bankTransfer
        ]);

    }
    public function update(Request $request,$id){

        $this->validate($request,[
            'user_id' => 'required|exists:drivers,id',
            'bank_account_id' => 'required|exists:bank_accounts,id',
            'transfer_value' => 'required',
            'transfer_no' => 'required',
        ]);
        $bankAccount=BankAccount::whereId($request->bank_account_id)->first();
        $add                = BankingTransfer::whereId($id)->first();
        $add->user_id        = $request->user_id;
        $add->bank_account_id        = $request->bank_account_id;
        $add->bank_name        = $bankAccount->bank_name;
        if($request->image) {
            $add->image = $request->image;
        }
        $add->transfer_value     = $request->transfer_value;
        $add->transfer_no     = $request->transfer_no;
        $add->from_admin     = 1;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect()->route('bank_transfers.from_admin')->with('success','Record added successfully');

    }


//    public function edit_reason(Request $request){
//        $this->validate($request,[
//            'ar_reason' => 'required',
//            'en_reason' => 'required',
//            'is_captin' => 'required',
//        ]);
//        $c=BankingTransfer::where('id', $request->reason_id)->first();
//        $c->update($request->all());
//
//        session()->flash('insert_message','تمت العملية بنجاح');
//        return back()->with('success','Record updated successfully');
//    }
//
//    public function delete_reason(Request $request,$id){
//
//        BankingTransfer::destroy($id);
//
//        session()->flash('insert_message','تمت العملية بنجاح');
//        return back()->with('success','Record deleted successfully');
//    }

}
