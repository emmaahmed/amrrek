<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\AboutUs;
use App\Models\AppExplanation;
use App\Models\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppPrivacyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $abouts = AppExplanation::orderBy('id','desc')
            ->where('privacy',1)
            ->get();
        return view('cp.app_privacies.index',[
            'abouts'=>$abouts,
        ]);

    }

    public function createPrivacy(Request $request){
        $this->validate($request,[
//            'ar_title' => 'required|',
//            'en_title' => 'required|',
            'ar_body' => 'required|',
            'en_body' => 'required|',
        ]);
        AppExplanation::create($request->all());
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Terms updated successfully');
    }

    public function editView(Request $request,$id){
        $result=AppExplanation::where('id', $id)->first();
        return view('cp.app_privacies.edit',[
            'result'=>$result,
        ]);

    }


    public function edit_explains(Request $request){
        $this->validate($request,[
//            'ar_title' => 'required|',
//            'en_title' => 'required|',
            'ar_body' => 'required|',
            'en_body' => 'required|',
        ]);
        $c=AppExplanation::where('id', $request->model_id)->first();
        $c->update($request->all());
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Terms updated successfully');
    }


    public function deleteAppExplanations(Request $request){
        AppExplanation::where('id', $request->model_id)->delete();
        return back()->with('success','Data added successfully');
    }

}
