<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\AboutUs;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($department_id){
//        $data = Admin::where('email','admin@admin.com')->first();
        if($department_id!=0) {
            $data = AppSetting::where('department_id', $department_id)->get();

        }

        else{
            $data = AppSetting::where('department_id', null)->get();

        }
        //        $app_percent = $data->app_percent;
//        $fee_percent = $data->fee_percent;
//        $user_max_value=$data->user_max_value;
//        $driver_max_value=$data->driver_max_value;
//        $delegate_max_value=$data->delegate_max_value;
//        $delegate_heavey_max_value=$data->delegate_heavy_max_value;
//        $user_cancel_fee=$data->user_cancel_fee;
//        $user_cancel_time=$data->user_cancel_time;
//        $driver_monthly_subscribtion=$data->driver_monthly_subscribtion;
//        $delegate_monthly_subscribtion=$data->delegate_monthly_subscribtion;
//        $heavey_delegate_monthly_subscribtion=$data->heavey_delegate_monthly_subscribtion;
//        $subscription_tax=$data->subscription_tax;

        return view('cp.settings.index',[
            'data'=>$data,
        ]);

    }


    public function edit_settings(Request $request){
//        $this->validate($request,[
//            'app_percent' => 'required|',
//            'fee_percent' => 'required|',
//            'user_max_value' => 'required|',
//            'driver_max_value' => 'required|',
//            'delegate_max_value' => 'required|',
//            'delegate_heavy_max_value' => 'required|',
//            'user_cancel_fee' => 'required|',
//            'user_cancel_time' => 'required|',
//            'driver_monthly_subscribtion' => 'required|',
//            'delegate_monthly_subscribtion' => 'required|',
//            'heavey_delegate_monthly_subscribtion' => 'required|',
//            'subscription_tax' => 'required|',
//        ]);
        //$data = Admin::where('email','admin@admin.com')->first();
        $data=AppSetting::whereId($request->id)->first();
        $data->update([
            'val'=>$request->val
        ]);
//        if($request->subscription_tax){$data->where('key_name','subscription_tax')->update(['subscription_tax' => $request->subscription_tax ]);}
//        if($request->fee_percent){$data->update(['fee_percent' => $request->fee_percent ]);}
//        if($request->user_max_value){$data->update(['user_max_value' => $request->user_max_value ]);}
//        if($request->driver_max_value){$data->update(['driver_max_value' => $request->driver_max_value ]);}
//        if($request->delegate_max_value){$data->update(['delegate_max_value' => $request->delegate_max_value ]);}
//        if($request->delegate_heavy_max_value){$data->update(['delegate_heavy_max_value' => $request->delegate_heavy_max_value ]);}
//        if($request->user_cancel_fee){$data->update(['user_cancel_fee' => $request->user_cancel_fee ]);}
//        if($request->user_cancel_time){$data->update(['user_cancel_time' => $request->user_cancel_time ]);}
//        if($request->driver_monthly_subscribtion){$data->update(['driver_monthly_subscribtion' => $request->driver_monthly_subscribtion ]);}
//        if($request->delegate_monthly_subscribtion){$data->update(['delegate_monthly_subscribtion' => $request->delegate_monthly_subscribtion ]);}
//        if($request->heavey_delegate_monthly_subscribtion){$data->update(['heavey_delegate_monthly_subscribtion' => $request->heavey_delegate_monthly_subscribtion ]);}
//        if($request->subscription_tax){$data->update(['subscription_tax' => $request->subscription_tax ]);}
//        Admin::where('email','admin@admin.com')->update([
//            'app_percent' => $request->app_percent,
//            'fee_percent' => $request->fee_percent,
//            'user_max_value' => $request->user_max_value,
//            'driver_max_value' => $request->driver_max_value,
//            'delegate_max_value' => $request->delegate_max_value,
//            'delegate_heavy_max_value' => $request->delegate_heavy_max_value,
//            'user_cancel_fee' => $request->user_cancel_fee,
//            'user_cancel_time' => $request->user_cancel_time,
//            'driver_monthly_subscribtion' => $request->driver_monthly_subscribtion,
//            'delegate_monthly_subscribtion' => $request->delegate_monthly_subscribtion,
//            'heavey_delegate_monthly_subscribtion' => $request->heavey_delegate_monthly_subscribtion,
//            'subscription_tax' => $request->subscription_tax,
//        ]);
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Terms updated successfully');
    }



}
