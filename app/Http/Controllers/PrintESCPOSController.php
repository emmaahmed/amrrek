<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\Order;
use Illuminate\Http\Request;


//*********************************
// IMPORTANT NOTE
// ==============
// If your website requires user authentication, then
// THIS FILE MUST be set to ALLOW ANONYMOUS access!!!
//
//*********************************

//Includes WebClientPrint classes
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');

use Illuminate\Support\Facades\Storage;
use Neodynamic\SDK\Web\ClientPrintJobGroup;
use Neodynamic\SDK\Web\PrintFilePDF;
use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\Utils;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\PrintFile;
use Neodynamic\SDK\Web\ClientPrintJob;

use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Session;

class PrintESCPOSController extends Controller
{
    public function printCommands(Request $request){



        $tax = AppSetting::where('key_name', 'subscription_tax')->first();

        $order = Order::whereId($request->id)
            ->with(['user', 'delegate', 'shop', 'order_images', 'offer', "order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->first();
        //return view('cp_shop.orders.invoice', compact('order', 'tax'));

//dd($order);



        $printerName = urldecode('XP-800');
        $cpjLogo = new ClientPrintJob();
//        $esc = '0x1B'; //ESC byte in hex notation
//        $newLine = '0x0A'; //LF byte in hex notation
//
//        $cmdImage = $esc . "*"; //Initializes the printer (ESC @)
//        $cmdImage = public_path('logo-login.bmp').$esc . "0x2F0"; //Initializes the printer (ESC @)
//        $cmdImage .=$newLine;
//
//        //set ESCPOS commands to print...
        $cpjLogo->printFile = new PrintFile(public_path('logo-login.png'), 'MyPicture-PX=0.9-PY=0.2-PW=1-PH=1-PO=P.jpg', null);

        $cpjLogo->clientPrinter = new InstalledPrinter($printerName);

        //Create ESC/POS commands for sample receipt
        $esc = '0x1B'; //ESC byte in hex notation
        $newLine = '0x0A'; //LF byte in hex notation

        $cmds = '';
        $cmds = $esc . "@"; //Initializes the printer (ESC @)
//        $cmds .= $esc . '!' . '0x38'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
//        $cmds .=$esc . "*".public_path('logo-login.bmp');
        $cmds .=$newLine;

        $cmds .=
        $cmds .= '                      '.$order->shop->name_en; //text to print
        $cmds .=$newLine;
        $cmds .= '                  Order No.'.$order->order_number; //text to print
        $cmds .= $newLine . $newLine;

        $cmds .= '------------------------------------------------'; //text to print
        $cmds .=$newLine;

        $cmds .= 'Customer Name:'.$order->user->name;
        $cmds .=$newLine;

        $cmds .= 'Phone:'.$order->user->phone;
        $cmds .=$newLine;

        $cmds .= 'Address:'.$order->address;
        $cmds .=$newLine;

        $cmds .= '------------------------------------------------'; //text to print
//
        $cmds .=$newLine;

        $cmds .= 'Delegate:'.$order->delegate->f_name.' '.$order->delegate->l_name;
        $cmds .=$newLine;

        $cmds .= 'Phone:'.$order->delegate->phone;
        $cmds .=$newLine;

//        $cmds .= 'Address:'.$order->delegate->address;
//        $cmds .=$newLine;

        $cmds .= '------------------------------------------------'; //text to print
//
//        $cmds .= "^FO010,510^CI28^AZN,50,40^FD- Arabic: زيبرة  تكنوليجيز اوربا المحدودة^FS";
        $cmds .= $newLine . $newLine;
        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
        $items='';
        foreach ($order->order_products as $product) {
            $items.=$product->quantity.' * '.$product->product_name_en.'                    '.$product->price_after.$newLine;

        }

        $cmds .= $items;
        $cmds .= '------------------------------------------------'; //text to print
        $cmds .= $newLine . $newLine;

        $cmds.='Subtotal                                '.$order->sub_total;
        $cmds .=$newLine;
        $cmds.=$order->tax.'% tax'.'                                 '.(($order->tax * $order->sub_total) / 100 );
        $cmds .=$newLine;
        $cmds.='Total                                   '.$order->total_cost;
        $cmds .=$newLine;

        $cmds .= '------------------------------------------------'; //text to print
        $cmds .=$newLine;
        $cmds .='   '.date('l jS \of F Y h:i:s A');
        $cmds .= $newLine . $newLine;

        $cmds .='    Thank you for shopping at '.$order->shop->name_en;
        $cmds .=$newLine;

//        $cmds .= $newLine;
//        $cmds .= 'MILK 65 Fl oz             3.78';
//        $cmds .= $newLine . $newLine;
//        $cmds .= 'SUBTOTAL                  8.78';
//        $cmds .= $newLine;
//        $cmds .= 'TAX 5%                    0.44';
//        $cmds .= $newLine;
//        $cmds .= 'TOTAL                     9.22';
//        $cmds .= $newLine;
//        $cmds .= 'CASH TEND                10.00';
//        $cmds .= $newLine;
//        $cmds .= 'CASH DUE                  0.78';
//        $cmds .= $newLine . $newLine;
//        $cmds .= $esc . '!' . '0x18'; //Emphasized + Double-height mode selected (ESC ! (16 + 8)) 24 dec => 18 hex
//        $cmds .= '# ITEMS SOLD 2';
//        $cmds .= $esc . '!' . '0x00'; //Character font A selected (ESC ! 0)
//        $cmds .= $newLine . $newLine;
//        $cmds .= '11/03/13  19:53:17';
//        $cmds .= $newLine . $newLine;
//        $cmds .= $newLine . $newLine;
//        $cmds .= $newLine . $newLine;
//        $cmds .= $esc . "m";



        $cpj1 = new ClientPrintJob();

        $cpj1->printerCommands = $cmds;
        $cpj1->formatHexValues = true;


        $cpj1->clientPrinter = new InstalledPrinter($printerName);
        //   $cpj->clientPrinter->clone();
        $input = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl="."eman";
        $output = public_path('google.com.png');
        //dd(public_path());


        file_put_contents($output, file_get_contents($input));

        $cpj2 = new ClientPrintJob();
        //set ESCPOS commands to print...
        $cpj2->printFile = new PrintFile($output, 'MyPicture-PX=0.9-PY=0.2-PW=1-PH=1-PO=P.jpg', null);
//        $cpj2->printFile = new PrintFile($output, 'MyPicture-PX=0.9-PY=0.2-PW=1-PH=1-PO=P.jpg', null);
        $cpj2->clientPrinter = new InstalledPrinter($printerName);



        //Send ClientPrintJob back to the client
        $cpjg = new ClientPrintJobGroup();
        //Add ClientPrintJob objects
        $cpjg->clientPrintJobGroup = array($cpjLogo,$cpj1, $cpj2);



//        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
//
//        fwrite($myfile, $cmds);
//
//        fclose($myfile);
        $pdfFilePath = public_path('invoices/orders-'.$order->order_number.'.pdf');

        $cpjLogo = new ClientPrintJob();
        //dd(public_path());



        $cpjLogo->printFile = new PrintFilePDF($pdfFilePath, 'invoice.pdf', null);

        $cpjLogo->clientPrinter = new InstalledPrinter($printerName);

        return response($cpjLogo->sendToClient())
            ->header('Content-Type', 'application/octet-stream');



    }

}
