<?php

namespace App\Http\Controllers\Api\Delegate;

use App\Http\Controllers\Interfaces\Delegate\AuthRepositoryInterface;
use App\Http\Controllers\Interfaces\Delegate\DelegateRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Pusher\PushNotifications\PushNotifications;

class DelegateController extends Controller
{
    protected $delegateRepository;
    public function __construct(DelegateRepositoryInterface $delegateRepository)
    {
        $this->delegateRepository = $delegateRepository;
    }

    public function getShops(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->getShops($request,$request->header('lang'),$d->id,$d->country_id);

            if($data['warning_admin_message'] == 1){
//                if($request->header('lang') == "ar"){
//                    $reason = $d
//                }else{
//
//                }
                if($d->subscription_type==2){
                    $is_subscribed=$d->is_subscribed;
                }
                else{
                    $is_subscribed=1;

                }

                return response()->json(msgdata($request,failed(),'pay_your_debit',
                    [
                        'shops' => $data['shops'] ,
                        'warning_admin_message' => $data['warning_admin_message'] ,
                        'files_completed' => $d->files_completed,
                        'is_subscribed' => $is_subscribed
                    ]));
            }


            return response()
                ->json(msgdata($request, success(), 'success',
                    [
                        'shops' => $data['shops'] ,
                        'warning_admin_message' => $data['warning_admin_message'] ,
                        'files_completed' => $d->files_completed
                    ]
                ));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function subscribeAsDelegate(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->subscribeAsDelegate($request,$d->id,$request->header('lang'));
            if(isset($data->id)) {
                return response()->json(msg($request, success(), 'delegate_subscribed'));
            }
            elseif($data==false) {
                return response()->json(msg($request, failed(), 'delegate_unsubscribed'));
            }
            else{
                return response()->json(msg($request, failed(), 'pending-approval'));
            }
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function waitingOrders(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->waitingOrders($request,$d->id,$request->header('lang'),$d->country_id);
            return response()->json(msgdata($request, success(), 'success',$data));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function allWaitingOrders(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository
                ->allWaitingOrders($request,$d->id,$d->near_orders,$request->header('lang'),$d->country_id);
            return response()->json(msgdata($request, success(), 'success',$data));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function myOrders(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->myOrders($request,$d->id,$request->header('lang'),$d->country_id);
            return response()->json(msgdata($request, success(), 'success',$data));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function orderDetails(Request $request)
    {
        $type = 0;//user
        $d = checkDelegateJWT($request->header('jwt'));
        $d2 = checkJWT($request->header('jwt'));
        if ($d || $d2) {
            if($d){
                $type = 1;
                if($d->accept == 0)
                    return response()->json(msg($request, failed(), 'activated_waiting'));
            }

            if($d){
                $data = $this->delegateRepository->orderDetails($request,$d->id,$type,$d->lat,$d->lng,$request->header('lang'));
            }elseif($d2){
                $data = $this->delegateRepository->orderDetails($request,$d2->id,$type,null,null,$request->header('lang'));
            }
            return response()->json(msgdata($request, success(), 'success',$data));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function subscribedShops(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->subscribedShops($request,$d->id,$request->header('lang'));
            return response()
                ->json(msgdata($request, success(), 'success',['shops' => $data , 'files_completed' => $d->files_completed]));
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));

    }

    public function changeStatus(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->changeStatus($request,$d->id,$request->header('lang'));
            if($data){
                if( $data == "need_confirm_order") {
                    return response()->json(msg($request, redirectt(), 'need_confirm_order'));
                }
                elseif($data =='order-not-ready'){
                    return response()->json(msg($request, failed(), 'order-not-ready'));

                }
                elseif($data =='invoice-not-confirmed'){
                    return response()->json(msg($request, failed(), 'invoice-not-confirmed'));

                }
                    else {
                        return response()->json(msgdata($request, success(), 'success', $data));
                    }
            }

            return response()->json(msg($request, failed(), 'user_cancel_order'));

        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function sendConfirmRequest(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {
            if($d->accept == 0) return response()->json(msg($request, failed(), 'activated_waiting'));

            $data = $this->delegateRepository->sendConfirmRequest($request,$d->id,$request->header('lang'));
            if($data)
                return response()->json(msgdata($request, success(), 'success',$data));
            return response()->json(msg($request, failed(), 'exceeded_confirm_order_count'));

        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function ratesOfOrders(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {

            if ($d) {
                $rates = $this->delegateRepository->ratesOfOrders($d->id);

                return response()
                    ->json(msgdata($request, success(), 'success',$rates ));
            }
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function getOrderOffers(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {

            if ($d) {
                $data = $this->delegateRepository->getOrderOffers($request,$d->id);

                return response()
                    ->json(msgdata($request, success(), 'success',$data ));
            }
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function addOrderOffer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'offer' => 'numeric',
            'lat' => 'required',
            'lng' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
        if ($d = checkDelegateJWT($request->header('jwt'))) {


            $data = $this->delegateRepository->addOrderOffer($request,$d->id,$d->token);
            if(!$data){
                return response()->json(msg($request, failed(), 'user_cancel_order'));

            }
            return response()
                ->json(msg($request, success(), 'success'));

        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function getChatMessages(Request $request)
    {
        if ($d = checkDelegateJWT($request->header('jwt'))) {

            if ($d) {
                $data = $this->delegateRepository->getChatMessages($request,$d->id);

                return response()
                    ->json(msgdata($request, success(), 'success',$data));
            }
        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function getDelegateMessages(Request $request)
    {
        if ($user = checkDelegateJWT($request->header('jwt'))) {
            $data = $this->delegateRepository
                ->getDelegateMessages($request,$user->id,$user->image,$request->header('lang'));

            return response()->json(msgdata($request, success(), 'success', $data));
        }
        return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function sendMessage(Request $request)
    {
        if ($user = checkDelegateJWT($request->header('jwt'))) {
            $data = $this->delegateRepository
                ->sendMessage($request,$user->id,$user->image,$request->header('lang'));

            return response()->json(msgdata($request, success(), 'success', $data));
        }
        return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function getReplacedPoints(Request $request)
    {
        if ($user = checkDelegateJWT($request->header('jwt'))) {
            $data = $this->delegateRepository->getReplacedPoints($request,$user->id,$request->header('lang'));

            return response()->json(msgdata($request, success(), 'success', $data));
        }
        return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function rejectOrder(Request $request)
    {
        if ($user = checkDelegateJWT($request->header('jwt'))) {
            $data = $this->delegateRepository->rejectOrder($request,$user->id,$request->header('lang'));
            if($data)
                return response()->json(msg($request, success(), 'success'));
            return response()->json(msg($request, failed(), 'cant_reject_order'));
        }
        return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function generateAuthToken(Request $request){

        $user=checkDelegateJWT($request->header('jwt'));
        if($user) {
            $beamsClient = new PushNotifications(array(
                "instanceId" => config('services.Beams.Beams_Instance_Id'),
                "secretKey" => config('services.Beams.Beams_Secret_key')
            ));
            $beamsToken = $beamsClient->generateToken("delegates-".$user->jwt);
            return response()->json($beamsToken);

            // return response()->json(msgdata($request,success(),'success',$beamsToken));

//            return response()->json($beamsToken);

        }

        else return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function createOrderInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'sub_total' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
        if ($d = checkDelegateJWT($request->header('jwt'))) {


            $data = $this->delegateRepository->createOrderInvoice($request,$d);

            return response()
                ->json(msgdata($request, success(), 'success',$data));

        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }
    public function confirmInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'payment_method' => 'numeric|in:0,1',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
        if ($d = checkDelegateJWT($request->header('jwt'))) {


            $data = $this->delegateRepository->confirmInvoice($request);

            return response()
                ->json(msg($request, success(), 'success'));

        } else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

}
