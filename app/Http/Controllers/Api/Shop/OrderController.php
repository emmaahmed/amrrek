<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Interfaces\Shop\OrderRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use Pusher\PushNotifications\PushNotifications;

class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getOrders(Request $request)
    {
        if($data = checkShopJWT($request->header('jwt')))
        {
                        app()->setLocale($request->header('lang'));

            $results = $this->orderRepository->getOrders($data->shop_id);
            return response()->json(msgdata($request,success(),'success',$results));
        }
        else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function orderDetails(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'order_id' => 'required|exists:orders,id'
        ]);

        if($validator->fails()) return response()->json(['status'=>'failed','msg'=>$validator->messages()->first()]);

        if($data = checkShopJWT($request->header('jwt')))
        {
                        app()->setLocale($request->header('lang'));

            $results = $this->orderRepository->orderDetails($request);
            return response()->json(msgdata($request,success(),'success',$results));
        }
        else return response()->json(msg($request, failed(), 'invalid_data'));
    }

    public function myOrders(Request $request)
    {
        if($data = checkShopJWT($request->header('jwt')))
        {
            //dd($data->shop_id);
            app()->setLocale($request->header('lang'));
            $results = $this->orderRepository->myOrders($data->shop_id,$request);
            return response()->json(msgdata($request,success(),'success',$results));
        }
        else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function changeStatus(Request $request)
    {
        if($data = checkShopJWT($request->header('jwt')))
        {
            $results = $this->orderRepository->changeStatus($data->shop_id,$request,$data->lang);
            return response()->json(msgdata($request,success(),'success',$results));
        }
        else return response()->json(msg($request, not_authoize(), 'invalid_data'));
    }

    public function generateAuthToken(Request $request){

        $user=checkShopJWT($request->header('jwt'));
        if($user) {
            $beamsClient = new PushNotifications(array(
                "instanceId" => config('services.Beams.Beams_Instance_Id'),
                "secretKey" => config('services.Beams.Beams_Secret_key')
            ));
            $beamsToken = $beamsClient->generateToken("shops-".$user->jwt);
            return response()->json($beamsToken);

            // return response()->json(msgdata($request,success(),'success',$beamsToken));

//            return response()->json($beamsToken);

        }

        else return response()->json(msg($request,not_authoize(),'invalid_data'));


    }

}
