<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Includes WebClientPrint classes
//use
//include_once(app_path() . '\WebClientPrint\WebClientPrint.php');

use Illuminate\Support\Facades\Session;
use Neodynamic\SDK\Web\WebClientPrint;

class PrintController extends Controller
{
    public function index(){
return app_path();
        $wcppScript = WebClientPrint::createWcppDetectionScript(action('WebClientPrintController@processRequest'), Session::getId());

        return view('bill.index', ['wcppScript' => $wcppScript]);
    }

    public function printESCPOS(){
        $wcpScript = WebClientPrint::createScript(action('WebClientPrintController@processRequest'), action('PrintESCPOSController@printCommands'), Session::getId());

        return view('bill.print', ['wcpScript' => $wcpScript]);
    }

}
