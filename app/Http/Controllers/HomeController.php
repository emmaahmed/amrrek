<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\AppExplanation;
use App\Models\AppSetting;
use App\Models\CaptinInfo;
use App\Models\Category;
use App\Models\City;
use App\Models\ComplainSuggests;
use App\Models\Country;
use App\Models\Delegate;
use App\Models\Driver;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\OrderStatus;
use App\Models\Review;
use App\Models\Shop;
use App\Models\Shop_detail;
use App\Models\Offer;
use App\Models\Slider;
use App\Models\Term;
use App\Models\Trip;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cats=Category::count();
        $shops=Shop::count();
        $users=User::count();
        $drivers=Driver::whereActive(1)->count();
        $moderators=Admin::where('id','>',1)->where('type',0)->count();
        $delegates=Delegate::whereType(0)->whereActive(1)->count();
        $delegates_heavey=Delegate::whereType(1)->whereActive(1)->count();
        $offers=Slider::count();
        $nots=Notification::count();
        $shops_orders=Order::where('department_id',2)->count();
        $normal_orders=Order::where('department_id',3)->count();

        $rates=0;

        $users_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from users
            WHERE  date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $drivers_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from drivers
            WHERE  date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $delegates_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from delegates
            WHERE type = 0 AND date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $delegates_heavey_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from delegates
            WHERE type = 1 AND date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $shops_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from shops
            WHERE  date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $shops_orders_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from orders
            WHERE  
                date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY 
            AND 
                department_id = 2
            GROUP BY date(created_at),id");

        $normal_orders_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from orders
            WHERE  
                date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY 
            AND 
                department_id = 2
            GROUP BY date(created_at),id");

        $offers_charts = DB::SELECT("select id, count(*) as count,
            date(created_at) as date from sliders
            WHERE  `active`=1 AND date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");
//dd($shops_charts);
        $rates_charts = [];
        ////////

        $urgentTrips=Trip::where("type","urgent")->count();
        $scheduledTrips=Trip::where("type","scheduled")->count();
        $countries=Country::count();
        $cities=City::count();
        $comolains_suggestions=ComplainSuggests::count();

        $urgentTrips_charts = DB::SELECT("select id,count(*) as count,
            date(created_at) as date from trips
            WHERE  `type`='urgent' AND date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");

        $scheduledTrips_charts = DB::SELECT("select id,count(*) as count,
            date(created_at) as date from trips
            WHERE  `type`='scheduled' AND date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at),id");




        /////////////////////
//        //orders chart
//        $refundedOrders=20;
//        $pendingOrders=30;
//        $processingOrders=40;
//        $onTheWayOrders=50;
//        $deliveredOrders=60;
//        //products chart
//        $totalProducts=140;
//        $products = 50;//sold
        /////////////////////
        $activeUsers = User::whereActive(1)->count();
        $inActiveUsers = User::whereActive(0)->count();
        //
        $activeDrivers = Driver::whereActive(1)->whereSuspend(0)->count();
        $inActiveDrivers = Driver::whereActive(1)->whereSuspend(1)->count();
        //
        $activeDelegates = Delegate::whereActive(1)->whereType(0)->whereSuspend(0)->count();
        $inActiveDelegates = Delegate::whereActive(1)->whereType(0)->whereSuspend(1)->count();
        //
        $activeHeaveyDelegates = Delegate::whereActive(1)->whereType(1)->whereSuspend(0)->count();
        $inActiveHeaveyDelegates = Delegate::whereActive(1)->whereType(1)->whereSuspend(1)->count();
        //
        $shops_pie_chart = Shop::where('parent_id',0)->count();
        $branches_pie_chart = Shop::where('parent_id','>',0)->count();
        /////////////////////
        ///
        ///financial calculations
        $settings_trips_app_percent = AppSetting::where('key_name','app_percent_uber')
            ->select('val')->first()->val;
        $drivers_3mola=Driver::where('subscription_type',1)->pluck('id');
        $trips_3mola = Trip::whereIn('driver_id',$drivers_3mola)
            ->where('status',3)->sum('trip_total');
        $trips_app_percent = (int)$settings_trips_app_percent/100 * $trips_3mola;
        //
        $settings_orders_any_app_percent = AppSetting::where('key_name','app_percent_any')
            ->select('val')->first()->val;
        $delegates_3mola=Delegate::where('subscription_type',1)->pluck('id');
        $finished_order_ids = OrderStatus::whereNull('cancelled')
            ->whereNotNull('finished')->pluck('order_id');
        $orders_any_offers_ids = Order::where('department_id',3)
            ->whereNotNull('offer_id')->whereIn('delegate_id',$delegates_3mola)
            ->whereIn('id',$finished_order_ids)->pluck('offer_id');
        $orders_any_3mola = OrderOffer::whereIn('id',$orders_any_offers_ids)->sum('offer');
        $orders_any_app_percent = (int)$settings_orders_any_app_percent/100 * $orders_any_3mola;
        //
        $settings_orders_shops_app_percent = AppSetting::where('key_name','app_percent_shops')
            ->select('val')->first()->val;
        $orders_shops_offers_ids = Order::where('department_id',2)
            ->whereNotNull('offer_id')->whereIn('delegate_id',$delegates_3mola)
            ->whereIn('id',$finished_order_ids)->pluck('offer_id');
        $orders_shops_3mola = OrderOffer::whereIn('id',$orders_shops_offers_ids)->sum('offer');
        $orders_shops_app_percent = (int)$settings_orders_shops_app_percent/100 * $orders_shops_3mola;
        //
        $settings_orders_heavey_app_percent = AppSetting::where('key_name','app_percent_heavey')
            ->select('val')->first()->val;
        $orders_heavey_offers_ids = Order::where('department_id',5)
            ->whereNotNull('offer_id')->whereIn('delegate_id',$delegates_3mola)
            ->whereIn('id',$finished_order_ids)->pluck('offer_id');
        $orders_heavey_3mola = OrderOffer::whereIn('id',$orders_heavey_offers_ids)->sum('offer');
        $orders_heavey_app_percent = (int)$settings_orders_heavey_app_percent/100 * $orders_heavey_3mola;
        /////////////////////
        return view('cp.home',
            compact('trips_app_percent','orders_any_app_percent',
                'orders_shops_app_percent','orders_heavey_app_percent',
                'cats','shops','users','offers','nots','rates','delegates',
                'delegates_heavey','delegates_heavey_charts',
                'users_charts','drivers_charts','delegates_charts','shops_charts',
                'offers_charts','rates_charts',
                'drivers','urgentTrips','scheduledTrips','comolains_suggestions',
                'countries','cities','urgentTrips_charts','scheduledTrips_charts',
                'shops_orders_charts','normal_orders_charts',
                'shops_orders','normal_orders',
            'moderators',
            'activeUsers', 'inActiveUsers',
            'activeDrivers', 'inActiveDrivers',
            'activeDelegates', 'inActiveDelegates',
            'activeHeaveyDelegates', 'inActiveHeaveyDelegates',
            'shops_pie_chart', 'branches_pie_chart'
            //'refundedOrders','pendingOrders','processingOrders','onTheWayOrders','deliveredOrders','totalProducts','products'
            ));
    }
    public function privacy($type,$lang){
        $data = AppExplanation::orderBy('id','desc')
            ->where('privacy',1)
            ->where('type',$type)->select($lang.'_body as body' )
            ->get();
        return view('cp.app_privacies.view',[
            'data'=>$data,
            'type'=>$type
        ]);

    }
    public function terms($type,$lang){
        $data = Term::orderBy('id','desc')->where('type',$type)->select('term_'.$lang.' as term' )
            ->get();
        return view('cp.terms.view',[
            'data'=>$data,
            'type'=>$type,
            'lang'=>$lang
        ]);

    }

}
