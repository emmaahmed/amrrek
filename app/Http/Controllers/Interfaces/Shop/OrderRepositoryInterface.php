<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 27/06/2019
 * Time: 11:06 ص
 */

namespace App\Http\Controllers\Interfaces\Shop;


interface OrderRepositoryInterface
{
    public function getOrders($shop_id);
    public function orderDetails($request);
    public function myOrders($shop_id,$request);
    public function changeStatus($order_id,$request,$lang);
}
