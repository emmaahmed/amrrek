<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Interfaces\Admin\OrderRepositoryInterface;
use App\Http\Controllers\Interfaces\Admin\WorkerRepositoryInterface;
use App\Http\Controllers\PrintController;
use App\Models\AppSetting;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductVariation;
use App\Models\OrderProductVariationOption;
use App\Models\OrderStatus;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\OrdersExport;
use App\Exports\CostsExport;
use App\Exports\CostsUserExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\DummyPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Session;
use Carbon\Carbon;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
include_once(app_path() . '/WebClientPrint/WebClientPrint.php');
use Neodynamic\SDK\Web\WebClientPrint;

class OrderController extends Controller
{

    public function __construct()
    {
    }

    public function downloadOrders(Request $request)
    {
//        return $request->all();
        return Excel::download(new OrdersExport(Auth::guard('shop')->user()->shop_id, $request), 'الطلبات.xlsx');

    }

    public function printOrder($id)
    {
        $tax = AppSetting::where('key_name', 'subscription_tax')->first();

        $order = Order::whereId($id)
            ->with(['user', 'delegate', 'shop', 'order_images', 'offer', "order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->first();
        //return view('cp_shop.orders.invoice', compact('order', 'tax'));

        $items=[];

        try {
            $connector = new WindowsPrintConnector("XP-80");

            $profile = CapabilityProfile::load("default");//smb://Guest@computername/Receipt Printer

            $printer = new Printer($connector, $profile);
//dd($order->order_products);
            if(count($order->order_products)>0)
            foreach ($order->order_products as $product) {
                $item= new item($product->product_name,$product->price_after, $product->quantity);
                 $items[]=$item;
            }
            $subtotal = new item('Subtotal', $order->sub_total);
            $tax = new item('A local tax', $order->tax);
            $total = new item('Total', $order->total, '',true);
            /* Date is kept the same for testing */
            $date = Carbon::now()->format('l jS \of F Y h:i:s A');
//            $date = "Monday 6th of April 2015 02:56:25 PM";
            $logo = EscposImage::load(public_path('logo-login.png'), false);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->bitImage($logo);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text($order->shop->name."\n");
            $printer->selectPrintMode();
            $printer->text($order->order_number."\n");
            $printer->feed();
            $printer->selectPrintMode();
            $printer->text($order->shop->address."\n");
            $printer->feed();
            $printer->setEmphasis(true);
            $printer->text($order->shop->phone."\n");
            $printer->setEmphasis(false);
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->setEmphasis(true);
//            $printer->text(new item('', '$'));
            $printer->setEmphasis(false);
            foreach ($items as $item) {
                $printer->text($item);
            }
            $printer->setEmphasis(true);
            $printer->text($subtotal);
            $printer->setEmphasis(false);
            $printer->feed();
            $printer->text($tax);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text($total);
            $printer->selectPrintMode();
            $printer->feed(2);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Thank you for shopping at".$order->shop->name."\n");
//            $printer->text("For trading hours, please visit example.com\n");
            $printer->feed(2);
            $printer->text($date . "\n");
//            $printer->setBarcodeHeight(48);
//            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
//            $printer->barcode('https://amrk1.my-staff.net/', Printer::BARCODE_CODE128);
//            $testStr = "Testing 123";
//            $models = array(Printer::QR_MODEL_1 => "QR Model 1", Printer::QR_MODEL_2 => "QR Model 2 (default)", Printer::QR_MICRO => "Micro QR code\n(not supported on all printers)");
//            foreach ($models as $model => $name) {
//                $printer->qrCode($testStr, Printer::QR_ECLEVEL_L, 3, $model);
//                $printer->text("{$name}\n");
//                $printer->feed();
//            }
//            $printer->setJustification(Printer::JUSTIFY_CENTER);
//            $printer->qrCode('test');
//            QrCode::generate('test');
            $qr = EscposImage::load(public_path('qr.png'), false);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->bitImage($qr);
            $printer->text("Download Pdf\n");
            $printer->setJustification();
            $printer->feed();
            $printer->cut();
            /* Pulse */
            $printer->pulse();
            $printer->close();

        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }


    }

    public function pdfOrder($id)
    {
        $tax = AppSetting::where('key_name', 'subscription_tax')->first();

        $order = Order::whereId($id)
            ->with(['user', 'delegate', 'shop', 'order_images', 'offer', "order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->first();
        $data = ['foo' => 'foo'];
        $pdf = PDF::loadView('cp_shop.orders.pdf', compact('data', 'order', 'tax'));
        return $pdf->stream('Order-' . $order->order_number . '.pdf');


    }

    public function printReceipt()
    {
        $connector = new WindowsPrintConnector("smb://192.168.1.100/XP-80");


        $order = Order::whereId(115)
            ->with(['user', 'delegate', 'shop', 'order_images', 'offer', "order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->first();
//dd($order);
        try {
            if(count($order->order_products)>0)
                foreach ($order->order_products as $product) {
                    $items[] = new item($product->product_name, $product->price_after, $product->quantity);
                }

            $subtotal = new item('Subtotal', $order->sub_total);
            $tax = new item($order->tax.'% tax', ($order->tax * $order->sub_total) / 100 );
            $total = new item('Total', $order->total_cost);
            /* Date is kept the same for testing */
            $date = date('l jS \of F Y h:i:s A');
//            $date = "Monday 6th of April 2015 02:56:25 PM";
            $logo = EscposImage::load(public_path('logo-login.png'), false);
            $profile = CapabilityProfile::load("default");//smb://Guest@computername/Receipt Printer

            $printer = new Printer($connector, $profile);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->bitImage($logo);
            $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text($order->shop->name_en."\n");
            $printer->selectPrintMode();

            $printer->text("#Order No. ".$order->order_number."\n");
            $printer->feed();

            $printer->text("---------------------------------------------\n");
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->setEmphasis(true);
            $printer->text("Customer: ".$order->user->name. "\n");
            $printer->text("Phone: ".$order->user->phone. "\n");
            $printer->text("Address: 1 street 5 building 8 door \n");
            $printer->selectPrintMode();
            $printer->text("---------------------------------------------\n");
            $printer->feed();

            $printer->setEmphasis(false);
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->setEmphasis(true);
            $printer->setEmphasis(false);
            foreach ($items as $item) {
                $printer->text($item);
            }
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("---------------------------------------------\n");
            $printer->selectPrintMode();

            $printer->feed();
            $printer->setEmphasis(true);
            $printer->text($subtotal);
            $printer->setEmphasis(false);
            $printer->text($tax);
            $printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $printer->text($total);
            $printer->selectPrintMode();

            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("---------------------------------------------\n");
            $printer->selectPrintMode();
            $printer->feed(2);

            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Thank you for shopping at ".$order->shop->name_en."\n");
            $printer->text("please visit https://amrrek.net/\n");
            $printer->feed(2);
            $printer->text($date . "\n");
//            $printer->setBarcodeHeight(48);
//            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
//                 $printer->barcode('https://amrk1.my-staff.net/', Printer::BARCODE_CODE128);
//             $testStr = "Testing 123";
//             $models = array(Printer::QR_MODEL_1 => "QR Model 1", Printer::QR_MODEL_2 => "QR Model 2 (default)", Printer::QR_MICRO => "Micro QR code\n(not supported on all printers)");
//             foreach ($models as $model => $name) {
//                 $printer->qrCode($testStr, Printer::QR_ECLEVEL_L, 3, $model);
//                 $printer->text("{$name}\n");
//                 $printer->feed();
//             }
//             $printer->setJustification(Printer::JUSTIFY_CENTER);
//             $printer->qrCode('test');
//             QrCode::generate('test');

            $input = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl="."eman";
            $output = public_path('google.com.png');
            //dd(public_path());


            file_put_contents($output, file_get_contents($input));


            $qr = EscposImage::load($output, false);

            $printer->setJustification(Printer::JUSTIFY_CENTER);
              $printer->bitImage($qr);
              $printer->text("Same example, centred\n");
              $printer->setJustification();
              $printer->feed();
           // QrCode::format('png')->generate('test');

//            $models = array(
//                Printer::QR_MODEL_1 => "QR Model 1",
//                Printer::QR_MODEL_2 => "QR Model 2 (default)",
//                Printer::QR_MICRO => "Micro QR code\n(not supported on all printers)");
//            foreach($models as $model => $name) {
//                $printer -> qrCode("test", Printer::QR_ECLEVEL_L, 3, $model);
//                $printer -> text("$name\n");
//                $printer -> feed();
//            }


            $printer->cut();
             /* Pulse */
             $printer->pulse();
              $printer->close();

        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
    }

    protected function index()
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $type = "كل الطلبات";
        $orders = Order::where('shop_id', $shop_id)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->paginate(20);
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
//            $order->order_products = OrderProduct::where('order_id',$order->id)->get();
//            if($order->order_products){
//                foreach ($order->order_products as $order_product){
//                    $order_product->order_product_variations = OrderProductVariation::where('order_product_id',$order_product->id)->get();
//                    if($order_product->order_product_variations){
//                        foreach ($order_product->order_product_variations as $order_product_variation){
//                            $order_product_variation->order_product_variation_options = OrderProductVariationOption::where('order_product_var_id',$order_product_variation->id)->get();
//                        }
//                    }
//                }
//            }
        }
//        dd($orders);
        return view('cp_shop.orders.all-orders', compact('orders', 'type'));
    }

    protected function acceptOrders()
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $type = "الطلبات المقبولة";
        $orderStatusArray = OrderStatus::where('accept', '!=', null)
//            ->where('on_way','=',null)
            ->where('finished', '=', null)
//            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::where('shop_id', $shop_id)
            ->whereIn('id', $orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        dd($orders);
        return view('cp_shop.orders.index', compact('orders', 'type'));
    }

    protected function onwayOrders()
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $type = "طلبات في الطريق";
        $orderStatusArray = OrderStatus::where('accept', '!=', null)
            ->where('on_way', '!=', null)
            ->where('finished', '=', null)
//            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::where('shop_id', $shop_id)
            ->whereIn('id', $orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        dd($orders);
        return view('cp_shop.orders.index', compact('orders', 'type'));
    }

    protected function finishedOrders()
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $type = "الطلبات المنتهية";
        $orderStatusArray = OrderStatus::
//            ->where('on_way','!=',null)
        where('finished', '!=', null)
//            ->where('cancelled','=',null)
            ->pluck('order_id');
        $orders = Order::where('shop_id', $shop_id)
            ->whereIn('id', $orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        dd($orders);
        return view('cp_shop.orders.index', compact('orders', 'type'));
    }
//     public function printReceipt(){
//// //        dd('test');
//// //      return  phpinfo();
//// //        return QrCode::format('png')->generate('test');
////         $pdf = 'pdf.pdf';
//// //        $tmpdir = sys_get_temp_dir();
//// //        $file =  tempnam($tmpdir, 'ctk');
//
//
//         try {
//             $tmpdir = sys_get_temp_dir();
//             $file =  tempnam($tmpdir, 'ctk');
//
////             $connector = new FilePrintConnector('XP-80');
//
//              $connector = new NetworkPrintConnector("192.168.1.100", 8000);
////              $connector = new WindowsPrintConnector("smb://192.168.1.100/XP-80");
//
//             $profile = CapabilityProfile::load("SP2000");//smb://Guest@computername/Receipt Printer
////             $connector = new WindowsPrintConnector("smb://DESKTOP-IO1N5GC/PrinterXP");
////             $profile = CapabilityProfile::load("simple");
////dd('test');
////             $connector = new WindowsPrintConnector("smb://DESKTOP-IO1N5GC/PrinterXP");
//// ////            dd($file);
//// ////            $tmpdir = sys_get_temp_dir();
//// ////            dd($tmpdir);
//// ////            $file =  tempnam($tmpdir, 'ctk');
//// //
//// //            /* Do some printing */
//// //            $connector = new FilePrintConnector("php://output");
// //            $profile = CapabilityProfile::getInstance();
////         return $_SERVER['HTTP_CLIENT_IP'];
////         $connector = new FilePrintConnector("/dev/ttyS0");
////    $connector = new NetworkPrintConnector("192.168.1.101", 9100,10);
//
//         $printer = new Printer($connector,$profile);
//
//         $printer -> text("t\n");
//
//             $printer->cut();
////             dd('test');
//
//         $printer -> close();
////         $connector = new NetworkPrintConnector("192.168.1.100",9100,30);
////
////         $printer = new Printer($connector);
////             $printer -> text("Sandwich burger\n");
////             $printer->cut();
////             $printer->pulse();
////
////             $printer->close();
//// //            copy($file, "public_html");
//// //            unlink($file);
////             // $profile = CapabilityProfile::load("SP2000");
////             // $connector = new WindowsPrintConnector("Printer");
//
////             // $connector = new FilePrintConnector("/dev/usb");
////             // $connector = new FilePrintConnector("/dev/ttyUSB0");
////             $connector = new FilePrintConnector("/dev/ttyS0");
////             $printer = new Printer($connector);
////             // $tux = EscposImage::load(public_path('logo-login.png'), false);
////             // $printer->bitImage($tux);
////             // $printer->setPrintLeftMargin(20);
//
////             $printer -> text("Sandwich burger\n");
////              $printer -> text("Sandwich shawerma\n");
////              $printer -> text("Sandwich fries\n");
////              $printer -> text("Roz Blaban \n");
////                           $printer -> feed();
//
////              $printer -> cut();
////             $printer->pulse();
////             $printer->close();
//
////           /* $tux = EscposImage::load(public_path('logo-login.png'), false);
////             $printer->setPrintLeftMargin(230);
////             $printer->bitImage($tux);
////             $printer->text("Invoice #1069\n");
////             $printer->setPrintLeftMargin(10);
//
////             $printer->text("Pizza hut store\n");
////             $printer->text("New cairo , 8-rahmani st , egypt\n");
////             $printer->text("0102655555454\n");
////             $printer->feed();
////             $printer->bitImage($tux, Printer::JUSTIFY_CENTER);
////             $printer->text("Wide Tux.\n");
////             $printer->feed();
////             $printer->bitImage($tux, Printer::IMG_DOUBLE_HEIGHT);
////             $printer->text("Tall Tux.\n");
////             $printer->feed();
////             $printer->bitImage($tux, Printer::IMG_DOUBLE_WIDTH | Printer::IMG_DOUBLE_HEIGHT);
////             $printer->text("Large Tux in correct proportion.\n");
//
////          //////////
////             $items = array(new item("Example item #1", "4.00"), new item("Another thing", "3.50"), new item("Something else", "1.00"), new item("A final item", "4.45"));
////             $subtotal = new item('Subtotal', '12.95');
////             $tax = new item('A local tax', '1.30');
////             $total = new item('Total', '14.25', true);
////             /* Date is kept the same for testing */
//// // $date = date('l jS \of F Y h:i:s A');
////             //$date = "Monday 6th of April 2015 02:56:25 PM";
////             /* Start the printer */
////             // $logo = EscposImage::load(public_path('logo-login.png'), false);
////             // $printer = new Printer($connector, $profile);
////             // /* Print top logo */
////             // $printer->setJustification(Printer::JUSTIFY_CENTER);
////             // $printer->bitImage($logo);
////             // /* Name of shop */
////             // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
////             // $printer->text("ExampleMart Ltd.\n");
////             // $printer->selectPrintMode();
////             // $printer->text("Shop No. 42.\n");
////             // $printer->feed();
////             // /* Title of receipt */
////             // $printer->setEmphasis(true);
////             // $printer->text("SALES INVOICE\n");
////             // $printer->setEmphasis(false);
////             // /* Items */
////             // $printer->setJustification(Printer::JUSTIFY_LEFT);
////             // $printer->setEmphasis(true);
////             // $printer->text(new item('', '$'));
////             // $printer->setEmphasis(false);
////             // foreach ($items as $item) {
////             //     $printer->text($item);
////             // }
////             // $printer->setEmphasis(true);
////             // $printer->text($subtotal);
////             // $printer->setEmphasis(false);
////             // $printer->feed();
////             // /* Tax and total */
////             // $printer->text($tax);
////             // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
////             // $printer->text($total);
////             // $printer->selectPrintMode();
////             // /* Footer */
////             // $printer->feed(2);
////             // $printer->setJustification(Printer::JUSTIFY_CENTER);
////             // $printer->text("Thank you for shopping at ExampleMart\n");
////             // $printer->text("For trading hours, please visit example.com\n");
////             // $printer->feed(2);
////             // $printer->text($date . "\n");
//// //            $printer->setBarcodeHeight(48);
//// //            $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW)
//// //                $printer->barcode('https://amrk1.my-staff.net/', Printer::BARCODE_CODE128);
//// //            $testStr = "Testing 123";
//// //            $models = array(Printer::QR_MODEL_1 => "QR Model 1", Printer::QR_MODEL_2 => "QR Model 2 (default)", Printer::QR_MICRO => "Micro QR code\n(not supported on all printers)");
//// //            foreach ($models as $model => $name) {
//// //                $printer->qrCode($testStr, Printer::QR_ECLEVEL_L, 3, $model);
//// //                $printer->text("{$name}\n");
//// //                $printer->feed();
//// //            }
////             // $printer -> setJustification(Printer::JUSTIFY_CENTER);
//// //            $printer -> qrCode('test');
//// //            QrCode::generate('test');
////           // $qr = EscposImage::load(public_path('qr.png'), false);
////           // $printer->setJustification(Printer::JUSTIFY_CENTER);
////             // $printer->bitImage($qr);
////             // $printer -> text("Same example, centred\n");
////             // $printer -> setJustification();
////             // $printer -> feed();
////           //// $printer->cut();
////             /* Pulse */
////             // $printer->pulse();
////             // $printer->close();
//
//
////             /* Close printer */
//         } catch (\Exception $e) {
//             echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
//         }
//     }

    protected function cancelledOrders()
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $type = "الطلبات الملغية";
        $orderStatusArray = OrderStatus::where('cancelled', '!=', null)
            ->pluck('order_id');
        $orders = Order::where('shop_id', $shop_id)
            ->whereIn('id', $orderStatusArray)
            ->with('user')
            ->with('delegate')
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->with("order_images")
            ->get();
        foreach ($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
//        dd($orders);
        return view('cp_shop.orders.index', compact('orders', 'type'));
    }
//    public function printESCPOS(){
//        $wcpScript = WebClientPrint::createScript(route('print.process'), route('print.commands'), Session::getId());
//
//        return view('bill.home.printESCPOS', ['wcpScript' => $wcpScript]);
//    }

}
