<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\CarLevel;
use App\Models\PromoCode;
use App\Models\Country;
use App\Models\Gov;
use Illuminate\Http\Request;
use App\Models\Rushhour;
use DB;
use Illuminate\Support\Facades\Auth;
use Route;
use Session;


class PromocodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $code = PromoCode::where('shop_id',Auth::guard('shop')->user()->shop_id)
            ->first();

        return view('cp_shop.promocodes.index',[
            'code'=>$code,
        ]);

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|unique:promo_codes,code',

        ]);


        $add                = new PromoCode();
        $add->department_id          = 2;
        $add->code          = $request->code;
        $add->value         = $request->value;
        $add->type          = $request->type;/*0 = fixed discount , 1 = percentage discount*/
        $add->expire_times  = $request->expire_times;
        $add->expire_at     = $request->expire_at;
        $add->en_desc       = $request->en_desc;
        $add->ar_desc       = $request->ar_desc;
        $add->shop_id       = Auth::guard('shop')->user()->shop_id;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Record added successfully');
    }



//    public function edit_reason(Request $request){
//        $this->validate($request,[
//            'reason' => 'required',
//            'is_captin' => 'required',
//        ]);
//        $c=CancellingReason::where('id', $request->reason_id)->first();
//        $c->update($request->all());
//
//        session()->flash('insert_message','تمت العملية بنجاح');
//        return back()->with('success','Record updated successfully');
//    }

    public function delete_promo(Request $request,$id){

        PromoCode::destroy($id);

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Record deleted successfully');
    }

}
