<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\AboutUs;
use App\Models\Admin;
use App\Models\AppExplanation;
use App\Models\Category;
use App\Models\Country;
use App\Models\Day;
use App\Models\Offer;
use App\Models\Shop;
use App\Models\ShopDay;
use App\Models\ShopModerator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BranchController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){
        $type='الفروع المفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('parent_id',Auth::user()->shop_id)
            ->with('country')
            ->where('suspend',0)
            ->get();
        return view('cp_shop.branches.index',['users'=>$users,'categories'=>$categories,'type'=>$type]);

    }

    public function createBranch(Request $request){
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $category_id = Shop::whereId($shop_id)->first()->category_id;
        $categories  = Category::get();
        $countries  = Country::where('active',1)->get();
        return view('cp_shop.branches.create',[
            'categories'=>$categories,
            'countries'=>$countries,
            'category_id'=>$category_id,
        ]);
    }

    public function store(Request $request){
        //$permissions = Permission::where('id','>', 103)->pluck('id')->toArray();
        //dd($permissions);
        $this->validate($request,[
            'moderator_email' => 'required|unique:admins,email',
        ]);
        //type >>>>> 0 => admin ,1=>admin_shop ,2=>subadmin_shop ,3=>admin_branch ,4=>subadmin_branch
        $shop = Shop::create($request->all());
        $shopModerator = Admin::create([
            'shop_id' => $shop->id,
            'jwt' => generateJWT(),
            'active' => 1,
            'type' => 3,
            'name' => $request->moderator_name,
            'email' => $request->moderator_email,
            'phone' => $request->moderator_phone,
            'password' => $request->password,
        ]);
        $permissions = Permission::where('id','>', 103)->pluck('id')->toArray();
        $shopModerator->givePermissionTo($permissions);

//        $role2 = Role::where('guard_name', 'shop')->first();
//        $shopModerator->assignRole($role2);
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect(asset('shop/branches'));
        //return back()->with('success','Data stored successfully');
    }

    public function editBranch(Request $request,$shop_id){
        //$shop_id = Auth::guard('shop')->user()->shop_id;

        $shop=Shop::where('id', $shop_id)->first();
        $categories  = Category::get();
        $countries  = Country::where('active',1)->get();
        return view('cp_shop.branches.edit',[
            'categories'=>$categories,
            'countries'=>$countries,
            'shop'=>$shop
        ]);

    }

    public function updateBranch(Request $request){

        $c=Shop::where('id', $request->model_id)->first();
        $c->update($request->all());
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
//        return back()->with('success','Data updated successfully');
    }

    public function activeBranchs(){
        $type='الفروع المفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('parent_id',Auth::user()->shop_id)
            ->where('suspend',0)
            ->get();
        return view('cp_shop.shops.index',['users'=>$users,'categories'=>$categories,'type'=>$type]);
    }

    public function inactiveBranchs(){
        $type='الفروع الغير مفعلة';
        $categories  = Category::get();
        $users = Shop::orderBy('id','desc')
            ->where('parent_id',Auth::user()->shop_id)
            ->where('suspend',1)
            ->get();
        return view('cp_shop.shops.index',['users'=>$users,'categories'=>$categories,'type'=>$type]);
    }

    public function editBranchStatus(Request $request)
    {
        $cat=Shop::where("id",$request->model_id)->first();
        if($cat->suspend == 1){
            Shop::where("id",$request->model_id)
                ->update(["suspend" => 0 ]);
        }else{
            Shop::where("id",$request->model_id)
                ->update(["suspend" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function editBranchVerified(Request $request)
    {

        $cat=Shop::where("id",$request->model_id)->first();
        if($cat->verified == 1){
            Shop::where("id",$request->model_id)
                ->update(["verified" => 0 ]);
        }else{
            Shop::where("id",$request->model_id)
                ->update(["verified" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function deleteBranch(Request $request)
    {
        Shop::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }



}
