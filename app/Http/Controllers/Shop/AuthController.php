<?php

namespace App\Http\Controllers\Shop;

use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\Category;
use App\Models\Notification;
use App\Models\Payment;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Pusher\PushNotifications\PushNotifications;

class AuthController extends Controller
{
    public function login_view()
    {
        if(Auth::guard('shop')->user())
        {
            $shop = Shop::whereId( Auth::guard('shop')->user()->shop_id)->first();
            if($shop && $shop != null){
                return redirect(route('shop_home'));
            } else{
                return view('cp_shop.login.login')->with('error', 'Invalid Credentials');
            }
//            return redirect('/shop/home');
        }

        return view('cp_shop.login.login');
    }

    public function login(Request $request)
    {
//        dd('test');
        $admin=Admin::where('email',$request->email)->first();
//        dd($admin);
        if($admin){
            $shop=Shop::whereId($admin->shop_id)->first();

          if($shop && Admin::where('email',$request->email)->first()->active==0){
                            // dd(Auth::guard('shop')->user()->active);

                return back()->with('error', 'Invalid Credentials');

            }
          if($shop && $shop->expiry_date==null &&Auth::guard('shop')->attempt(['email' => $request->email, 'password' => $request->password], false)){
              return redirect()->route('shops.payment',['shop_id'=>$shop->id]);
          }
        if(Auth::guard('shop')->attempt(['email' => $request->email, 'password' => $request->password], false))
        {
          
//            return Auth::guard('shop')->user();
            $shop = Shop::whereId( Auth::guard('shop')->user()->shop_id)->first();
            if($shop){
//                dd(Auth::guard('shop'));
                return redirect(route('shop_home'));
            } else{
                return back()->with('error', 'Invalid Credentials');
            }

        }
        }
        else{
            return back()->with('error', 'Invalid Credentials');
        }
    }

    public function logout()
    {
        Auth::guard('shop')->logout();
        return redirect('shop/login');
    }
    public function forgetPassword(){
        return view('cp_shop.login.forget-password');
    }
    public function checkMail(Request $request){
//        return $request->all();
        $this->validate($request,[
            'email' => 'required',
        ]);

        $admin=Admin::whereEmail($request->email)->where('type','!=',0)->first();
        if($admin){
            $link=route('change-password',$admin->id);
            $data = array('name'=>$admin->name,'link'=>$link);

            Mail::send('cp.login.mail', $data, function($message)use($admin) {
                $message->to($admin->email)->subject
                ('Forget Admin Password');
                $message->from('info@amrrek.net	','Amrak Admin');
            });

            return back()->with('success', 'تم ارسال رسالة لبريدك الالكتروني');

        }
        else{
            return back()->with('error', 'البريد الإلكتروني غير موجود');

        }

    }
    public function changePassword($admin_id){
        return view('cp_shop.login.update-password',compact('admin_id'));


    }
    public function changePasswordSubmit(Request $request){
        $this->validate($request,[
            'password' => 'required|confirmed',
        ]);

        $admin=Admin::whereId($request->admin_id)->where('type','!=',0)->first();
        $admin->password=$request->password;
        $admin->save();
        return redirect()->back()->with('success','تم تغيير كلمة السر بنجاح');
    }
    public function generateAuthToken(){
//        return "shops-".Auth::user()->jwt;
            $beamsClient = new PushNotifications(array(
                "instanceId" => config('services.Beams.Beams_Instance_Id'),
                "secretKey" => config('services.Beams.Beams_Secret_key')
            ));
            $beamsToken = $beamsClient->generateToken("shops-".Auth::user()->jwt);
            return response()->json($beamsToken);

            // return response()->json(msgdata($request,success(),'success',$beamsToken));

//            return response()->json($beamsToken);


//        else return response()->json(msg($request,not_authoize(),'invalid_data'));


    }
    public function turnNotification(){
        $shop=Auth::guard('shop')->user();
        if($shop->notification_flag==0) {
            $shop->update([
                'notification_flag' =>1
        ]);
        }else{
            $shop->update([
                'notification_flag' =>0
            ]);

        }
        session()->flash('success', 'تم التعديل بنجاح');
        //return redirect()->route('shop.products');
        return response()->json($shop);


    }
    public function notifications(){
        $nots=Notification::where(['type'=>5,'user_id'=>Auth::guard('shop')->user()->shop_id])->orderBy('created_at','desc')->get();

        return view('cp_shop.notifications.index',compact('nots'));

    }
//    public function Payment(Request $request)
//    {
//        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
//            'shop_id' => 'required|exists:shops,id',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
//        }
//        $amount=AppSetting::where('key_name','shop_payment_price_monthly')->first()->val;
//        $expiryDate=Carbon::now()->addMonth()->format('YY-d-m H:i');
//
//        $shop=Shop::whereId($request->shop_id)->first();
//        $CustomerName = $shop->name;
//        $CustomerEmail = $shop->email;
//        $CustomerMobile = $shop->mobile;
//        $InvoiceValue = $amount;
//
//        $token = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";
//        // $token = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";
//        //$token = "4ZPYVZD4YpR-FcJCWDUFB5cxd0cJDAPqzfyBDVJnvvTaq4zeFbizb8KWS_sfyCg66lOv73LmEmAaFrKFsssR_hs0ySNDk_9f9_SzzFIV2OPi7M6tRW6i-T9kw8hj_bBFv1LFSMZ3IQrc7iVjw6AV6i49wIGDsz-ed3KOQzZ2FsGDqJ4KSuW_Ww050GN_-xvxIAedQ__lMVV7_YC8YSnuPXD-Sv1v6iODqxUMlNDwDvUodXMdh-RwWr8ZxZZhmALBVHsNCpEkyH9w-CPBi7E40zuRSwwXwToI0oEtMmun5EsdZ6nFBEYWbocqyNPF18U1h9C8kv3Nj4oldaWhnZRmSxbWRDHs2ITUPn_Tggg6g8h96SWWH8HraGfdcDPLg2-Dbk3zIjOLRvLKRZEBuA0Hl-Y3phbiEsecdQqoZ5ynzzSdjZVwo90cpsI4BgNVekXCl5PRla98IK3bgx2vC4ykbH8CTM0FD7OeIXTEiIuktADe-uRFiglHfWUCLoHPVoDP_Y2pq-LaUT3g9cdvxADt2D7Py4o0SwBYVaB9LoEAADVy_4IKa1fNGckW0z8BNF6Qe7RREf6haIuMWZp1X0Q7LsjbagE8U5fFntkcx7gtALwmGu971GFOlJ4u539RwAGZm-5r8CS9TXlbwg9v3TBuokoXfsu4n5pP3aIZ4C5zEFuUVxnSAhilT9ildrUoYwqvzGdJZQ";
//        $basURL = "https://apitest.myfatoorah.com/";
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => "$basURL/v2/SendPayment",
////            CURLOPT_URL => "$basURL/v2/SendPayment",
//            CURLOPT_CUSTOMREQUEST => "POST",
//            //CURLOPT_POSTFIELDS => "{\"NotificationOption\":\"ALL\",\"CustomerName\": \"Ahmed\",\"DisplayCurrencyIso\": \"KWD\", \"MobileCountryCode\":\"+965\",\"CustomerMobile\": \"92249038\",\"CustomerEmail\": \"aramadan@myfatoorah.com\",\"InvoiceValue\": 200,\"CallBackUrl\": \"https://google.com\",\"ErrorUrl\": \"https://facebook.com\",\"Language\": \"en\",\"CustomerReference\" :\"ref 1\",\"CustomerCivilId\":12345678,\"UserDefinedField\": \"Custom field\",\"ExpireDate\": \"\",\"CustomerAddress\" :{\"Block\":\"\",\"Street\":\"\",\"HouseBuildingNo\":\"\",\"Address\":\"\",\"AddressInstructions\":\"\"},\"InvoiceItems\": [{\"ItemName\": \"Product 01\",\"Quantity\": 1,\"UnitPrice\": 200}]}",
//            CURLOPT_POSTFIELDS => "{\"NotificationOption\":\"ALL\",\"CustomerName\": \"$CustomerName\",\"DisplayCurrencyIso\": \"SAR\", \"MobileCountryCode\":\"+974\",\"CustomerMobile\": \"$CustomerMobile\",\"CustomerEmail\": \"$CustomerEmail\",\"InvoiceValue\": $InvoiceValue,\"CallBackUrl\": \"http://ezhel.my-staff.net/api/payment-success\",\"ErrorUrl\": \"http://ezhel.my-staff.net/api/payment-failed\",\"Language\": \"en\",\"CustomerReference\" :\"ref 1\",\"CustomerCivilId\":12345678,\"UserDefinedField\": \"Custom field\",\"ExpireDate\": \"\",\"CustomerAddress\" :{\"Block\":\"\",\"Street\":\"\",\"HouseBuildingNo\":\"\",\"Address\":\"\",\"AddressInstructions\":\"\"},\"InvoiceItems\": [{\"ItemName\": \"Product & Service\",\"Quantity\": 1,\"UnitPrice\": $InvoiceValue}]}",
////            CURLOPT_POSTFIELDS => json_encode($request->all()),
//            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token", "Content-Type: application/json"),
//
//        ));
//        //http://ghusn.net/api/payment-success
//        //http://ghusn.net/api/payment-failed
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
////dd($err);
//        curl_close($curl);
//        if ($err) {
//            return "cURL Error #:" . $err;
//        } else {
//            $data = json_decode($response);
//            dd($data);
//            if($data){
//                $InvoiceId = $data->Data->InvoiceId;
//
//                if($data->IsSuccess == true){
//                    //'attribute_id', 'attribute_type', 'amount','transaction_id', 'status', 'type'
//                    Payment::create([
//                        'attribute_id' => $request->shop_id,
//                        'attribute_type' => 'App/Models/Shop',
//                        'transaction_id' => $InvoiceId,
//                        'amount' => $amount,
//                        'status' => 1,
//                        'type' => 1,
//                    ]);
//                    return json_encode($data);
//                }
//                else{
//                    Payment::create([
//                        'attribute_id' => $request->shop_id,
//                        'attribute_type' => 'App/Models/Shop',
//                        'transaction_id' => $InvoiceId,
//                        'amount' => $amount,
//                        'status' => 0,
//                        'type' => 1,
//                    ]);
//
//                }
//            }
//
//            return "$response ";
//        }
//    }
    public function Payment(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'shop_id' => 'required|exists:shops,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 401, 'msg' => $validator->messages()->first()]);
        }
        $amount=AppSetting::where('key_name','shop_payment_price_monthly')->first()->val;
        $expiryDate=Carbon::now()->addMonth()->format('YY-d-m H:i');

        $shop=Shop::whereId($request->shop_id)->first();

        $apiURL = 'https://api-sa.myfatoorah.com';
        $apiKey = 'KLJew3bKpkx9DxixsosqzZY4QZI9mB7pEnKZkAo6auT7C1b7KBqLq29aimpDB-R0EvjhDa-aXttwyk7s0kkenahjFPP6E2sZLvlODeqxRNyxmVdf-JCrws6t-K_sxu6ksAQimg9bkEMimV_8m-ek2o68helzvSy0cROKbcM7vKajou8W6TlK5hspviqrEZoBWGyeuCPXTkfl004HUwj9diWudQlkaG7X_17KFUJLeypgwXN86NTz8tF14v4F4QDG1xv0Xe13Gm5z35Xk-iIABVeeQ6tWfN4gAJtTdBACl5EYmlT42Uxjv9-BZkhp5t0T9_QqpR9A9yXg3YEOXRVuYRZYnmn3h0plyzHytlQ-uUNqIvw_Cy6I3azY2Runyqalsb3-iyowZaXJWdjzesNnkiSd7ThrUViYVdXaA8JXXpqDZJLjHzY9Zx_Iflrb5KAqDA-uhD5C8TfiwB_t864KXNJx1RPqL3srYvZVLRzs-tbByo_anK7LWK7vSbwWEhEUahwEo3P3YtbEUhCA2lw5io3fh0WSFbYh77ERAt5fYAfME3u6iegI_JLq6EaU6_CcL-nYTV4-PHz6-v8ATaIxGT0NZMA7XIWNPC2RZV9GXWFE0MsjBYA_f9Gg8TXIgHSPTNyRyZ2H-ZZpqQx42w3r01nk6_E-lH9a1SrwPZNZLYX5ObZnPwle95BwBKOPgVHcW5-St1XpKOHrb90k1CcT0qBvh84'; //Test token value to be placed here: https://myfatoorah.readme.io/docs/test-token

//Live
//$apiURL = 'https://api.myfatoorah.com';
//$apiKey = ''; //Live token value to be placed here: https://myfatoorah.readme.io/docs/live-token


        /* ------------------------ Call SendPayment Endpoint ----------------------- */
//Fill customer address array
        /* $customerAddress = array(
          'Block'               => 'Blk #', //optional
          'Street'              => 'Str', //optional
          'HouseBuildingNo'     => 'Bldng #', //optional
          'Address'             => 'Addr', //optional
          'AddressInstructions' => 'More Address Instructions', //optional
          ); */

//Fill invoice item array
        /* $invoiceItems[] = [
          'ItemName'  => 'Item Name', //ISBAN, or SKU
          'Quantity'  => '2', //Item's quantity
          'UnitPrice' => '25', //Price per item
          ]; */

//Fill POST fields array
        $expiryDate=Carbon::now()->addMonth()->format('YY-d-m H:i');

        $postFields = [
            //Fill required data
            'NotificationOption' => 'Lnk', //'SMS', 'EML', or 'ALL'
            'InvoiceValue'       => $amount,
            'CustomerName'       => $shop->name,
            //Fill optional data
            'DisplayCurrencyIso' => 'SAR',
            'MobileCountryCode'  => '+966',
            'CustomerMobile'     => $shop->phone,
            'CustomerEmail'      => $shop->email,
            'CallBackUrl'        => route('shops.check.payment',['shop_id'=>$shop->id]),
            //'ErrorUrl'           => 'https://example.com/callback.php', //or 'https://example.com/error.php'
            'Language'           => 'ar', //or 'ar'
            'CustomerReference'  => $shop->id,
            //'CustomerCivilId'    => 'CivilId',
            'UserDefinedField'   => $expiryDate,
            //'ExpiryDate'         => '', //The Invoice expires after 3 days by default. Use 'Y-m-d\TH:i:s' format in the 'Asia/Kuwait' time zone.
            //'SourceInfo'         => 'Pure PHP', //For example: (Laravel/Yii API Ver2.0 integration)
            //'CustomerAddress'    => $customerAddress,
            //'InvoiceItems'       => $invoiceItems,
        ];

//Call endpoint
        $data = $this->sendPayment($apiURL, $apiKey, $postFields);
//dd($data);
//You can save payment data in database as per your needs
        $invoiceId   = $data->Data->InvoiceId;
        $paymentLink = $data->Data->InvoiceURL;
      if($data->IsSuccess == true){
                    //'attribute_id', 'attribute_type', 'amount','transaction_id', 'status', 'type'
                    Payment::create([
                        'attribute_id' => $request->shop_id,
                        'attribute_type' => 'App/Models/Shop',
                        'transaction_id' => $invoiceId,
                        'amount' => $amount,
                        'status' => 0,
                        'type' => 1,
                    ]);
                }


//Redirect your customer to the invoice page to complete the payment process
//Display the payment link to your customer
//        echo "Click on <a href='$paymentLink' target='_blank'>$paymentLink</a> to pay with invoiceID $invoiceId.";
//        die;
return redirect($paymentLink);
    }
    function sendPayment($apiURL, $apiKey, $postFields) {

        $json = $this->callAPI("$apiURL/v2/SendPayment", $apiKey, $postFields);
       return $json;
//        return $json->Data;
    }

//------------------------------------------------------------------------------
    /*
     * Call API Endpoint Function
     */

    function callAPI($endpointURL, $apiKey, $postFields = [], $requestType = 'POST') {

        $curl = curl_init($endpointURL);
        curl_setopt_array($curl, array(
            CURLOPT_CUSTOMREQUEST  => $requestType,
            CURLOPT_POSTFIELDS     => json_encode($postFields),
            CURLOPT_HTTPHEADER     => array("Authorization: Bearer $apiKey", 'Content-Type: application/json'),
            CURLOPT_RETURNTRANSFER => true,
        ));

        $response = curl_exec($curl);
        $curlErr  = curl_error($curl);

        curl_close($curl);

        if ($curlErr) {
            //Curl is not working in your server
            die("Curl Error: $curlErr");
        }

        $error = $this->handleError($response);
        if ($error) {
            die("Error: $error");
        }

        return json_decode($response);
    }

//------------------------------------------------------------------------------
    /*
     * Handle Endpoint Errors Function
     */

    function handleError($response) {

        $json = json_decode($response);
        if (isset($json->IsSuccess) && $json->IsSuccess == true) {
            return null;
        }

        //Check for the errors
        if (isset($json->ValidationErrors) || isset($json->FieldsErrors)) {
            $errorsObj = isset($json->ValidationErrors) ? $json->ValidationErrors : $json->FieldsErrors;
            $blogDatas = array_column($errorsObj, 'Error', 'Name');

            $error = implode(', ', array_map(function ($k, $v) {
                return "$k: $v";
            }, array_keys($blogDatas), array_values($blogDatas)));
        } else if (isset($json->Data->ErrorMessage)) {
            $error = $json->Data->ErrorMessage;
        }

        if (empty($error)) {
            $error = (isset($json->Message)) ? $json->Message : (!empty($response) ? $response : 'API key or API URL is not correct');
        }

        return $error;
    }

    /////////////////////////////////////////////////////////////
      public function checkPayment(Request $request){
//        return $shop_id;
          $apiURL = 'https://api-sa.myfatoorah.com';
          $apiKey = 'KLJew3bKpkx9DxixsosqzZY4QZI9mB7pEnKZkAo6auT7C1b7KBqLq29aimpDB-R0EvjhDa-aXttwyk7s0kkenahjFPP6E2sZLvlODeqxRNyxmVdf-JCrws6t-K_sxu6ksAQimg9bkEMimV_8m-ek2o68helzvSy0cROKbcM7vKajou8W6TlK5hspviqrEZoBWGyeuCPXTkfl004HUwj9diWudQlkaG7X_17KFUJLeypgwXN86NTz8tF14v4F4QDG1xv0Xe13Gm5z35Xk-iIABVeeQ6tWfN4gAJtTdBACl5EYmlT42Uxjv9-BZkhp5t0T9_QqpR9A9yXg3YEOXRVuYRZYnmn3h0plyzHytlQ-uUNqIvw_Cy6I3azY2Runyqalsb3-iyowZaXJWdjzesNnkiSd7ThrUViYVdXaA8JXXpqDZJLjHzY9Zx_Iflrb5KAqDA-uhD5C8TfiwB_t864KXNJx1RPqL3srYvZVLRzs-tbByo_anK7LWK7vSbwWEhEUahwEo3P3YtbEUhCA2lw5io3fh0WSFbYh77ERAt5fYAfME3u6iegI_JLq6EaU6_CcL-nYTV4-PHz6-v8ATaIxGT0NZMA7XIWNPC2RZV9GXWFE0MsjBYA_f9Gg8TXIgHSPTNyRyZ2H-ZZpqQx42w3r01nk6_E-lH9a1SrwPZNZLYX5ObZnPwle95BwBKOPgVHcW5-St1XpKOHrb90k1CcT0qBvh84'; //Test token value to be placed here: https://myfatoorah.readme.io/docs/test-token
          $payment=Payment::where(['attribute_id'=>$request->shop_id,'attribute_type'=>'App/Models/Shop'])->first();

//Live
//$apiURL = 'https://api.myfatoorah.com';
//$apiKey = ''; //Live token value to be placed here: https://myfatoorah.readme.io/docs/live-token


          /* ------------------------ Call getPaymentStatus Endpoint ------------------ */
//Inquiry using paymentId
          $keyId   =$request->paymentId;
          $KeyType = 'paymentId';

//Inquiry using invoiceId
          /*$keyId   = '613842';
          $KeyType = 'invoiceId';*/

//Fill POST fields array
          $postFields = [
              'Key'     => $keyId,
              'KeyType' => $KeyType
          ];
//Call endpoint
          $json       = $this->callAPI("$apiURL/v2/getPaymentStatus", $apiKey, $postFields);

//Display the payment result to your customer
          if($json->Data->InvoiceStatus=='Paid'){
//              $payment=Payment::where(['transaction_id'=>$payment_id,'attribute_type'=>'App\Models\Shop'])->first();
              $payment->update(['status'=>1]);
              Shop::whereId($request->shop_id)->update(['expiry_date'=>Carbon::now()->addMonth()]);
              $admin=Admin::where('shop_id',$request->shop_id)->first();

              Auth::login($admin);
              return redirect(route('shop_home'));

          }
          else{
              return redirect()->route('shops.login-page')->with('error', 'لم يتم الدفع من فضلك حاول مرة اخري');
          }
//          echo 'Payment status is ' . $json->Data->InvoiceStatus;

      }
//    public function editShopProfile(Request $request){
//        $shop=Shop::where('id', Auth::guard('shop')->user()->id)->first();
////        dd($shop);
//        $categories  = Category::get();
//
//        return view('cp_shop.shops.edit',['categories'=>$categories,'shop'=>$shop]);
//    }
//
//    public function updateShopProfile(Request $request){
//        $c=Shop::where('id', Auth::guard('shop')->user()->id)->first();
//        $c->update($request->all());
//        session()->flash('insert_message','تمت العملية بنجاح');
//        return back();
////        return back()->with('success','Data updated successfully');
//    }

}
