<?php

namespace App\Http\Controllers\Shop;

use App\Exports\ProductsExport;
use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use App\Models\BranchProduct;
use App\Models\Gov;
use App\Models\Menu;
use App\Models\Option;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Variation;
use Illuminate\Http\Request;
use App\Models\Country;
use DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Route;
use Session;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();
        if($shop->parent_id == null){
            $menus = Menu::where('shop_id',$shop->id)->get();
            $countries = Product::orderBy('id','desc')
                ->where('shop_id',$shop->id)
                ->with('menue')
                ->with(["variations" => function ($query) {
                    $query->with(["options"]);
                }])
                ->with('variations')
                ->paginate(10);
        }
        else{
            $menus = Menu::where('shop_id',$shop->parent_id)->get();
            $countries = Product::orderBy('id','desc')
                ->where('shop_id',$shop_id)
                ->orWhere('shop_id',$shop->parent_id)
                ->with('menue')
                ->with(["variations" => function ($query) {
                    $query->with(["options"]);
                }])
                ->with('variations')
                ->paginate(10);

            foreach ($countries as $c){
                $branch_product = BranchProduct::where('product_id',$c->id)
                    ->where('branch_id',$shop_id)->first();
                if($branch_product)
                    $c->active = $branch_product->status;
            }
        }

        return view('cp_shop.products.index',[
            'countries'=>$countries,
            'menus'=>$menus,
        ]);
    }
    public function create(){
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();

        if($shop->parent_id == null) {
            $menus = Menu::where('shop_id', $shop->id)->get();
        }
        else {
            $menus = Menu::where('shop_id', $shop->parent_id)->get();
        }
        return view('cp_shop.products.create',[
            'menus'=>$menus,
        ]);

        }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => '',
            'description_ar' => 'required',
            'description_en' => '',
            'menu_id' => 'required',
        ]);
        $add            = new Product();
        $add->name_ar   = $request->name_ar;
        $add->name_en   = $request->name_en;
        $add->description_ar   = $request->description_ar;
        $add->description_en   = $request->description_en;
        $add->has_sizes   = isset($request->has_sizes) ? 1 : 0;
        $add->menu_id   = $request->menu_id;
        $add->shop_id   = Auth::guard('shop')->user()->shop_id;
        $add->price_before   = $request->price_before;
        $add->price_after   = $request->price_after;
        $add->image   = $request->image;
        $add->save();
        if ($request->has('variations')){
            foreach ($request->variations as $var) {
                $variation = $add->variations()->create([
                    'type' => $var['type'],
                    'required' => isset($var['required']) ? 1 : 0,
                    'name_ar' => isset($var['name_ar']),
                    'name_en' => isset($var['name_en']),
                ]);
                if (isset($var['options'])) {
                    // type=3 , then this is size of product
                    //
                    foreach ($var['options'] as $option) {
                        $variation->options()->create([
                            'name_ar' => isset($option['name_ar']),
                            'name_en' => isset($option['name_en']),
                            'price' => isset($option['price']),
                            'type' => isset($option['type']),

                        ]);
                    }
                }
            }
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return redirect()->route('products.index')->with('success','Country added successfully');
    }



    public function editProduct(Request $request){
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'menu_id' => 'required',
        ]);

        Product::where('id', $request->model_id)->update($request->except('_token','model_id','has_sizes'));
        if(isset($request->has_sizes)){
            Product::where('id', $request->model_id)->update(['has_sizes' => 1]);
        }else{
            Product::where('id', $request->model_id)->update(['has_sizes' => 0]);
        }

        if($request->image)
        {
            $x=Product::where('id', $request->model_id)->first();
            $x->image = $request->image;
            $x->save();
        }


        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function editVariation(Request $request){
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'type' => 'required',
        ]);

        if(isset($request->required)){
            $required = 1;
        }else{
            $required = 0;
        }
        Variation::whereId($request->variation_id)->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'required' => isset($request->type)&&$request->type==0 ? 1 : 0,
            'type' => $request->type,
        ]);
        Option::where('variation_id',$request->variation_id)->update([
            'type' => $request->type
        ]);

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function editOption(Request $request){
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'price' => 'required',
        ]);
        Option::whereId($request->option_id)->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'price' => $request->price,
        ]);

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country updated successfully');
    }

    public function deleteProduct(Request $request)
    {
        Product::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function addVariation(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'type' => 'required',
        ]);
        $add            = new Variation();
        $add->name_ar   = $request->name_ar;
        $add->name_en   = $request->name_en;
        $add->required   = isset($request->type)&&$request->type==0 ? 1 : 0;
        $add->type   = $request->type;
        $add->product_id   = $request->product_id;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country added successfully');
    }

    public function deleteVariation(Request $request)
    {
        Variation::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function deleteOption(Request $request)
    {
        Option::whereId($request->model_id)->delete();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function addOption(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'name_ar' => 'required',
            'name_en' => 'required',
            'price' => 'required',
        ]);
        $variation = Variation::whereId($request->variation_id)->first();
        $add            = new Option();
        $add->name_ar   = $request->name_ar;
        $add->name_en   = $request->name_en;
        $add->price   = $request->price;
        $add->type   = $variation->type;
        $add->variation_id   = $request->variation_id;
        $add->save();
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Country added successfully');
    }

    public function editProductStatus(Request $request)
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();
        $porduct=Product::where("id",$request->model_id)->first();
        if($shop->parent_id == null){
            if($porduct->active == 1){
                Product::where("id",$request->model_id)
                    ->update(["active" => 0 ]);
            }else{
                Product::where("id",$request->model_id)
                    ->update(["active" => 1 ]);
            }
        }else{
            $check = BranchProduct::where('product_id',$request->model_id)
                ->where('branch_id',$shop_id)->first();
            if($check){
                if($check->status == 0){
                    $check->update(['status' => 1]);
                }else{
                    $check->update(['status' => 0]);
                }
            }else{
                BranchProduct::create([
                    'product_id' =>  $request->model_id,
                    'branch_id' =>  $shop_id,
                    'status' => $porduct->active == 1 ? 0 : 1
                ]);
            }

        }


        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }
    public function downloadProducts(){
        return Excel::download(new ProductsExport(), 'المنتجات.xlsx');

    }
    public function importProducts(){
       if(Excel::import(new ProductsImport(), request()->file('excel')));
        session()->flash('success', 'تم الرفع بنجاح');
        //return redirect()->route('shop.products');
        return redirect()->back();

    }

}
