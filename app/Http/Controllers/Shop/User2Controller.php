<?php

namespace App\Http\Controllers\Shop;

use App\Exports\DelegatesExport;
use App\Exports\DelegatesForShopExport;
use App\Http\Controllers\Interfaces\Admin\CategoryRepositoryInterface;
use App\Models\Admin;
use App\Models\CarLevel;
use App\Models\CarType;
use App\Models\Delegate;
use App\Models\DelegateDocument;
use App\Models\NationalType;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\ShopDelegate;
use App\Models\Slider;
use App\Models\SubscriptionsType;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Permission;

class User2Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:shop');
    }

    public function index(){
        $users = User::orderBy('id','desc')
            ->get();
        return view('cp.users.index',['users'=>$users]);

    }
    public function delegates(){
        $users = Delegate::join('shops_delegates','shops_delegates.delegate_id','delegates.id')
            ->where('shop_id',Auth::guard('shop')->user()->shop_id)
            ->where('shops_delegates.status',1)
            ->get();
        return view('cp_shop.delegates.index',['users'=>$users]);

    }
    public function pendingDelegate(){
        $users = Delegate::join('shops_delegates','shops_delegates.delegate_id','delegates.id')
            ->where('shop_id',Auth::guard('shop')->user()->shop_id)
            ->where('shops_delegates.status',0)
            ->get();
        return view('cp_shop.delegates.pending-delegates',['users'=>$users]);

    }
    public function acceptDelegate($id){
        $request=ShopDelegate::whereId($id)->first();
        $request->update(['status'=>1]);
        $delegate=Delegate::whereId($request->delegate_id)->first();
        Notification::sendNotifyTo($delegate->jwt, 'تم قبول طلب الانضمام من المتجر','تم قبول طلب الانضمام من المتجر', 4, 0, 0, 'delegates');

        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Record updated successfully');

    }
    public function sliders(){
        $offers = Slider::where('shop_id',Auth::user()->shop_id)
            ->where('active',1)
            ->get();
        return view('cp_shop.offers.index',['offers'=>$offers]);

    }

    public function index2(){
        $users = User::join("captin_infos","captin_infos.user_id","users.id")
            ->join("countries","countries.id","users.country_id")
            ->join("car_levels","car_levels.id","captin_infos.car_level")
            ->orderBy('id','desc')
            ->where("is_captin",1)
            ->select("users.id","users.name","users.phone","users.email","users.active",
                "users.suspend","countries.name_ar","countries.name_en","users.is_captin",
                "users.image","accept","busy","online","driving_license","working_hours",
                "id_image_1","id_image_2","car_license_1","car_license_2","feesh",
                "car_color","car_num","car_model",'color_name','car_image',
                'car_levels.id as car_level_id','car_levels.name as car_level_name')
            ->get();
        $carLevels = CarLevel::get();
        return view('cp.drivers.index',[
            'users'=>$users,
            'carLevels'=>$carLevels
        ]);
    }

    public function editCarLevel(Request $request)
    {
        CaptinInfo::where("user_id",$request->user_id)
            ->update(["car_level" => $request->car_level ]);

        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function accept_driver(Request $request,$id){
        CaptinInfo::where('user_id', $id)
            ->update(["accept" => 1]);
        session()->flash('insert_message','تمت العملية بنجاح');
        return back()->with('success','Record updated successfully');
    }

    public function indexAdmin(){
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();
        if($shop->parent_id == null){
            $permissions = Permission::where('id','>',103)->get();
            $users = Admin::orderBy('id','desc')
                ->where('id','>',1)
                ->where('id','!=',Auth::guard('shop')->user()->id)
                ->where('shop_id',$shop_id)
                ->where('type',2)
                ->with('permissions')
                ->get();
        }else{
            $permissions = Permission::where('id','>',103)
                ->where('id','!=',110)
                ->where('id','!=',111)
                ->where('id','!=',112)
                ->where('id','!=',113)
                ->where('id','!=',114)
                ->get();
            $users = Admin::orderBy('id','desc')
                ->where('id','>',1)
                ->where('id','!=',Auth::guard('shop')->user()->id)
                ->where('shop_id',$shop_id)
                ->where('type',4)
                ->with('permissions')
                ->get();
        }


        return view('cp_shop.admins.index',[
            'users' => $users,
            'permissions' =>$permissions,
        ]);
    }

    public function createAdmin(Request $request){
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();
        $user = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'jwt' => uniqid(),
            'phone' => $request->phone,
            'active' => 1,
            'token' => uniqid(),
            //'image' => isset($request->image) ? $request->image : null,
            'type' => $shop->parent_id == null ? 2 : 4,
            'shop_id' => $shop_id,
        ]);
        $user->givePermissionTo($request->permissions);
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function editAdmin(Request $request){
        $admin = Admin::whereId($request->model_id)->first();
        $admin->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => isset($request->password) ? $request->password : '123456',
            'phone' => $request->phone,
        ]);
        if(isset($request->image))
            $admin->update([ 'image' => $request->image ]);
        $admin->syncPermissions($request->permissions);
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function editAdminStatus(Request $request,$id)
    {
        $cat=Admin::where("id",$id)->first();
        if($cat->active == 1){
            Admin::where("id",$id)
                ->update(["active" => 0 ]);
        }else{
            Admin::where("id",$id)
                ->update(["active" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }

    public function deleteAdmin(Request $request,$id)
    {

        Admin::destroy($id);

        session()->flash('insert_message', 'تمت العملية بنجاح');
        return back()->with('success', 'Record deleted successfully');
    }



    public function editClientStatus(Request $request,$id)
    {
        $cat=User::where("id",$id)->first();
        if($cat->suspend == 1){
            User::where("id",$id)
                ->update(["suspend" => 0 ]);
        }else{
            User::where("id",$id)
                ->update(["suspend" => 1 ]);
        }
        session()->flash('insert_message','تمت العملية بنجاح');
        return back();
    }
    public function downloadDelegates(){
        return Excel::download(new DelegatesForShopExport(), 'المناديب.xlsx');

    }
    public function createDelegate(){
        $nationals=NationalType::where('active',1)
            ->where('type',0)

            ->select('id',"name_ar","num")
            ->get();
        $subscriptionTypes=SubscriptionsType::where('active',1)->select('id','name_ar')->get();
        $carTypes= CarType::where('active',1)
            ->where('type',1)
            ->select('id','name_ar')
            ->get();

        return view('cp_shop.delegates.create',compact('nationals','subscriptionTypes','carTypes'));

    }
    public function storeDelegate(Request $request){
//        dd($request->all());
//        Storage::put('text1.txt',$request);
        $this->validate($request,[
            'f_name' => 'required|max:190',
            'l_name' => 'required|max:190',
            'email' => 'required|max:190',
            'phone' => 'required|',
            'password' => 'required|min:6,max:190',
            //'city_id' => 'required|exists:cities,id',
            'image' => 'required',
            'front_car_image' => 'required',
            'back_car_image' => 'required',
            'insurance_image' => 'required',
            'license_image' => 'required',
            'civil_image' => 'required',
            'id_image' => 'required',
        ]);

        $data=$request->except('csrf_token');
        $data['shop_id']=Auth::guard('shop')->user()->shop_id;
        $data['active']=1;
        $data['accept']=1;
        $Delegate = Delegate::create($data);

        $add = new DelegateDocument();
        $add->user_id = $Delegate->id;
        $add->front_car_image = $input->front_car_image;
        $add->back_car_image = $input->back_car_image;
        $add->insurance_image = $input->insurance_image;
        $add->license_image = $input->license_image;
        $add->civil_image = $input->civil_image;
        $add->id_image = $input->id_image;
        $add->save();

//        $this->sendSMS('delegate', 'activate', $Delegate->phone);

    }

}
