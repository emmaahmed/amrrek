<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function registerNewShop(Request $request)
    {
        if (!isset($request->terms)) {
            return back();
        }
        $city=getCityLocation($request->lat,$request->lng);
        $request['city']=$city;
        $data = \App\Models\ShopRequest::create($request->all());
//        $data = \App\Models\ShopRequest::create([
//            'name' => $request->name,
//            'commercial_register' => $request->commercial_register,
//            'email' => $request->email,
//            'phone' => $request->phone,
//            'category_id' => $request->category_id,
//            'tax_num' => $request->tax_num,
//            'tax_img' => $request->tax_img,
//            'city' => $request->city,
//            'moderator_name' => $request->moderator_name,
//            'moderator_email' => $request->moderator_email,
//            'moderator_phone' => $request->moderator_phone,
//            'moderator_title' => $request->moderator_title,
//            'bank_name' => $request->bank_name,
//            'bank_account' => $request->bank_account
//        ]);
        if ($data)
            return view('success');
        return back();
    }
}
