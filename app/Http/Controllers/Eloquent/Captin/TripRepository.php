<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 27/06/2019
 * Time: 11:06 ص
 */

namespace App\Http\Controllers\Eloquent\Captin;


use App\Events\SendTripEvent;
use App\Models\Admin;
use App\Models\CaptinInfo;
use App\Http\Controllers\Interfaces\Captin\TripRepositoryInterface;
use App\Models\ActiveRequest;
use App\Models\CarLevel;
use App\Models\Category;
use App\Models\Country;
use App\Models\CountryCarLevel;
use App\Models\Driver;
use App\Models\Notification;
use App\Models\Notify;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PromoCode;
use App\Models\Rushhour;
use App\Models\ThirdCatOrder;
use App\Models\Trip;
use App\Models\TripDistance;
use App\Models\User;
use App\Models\UserWallet;
use Carbon\Carbon;
use DB;

class TripRepository implements TripRepositoryInterface
{
    public function collectedMoneyToday($driver_id, $driver_country_id, $files_completed, $lang)
    {
        $trips = Trip::orderBy('id', 'desc')
            ->where("country_id", $driver_country_id)
            ->where("driver_id", $driver_id)
            ->whereDate("created_at", Carbon::today())
            ->where("status", 3)
            ->select('trip_total')
            ->get();
        $total = 0;
        foreach ($trips as $trip) {
            $total = $total + $trip->trip_total;
        }

        /////////========================================
        $all_trips = Trip::orderBy('id', 'desc')
            ->where("country_id", $driver_country_id)
            ->where("driver_id", $driver_id)
            ->where("status", 3)
            ->select('trip_total')
            ->get();
        $total_cost = 0;
        foreach ($all_trips as $trip) {
            $total_cost = $total_cost + $trip->trip_total;
        }
        $total_cost = $total_cost * .3;//app_percentage
        $data = Admin::where('email', 'Amrrek@amrrek.com')->first();
        $driver_max_value = $data->driver_max_value;
        ////////////=====================================

        $currency = (string)Country::whereId($driver_country_id)->first()->currency;

        $collectedMoneyToday = number_format($total, 1, '.', '');

        $data = [];
        $data['collectedMoneyToday'] = $collectedMoneyToday;
        $data['currency'] = $currency;
        $data['files_completed'] = $files_completed;
        $data['total_money_must_paied'] = $total_cost;
        $data['warning_admin_message'] = $total_cost > $driver_max_value ? 1 : 0;

        return $data;

    }

    public function changeStatus($input, $driver_id, $country_id, $lang)
    {
        if($input->status==0 && $input->trip_id==0){
              Driver::where("id", $driver_id)->update(["busy" => 0]);
            return 'has-no-trip';
        }

        $trip = Trip::where("id", $input->trip_id)->with('trip_paths')->first();
        //return $trip;
        if (!$trip)
            return 'false-trip';
        if ($input->status == 1) {
            if ($trip->driver_id != 0) {
                //trip accepted by another driver
                return false;
            }
            // 1=waiting_captin
            $driver_trip =
                Trip::filterbylatlng($trip->start_lat, $trip->start_lng, 10000, 'drivers', $input->car_level_id,$driver_id);
            if(count($driver_trip)>0)
            {
                foreach ($driver_trip as $driver) {
                    Broadcast(new SendTripEvent($trip, $driver->jwt, $driver));
//                    Notification::sendNotifyTo($driver->jwt, $title, $message, 1, $trip->id, 0, 'users');
                }

            }
            $trip->update([
                "status" => 1,
                "driver_id" => $driver_id
            ]);

            Driver::where("id", $driver_id)->update(["busy" => 1]);
            if ($lang == "en") {
                $title = 'Trip Accepted ';
                $message = 'Trip Accepted ';
            } else {
                $title = 'تم قبول الرحلة ';
                $message = 'تم قبول الرحلة ';
            }
            $driver = Driver::where("id", $driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image as car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips','jwt')
                ->first();
            //$driver->car_image = !empty($driver->car_image) ? asset('captins/car_images'.$driver->car_image) : asset('/default.png') ;
            $driver->trip_status = 1;
            $driver->car_model = $trip->car_model;
            $driver->trip_msg = $message;

            $user = User::where("id", $trip->user_id)->first();

            $add            = new Notification();
            $add->title     = $title;
            $add->body      = $message;
            $add->user_id   = $trip->user_id;
            $add->type      = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
            $add->order_type   = 1;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
            $add->order_id   = $trip->id;
            $add->save();
            $trip->driver = $driver;
            $trip['test'] = $driver;
            unset($trip->pay_id, $trip->pay_status,
                $trip->canceled_by, $trip->cancel_id, $trip->cancel_reason,
                $trip->user_rate, $trip->driver_rate, $trip->user_comment, $trip->driver_comment);
//            Notification::send($user->token, $title,
//                $message, 6, 1,
//                null, NULL, NULL,
//                null, null, "$trip->id", "", $trip->id);
//            dd($trip);
            Broadcast(new SendTripEvent($trip,$user->jwt,$driver));
            Notification::sendNotifyTo($user->jwt,$title,$message,1,$trip->id,0,'users');

//            Notification::send(
//                "$user->token",
//                $title ,
//                $message ,
//                "" ,
//                1,
//                $trip,
//                null
//            );


        }
        ///////////////
        if ($input->status == 5) {
            // 1=driver_arrived
            $trip->update([
                "status" => 5,
                "driver_id" => $driver_id,
                "driver_arrive_date"=>Carbon::now()
            ]);

            if ($lang == "en") {
                $title = 'Order Accepted ';
                $message = 'Order Accepted ';
            } else {
                $title = 'السائق وصل لموقعك';
                $message = 'السائق وصل لموقعك';
            }
            $driver = Driver::where("id", $driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image as car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips')
                ->first();
            //$driver->car_image = !empty($driver->car_image) ? asset('captins/car_images'.$driver->car_image) : asset('/default.png') ;
            $driver->trip_status = 5;
            $driver->car_model = $trip->car_model;
            $driver->trip_msg = $message;

            $user = User::where("id", $trip->user_id)->first();

                    $add            = new Notification();
                    $add->title     = $title;
                    $add->body      = $message;
                    $add->user_id   = $trip->user_id;
                    $add->type      = 0;
                    $add->order_type   = 1;
                    $add->order_id   = $trip->id;
                    $add->order_id   = $trip->id;
                    $add->save();
            $trip->driver = $driver;
            Broadcast(new SendTripEvent($trip,$user->jwt,$driver));
            Notification::sendNotifyTo($user->jwt,$title,$message,1,$trip->id,0,'users');

//            Notification::send($user->token, $title,
//                $message, 6, 1,
//                null, NULL, NULL,
//                null, null, "$trip->id", "", $trip->id);

            //            Notification::send(
            //                "$user->token",
            //                $title ,
            //                $message ,
            //                "" ,
            //                1,
            //                $trip,
            //                null
            //            );


        }
        ///////////////
        if ($input->status == 2) {
            // 2=trip_started ,
            $trip->update([
                "status" => 2,
                "start_date"=>Carbon::now()

            ]);

            if ($lang == "en") {
                $title = 'Trip started ';
                $message = 'Trip started ';
            } else {
                $title = 'تم بدء الرحلة ';
                $message = 'تم بدء الرحلة ';
            }
            $driver = Driver::where("id", $driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image as car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips')
                ->first();
            $driver->trip_status = 2;
            $driver->car_model = $trip->car_model;
            $driver->trip_msg = $message;

            $user = User::where("id", $trip->user_id)->first();

            $add            = new Notification();
            $add->title     = $title;
            $add->body      = $message;
            $add->user_id   = $trip->user_id;
            $add->type      = 0;
            $add->order_type   = 1;
            $add->order_id   = $trip->id;
            $add->save();
            $trip->driver = $driver;
            unset($trip->pay_id, $trip->pay_status,
                $trip->canceled_by, $trip->cancel_id, $trip->cancel_reason,
                $trip->user_rate, $trip->driver_rate, $trip->user_comment, $trip->driver_comment);
            Broadcast(new SendTripEvent($trip,$user->jwt,$driver));
            Notification::sendNotifyTo($user->jwt,$title,$message,1,$trip->id,0,'users');

//            Notification::send($user->token, $title,
//                $message, 6, 1,
//                null, NULL, NULL,
//                null, null, "$trip->id", "", $trip->id);
            //
        }
        if ($input->status == 3) {   // 3=trip_finished

            $is_in_rush_period = $this->check_rush_time($trip, $country_id);
            //calculate trip
            $input['start_date']=$trip->start_date;
            $input['country_id']=$trip->country_id;
            $input['car_level_id']=$trip->car_level_id;
            $input['driver_arrive_date']=$trip->driver_arrive_date;
            $total= $this->calculateTripCost($input);

            $trip->update([
                "status" => 3,
                "end_date"=>Carbon::now()

            ]);
            $trip = Trip::where("id", $input->trip_id)->with('trip_paths')->first();
            if ($is_in_rush_period) {
                $service = CountryCarLevel::where('car_level_id', $trip->car_level_id)->first();
                $start_trip_unit = $service->rush_start_trip_unit;
                $distance_trip_unit = $service->rush_distance_trip_unit;
                $waiting_trip_unit = $service->rush_waiting_trip_unit;
            } else {
                $service = CountryCarLevel::where('car_level_id', $trip->car_level_id)->first();
                $start_trip_unit = $service->start_trip_unit;
                $distance_trip_unit = $service->distance_trip_unit;
                $waiting_trip_unit = $service->waiting_trip_unit;
            }
            // dd($service);
            if ($service) {
//                $total = $start_trip_unit + (($trip->distance)
//                        * $distance_trip_unit) +
//                    (($trip->waiting_time)
//                        * $waiting_trip_unit);
// dd($total);
                if ($trip->promo_id) {
                    //the user entered promo_id in creating trip
                    $promo = $this->checkPromo($input->promo_id, $input->car_level_id, $trip->user->country_id, $lang);
                } else {
                    //the saved promo in users table
                    if (isset($trip->user->promo_code))
                        $promo = $this->checkPromo($trip->user->promo_code, $input->car_level_id, $trip->user->country_id, $lang);
                }

                if (!empty($promo) && !is_string($promo)) {
                    //promo code set
                    if ($promo->type == 0) {
                        //promo fixed value
                        $trip_total = floor(($total) - ($promo->value));
                    } else {
                        //promo fixed value
                        $trip_total = floor(($total) - ($total) * ($promo->value / 100));
                    }
                    $trip->trip_total = $trip_total < 0 ? 0 : $trip_total;
                } else {
//                    if ($total > $trip->trip_total)
                        Trip::whereId($input->trip_id)->update(['trip_total' => $total]);
                }

                $user_wallet = User::whereId($trip->user_id)->select('wallet')->first();
                // dd($user_wallet);
                if ($user_wallet->wallet < $total) {
                    Trip::whereId($input->trip_id)->where('payment', 2)->update(['payment' => 0]);
                } elseif ($trip->payment == 2) {
//                    send notification to user
                }
//***********************************
                Driver::where('id', $trip->driver_id)->update(['busy' => 0]);

            }
            if ($trip->payment == 2) {
                // dd('test');
                if ($lang == "en"){
                    $title = 'wallet has been discount by '.$total;
                    $message = 'wallet has been discount by '.$total;
                }else{
                    $title = 'تم خصم ' .$total. " من المحفظة";
                    $message ='تم خصم ' .$total. " من المحفظة";
                }
                //
                $user = User::where("id", $trip->user_id)->first();
                $old_user_wallet = $user->wallet;
                $new_user_wallet = $old_user_wallet - $total;
                UserWallet::create([
                    'user_id' => $trip->user_id,
                    'old_value' => $old_user_wallet,
                    'new_value' => $new_user_wallet,
                    'description' => $title . 'تكلفة الرحلة الخاصة بتاكسي امرك',
                ]);
                    User::whereId($trip->user_id)->update(['wallet' => $user_wallet->wallet - $total]);
                //
            }
            else {
                if ($lang == "en") {
                    $title = 'Order Finished ';
                    $message = 'Order Finished ';
                } else {
                    $title = 'تم انهاء الرحلة ';
                    $message = 'تم انهاء الرحلة ';
                }
            }

            $driver = Driver::where("id", $driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image as car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips')
                ->first();
            $driver->trip_status = 3;
            $driver->car_model = $trip->car_model;
            $driver->trip_msg = $message;
            $trip->trip_paths = [];
            unset($trip->trip_paths);
            $user = User::where("id", $trip->user_id)->first();
            Notification::where('order_id',$trip->id)
                ->where('order_type',1)->delete();

            $add            = new Notification();
            $add->title     = $title;
            $add->body      = $message;
            $add->user_id   = $trip->user_id;
            $add->order_id   = $trip->id;
            $add->type      = 0;
            $add->order_type   = 1;
            $add->save();
            $trip->driver = $driver;
//            Notification::send($user->token, $title,
//                $message, 6, 1,
//                null, NULL, $user->wallet,
//                null, null, "$trip->id", "", $trip->id);
            //
            Broadcast(new SendTripEvent($trip,$user->jwt,$driver));
            Notification::sendNotifyTo($user->jwt,$title,$message,1,$trip->id,0,'users');

            User::whereId($trip->user_id)->update(['no_of_trips' => $user->no_of_trips + 1]);
            Driver::whereId($driver_id)->update(['no_of_trips' => $user->no_of_trips + 1]);
            //

        }
        if ($input->status == 4) {
            // 4=trip_cancelled"
            $trip->update([
                "status" => 4,
                "canceled_by" => 1,
                "cancel_id" => $input->cancel_id,
                "cancel_reason" => $input->cancel_reason,
            ]);

            Driver::where("id", $driver_id)->update(["busy" => 0]);

            if ($lang == "en") {
                $title = 'Order Cancelled ';
                $message = 'Order Cancelled ';
                $titleNewTrip = 'New order ';
                $messageNewTrip = 'You have a new order request,please respond ';
            } else {
                $title = 'تم الغاء الرحلة ';
                $message = 'تم الغاء الرحلة ';
                $titleNewTrip = 'طلب جديد ';
                $messageNewTrip = 'لديك طلب خدمة جديد,الرجاء الإستجابة ';
            }
            $driver = Driver::where("id", $driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image as car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips')
                ->first();
            $driver->trip_status = 4;
            $driver->car_model = $trip->car_model;
            $driver->trip_msg = $message;
            Notification::where('order_id',$trip->id)
                ->where('order_type',1)->delete();

            $user = User::where("id", $trip->user_id)->first();

//            $add            = new Notification();
//            $add->title     = $title;
//            $add->body      = $message;
//            $add->user_id   = $trip->user_id;
//            $add->type      = 0;
//            $add->order_type   = 1;
//            $add->order_id   = $trip->id;
//            $add->save();

            $trip->driver = $driver;
            Broadcast(new SendTripEvent($trip,$user->jwt,$driver,0));
            Notification::sendNotifyTo($user->jwt,$title,$message,1,$trip->id,0,'users');
            $driver_trip =
                Trip::filterbylatlng($trip->start_lat, $trip->start_lng, 10000, 'drivers', $user->user_country_id,$trip->car_level_id,$driver->id);
//          dd($driver_trip);
            if (count($driver_trip) > 0) {
                foreach ($driver_trip as $driver) {
//                            Notification::send("$driver->token", $title,
//                $message, "2", 1,
//                Null, NULL, NULL,
//                NULL,NULL,(object)[],
//                null, Null);
//                    dd($trip);
                    Broadcast(new SendTripEvent($trip, $driver->jwt,$driver));
                    Notification::sendNotifyTo($driver->jwt, $titleNewTrip, $messageNewTrip, 1, $trip->id, 0,'drivers');

                }

            }

//            Notification::send($user->token, $title,
//                $message, 6, 1,
//                null, NULL, NULL,
//                null, null, "$trip->id");
//            $add            = new Notification();
//            $add->title     = $title;
//            $add->body      = $message;
//            $add->user_id   = $trip->user_id;
//            $add->trip_id   = $trip->id;
//            $add->save();

//            Notification::send(
//                "$user->token",
//                $title ,
//                $message ,
//                "" ,
//                1,
//                $trip,
//                null
//            );


        }
        $trip = Trip::where("id", $input->trip_id)->with('user')->with('driver')->with('trip_paths')->first();
        //dd($trip);
        $trip->trip_id = $trip->id;
        if ($trip->driver_id > 0) {

            $driver = Driver::where("id", $trip->driver_id)
                ->select('id', 'f_name', 'l_name', 'phone', 'image', 'car_level as car_model',
                    'front_car_image', 'car_level', 'car_num',
                    'car_color', 'color_name', 'lat', 'lng', 'rate', 'no_of_trips')
                ->first();


        $trip['car_image'] = $driver->driver_documents?$driver->driver_documents->front_car_image:'';
        $trip['driver'] = $driver;
                    // return $driver;

        // dd($driver);

        }
        $size = sizeof($trip->trip_paths);

        $trip['end_lat'] = !empty($trip->end_lat) ? $trip->end_lat : $trip->trip_paths[($size) - 1]->lat;
        $trip['end_lng'] = !empty($trip->end_lng) ? $trip->end_lng : $trip->trip_paths[($size) - 1]->lng;
        $trip['end_address'] = !empty($trip->end_address) ? $trip->end_address : $trip->trip_paths[($size) - 1]->address;
        // return $trip->driver;

        return $trip;
    }

    public function calculateTripCost($input)
    {
        $carSettings=CountryCarLevel::where(['country_id'=>$input['country_id'],'car_level_id'=>$input['car_level_id']])->first();
        ///eslam yb3tlk al cost
         $startTrip=$carSettings->start_trip_unit;
         $kmPrice=$carSettings->distance_trip_unit;
         $timePrice=$carSettings->waiting_trip_unit;
//         $currentDate=Carbon::now();

        $time=Carbon::now()->diffInMinutes(Carbon::parse($input['start_date']));
         $driverWaitingTime=Carbon::parse($input['start_date'])->diffInMinutes(Carbon::parse($input['driver_arrive_date']));
//        dd($driverWaitingTime);
         if($driverWaitingTime>$carSettings->max_driver_waiting_time) {
             $driverWaitingTime=$driverWaitingTime-$carSettings->max_driver_waiting_time;
             }
         else{
             $driverWaitingTime=0;
         }
         //distance by km
//                dd($driverWaitingTime);
//dd((($driverWaitingTime)));
        $total=$startTrip+($input->distance*$kmPrice)+(($time+$driverWaitingTime)*$timePrice);
        $trip_distance = Trip::whereId($input->trip_id)->first();
        if ($trip_distance) {
            $trip_distance->trip_distance = $input->distance;
            $trip_distance->waiting_time = $time+$driverWaitingTime;
            $trip_distance->save();
        }

         return (int)$total;
         //        $locations = $input->locations;
//        $waiting_time = 0;
//        $total_distance = 0;
//
//        $count = count($locations);
//        $locations = json_encode($locations);
//
//
//        if ($count > 0) {
//
//            $actualTotal = getDistanceLatLng($locations);
//
//            $total_distance = $actualTotal['distance'];
//        }
//
//        //store in DB
//
//        $trip_distance = Trip::whereId($input->trip_id)->first();
//        if ($trip_distance) {
//            $trip_distance->trip_distance = $total_distance;
//            $trip_distance->waiting_time = $waiting_time;
//            $trip_distance->save();
//        }
    }

    public function tripHistory($driver_id, $type, $key, $lang, $driver_country_id)
    {
        //$type = 0=>all, 1=>cash, 2=>credit
        //$key = 0=>weekly, 1=>monthly, 2=>yearly

        global $trips;
        $data = [];
        $allTrips = [];
        $allTrips2 = [];

        $currentDate = Carbon::now();
        $lastWeek = $currentDate->subDays(7);
        $lastmonth = $currentDate->subDays(30);
        $lastYear = $currentDate->subDays(365);
        $data = [];
        if ($type == 0) { //$type= payment all
            if ($key == 0) { //$key=weekly
                $trips = Trip::orderBy('created_at', 'desc')
                
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastWeek)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            }
            elseif ($key == 1) { //$key=monthly
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastmonth)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            }
            elseif ($key == 2) { //$key=$lastYear
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastYear)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            }
        }
        elseif ($type == 1) { //$type= payment cash
            if ($key == 0) { //$key=weekly
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastWeek)
                    ->where("payment", 0)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            } elseif ($key == 1) { //$key=monthly
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastmonth)
                    ->where("payment", 0)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            } elseif ($key == 2) { //$key=$lastYear
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastYear)
                    ->where("payment", 0)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            }
        }
        elseif ($type == 2) { //$type= payment online
            if ($key == 0) { //$key=weekly
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastWeek)
                    ->where("payment", 1)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            } elseif ($key == 1) { //$key=monthly
                $trips = Trip::orderBy('id', 'desc')
                    ->where("driver_id", $driver_id)
                    ->where("country_id", $driver_country_id)
                    ->where("created_at", ">", $lastmonth)
                    ->where("payment", 1)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            } elseif ($key == 2) { //$key=$lastYear
                $trips = Trip::orderBy('id', 'desc')
                    ->where("country_id", $driver_country_id)
                    ->where("driver_id", $driver_id)
                    ->where("created_at", ">", $lastYear)
                    ->where("payment", 1)
                    ->where("status", 3)
//                    ->with('driver')
//                    ->with('user')
                ->select('id','type','date','time','trip_total','created_at','driver_rate as rate','driver_comment as comment')

                    ->with(["trip_paths" => function ($query) use ($lang) {
                        $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
                    }])->paginate(20);
            }
        }

//         $total = 0;
//         foreach ($trips as $trip) {
//             $data['id'] = $trip->id;
//             $data['start_lat'] = $trip->start_lat;
//             $data['start_lng'] = $trip->start_lng;
//             $data['start_address'] = $trip->start_address;
//             $size = sizeof($trip->trip_paths);
//             $data['end_lat'] = !empty($trip->end_lat) ? $trip->end_lat : $trip->trip_paths[($size) - 1]->lat;
//             $data['end_lng'] = !empty($trip->end_lng) ? $trip->end_lng : $trip->trip_paths[($size) - 1]->lng;
//             $data['end_address'] = !empty($trip->end_address) ? $trip->end_address : $trip->trip_paths[($size) - 1]->address;
//             $data['trip_total'] = isset($trip->trip_total) ? $trip->trip_total : "";
//             $data['created_at'] = $trip->created_at;
//             $data['rate'] = isset($trip->driver_rate) ? $trip->driver_rate : "";
//             $data['comment'] = isset($trip->driver_comment) ? $trip->driver_comment : "";
//             $data['currency'] = $trip->currency;

//             array_push($allTrips, $data);
//             $total = $total + $trip->trip_total;
// //            unset($trip->end_address,$trip->end_lat,$trip->end_lng);
//         }

//        $trips=Trip::where("driver_id",$driver_id)
//            ->with('driver')
//            ->with('user')
//            ->with(["trip_paths" => function($query) use($lang){
//                $query->select('id','status','address','lat','lng','trip_id');
//            }])->get();
        $currency = (string)Country::whereId($driver_country_id)->first()->currency;
        $total=$trips->sum('trip_total');
        //$total = $total * 0.7;

        $allTrips['total_trips'] = sizeof($trips);
        $allTrips['total_profits'] = number_format($total, 1, '.', '') . " $currency";
        $allTrips['trips'] = $trips;
        // $allTrips2 ['total_profits'] = ;
        // $allTrips2 ['trips'] = $allTrips;

        return $allTrips;
    }

    public function rateTrip($input, $user_id, $is_captin, $lang)
    {
        if ($is_captin == 0) {
            Trip::where("id", $input->trip_id)
                ->update([
                    "user_rate" => $input->rate,
                    "user_comment" => $input->comment,
                ]);
            $avg_rate = Order::where('user_id',$user_id)
                ->whereNotNull('user_rate')
                ->avg('user_rate');
            $avg_trip_rate = Trip::where('user_id',$user_id)
                ->whereNotNull('user_rate')
                ->avg('user_rate');
            $avg = ($avg_rate + $avg_trip_rate) / 2;
            User::whereId($user_id)->update(['rate' => $avg]);
        } else {
            Trip::where("id", $input->trip_id)
                ->where("driver_id", $user_id)->update([
                    "driver_rate" => $input->rate,
                    "driver_comment" => $input->comment,
                ]);
            $driver_rate = Trip::where("id", $input->trip_id)
                ->avg('driver_rate');
            Driver::whereId($user_id)->update(['rate' => $driver_rate]);
        }
    }

    public function updateStatus($input, $driver_id, $lang)
    {
        CaptinInfo::where("user_id", $driver_id)
            ->update([
                "online" => $input->online,
            ]);
    }

    public function collectMoney($input, $driver_id, $lang)
    {
        $trip = Trip::where("id", $input->trip_id)->where("driver_id", $driver_id)->first();
        $user = User::where('id', $trip->user_id)->first();
        $old_user_wallet = $user->wallet;
        $user->wallet += ($input->money) - ($trip->trip_total);
        $user->save();
//        User::where('id',$trip->user_id)->update([
//            'wallet' => ($input->money) - ($trip->trip_total)
//        ]);

        UserWallet::create([
            'user_id' => $trip->user_id,
            'old_value' => $old_user_wallet,
            'new_value' => $user->wallet,
            'description' => 'إضافة باقي النقدية بعد انتهاء الرحلة الخاصة بتاكسي امرك',
        ]);

        $added_wallet_value = ($input->money) - ($trip->trip_total);
        if ($lang == "en") {
            $title = 'New Message ';
            $message = 'Your wallet raised by ' . $added_wallet_value;
        } else {
            $title = 'رسالة جديدة';
            $message = 'تم اضافة ' . $added_wallet_value . 'الي محفظتك';
        }
        Notification::send(
            "$user->token",
            $title,
            $message,
            "",
            1,
            null,
            null,
            $user->wallet
        );

    }

    public function check_rush_time($trip, $country_id)
    {
        $flag = false;
        if ($trip->type == 'urgent') {
            $trip_time = $trip->created_time;
        } else {
            $trip_time = $trip->time;
        }
        $rush_periods = Rushhour::where('country_id', $country_id)->get();
        if (sizeof($rush_periods) > 0) {
            foreach ($rush_periods as $rush_period) {
//            dd($trip_time, $rush_period->to, $rush_period->from);
                if ($trip_time <= $rush_period->to && $trip_time >= $rush_period->from) {
                    $flag = true;
                }
            }
            return $flag;
        }
        return false;

    }

    public function checkPromo($promo_id, $car_level_id, $country_id, $lang)
    {
        $codeCheck = PromoCode::where("id", $promo_id)
            ->select('id', 'code', 'value', 'type', 'country_ids', 'car_level_ids', 'expire_times', 'expire_at', $lang . '_desc as description')
            ->first();
        if ($codeCheck) {
            if ((int)strtotime($codeCheck->expire_at) < (int)strtotime(Carbon::now()->format('d F Y')))
                return "code_expired";

            $car_level_ids = explode(',', $codeCheck->car_level_ids);
            if (!(in_array($car_level_id, $car_level_ids)))
                return "invalid_code.";

            $trips = Trip::where("promo_id", $codeCheck->id)->get()->count();
            if ($trips >= $codeCheck->expire_times)
                return "code_expired";

            $country_ids = explode(',', $codeCheck->country_ids);
            if (!(in_array($country_id, $country_ids)))
                return "invalid_code_";

            unset($codeCheck->country_ids, $codeCheck->expire_times,
                $codeCheck->expire_at, $codeCheck->created_at, $codeCheck->updated_at);

            $codeCheck->type = (int)$codeCheck->type;
            return $codeCheck;
        }
        return "invalid_code";
    }

    public function getCredits($request, $driver_id, $lang)
    {
        $driver=Driver::whereId($driver_id)->first();
        // global $total_trips;
        // global $driver_credit;
        // global $admin_credit;
        // global $driver_has_money;
        // global $admin_has_money;
        // $total_trips = 0;
        // $driver_credit = 0;
        // $admin_credit = 0;
        // $driver_has_money = 0;
        // $admin_has_money = 0;
        // $trips = Trip::where('driver_id', $driver_id)->where('status', 3)->get();
        // foreach ($trips as $trip) {
        //     $total_trips = $total_trips + $trip->trip_total;
        //     if ($trip->payment == 0) {//cash
        //         $driver_has_money = $driver_has_money + $trip->trip_total;
        //     } else {//online & wallet
        //         $admin_has_money = $admin_has_money + $trip->trip_total;
        //     }
        // }
        // //dd($total_trips);
        // //dd($driver_has_money);
        // $driver_credit = $total_trips * 0.7;
        // //$driver_credit = $driver_credit - $driver_has_money;
        // $admin_credit = $total_trips * 0.3;
        //$admin_credit = $admin_credit - $driver_has_money;
        return $data = [
            'driver_credit' => number_format($driver->trips_total, 1, '.', ''),
            'admin_credit' => number_format($driver->app_total, 1, '.', ''),
        ];
    }

    public function putInWallet($input,$driver_id,$lang)
    {
        $trip = Trip::whereId($input->trip_id)->first();
        $user = User::whereId($trip->user_id)->first();
        $old_user_wallet = $user->wallet;
        $money = $user->wallet + $input->money;
        $user->update(['wallet' => $money]);
//                    send notification to user
        if ($lang == "en") {
            $title = 'Your wallet has updated, current value: ' . $input->money;
            $message = 'Your wallet has updated, current value: ' . $input->money;
//            $message = 'wallet has been discount by ' . $input->money;
        } else {
//            $title = 'تم خصم ' . $input->money . " من المحفظة";
            $title = 'تم تعديل قيمة محفظتك, القيمة الحالية: ' . $input->money ;
            $message = 'تم تعديل قيمة محفظتك, القيمة الحالية: ' . $input->money ;
        }

        UserWallet::create([
            'user_id' => $trip->user_id,
            'old_value' => $old_user_wallet,
            'new_value' => $money,
            'description' => 'إضافة باقي النقدية بعد انتهاء الرحلة الخاصة بتاكسي امرك',
        ]);
        $add            = new Notification();
        $add->title     = $title;
        $add->body      = $message;
        $add->user_id   = $trip->user_id;
        $add->type      = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
        $add->order_type   = 1;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        $add->order_id   = $trip->id;
        $add->save();

        Notification::send($user->token, $title,
            $message, 50, 1,
            null, NULL, $user->wallet,
            null, null, null);
    }
    public function tripDetails($input, $user_id, $lang)
    {
        $trip = Trip::
        where("id", $input->trip_id)
            ->with('user')
            ->with('driver')
            ->with(["trip_paths" => function ($query) use ($lang) {
                $query->select('id', 'status', 'address', 'lat', 'lng', 'trip_id');
            }])
            ->select('id', 'type', 'user_id', 'driver_id', 'date', 'time',
                'user_rate', 'driver_rate', 'trip_total',
                'start_address', 'start_lat', 'start_lng',
                'end_lat', 'end_lng', 'end_address',
                'created_at')
            ->first();
        $trip['end_path']=$trip->lastPath[0];

        $size = sizeof($trip->trip_paths);
        $trip->end_lat = !empty($trip->end_lat) ? $trip->end_lat : $trip->trip_paths[($size) - 1]->lat;
        $trip->end_lng = !empty($trip->end_lng) ? $trip->end_lng : $trip->trip_paths[($size) - 1]->lng;
        $trip->end_address = !empty($trip->end_address) ? $trip->end_address : $trip->trip_paths[($size) - 1]->address;

        if ($trip->driver) {
            $data = [
                'car_image' => $trip->driver->front_car_image,
                'car_color' => $trip->driver->car_color,
                'color_name' => $trip->driver->color_name,
                'car_num' => $trip->driver->car_num,
                'car_model' => $trip->driver->car_model,
            ];


            $trip->car_info = $data;
        } else {
            $trip->car_info = null;
        }
        unset($trip->trip_paths);
//        if($trip->driver_id){
//            $trip->car_info = CaptinInfo::where('user_id',$trip->driver_id)
//                ->select('car_image','car_color','color_name','car_num','car_model')
//                ->first();
//
//        }else{
//            $trip->car_info = (object)[];
//            $trip->driver = (object)[];
//        }


        return $trip;
    }
public function updateBusy($driver_id){
        Driver::whereId($driver_id)->update([
            'busy'=>0
        ]);
}
}
