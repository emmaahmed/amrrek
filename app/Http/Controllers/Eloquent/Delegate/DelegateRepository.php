<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 04/07/2019
 * Time: 02:43 م
 */

namespace App\Http\Controllers\Eloquent\Delegate;


use App\Events\OffersEvent;
use App\Events\OrdersChatEvent;
use App\Http\Controllers\Interfaces\Delegate\DelegateRepositoryInterface;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\Category;
use App\Models\Delegate;
use App\Models\Message;
use App\Models\MessageImage;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderChangedDelegate;
use App\Models\OrderInvoice;
use App\Models\OrderOffer;
use App\Models\OrderStatus;
use App\Models\RejectedOrder;
use App\Models\ReplacedPoint;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\ShopDelegate;
use App\Models\User;
use App\Models\UserReplacedPoint;
use App\Models\Worker;
use App\Models\WorkerThirdCat;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DB;

class DelegateRepository implements DelegateRepositoryInterface
{

    public function getShops($request, $lang, $delegate_id, $delegate_country_id)
    {
        Delegate::whereId($delegate_id)->update([
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);
        ///////===================================
        $all_orders_ids = Order::orderBy('id', 'desc')
            ->where("delegate_id", $delegate_id)
            ->where("offer_id", '!=', null)
            ->pluck('id');
        $orders_ids = OrderStatus::whereIn('order_id', $all_orders_ids)->pluck('order_id');
        $driver_orders = Order::whereIn('id', $orders_ids)->with('offer')->get();
        $total_cost = 0;
        foreach ($driver_orders as $driver_order) {
            if ($driver_order->offer)
                $total_cost = $total_cost + $driver_order->offer->offer;
        }
        $total_cost = $total_cost * .3;//app_percentage
//        $data = Admin::where('email', 'admin@admin.com')->first();
        $settings = AppSetting::where('key_name', 'delegate_max_value')->first();
        $delegate_max_value = $settings->val;
        //////////================================================
        $shop = new Shop();
        if (isset($request->search_key)) {
            $shops = $shop->filterbylatlngbySearchKey($request->lat, $request->lng, 50, "shops", $delegate_id, $request->search_key, $delegate_country_id);
        } else {
            $shops = $shop->filterbylatlng($request->lat, $request->lng, 50, "shops", $delegate_id, $delegate_country_id);
        }

        $data = [
            'shops' => $shops,
            'warning_admin_message' => $total_cost > $delegate_max_value ? 1 : 0,
        ];
        return $data;

//        if($request->category_id==0)
//
//        return $shop->filterbylatlngByCatId($request->lat, $request->lng,
//            500, "shops", $request->category_id);
    }

    public function subscribeAsDelegate($request, $delegate_id, $lang)
    {
        $shopDelegateCheck = ShopDelegate::where('delegate_id', $delegate_id)
            ->where('shop_id', $request->shop_id)
            ->first();
        if ($shopDelegateCheck) {
            if($shopDelegateCheck->status==1) {
                $shopDelegateCheck->delete();
                return false;
            }else{
                return 'pending';

            }
        }

        else {
            ShopDelegate::create([
                'delegate_id' => $delegate_id,
                'shop_id' => $request->shop_id,
            ]);
            return true;
        }
    }

    public function waitingOrders($request, $delegate_id, $lang, $delegate_country_id)
    {
        $shop = Shop::whereId($request->shop_id)->first();
        ///
        $latitudeFrom = $shop->lat;
        $longitudeFrom = $shop->lng;
        $latitudeTo = $request->lat;//delegate lat
        $longitudeTo = $request->lng;//delegate lng
        $earthRadius = 6371;
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $angle2 = $angle * $earthRadius;
        ///

        $order = new Order();
        $orders = $order->filterbylatlng($shop->lat, $shop->lng, 100, "orders", null, null, "", $delegate_country_id, $delegate_id);
        $dist = request()->header('lang') == "ar" ? "كم" : "Km";

        foreach ($orders as $order) {
            $distance = (double)($order->distance) + (double)($angle2);
            $order->distance = number_format($distance, 2, '.', '') . " " . $dist;
        }
        return ($orders);
    }

    public function allWaitingOrders($request, $delegate_id, $near_orders, $lang, $delegate_country_id)
    {
        $data = [];

        $department_id = isset($request->department_id) ? $request->department_id : 3;
        $order = new Order();
        if ($request->department_id == 5) { //orders of heavey machines
            $rejecredOrders = RejectedOrder::where('delegate_id', $delegate_id)->pluck('order_id')->toArray();
            $old_orders_delegate = OrderChangedDelegate::where('delegate_id', $delegate_id)->pluck('order_id')->toArray();
            $orders = $order->filterbylatlngWaitingOrderforDelegatesHeavy($request->lat, $request->lng, 50, "orders", $delegate_id, $near_orders, null, $delegate_country_id, $department_id);
            foreach ($orders as $order) {
                if (!in_array($order->id, $rejecredOrders) &&
                    !in_array($order->id, $old_orders_delegate)
                ) {
                    array_push($data, $order);
                }
            }
            $orders = $data;
        }
        else {
            $orders = $order->filterbylatlngWaitingOrderforDelegates($request->lat, $request->lng, 50, "orders", $delegate_id, $near_orders, null, $delegate_country_id, $department_id);
        }
        foreach ($orders as $order) {
            if (request()->department_id == 5) {
                unset($order->counter, $order->order_number, $order->order_number, $order->offer_id,
                    $order->country_id, $order->confirm_code, $order->confirm_accept, $order->title,
                    $order->promo_id, $order->distance, $order->shop_rate, $order->shop_comment,
                    $order->delegate_rate, $order->delegate_comment, $order->user_rate, $order->user_comment);
            } else {
                //
                $latitudeFrom = $order->out_lat;
                $longitudeFrom = $order->out_lng;
                $latitudeTo = $request->lat;//delegate lat
                $longitudeTo = $request->lng;//delegate lng
                $earthRadius = 6371;
                // convert from degrees to radians
                $latFrom = deg2rad($latitudeFrom);
                $lonFrom = deg2rad($longitudeFrom);
                $latTo = deg2rad($latitudeTo);
                $lonTo = deg2rad($longitudeTo);
                $latDelta = $latTo - $latFrom;
                $lonDelta = $lonTo - $lonFrom;
                $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
                $angle2 = $angle * $earthRadius;
                ///
                $dist = request()->header('lang') == "ar" ? "كم" : "Km";

                $distance = (double)($order->distance) + (double)($angle2);
                $order->distance = number_format($distance, 2, '.', '') . " " . $dist;
            }

        }
        return ($orders);
    }

    public function myOrders($request, $delegate_id, $lang, $delegate_country_id)
    {
        global $data;
        $data = [];
        $order_status = request()->status;
        $old_orders_delegate = OrderChangedDelegate::where('delegate_id', $delegate_id)
            ->pluck('order_id')->toArray();
        $cancelled_orders = OrderStatus::whereNotNull('cancelled')->pluck('order_id')->toArray();
        if (request()->department_id == 2) {
            $order = new Order();
            $orders = $order->filterbylatlng(1.1, 1.1, 100, "orders", $delegate_id, null, "$order_status", "$delegate_country_id");
            foreach ($orders as $order) {

                $shop = Shop::whereId($order->shop_id)->first();
                if ($shop) {
                    $order->shop = $shop;
                    $latitudeFrom = $shop->lat;
                    $longitudeFrom = $shop->lng;
                    $latitudeTo = $request->lat;//delegate lat
                    $longitudeTo = $request->lng;//delegate lng
                    $earthRadius = 6371;
                    // convert from degrees to radians
                    $latFrom = deg2rad($latitudeFrom);
                    $lonFrom = deg2rad($longitudeFrom);
                    $latTo = deg2rad($latitudeTo);
                    $lonTo = deg2rad($longitudeTo);
                    $latDelta = $latTo - $latFrom;
                    $lonDelta = $lonTo - $lonFrom;
                    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
                    $angle2 = $angle * $earthRadius;
                    ///
                } else {
                    $angle2 = 0;
                }
                $distance = (double)($order->distance) + (double)($angle2);
                $dist = request()->header('lang') == "ar" ? "كم" : "Km";

                $order->distance = number_format($distance, 2, '.', '') . " " . $dist;
                $order_cases = OrderStatus::where('order_id', $order->id)->first();
                if ($order_status == 0) { // current
                    if ($order_cases->finished == "" &&
                        (!in_array($delegate_id, $old_orders_delegate)) &&
                        (!in_array($order->id, $cancelled_orders))
                    )
                        array_push($data, $order);
                } elseif ($order_status == 1) { // finished
                    if ($order_cases->finished != "" &&
                        (!in_array($delegate_id, $old_orders_delegate)) &&
                        (!in_array($order->id, $cancelled_orders))
                    )
                        array_push($data, $order);
                }

            }
        }
        if (request()->department_id == 3) {
            $order = new Order();
            $orders = $order->filterbylatlng(1.1, 1.1, 100, "orders", $delegate_id, null, "", "$delegate_country_id");

            foreach ($orders as $order) {
                $order->shop = null;
                $latitudeFrom = $order->in_lat;
                $longitudeFrom = $order->in_lng;
                $latitudeTo = $request->lat;//delegate lat
                $longitudeTo = $request->lng;//delegate lng
                $earthRadius = 6371;
                // convert from degrees to radians
                $latFrom = deg2rad($latitudeFrom);
                $lonFrom = deg2rad($longitudeFrom);
                $latTo = deg2rad($latitudeTo);
                $lonTo = deg2rad($longitudeTo);
                $latDelta = $latTo - $latFrom;
                $lonDelta = $lonTo - $lonFrom;
                $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
                $angle2 = $angle * $earthRadius;
                ///
                $dist = request()->header('lang') == "ar" ? "كم" : "Km";

                $distance = (double)($order->distance) + (double)($angle2);
                $order->distance = number_format($distance, 2, '.', '') . " " . $dist;
                $order_cases = OrderStatus::where('order_id', $order->id)->first();
                if ($order_status == 0) { // current
                    if ($order_cases->finished == "" &&
                        (!in_array($delegate_id, $old_orders_delegate)) &&
                        (!in_array($order->id, $cancelled_orders))
                    )
                        array_push($data, $order);
                } elseif ($order_status == 1) { // finished
                    if ($order_cases->finished != "" &&
                        (!in_array($delegate_id, $old_orders_delegate)) &&
                        (!in_array($order->id, $cancelled_orders))
                    )
                        array_push($data, $order);
                }
                //
                if ($order->department_id == 3)
                    $order->total_cost = isset($order->offer->offer) ? $order->offer->offer : 0;
            }
        }

        $date = now()->format('Y-m-d');

        if (request()->department_id == 5) {
            if (request()->status == 0) { // current orders
                $current = OrderStatus::whereNotNull('accept')
                    ->whereNull('finished')
                    ->whereNull('cancelled')->pluck('order_id');

                $orders = Order::orderBy('id', 'desc')
//                    ->where('id','!=',269)
                    ->where('department_id', 5)
                    ->whereNotIn('delegate_id', $old_orders_delegate)
                    ->where('delegate_id', $delegate_id)
                    ->whereIn('id', $current)
//                    ->where(DB::raw("DATE(delivery_time) = '".Carbon::now()->toTimeString()."'"))
                    //  ->whereDate('delivery_time',$date)
//                    ->where('delivery_time',Carbon::now()->toTimeString())
                    ->whereHas('order_status', function ($q) {
                        $q->whereNull('finished');
                    })
                    ->with(['sub_department' => function ($q) use ($lang) {
                        $q->select('id', 'name_' . $lang . ' as name');
                    }])
                    ->with('order_images')
                    ->with('offer')
                    ->with('user')
                    ->get();

                $data = [];

                foreach ($orders as $order) {

                    if ($date == date('Y-m-d', strtotime($order->delivery_time))) {
                        array_push($data, $order);
                    }
                }
//                dd($data[0]->delivery_time);
            } elseif (request()->status == 1) { // comming orders
                $date = now()->format('Y-m-d');

                $comming = OrderStatus::whereNotNull('accept')
                    ->whereNull('finished')
                    ->whereNull('cancelled')->pluck('order_id');

                $orders = Order::where('department_id', 5)
                    ->whereNotIn('delegate_id', $old_orders_delegate)
                    ->where('delegate_id', $delegate_id)
                    // ->whereDate('created_at', '<', now()->format('Y-m-d H:i'))
                    ->whereIn('id', $comming)
                    // ->where(DB::raw('DATE_FORMAT(cust.cust_dob, "%d-%b-%Y") as formatted_dob'))
                    // ->whereDate('delivery_time', '>=', $date)
                    //   ->where(DB::raw('CAST(delivery_time AS DATETIME)'),'>',$date)
                    //->where('delivery_time','>',Carbon::now())
//                    ->whereHas('order_status' , function ($q){
//                        $q->whereNotNull('finished');
//                    })
                    // ->where('delivery_time','>',$date)
                    ->with(['sub_department' => function ($q) use ($lang) {
                        $q->select('id', 'name_' . $lang . ' as name');
                    }])
                    ->with('order_images')
                    ->with('offer')
                    ->with('user')
                    ->get();

                $data = [];

                foreach ($orders as $order) {

                    if ($date < date('Y-m-d', strtotime($order->delivery_time))) {
                        array_push($data, $order);
                    }
                }

                //  dd($data[0]->delivery_time);


            } elseif (request()->status == 2) { // previous orders
                $previous = OrderStatus::whereNotNull('accept')
                    ->whereNotNull('finished')
                    ->whereNull('cancelled')->pluck('order_id');
                $data = Order::where('department_id', 5)
                    ->where('delegate_id', $delegate_id)
                    ->whereNotIn('delegate_id', $old_orders_delegate)
                    ->whereIn('id', $previous)
//                    ->where(DB::raw("DATE(delivery_time) < '".Carbon::now()."'"))
                    ->where('delivery_time', '<', $date)
                    ->orwhere('delivery_time', $date)
                    ->whereHas('order_status', function ($q) {
                        $q->whereNotNull('finished');
                    })
                    ->with(['sub_department' => function ($q) use ($lang) {
                        $q->select('id', 'name_' . $lang . ' as name');
                    }])
                    ->with('order_images')
                    ->with('offer')
                    ->with('user')
                    ->orderBy('id', 'desc')
                    ->get();
            }
        }

        return ($data);
    }

    public function orderDetails($request, $delegate_id, $type, $delegate_lat, $delegate_lng, $lang)
    {
        app()->setLocale($lang);
        if ($type = 1) {
            $order = Order::whereId($request->order_id)
                ->with(["order_products" => function ($query) {
                    $query->with(["variations" => function ($query) {
                        $query->with('options');
                    }]);
                }])
                ->with("order_status")
                ->with("order_images")
                ->with(['sub_department' => function ($q) use ($lang) {
                    $q->select('id', 'name_' . $lang . ' as name');
                }])
                ->with("shop")
                ->with("user")
                // ->with('offer')

                ->first();
        } else {
            $order = Order::whereId($request->order_id)
                ->with(["order_products" => function ($query) {
                    $query->with(["order_product_variations" => function ($query) {
                        $query->with('order_product_variation_options');
                    }]);
                }])
                ->with("order_status")
                ->with("order_images")
                ->with(['sub_department' => function ($q) use ($lang) {
                    $q->select('id', 'name_' . $lang . ' as name');
                }])
                ->with("shop")
                ->with("user")
                // ->with('offer')

                ->first();
        }

        $delegate_id = $order->delegate_id;
        $delegate = Delegate::whereId($delegate_id)->first();
        if ($delegate) {
            $delegate_lat = $delegate->lat;
            $delegate_lng = $delegate->lng;
        } else {
            $delegate_lat = $order->in_lat;
            $delegate_lng = $order->in_lng;
        }

        //dd($delegate_lat,$delegate_lng);
        //
        //
        if ($order) {
            if ($order->order_products) {
                foreach ($order->order_products as $product) {
                    if (isset($product->order_product_variations->order_product_variation_options)) {
                        global $product_details;
                        $product_details = "";
                        foreach ($product->order_product_variations->order_product_variation_options as $option) {
                            $product_details = $product_details . $option->name . ', ';
                        }
                        $product_details = substr($product_details, 0, -2);
                        $product->product_details = $product_details;
                    }
                }
            }
        }


//        $result = getDistanceAndTime($order->out_lat,$order->out_lng,$delegate_lat,$delegate_lng);
//        if(isset($result->rows[0]) && isset($result->rows[0]->elements[0]) && isset($result->rows[0]->elements[0]->distance)){
//            $distance_val_in_miles = $result->rows[0]->elements[0]->distance->value;
//            $distance_val_in_km = ($distance_val_in_miles * 1.609344)/1000;
//            $distance_val_in_km = number_format($distance_val_in_km, 2, '.', '');
//        }else{
//            $distance_val_in_km = 0;
//        }

//        $minMax=0;
        if ($order->department_id == 2) {
            $km_price = AppSetting::where('key_name', 'price_of_km_in_shops')->select('val')->first()->val;
            $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_shops')->select('val')->first()->val;
            $distance = getDistance($request->lat, $request->lng, $order->out_lat, $order->out_lng, $order->in_lat, $order->in_lng);
//dd($distance);
        }
        if ($order->department_id == 3) {
            $km_price = AppSetting::where('key_name', 'price_of_km_in_heavey')->select('val')->first()->val;
            $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_heavy')->select('val')->first()->val;
            $distance = getDistance($request->lat, $request->lng, $order->in_lat, $order->in_lng, $order->out_lat, $order->out_lng, $order->last_lat == null ? 0 : $order->last_lat, $order->last_lng == null ? 0 : $order->last_lng);

        }
        if ($order->department_id == 5) {
            $km_price = AppSetting::where('key_name', 'price_of_km_in_heavey')->select('val')->first()->val;
            $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_heavy')->select('val')->first()->val;
            $distance = getDistance($request->lat, $request->lng, $order->in_lat, $order->in_lng, $order->out_lat, $order->out_lng, $order->last_lat == null ? 0 : $order->last_lat, $order->last_lng == null ? 0 : $order->last_lng);
        }
        if($distance<=0) {
            $delivery_price = 1;
        }
        else{
            $delivery_price = $km_price * ($distance);

        }
        $order->update(['distance' => $distance]);

        $min_delivery_price = (int)($delivery_price * $percentage);
        $max_delivery_price = (int)($delivery_price + ($delivery_price * $percentage));

        $data = [];
        // $maxOffer = OrderOffer::where('order_id', $request->order_id)
        //     ->min('offer');
        $maxOffer = OrderOffer::whereId($order->offer_id)
            ->first();

        $lastOffer = OrderOffer::where('order_id', $request->order_id)
            ->where('delegate_id', $delegate_id)->first();

//        if($min_delivery_price==0){
//            $data['maxOffer'] = 0;
//            $data['minOffer'] = 0;
//        }else {
        $data['maxOffer'] = empty($maxOffer) ? (string)$max_delivery_price : (string)$maxOffer->offer;
        $data['minOffer'] = empty($lastOffer) ? (isset($min_delivery_price) ? (string)$min_delivery_price : 0) : (string)$lastOffer->offer;
//        }

        $data['lastOffer'] = empty($lastOffer) ? "" : (string)$lastOffer->offer;

        $order->offer = $data;
        $data['currency'] = $order->country->currency;

        if (request()->header('lang') == "en") {
            $order->distance = $distance . " " . "km";
        } else {
            $order->distance = $distance . " " . "كم";
        }
        return $order;
    }

    public function subscribedShops($request, $delegate_id, $lang)
    {
        $shops_ids = ShopDelegate::where('delegate_id', $delegate_id)->where('status',1)->pluck('shop_id');
        ///
        $shops = Shop::whereIn('id', $shops_ids)
            ->select('id', 'name', 'image', 'description_' . $lang . ' as description')->paginate(20);
        return ($shops);
    }

    public function changeStatus($request, $delegate_id, $lang)
    {
        //    public static final int accept = 1;
        //    public static final int on_way = 2;
        //    public static final int finished = 3;
        //    public static final int received = 4;
        //    public static final int on_first_way = 5;
        //    public static final int processed = 6;
        //    public static final int on_last_way = 7;
        //    public static final int with_draw = 8;
        //==========================================
        //1=>accept ,4=received , 2 =>on_way , 3=>finished ,
        // 5=on_first_way , 6=processed , 7=on_last_way
        // 10=processing_started
        //============================

        $order = Order::whereId($request->order_id)->first();
        $order_status = OrderStatus::where('order_id', $request->order_id)->first();
        $user = User::whereId($order->user_id)->first();
        //--to update status from notifications in mobile
        $chat = [];
        $chat['order_id'] = $order->id;
        //---------------------------------------------
        if ($request->status == 1) {
            //this accept is negligtable because the accept from user
            if (empty($order->delegate_id)) {
                $order->delegate_id = $delegate_id;
                $order->save();
                OrderStatus::where('order_id', $request->order_id)
                    ->update(['accept' => Carbon::now()]);
            }
            //
        }
        elseif ($request->status == 4) {
            if ($order->delegate_id == $delegate_id)
                if ($order_status->cancelled != null) {
                    $title = 'تم إلغاء الطلب من العميل';
                    return false;
                }
            if ($order_status->processing_started == null && $order->department_id == 2) {
//                    $title = 'تم إلغاء الطلب من العميل';
                return 'order-not-ready';
            }
            OrderStatus::where('order_id', $request->order_id)
                ->update(['received' => Carbon::now()]);


            if (request()->header('lang') == "ar") {
                $title = 'المندوب استلم طلبك';
            } else {
                $title = 'Delegate received your order';
            }

            $chat['confirm_accept_order'] = 5;
//            Notification::send("$user->token", $title ,
//                $title , 1 ,1,
//                "",$chat,NULL,$order->id,0);
            Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 5));
            Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

        }
        elseif ($request->status == 2) {
            if ($order->delegate_id == $delegate_id)
                if ($order_status->cancelled != null) {
                    $title = 'تم إلغاء الطلب من العميل';
                    return false;
                }
            OrderStatus::where('order_id', $request->order_id)
                ->update(['on_way' => Carbon::now()]);


            if (request()->header('lang') == "ar") {
                $title = 'المندوب في الطريق إلي المعالجة';
            } else {
                $title = 'The delegate is on the way to treatment';
            }
            $chat['confirm_accept_order'] = 6;
//            Notification::send("$user->token", $title ,
//                $title , 1 ,1,
//                "",$chat,NULL,$order->id,0);
            Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 6));
            Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

        }
        elseif ($request->status == 5) {
            if ($order->delegate_id == $delegate_id) {
                if ($order_status->cancelled != null) {
                    $title = 'تم إلغاء الطلب من العميل';
                    return false;
                }
                if ($order->confirm_accept < 2 && $order->department_id != 5) {
                    $title = 'يرجي تأكيد الطلب اولا';
                    return "need_confirm_order";
                }
                OrderStatus::where('order_id', $request->order_id)
                    ->update(['on_first_way' => Carbon::now()]);

                if (request()->header('lang') == "ar") {
                    $title = 'المندوب في الطريق لاستلام الشحنة';
                } else {
                    $title = 'The delegate is on the way to collect the shipment';
                }
                $chat['confirm_accept_order'] = 4;
                Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 4));
                Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');
                if ($order->department_id == 2) {
                    $shop = Admin::where(['shop_id' => $order->shop_id])->whereIn('type',[1,3])->first();

                    Notification::sendNotifyTo($shop->jwt, $title,
                        $title, 1, $order->id,
                        2, 'shops');
                }

//                Notification::send("$user->token", $title ,
//                    $title , 1 ,1,
//                    "",$chat,NULL,$order->id,0);
            }

        }
        elseif ($request->status == 6) {
            if ($order->delegate_id == $delegate_id)
                if ($order_status->cancelled != null) {
                    $title = 'تم إلغاء الطلب من العميل';
                    return false;
                }
            OrderStatus::where('order_id', $request->order_id)
                ->update(['processed' => Carbon::now()]);

            if (request()->header('lang') == "ar") {
                $title = 'تمت المعالجة';
            } else {
                $title = 'Treatment id done';
            }
            $chat['confirm_accept_order'] = 7;
            Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 7));
            Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

//            Notification::send("$user->token", $title ,
//                $title , 1 ,1,
//                "",$chat,NULL,$order->id,0);
        }
        elseif ($request->status == 7) {
            if ($order->delegate_id == $delegate_id)
                if ($order_status->cancelled != null) {
                    $title = 'تم إلغاء الطلب من العميل';
                    return false;
                }
            OrderStatus::where('order_id', $request->order_id)
                ->update(['on_last_way' => Carbon::now()]);

            if (request()->header('lang') == "ar") {
                $title = 'المندوب في الطريق لتسليم الشحنة';
            } else {
                $title = 'The delegate is on the way to deliver the shipment';
            }
            $chat['confirm_accept_order'] = 8;
            Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 8));
            Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

//            Notification::send("$user->token", $title ,
//                $title , 1 ,1,
//                "",$chat,NULL,$order->id,0);
        }
        elseif ($request->status == 3) {
            $confirm_code = $request->confirm_code;
            $settings=AppSetting::where('key_name','app_percent_delegate')->first();

            if ($order->delegate_id == $delegate_id) {
                    if ($order_status->cancelled != null) {
                        $title = 'تم إلغاء الطلب من العميل';
                        return false;
                    }
                    if($order->invoice && $order->invoice->confirmed==1) {
                        $pointsSetting=AppSetting::where('key_name','points_per_order')->first();
                        Notification::where('order_id', $order->id)
                            ->where('order_type', '>', 1)->delete();

                        OrderStatus::where('order_id', $request->order_id)
                            ->update(['finished' => Carbon::now()]);
                        $order->update(['confirm_accept' => 3]);
                        $delegate=Delegate::whereId($order->delegate_id)->first();
                        if($delegate->subscription_type==1){
                            $orders_total=$delegate->orders_total+$order->offer->offer-($order->offer->offer*($settings->val/100));
                            $app_total=$delegate->app_total+($order->offer->offer*($settings->val/100));
                            $delegate->update(['busy' => 0, 'points' => ($delegate->points + $pointsSetting->val),'orders_total'=>$orders_total,'app_total'=>$app_total]);

                        }
                        else {
                            $delegate->update(['busy' => 0, 'points' => ($delegate->points + $pointsSetting->val)]);
                        }
                        if (request()->header('lang') == "ar") {
                            $title = 'تم تسليم الشحنة';
                        } else {
                            $title = 'The shipment has delivered';
                        }
                        $chat['confirm_accept_order'] = 3;
                        Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, 3));
                        Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');
                    }
                    else{
                        return 'invoice-not-confirmed';
                    }
//                Notification::send("$user->token", $title ,
//                    $title , 1 ,1,
//                    "",(object)$chat,NULL,$order->id,3);
                } else {
                    return false;
                }

        }
        elseif ($request->status == 8) { //delegate leave order

            if ($order_status->cancelled != null) {

                if (request()->header('lang') == "ar") {
                    $title = 'تم إلغاء الطلب من العميل';
                } else {
                    $title = 'Customer cancelled the order';
                }
                Notification::where('order_id', $order->id)
                    ->where('order_type', '>', 1)->where('type', 1)->delete();

                OrderOffer::where('id', $order->offer_id)
                    ->update(['status' => 1]);
                Notification::send("$user->token", $title,
                    $title, 1, 1,
                    "", "", NULL, $order->id, 0);
                $add = new Notification();
                $add->title = $title;
                $add->body = $title;
                $add->user_id = $user->id;
                $add->order_id = $order->id;
                $add->type = 1;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                $add->order_type = 0;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                $add->save();
                return false;
            }
            if ($order_status->on_first_way == null) {
                //delegate can yns7b from order when the order isnt starting yet
                OrderStatus::where('order_id', $request->order_id)
                    ->update(['accept' => null]);
                Delegate::whereId($order->delegate_id)->update(['busy' => 0]);

                Order::whereId($request->order_id)
                    ->update([
                        'delegate_id' => null,
                        'offer_id' => null
                    ]);

                //
                OrderChangedDelegate::create([
                    'order_id' => $request->order_id,
                    'delegate_id' => $order->delegate_id,
                ]);
                if (request()->header('lang') == "ar") {
                    $title = 'قد انسحب المندوب من توصيل طلبك';
                } else {
                    $title = 'The delegate has withdrawn from delivering your order';
                }
                $chat['confirm_accept_order'] = -1;
                Notification::where('order_id', $order->id)
                    ->where('order_type', '>', 1)->where('type', 1)->delete();
                Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, -1));
                Notification::sendNotifyTo($user->jwt, $title, $title, 0, $order->id, $order->department_id, 'users');

//                Notification::send("$user->token", "$title" ,
//                    "$title" , 0 ,1,
//                    "",$chat,NULL,$order->id,0);
                //
            }
            //
        }
        elseif ($request->status == 9) { //delegate leave order
            if ($order_status->cancelled != null) {
                if (request()->header('lang') == "ar") {
                    $title = 'تم إلغاء الطلب من العميل';
                } else {
                    $title = 'User cancelled the order';
                }
                Notification::where('order_id', $order->id)
                    ->where('order_type', '>', 1)->delete();
                Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, -1));
                Notification::sendNotifyTo($user->jwt, $title, $title, 0, $order->id, $order->department_id, 'users');

//                Notification::send("$user->token", $title ,
//                    $title , 1 ,1,
//                    "","",NULL,$order->id,0);
                $add = new Notification();
                $add->title = $title;
                $add->body = $title;
                $add->user_id = $user->id;
                $add->order_id = $order->id;
                $add->type = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                $add->order_type = 0;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                $add->save();
                return false;
            }
            if ($order->confirm_accept < 2) {
                //delegate can yns7b from order when the order isnt starting yet
                // OrderStatus::where('order_id', $request->order_id)
                //     ->update([
                //         'accept' => null,
                //         'cancelled' => Carbon::now(),
                //         'cancel_by' => 1,
                //         'cancel_id' => $request->cancel_id,
                //         'cancel_reason' => $request->cancel_reason,
                //     ]);
                Delegate::whereId($order->delegate_id)->update(['busy' => 0]);

                Order::whereId($request->order_id)
                    ->update([
                        'delegate_id' => null,
                        'offer_id' => null
                    ]);
                //
                OrderStatus::where('order_id', $request->order_id)
                    ->update(['accept' => null]);

                if (request()->header('lang') == "ar") {
                    $title = 'قد ألغي المندوب توصيل طلبك';
                } else {
                    $title = 'Delegate cancelled the order';
                }
                $chat = [];
                $chat['confirm_accept_order'] = -1;
                $chat['order_id'] = $request->order_id;
                Broadcast(new OrdersChatEvent($chat, $user->jwt, "", $user->image, $order->department_id, -1));
                Notification::sendNotifyTo($user->jwt, $title, $title, 0, $order->id, $order->department_id, 'users');

//                Notification::send("$user->token", "$title" ,
//                    "$title" , 0 ,1,
//                    "",$chat,NULL,$order->id,0);
                $old_delegate = Delegate::whereId($order->delegate_id)->first();
                OrderChangedDelegate::create([
                    'order_id' => $request->order_id,
                    'delegate_id' => $order->delegate_id,
                ]);
                $old_delegates_array = OrderChangedDelegate::where('order_id', $order->id)
                    ->pluck('delegate_id')->toArray();
                $delegates = Order::filterDelegates($order->in_lat, $order->in_lng, 500000, 'delegates', null, null, null, "$order->country_id");
                foreach ($delegates as $delegate) {
                    if (!in_array($delegate->id, $old_delegates_array)) {
                        Notification::send("$delegate->token", $title,
                            $title, 8, 1,
                            "$order", NULL, NULL, $order->id);
                    }


                }

                //
            }
            //
        }

        $add = new Notification();
        $add->title = $title;
        $add->body = $title;
        $add->user_id = $user->id;
        $add->order_id = $order->id;
        $add->type = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
        $add->order_type = $order->department_id;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        $add->save();

        return OrderStatus::where('order_id', $request->order_id)->first();
    }

    public function sendConfirmRequest($request, $delegate_id, $lang)
    {
        $order = Order::whereId($request->order_id)->first();
        $settings = AppSetting::where('key_name', 'confirm_order_count')->first();
        if ($order->confirm_order_count == $settings->val) {
            return false;
        }
        $order->update(['confirm_accept' => 1, 'confirm_order_count' => ($order->confirm_order_count + 1)]);
        //delegate send accept order

        if (request()->header('lang') == "ar") {
            $title = "تأكيد الطلب";
            $message = "تم ارسال تأكيد الطلب";
        } else {
            $title = "confirm order";
            $message = "Order confirmation has been sent";
        }
        $user = User::whereId($order->user_id)->first();
        $messageChat = Message::create([
            'sender_id' => $delegate_id,
            'sender_type' => 1,//0=>user , 1=>delegate
            'receiver_id' => $order->user_id,
            'receiver_type' => 0,//0=>user , 1=>delegate
            'message' => $message,
            'confirm_accept_order' => 1,
            'type' => 0,//0=>orders , 1=>trips
            'order_id' => $request->order_id
        ]);
        $messageChat = Message::whereId($messageChat->id)->with('messages_image')->first();
        $add = new Notification();
        $add->title = $title;
        $add->body = $title;
        $add->user_id = $user->id;
        $add->order_id = $order->id;
        $add->type = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
        $add->order_type = $order->department_id;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        $add->save();
        $delegate = Delegate::whereId($delegate_id)->select('token', 'image')->first();
        $messageChat['delegate_image'] = $delegate->image;
        Broadcast(new OrdersChatEvent($messageChat, $user->jwt, $delegate->image, $user->image, $order->department_id, 1));
        Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

//        Notification::send("$user->token", $title ,
//            $message , 1 ,1,
//            "","$messageChat",NULL,$order->id,1);

        $data = Message::whereId($messageChat->id)
            ->with('messages_image')
            ->first();
        return $data;

    }

    public function ratesOfOrders($delegate_id)
    {
        return Order::where('delegate_id', $delegate_id)
            ->where('delegate_rate', '!=', null)
            ->where('delegate_comment', '!=', null)
            ->with('user')
            ->select('delegate_rate as rate', 'delegate_comment as comment', 'created_at', 'user_id', 'country_id')
            ->get();
    }

    public function getOrderOffers($request, $delegate_id)
    {
        $order = Order::whereId($request->order_id)->with('country')->first();
        $delivery_price = 0;
        $percentage = 0.1;
        //dd($order->country->currency);
        if ($order) {

            //department_id >> 2 => shops ,3=>anyToAny , 5 => heavyMachines
            if ($order->department_id == 2) {
                $km_price = AppSetting::where('key_name', 'price_of_km_in_shops')->select('val')->first()->val;
                $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_shops')->select('val')->first()->val;
                $distance = getDistance($request->lat, $request->lng, $order->out_lat, $order->out_lng, $order->in_lat, $order->in_lng);
//dd($distance);
            } elseif ($order->department_id == 3) {
                $km_price = AppSetting::where('key_name', 'price_of_km_in_any_to_any')->select('val')->first()->val;
                $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_any_thing')->select('val')->first()->val;
                $distance = getDistance($request->lat, $request->lng, $order->in_lat, $order->in_lng, $order->out_lat, $order->out_lng, $order->last_lat == null ? 0 : $order->last_lat, $order->last_lng == null ? 0 : $order->last_lng);

            } elseif ($order->department_id == 5) {
                $km_price = AppSetting::where('key_name', 'price_of_km_in_heavey')->select('val')->first()->val;
                $percentage = AppSetting::where('key_name', 'percentage_per_orders_in_heavy')->select('val')->first()->val;
                $distance = getDistance($request->lat, $request->lng, $order->in_lat, $order->in_lng, $order->out_lat, $order->out_lng, $order->last_lat == null ? 0 : $order->last_lat, $order->last_lng == null ? 0 : $order->last_lng);

            }
            $delivery_price = $km_price * ($distance);
            $order->update(['distance' => $distance]);

//            else {
//                $delivery_price = 0;
//            }

//            $temp = $delivery_price * (25/100) ;
            $min_delivery_price = $delivery_price * $percentage;
            $max_delivery_price = $delivery_price + ($delivery_price * $percentage);
//dd($delivery_price);
            if ($delivery_price == 0) {
                $min_delivery_price = 25;
                $max_delivery_price = 100;
            }
            $data = [];
            $lastOffer = OrderOffer::where('order_id', $request->order_id)
                ->min('offer');
            //////=========STATIC VALUES FOR TEST
//            $min_delivery_price = 25;
//            $max_delivery_price = 100;
            //////=========
//
            $data['minOffer'] = (int)$min_delivery_price;
            $data['maxOffer'] = (int)$max_delivery_price;
            $data['lastOffer'] = empty($lastOffer) ? "" : (string)$lastOffer;
            $data['currency'] = request()->header('lang') == "ar" ? "ريال" : "Riyal";
            //$data['currency'] = $order->country->currency;

            return $data;
        }


    }

    public function addOrderOffer($request, $delegate_id, $delegate_token)
    {
        $order = Order::whereId($request->order_id)
            ->whereNull('delegate_id')
            ->first();
        if ($order) {
            $status=OrderStatus::where('order_id', $request->order_id)->first();

            if ($status->cancelled != null) {
                $title = 'تم إلغاء الطلب من العميل';
                return false;
            }

            //getMinMaxPrice($country_id, $distance)
            $order_offer_count = OrderOffer::orderBy('id', 'desc')
                ->where('delegate_id', $delegate_id)
                ->where('order_id', $request->order_id)
                ->count();
            if ($order_offer_count > 100)//must be max as 3 //TODO
                return "max_offers_limit";

            $check_order_offer = OrderOffer::orderBy('offer', 'asc')
                ->where('delegate_id', $delegate_id)
                ->where('order_id', $request->order_id)
                ->first();
            if ($check_order_offer) {
                if ($request->offer > $check_order_offer->offer)
                    return false;
            }

            $order = Order::whereId($request->order_id)->with('user')->first();
            $distance1 = calculateDistanceBetweenTwoPoints($order->in_lat, $order->in_lng, $order->out_lat, $order->out_lng);
            $distance2 = calculateDistanceBetweenTwoPoints($order->in_lat, $order->in_lng, $request->lat, $request->lng);
            $total_distance = $distance1 + $distance2;

            //==
            $offer = OrderOffer::orderBy('id', 'desc')
                ->where('order_id', $request->order_id)
                ->where('delegate_id', $delegate_id)
                ->first();
            if ($offer) {

                $offer->update(['offer' => $request->offer]);
                $add = OrderOffer::whereId($offer->id)->first();
            } else {
                $add = new OrderOffer();
                $add->delegate_id = $delegate_id;
                $add->order_id = $request->order_id;
                $add->offer = $request->offer;
                $add->distance = number_format($total_distance, 1, '.', '');
                $add->save();
                $add = OrderOffer::whereId($add->id)->first();

            }


//////
            ////

            $lower_offers_counter = Order::whereId($request->order_id)->select('counter')->first()->counter;
            $orders_count = Order::where('delegate_id', $add->delegate_id)->count();
            $comments_count = Order::where('delegate_id', $add->delegate_id)
                ->where('delegate_rate', '!=', NULL)
                ->where('delegate_comment', '!=', NULL)
                ->count();

            $add->delegate->orders_count = $orders_count;
            $add->delegate->comments_count = $comments_count;
            $add->lower_offers_counter = $lower_offers_counter;
            $add->delivery_time = $order->delivery_time;
            ///
            if (request()->header('lang') == "ar") {
                $title = ' عرض سعر ';
                $message = 'لديك عرض سعر جديد ' . $request->offer;
            } else {
                $title = ' offer price ';
                $message = 'You have a new price offer' . $request->offer;
            }
            // return $add;
//            dd($order->user->jwt);
            Broadcast(new OffersEvent($add, $order->user->jwt, $add->delegate, $lower_offers_counter, $add->delivery_time,$orders_count,$comments_count));
            Notification::sendNotifyTo($order->user->jwt, $title, $message, 0, $order->id, $order->department_id, 'users');

//            Notification::send($order->user->token, $title ,
//                $message , 0 ,1,
//                "$add",NULL,NULL,$request->order_id);

            $add = new Notification();
            $add->title = $title;
            $add->body = $message;
            $add->user_id = $order->user_id;
            $add->type = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
            $add->order_type = $order->department_id;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
            $add->order_id = $order->id;
            $add->save();

///
            //notification to user
            return true;
        }

    }

    public function getDelegateMessages($request, $user_id, $delegate_image, $lang)
    {
        $order_id = $request->order_id;
        $order = Order::whereId($order_id)->first();
        $department_id = $order->department_id;

        $receiver_id = $order->user_id; //user
        if ($order) {
            $user_info = User::whereId($order->user_id)->select('id', 'name', 'name', 'phone')->first();
        } else {
            $user_info = (object)[];
        }
        $messages = DB::select("
                SELECT id,message,order_id,created_at,
                    sender_id,sender_type,receiver_id,receiver_type,confirm_accept_order
                FROM messages
                Where(
                (sender_id = $user_id and sender_type = 1 AND receiver_id = $receiver_id and receiver_type = 0)
                OR
                (sender_id = $receiver_id and sender_type = 0 AND receiver_id = $user_id and receiver_type = 1)
                )AND (order_id = $order_id)
                AND (type = 0)
                Order By id asc
            ");
        if ($messages) {
            //dd($messages[0]->sender_id,$messages[0]->sender_type == 0);
            if ($messages[0]->sender_id == $order->user_id && $messages[0]->sender_type == 0) {
                $user_image = User::whereId($messages[0]->sender_id)->first()->image;
            } else {
                $user_image = User::whereId($messages[0]->receiver_id)->first()->image;
            }

            foreach ($messages as $message) {

                if (isset($message)) {
                    if ($message->message == NULL  ) {
                        $message->message = "";
                        $message->messages_image['image'] = MessageImage::where('message_id', $message->id)->first()?MessageImage::where('message_id', $message->id)->first()->image:null;
                    }

                    elseif ($message->message!=null&&$message->confirm_accept_order==4 ){
                        $message->messages_image = null;
                        $message->invoice= OrderInvoice::where('order_id', $order_id)->first();

                    }
                    else {
                        $message->messages_image = null;
                    }
                    $message->created_at = Carbon::parse($message->created_at)->format('d F Y H:i A');
                    $message->user_image = $user_image;
                    $message->delegate_image = $delegate_image;
//                    if($order->confirm_accept==5){
//                        unset($message);
//                    }

                }

//                if ($message->message == NULL) {
//                    $message->message = "";
//                    $message->messages_image['image'] = MessageImage::where('message_id', $message->id)->first() ? MessageImage::where('message_id', $message->id)->first()->image : "";
//                } else {
//                    $message->messages_image = null;
//                }
//                $message->created_at = Carbon::parse($message->created_at)->format('d F Y H:i A');
//                $message->user_image = $user_image;
//                $message->delegate_image = $delegate_image;
            }
        }

        return [
            'department_id' => $department_id,
            'sub_department_id' => $order->sub_department_id,
            'last_lat' => $order->last_lat,
            'last_lng' => $order->last_lng,
            'last_address' => $order->last_address,
            'last_city_name' => $order->last_city_name,
            'confirm_accept_order' => $order->confirm_accept,
            'order_status' => OrderStatus::where('order_id', $order_id)->first(),
            'user_info' => $user_info,
            'messages' => $messages,
        ];
    }

    public function sendMessage($request, $user_id, $user_image, $lang)
    {
        $order = Order::whereId($request->order_id)->first();
        if ($order->delegate_id == $user_id) {
            $message = Message::create([
                'sender_id' => $user_id,
                'sender_type' => 1,//0=>user , 1=>delegate
                'receiver_id' => $request->receiver_id,
                'receiver_type' => 0,//0=>user , 1=>delegate
                'message' => $request->message,
                'type' => 0,//0=>orders , 1=>trips
                'order_id' => $request->order_id
            ]);
            if (isset($request->image)) {
                MessageImage::create([
                    'message_id' => $message->id,
                    'image' => $request->image,
                ]);
            }
            $data = Message::whereId($message->id)
                ->with('messages_image')
                ->first();
            $user = User::whereId($request->receiver_id)->select('id', 'token', 'image', 'jwt')->first();
            $data->delegate_image = $user_image;
            $data->user_image = $user->image;
            $data->department_id = $order->department_id;
            if ($lang == 'ar') {
                $title = 'رسالة جديدة';
            } else {
                $title = 'New Message';
            }
//            dd($user);
            Broadcast(new OrdersChatEvent($data, $user->jwt, $user_image, $user->image, $order->department_id, 0));
            Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

//            Notification::send($user->token, $title ,
//                $title , 1 ,1,
//                "",$data,NULL,$order->id,
//                null,null,null,null,$order->department_id);
            return $data;
        }
        return [];

    }

    public function getReplacedPoints($request, $user_id, $lang)
    {
        //type =>>>> 0=>user, 1=>delegate, 2=>driver
        $data = [
            'user_points' => Delegate::whereId($user_id)->select('points')->first()->points,
            'user_replaced_points' => ReplacedPoint::where('user_id', $user_id)
                ->where('type', 1)->get(),
        ];
        return $data;
    }

    public function rejectOrder($request, $user_id, $lang)
    {
        $order = Order::whereId($request->order_id)->first();
        if ($order->delegate_id) {
            return false;
        }
        $check = RejectedOrder::where('delegate_id', $user_id)
            ->where('order_id', $request->order_id)->first();
        if (!$check) {
            RejectedOrder::create([
                'order_id' => $request->order_id,
                'delegate_id' => $user_id,
            ]);
        }
        return true;
    }

    public function createOrderInvoice($request, $delegate)
    {
        if ($request->header('lang') == 'ar') {
            $title = 'تم ارسال الفاتورة من المندوب';
        } else {
            $title = 'Invoice sent by delegate';
        }

        $order = Order::whereId($request->order_id)->first();
        $user = User::whereId($order->user_id)->first();
        $offer = OrderOffer::where('order_id', $request->order_id)->first();
        $fees = AppSetting::where('key_name', 'fee_percent')->first()->val;
        $tax = ($fees/100) * $request->sub_total;
        $total = $request->sub_total  + $offer->offer;
        $order->update([
            'confirm_accept'=>4,
            'total_cost'=>$total
        ]);
        $message = Message::firstOrCreate([
            'sender_id' => $delegate->id, 'sender_type' => 1, 'receiver_id' => $order->user_id, 'receiver_type' => 0,
            'message' => $title, 'type' => 0, 'order_id' => $request->order_id, 'confirm_accept_order' => 4

        ]);
        $invoice= OrderInvoice::firstOrCreate([
            'order_id' => (int)$request->order_id,
            'total' => (int)$total,
            'sub_total' => (int)$request->sub_total,
            'offer' => (int)$offer->offer,
            'tax' => (int)$tax,
            'tax_percentage' => (int)$fees,
            'confirmed' =>0,
        ]);

        $message->messages_image = null;

        $message->user_image = $user->image;
        $message->delegate_image = $delegate->image;
        $message->invoice = $invoice;
        Broadcast(new OrdersChatEvent($message, $user->jwt, $delegate->image, $user->image, $order->department_id, 4,null,$invoice));


        Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

        return $message;

    }



}
