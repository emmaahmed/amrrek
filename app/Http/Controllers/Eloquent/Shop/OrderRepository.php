<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 27/06/2019
 * Time: 11:06 ص
 */

namespace App\Http\Controllers\Eloquent\Shop;


use App\Http\Controllers\Interfaces\Shop\OrderRepositoryInterface;
use App\Models\Category;
use App\Models\Delegate;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\User;
use Carbon\Carbon;

class OrderRepository implements OrderRepositoryInterface
{
    public function getOrders($shop_id){
        $current = OrderStatus::where('accept','!=',null)->where('finished',null)->pluck('order_id');
        $processing_started = OrderStatus::where('processing_started',null)->where('finished',null)->pluck('order_id');
        $cancelled = OrderStatus::where('cancelled','!=',null)->where('finished',null)->pluck('order_id');
        $orders = Order::orderBy('id','desc')
            ->where('shop_id',$shop_id)
            ->whereIn('id',$current)
            ->whereIn('id',$processing_started)
            ->whereNotIn('id',$cancelled)
            ->whereNotNull('delegate_id')
            //->where('created_at',Carbon::now())
            ->with('delegate')
            ->with('user')
            ->select('id','order_number','user_id','shop_id','delegate_id','total_cost','created_at')
            ->withCount('order_products')
                        ->with("order_status")

            ->get();
            return $orders;

//        return Order::where('shop_id',$shop_id)
//            ->whereNotNull('delegate_id')
//            ->with('delegate')
//            ->with('user')
//            ->select('id','order_number','shop_id','delegate_id','total_cost','created_at')
//            ->get();
    }

    public function orderDetails($request){
        $orders = Order::whereId($request->order_id)
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->withCount('order_products')
            ->with("order_status")
            ->with("order_images")
            ->with("sub_department")
            ->with("user")
            ->with("delegate")
            ->first();
        if($orders){
            if ($orders->order_products) {
                foreach ($orders->order_products as $product) {
                    if(isset($product->order_product_variations->order_product_variation_options)){
                        global $product_details;
                        $product_details = "";
                        foreach ($product->order_product_variations->order_product_variation_options as $option) {
                            $product_details = $product_details . $option->name . ', ';
                        }
                        $product_details = substr($product_details, 0, -2);
                        $product->product_details = $product_details;
                    }
                }
            }
        }


        return $orders;
    }

    public function myOrders($shop_id,$request){
        if($request->type == 0){
            //privious
            $finshed = OrderStatus::whereNotNull('finished')->pluck('order_id');
//            dd($finshed);
            $orders = Order::orderBy('id','desc')
                ->where('shop_id',$shop_id)
                ->whereIn('id',$finshed)
                ->whereNotNull('delegate_id')
                ->with('delegate')
                ->with('user')
                            ->with("order_status")

                ->select('id','order_number','user_id','shop_id','delegate_id','total_cost','created_at')
                ->withCount('order_products')
                ->get();
            foreach ($orders as $order){
                $order->processing_started = 1;
            }
        }elseif($request->type == 1){
            //current
            $current = OrderStatus::where('accept','!=',null)
                ->whereNull('finished')->whereNotNull('processing_started')->pluck('order_id');
            //$processing_started = OrderStatus::where('processing_started',null)->pluck('order_id');
            $cancelled = OrderStatus::where('cancelled','!=',null)->pluck('order_id');
            $orders = Order::orderBy('id','desc')
                ->where('shop_id',$shop_id)
                ->whereIn('id',$current)
                //->whereIn('id',$processing_started)
                ->whereNotIn('id',$cancelled)
                ->whereNotNull('delegate_id')
                //->where('created_at',Carbon::now())
                ->with('delegate')
                ->with('user')
                            ->with("order_status")

                ->select('id','order_number','user_id','shop_id','delegate_id','total_cost','created_at')
                ->withCount('order_products')
                ->get();
            foreach ($orders as $order)
                $order->processing_started = 0;
        }


        return $orders;
    }

    public function changeStatus($shop_id,$request,$lang){
        //1=>accept ,4=received , 2 =>on_way , 3=>finished ,
        // 5=on_first_way , 6=processing
        // 7=processing_started

        $order = Order::whereId($request->order_id)->first();
        $order_status = OrderStatus::where('order_id',$request->order_id)->first();
        $user = User::whereId($order->user_id)->first();
        $delegate = Delegate::whereId($order->delegate_id)->first();
        if ($request->status == 10) {

            OrderStatus::where('order_id', $request->order_id)
                ->update(['processing_started' => Carbon::now()]);

            if($lang == "ar"){
                $title = 'تم البدء في تجهيز طلب العميل';
            }else{
                $title = 'Customer order processing has begun';
            }
        //   dd($user);
//          Notification::sendNotify("$user->token", "$title" ,
//                "$title" , 1 ,1,
//                "","",NULL,$order->id,0);
            Notification::sendNotifyTo($user->jwt,$title,$title,2,$order->id,$order->department_id,'users');

            //
//            Notification::send("$delegate->token", "$title" ,
//                "$title" , 1 ,1,
//                "","",NULL,$order->id,0);
            Notification::sendNotifyTo($delegate->jwt,$title,$title,2,$order->id,$order->department_id,'delegates');

            //
        }

        $add            = new Notification();
        $add->title     = $title;
        $add->body      = $title;
        $add->user_id   = $user->id;
        $add->type      = 0;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
        $add->order_type   = 2;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        $add->order_id   = $order->id;
        $add->save();

        $add            = new Notification();
        $add->title     = $title;
        $add->body      = $title;
        $add->user_id   = $delegate->id;
        $add->type      = 1;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
        $add->order_type   = 2;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        $add->order_id   = $order->id;
        $add->save();

        return OrderStatus::where('order_id', $request->order_id)->first();
    }




}
