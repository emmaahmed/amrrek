<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 22/05/2019
 * Time: 01:53 م
 */

namespace App\Http\Controllers\Eloquent\User;


use App\Events\OffersEvent;
use App\Events\OrdersChatEvent;
use App\Events\OrdersEvent;
use App\Http\Controllers\Interfaces\User\HomeRepositoryInterface;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\BranchProduct;
use App\Models\Category;
use App\Models\Delegate;
use App\Models\Department;
use App\Models\GoogleShop;
use App\Models\Message;
use App\Models\MessageImage;
use App\Models\Notification;
use App\Models\OfferPoint;
use App\Models\Option;
use App\Models\Order;
use App\Models\OrderChangedDelegate;
use App\Models\OrderImage;
use App\Models\OrderInvoice;
use App\Models\OrderOffer;
use App\Models\OrderProduct;
use App\Models\OrderProductVariation;
use App\Models\OrderProductVariationOption;
use App\Models\OrderRequest;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\Shop;
use App\Models\ShopRate;
use App\Models\Slider;
use App\Models\SubDepartment;
use App\Models\Trip;
use App\Models\User;
use App\Models\UserAdminMessage;
use App\Models\UserAdminMessageImage;
use App\Models\UserReplacedPoint;
use App\Models\WalletRecharge;
use Carbon\Carbon;
use DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class HomeRepository implements HomeRepositoryInterface
{
    public $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function getDepartments($request,$lang)
    {
        return Department::
        select('id','name_'.$lang.' as name','image')
            ->get();
    }

    public function getSubDepartments($request,$lang)
    {
        return SubDepartment::where('active',1)
            ->where('department_id',$request->department_id)
            ->select('id','name_'.$lang.' as name','has_processing')
            ->get();
    }

    public function searchShopsGoogleMaps($request,$lang)
    {
        $search_keyword = $request->search_key;

        $shop = new Shop();
        return $shops = $shop->filterbylatlngbySearchKey($request->lat, $request->lng, 50, "shops", null,$search_keyword,188);


        $client = new \GuzzleHttp\Client(['defaults' => [
            'verify' => false
        ]]);
        $latitude = $request->lat;
        $longitude = $request->lng;
        $page_token = !empty($request->page_token) ? $request->page_token : "";
        $radius = 100000; // in meter
        $api_key = "AIzaSyACLGmyHsCiSodwDmyX_xAf_cfgTWWYfGE";
        // Creating default object of guzzle http client
        $response = $client->request('GET', "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$latitude,$longitude&radius=$radius&keyword=$search_keyword&language=ar&key=$api_key");

        $returned_data_from_google =  json_decode($response->getBody());

        $markets = [];
        foreach ($returned_data_from_google->results as $key => $result){
            $markets[$key]['id'] = 0;
            $markets[$key]['place_id'] = isset($result->place_id) ? $result->place_id : 'place_id';
            $markets[$key]['name'] = isset($result->name) ? $result->name : "google shop";
            $markets[$key]['lat'] = isset($result->geometry->location->lat) ? $result->geometry->location->lat : "google shop";
            $markets[$key]['lng'] = isset($result->geometry->location->lng) ? $result->geometry->location->lng : "google shop";
            //$markets[$key]['category'] = isset($result->types[0]) ? $result->types[0] : null;
            $markets[$key]['address'] = isset($result->vicinity) ? $result->vicinity : null;
            if (isset($result->photos)){
                $markets[$key]['image'] = "https://3maps.googleapis.com/maps/api/place/photo?maxwidth=".$result->photos[0]->width."&photoreference=".$result->photos[0]->photo_reference."&key=$api_key";
            }else{
                $markets[$key]['image'] = "https://takeme.my-staff.net/default.png";
            }
            if (isset($result->photos)){
                $markets[$key]['cover_image'] = "https://3maps.googleapis.com/maps/api/place/photo?maxwidth=".$result->photos[0]->width."&photoreference=".$result->photos[0]->photo_reference."&key=$api_key";
            }else{
                $markets[$key]['cover_image'] = "https://takeme.my-staff.net/default.png";
            }
            if (isset($result->geometry->location)){
                $markets[$key]['distance'] = number_format((float)distance($latitude,$longitude,$result->geometry->location->lat,$result->geometry->location->lng, "K"), 2, '.', '') + 0 ." Km";
            }else{
                $markets[$key]['distance'] = "";
            }

        }
//        $markets[]['page_token'] = $page_token;
        // decoding the response to a valid json format
        $data['page_token'] = null;
//        $data['page_token'] = isset($response_result->next_page_token) ? $response_result->next_page_token : null;
        //$data['markets'] = array_values(collect($markets)->sortBy('distance')->toArray());
//        /////////////////////
        $results = array_values(collect((array)$markets)->sortBy('distance')->toArray());
        return $results;
    }

    public function category($lang)
    {
        return Category::select('id', 'name_'.$lang. ' as name', 'image')
            ->whereActive(1)
            ->get();
    }

    public function shops($request, $lang, $user_country_id)
    {
//        "id": 1,
//        "name": "برجر كينج",
//        "lat": "30.1258846",
//        "lng": "31.3751404",
//        "address": "الهايكستب , قسم النزهة محافظة القاهرة …….",
//        "image": "https://takeme.my-staff.net/uploads/shops/images/Group4483.png",
//        "cover_image": "https://takeme.my-staff.net/uploads/shops/coverimages/Group4483.png",
//        "description": "برجر كنج هي سلسلة أمريكية متعددة الجنسيات من مطاعم الوجبات السريعة بالهمبرغر. يقع المقر الرئيسي للشركة في مقاطعة ميامي ديد بولاية فلوريدا",
//        "distance": "0.01 كم"

        $shop = new Shop();
         return  $shop->filterbylatlng($request->lat, $request->lng, 50, "shops", $user_country_id);
//dd($shops);

    }

    public function shopsByCategory($request, $lang, $user_country_id)
    {
        //dd($request->all());
        $category = Category::whereId($request->category_id)->first();
//        $google_category_name = $category->google_name;
//
//        $client = new \GuzzleHttp\Client(['defaults' => [
//            'verify' => false
//        ]]);
//        $latitude = $request->lat;
//        $longitude = $request->lng;
//        $page_token = !empty($request->page_token) ? $request->page_token : "";
//        $radius = 100000; // in meter
//        $api_key = "AIzaSyACLGmyHsCiSodwDmyX_xAf_cfgTWWYfGE";
//        // Creating default object of guzzle http client
//        $response = $client->request('GET', "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$latitude,$longitude&radius=$radius&type=$google_category_name&language=ar&key=$api_key");
//
//         $returned_data_from_google =  json_decode($response->getBody());
//
//        $markets = [];
//        foreach ($returned_data_from_google->results as $key => $result){
//            $markets[$key]['id'] = 0;
//            $markets[$key]['place_id'] = isset($result->place_id) ? $result->place_id : 'place_id';
//            $markets[$key]['name'] = isset($result->name) ? $result->name : "google shop";
//            $markets[$key]['lat'] = isset($result->geometry->location->lat) ? $result->geometry->location->lat : "google shop";
//            $markets[$key]['lng'] = isset($result->geometry->location->lng) ? $result->geometry->location->lng : "google shop";
//            //$markets[$key]['category'] = isset($result->types[0]) ? $result->types[0] : null;
//            $markets[$key]['address'] = isset($result->vicinity) ? $result->vicinity : null;
//            if (isset($result->photos)){
//                $markets[$key]['image'] = "https://3maps.googleapis.com/maps/api/place/photo?maxwidth=".$result->photos[0]->width."&photoreference=".$result->photos[0]->photo_reference."&key=$api_key";
//            }else{
//                $markets[$key]['image'] = "https://takeme.my-staff.net/default.png";
//            }
//            if (isset($result->photos)){
//                $markets[$key]['cover_image'] = "https://3maps.googleapis.com/maps/api/place/photo?maxwidth=".$result->photos[0]->width."&photoreference=".$result->photos[0]->photo_reference."&key=$api_key";
//            }else{
//                $markets[$key]['cover_image'] = "https://takeme.my-staff.net/default.png";
//            }
//            if (isset($result->geometry->location)){
//                $markets[$key]['distance'] = number_format((float)distance($latitude,$longitude,$result->geometry->location->lat,$result->geometry->location->lng, "K"), 2, '.', '') + 0 ." Km";
//            }else{
//                $markets[$key]['distance'] = "";
//            }
//        }
//
////        $markets[]['page_token'] = $page_token;
//        // decoding the response to a valid json format
//        $data['page_token'] = null;
////        $data['page_token'] = isset($response_result->next_page_token) ? $response_result->next_page_token : null;
//        //$data['markets'] = array_values(collect($markets)->sortBy('distance')->toArray());
////        /////////////////////
        $shop = new Shop();
        if ($request->category_id == 0){
            return $shops = $shop->filterbylatlng($request->lat, $request->lng, 50, "shops", $user_country_id);
        }else{
            return $shops = $shop->filterbylatlngByCatId($request->lat, $request->lng,
                50, "shops", $request->category_id, $user_country_id);
        }
//        foreach ($shops as $shop){
//            array_push($markets,$shop);
//        }
//
//        $results = array_values(collect((array)$markets)->sortBy('distance')->toArray());
//        return $results;
    }

    public function shopDetails($request, $lang)
    {
        $this_shop = Shop::whereId($request->shop_id)->first();
//        dd($this_shop);
        if($this_shop->parent_id == Null){
            $shop = Shop::whereId($request->shop_id)
//                ->orWhere('parent_id', $this_shop->parent_id)
                ->select('id', 'name', 'rate','image','description_'.$lang.' as description','cover_image','lat','lng','address')
                ->with(["menus" => function ($query) use ($lang) {
                    $query
                        ->select('id','name_'.$lang.' as name','shop_id')
                        ->with(['products' => function ($query) use ($lang) {
                            $query
                                ->where('active',1)

                                ->select('id','name_'.$lang.' as name','image',
                                'description_'.$lang.' as description',
                                'price_before','price_after','has_sizes',
                                'percent','quantity','selling','status',
                                'has_discount','rate','shop_id','menu_id');
                        }]);
                }])
                ->with('days')
                ->first();
        }
        else{
            $branches_products = BranchProduct::where('branch_id',$request->shop_id)
                ->where('status',0)->pluck('product_id');
            $parentShop = Shop::whereId($this_shop->parent_id)
                //->orWhere('parent_id', $this_shop->parent_id)
                ->select('id', 'name', 'rate','description_'.$lang.' as description','cover_image','lat','lng','address')
                ->with(["menus" => function ($query) use ($lang,$branches_products) {
                    $query
                        ->select('id','name_'.$lang.' as name','shop_id')
                        ->with(['products' => function ($query) use ($lang,$branches_products)  {
                            $query
                                ->whereNotIn('id',$branches_products)
                                ->select('id','name_'.$lang.' as name','image',
                                    'description_'.$lang.' as description',
                                    'price_before','price_after','has_sizes',
                                    'percent','quantity','selling','status',
                                    'has_discount','rate','shop_id','menu_id');
                        }]);
                }])
                ->with('days')
                ->first();
            $shop=Shop::whereId($request->shop_id)->first();
            $shop['menus']=$parentShop->menus;
//dd($shop);
            //$cats->prepend($catItem0);
//            foreach ($shop->menus as $menu){
//                $products = Product::where('menu_id',$menu->id)
//                    ->select('id','name_'.$lang.' as name','image',
//                    'description_'.$lang.' as description',
//                    'price_before','price_after','has_sizes',
//                    'percent','quantity','selling','status',
//                    'has_discount','rate','shop_id','menu_id')->get();
//                dd(is_object($menu));
//                if($products){
//                    foreach ($products as $product){
//                        array_push($menu ,$product);
//                    }
////                    $menu->products->prepend($products);
////                    array_push($menu->products ,$products);
//                }
//
////                $menu->products->prepend($products);
//            }
        }


        $currentDay = $this->getCurrentDay($shop->days);
        $shop->today_from = $currentDay['from'];
        $shop->today_to = $currentDay['to'];
        $shop->rates_count = ShopRate::where("shop_id", $request->shop_id)->count();
        //$shop->rates_count = "12300";
        $distance = $shop->getDistance($request->lat, $request->lng, 50, "shops", $request->shop_id);
        if($lang=='ar'){
            $km='كم';
        }
        else{
            $km='km';
        }
        $shop['distance']=$distance. " " .$km;
        return $shop;
    }

    public function shopRates($request, $lang)
    {
        if (isset($request->department_id) && $request->department_id == 3) {
//            $rates = Order::where("department_id", $request->department_id)
//                ->select('user_rate as rate', 'user_comment as comment', 'user_id', 'created_at')
//                ->where('user_rate','!=',null)
//                ->where('user_comment','!=',null)
//                ->with('user')
//                ->get();
            $rates = ShopRate::orderBy('rate', 'desc')
                ->where("department_id", $request->department_id)
                ->select('rate', 'comment', 'user_id', 'created_at')
                ->with('user')
                ->get();
            return (object)[
                'id' => 0,
                'name' => '',
                'rates' => $rates,
            ];
        }
        $rates = Shop::whereId($request->shop_id)
            ->orWhere('parent_id', $request->shop_id)
            ->select('id', 'name')
            ->with(["rates" => function ($query) use ($lang) {
                $query->orderBy('rate', 'desc')->with('user');
            }])->first();
        return $rates;
//        $rates = Shop::whereId($request->shop_id)
//            ->orWhere('parent_id', $request->shop_id)
//            ->select('id', 'name')
//            ->with(["rates" => function ($query) use ($lang) {
//                $query->with('user');
//            }])->first();
//        return $rates;
    }

    public function productDetails($request, $lang)
    {
        if($lang == null)
            $lang="ar";
        $product = Product::whereId($request->product_id)
            ->with(["variations" => function ($query) use ($lang) {
                $query
                    ->select('id','name_'.$lang.' as name','product_id','type','required')
                    ->with(["options" => function ($query) use ($lang) {
                        $query->select('id','name_'.$lang.' as name','variation_id','type','price','status');
                    }]);
            }])
            ->select('id','name_'.$lang.' as name','description_'.$lang.' as description','image',
                'price_before','price_after','percent','quantity','selling','status',
                'has_discount','rate')
            ->first();
        return $product;
    }

    public function rateShop($request, $user_id, $lang)
    {
//        $rate = ShopRate::create([
//
//        ]);
        $shop_id = isset($request->shop_id) ? $request->shop_id : 0;
        $check = ShopRate::where('user_id', $user_id)
            ->where('shop_id', $shop_id)->first();
        if($check){
            $rate = ShopRate::where('user_id', $user_id)
                ->where('shop_id', $shop_id)->update([
                    'rate' => $request->rate,
                    'comment' => $request->comment,
                ]);
            $rate = ShopRate::where('user_id', $user_id)
                ->where('shop_id', $shop_id)->first();
        }else{
            $rate = ShopRate::create([
                'user_id' => $user_id,
                'shop_id' => isset($request->shop_id) ? $request->shop_id : 0,
                'department_id' => isset($request->shop_id) ? 2 : 3,
                'rate' => $request->rate,
                'comment' => $request->comment,
            ]);
        }
        // dd($rate);

        $data = ShopRate::whereId($rate->id)->with('user')->first();
        if(isset($request->shop_id)){
            $data->count = ShopRate::where('shop_id',$request->shop_id)->count();
        }else{
            $data->count = 0;
        }

        return $data;
    }

//    public function getRates($request,$lang){
//        if(isset($request->department_id) && $request->department_id == 3)
//            return Order::where("department_id",$request->department_id)
//                ->select('user_rate as rate','user_comment as comment','user_id')
//                ->with('user')
//                ->get();
//        return ShopRate::orderBy('id','desc')
//            ->where("shop_id",$request->shop_id)
//            ->with('user')
//            ->get();
//    }

    public function homeThirdDepartment($request, $lang)
    {
//        $avg_rates = Order::where("department_id",$request->department_id)
//            ->where('user_rate','!=',null)
//            ->where('user_comment','!=',null)
//            ->avg('user_rate');
//        $rates_count =Order::where("department_id",$request->department_id)
//            ->where('user_rate','!=',null)
//            ->where('user_comment','!=',null)
//            ->count();
        $avg_rates = ShopRate::where("department_id", $request->department_id)
            ->avg('rate');
        $rates_count = ShopRate::where("department_id", $request->department_id)
            ->count();
        return (object)[
            'avg_rates' => number_format((float)($avg_rates), 1, '.', ''),
            'rates_count' => $rates_count
        ];
    }

    //
    public function getCurrentDay($shop_days)
    {
        $data['from'] = "";
        $data['to'] = "";
        foreach ($shop_days as $shop_day) {
            if ($shop_day->name_en == Carbon::now()->format('l')) {
                $data['from'] = $shop_day->from;
                $data['to'] = $shop_day->to;
                break;
            }
        }
        return $data;
//        $subset = $shop_days->map(function($day){
//            if ($day->name_en == Carbon::now()->format('l')){
//                return $day;
//            }
//        });
//        $subset = $subset->filter(function ($value){
//            return !is_null($value);
//        })->values();
//        $data['from'] = "";
//        $data['to'] = "";
//        if($subset){
//            $data['from'] = $subset->from;
//            $data['to'] = $subset->to;
//        }
    }

//    public function addToCart($request){
//
//    }

    public function makeOrder($request, $user_id, $lang = "ar", $user_country_id,$user_wallet)
    {
        $user_wallet = $user_wallet * -1;
        $settings=AppSetting::where('key_name','user_max_value')->first();

//        $data = Admin::where('email','admin@admin.com')->first();
        $user_max_value=$settings->val;
//        dd($user_max_value);
        if($user_wallet > $user_max_value)
            return "pay_your_debit";

        //dd($request->all());
        $delegates = Order::filterDelegates($request->in_lat, $request->in_lng, 50, 'delegates', null, null, null, $user_country_id,$request->department_id,$request->sub_department_id);
//       dd($delegates) ;
        if ($delegates) {
            if (isset($request->shop_id) && is_int($request->shop_id))
                $shop = Shop::whereId($request->shop_id)->first();
            $order = Order::create([
                'order_number' => $user_id . rand(100000, 999999),
                'department_id' => $request->department_id,
                'sub_department_id' => $request->sub_department_id,
                'user_id' => $user_id,
                'country_id' => $user_country_id,
                'shop_id' => (isset($request->shop_id)) ? (int)$request->shop_id : NULL,
                'confirm_code' => rand(100000, 999999) . $user_id,
                'title' => isset($request->title) ? $request->title : '',
                'notes' => isset($request->notes) ? $request->notes : '',
                //'date' => isset($request->date) ? $request->date : '',
                'in_lat' => isset($request->in_lat) ? $request->in_lat : (isset($shop) ? $shop->lat : NULL),
                'in_lng' => isset($request->in_lng) ? $request->in_lng : (isset($shop) ? $shop->lng : NULL),
                'in_address' => isset($request->in_address) ? $request->in_address : (isset($shop) ? $shop->address : NULL),
                'in_city_name' => isset($request->in_city_name) ? $request->in_city_name : (isset($shop) ? $shop->city_name : NULL),
                'out_lat' => isset($request->out_lat) ? $request->out_lat : NULL,
                'out_lng' => isset($request->out_lng) ? $request->out_lng : NULL,
                'out_address' => isset($request->out_address) ? $request->out_address : NULL,
                'out_city_name' => isset($request->out_city_name) ? $request->out_city_name : NULL,
                'last_lat' => isset($request->last_lat) ? $request->last_lat : NULL,
                'last_lng' => isset($request->last_lng) ? $request->last_lng : NULL,
                'last_address' => isset($request->last_address) ? $request->last_address : NULL,
                'last_city_name' => isset($request->last_city_name) ? $request->last_city_name : NULL,
                'promo_id' => isset($request->promo_id) && $request->promo_id > 0 ? $request->promo_id : null,
                'delivery_time' => isset($request->delivery_time) ? $request->delivery_time : null,
                'total_cost' => $request->department_id == 2 ? (isset($request->total_cost) ? $request->total_cost : -1) : null,
                'sub_total' => $request->department_id == 2 ? (isset($request->sub_total) ? $request->sub_total : -1) : null,
                'tax' => $request->department_id == 2 ? (isset($request->tax) ? $request->tax : -1) : null,
                //'distance' => number_format($delegates->distance, 1, '.', ''),
            ]);
            if(!$order->distance){
                $result = getDistanceAndTime($order->in_lat,$order->in_lng,$order->out_lat,$order->out_lng);
                if(isset($result->rows[0]) && isset($result->rows[0]->elements[0]) && isset($result->rows[0]->elements[0]->distance)){
                    $distance_val_in_miles = $result->rows[0]->elements[0]->distance->value;
                    $distance_val_in_km = ($distance_val_in_miles * 1.609344)/1000;
                    $distance_val_in_km = number_format($distance_val_in_km, 2, '.', '');
                }else{
                    $distance_val_in_km = 0;
                }
                if($order->last_lat){
                    //
                    $result2 = getDistanceAndTime($order->out_lat,$order->out_lng,$order->last_lat,$order->last_lng);
                    if(isset($result2->rows[0]) && isset($result2->rows[0]->elements[0]) && isset($result2->rows[0]->elements[0]->distance)){
                        $distance_val_in_miles2 = $result2->rows[0]->elements[0]->distance->value;
                        $distance_val_in_km2 = ($distance_val_in_miles2 * 1.609344)/1000;
                        $distance_val_in_km2 = number_format($distance_val_in_km2, 2, '.', '');
                    }else{
                        $distance_val_in_km2 = 0;
                    }
                    //
                }else{
                    $distance_val_in_km2 = 0;
                }
                $distance = $distance_val_in_km + $distance_val_in_km2;
            }else{
                $distance = $order->distance;
            }
            Order::whereId($order->id)->update(['distance' => $distance]);

            if (isset($request->image)) {
                foreach ($request->image as $image)
                    OrderImage::create([
                        'order_id' => $order->id,
                        'image' => $image,
                    ]);
            }
            OrderStatus::create(['order_id' => $order->id, 'user_id' => $user_id]);
            global $total_cost;
            $total_cost = 0;
            if (isset($request->products)) {
                foreach ($request->products as $product) {
                    //
                    $product_data = Product::whereId($product['id'])->first();
                    $total_cost = $product_data->price_after * ((isset($product['quantity']) ? $product['quantity'] : 1));
                    //
                    $OrderProduct = OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $product['id'],
                        'quantity' => isset($product['quantity']) ? $product['quantity'] : 1,
                        'description' => isset($product['description']) ? $product['description'] : "",
                    ]);
                    if (isset($product['variations'])) {
                        foreach ($product['variations'] as $variation) {
                            $OrderProductVariation = OrderProductVariation::create([
                                'order_product_id' => $OrderProduct->id,
                                'variation_id' => $variation['id'],
                            ]);
                            if (isset($variation['options'])) {
                                foreach ($variation['options'] as $option) {
                                    //
                                    $option_data = Option::whereId($option['id'])->first();
                                    $total_cost = $total_cost + ($option_data->price * ((isset($product['quantity']) ? $product['quantity'] : 1)));
                                    //
                                    OrderProductVariationOption::create([
                                        'order_product_var_id' => $OrderProductVariation->id,
                                        'option_id' => $option['id'],
                                    ]);
                                }
                            }
                        }
                    }
                }
//                $fee = (Admin::where('email', "admin@admin.com")->select('fee_percent')->first()->fee_percent) * $total_cost;
//                $total_cost = $total_cost + $fee;
                //Order::whereId($order->id)->update(['total_cost' => $total_cost]);
            }
            if($request->department_id == 4){
                GoogleShop::create([
                    'order_id' => $order->id,
                    'name' => $request->shop_name,
                    'cover_image' => $request->shop_cover_image,
                    'category_id' => $request->category_id,
                    'shop_id' => $request->shop_id,
                    'image' => $request->shop_image,
                    'lat' => $request->in_lat,
                    'lng' => $request->in_lng,
                    'address' => $request->in_address,
                    'city_name' => $request->city_name,
                    'description' => $request->description,
                ]);
            }


            if ($lang == 'ar') {
                $title = ' طلب جديد ';
                $message = 'لديك طلب خدمة جديد,الرجاء الإستجابة';
            } else {
                $title = 'New Order';
                $message = 'You have a new order ,please respond';
            }

            foreach ($delegates as $delegate) {
                OrderRequest::create([
                    'order_id' => $order->id,
                    'delegate_id' => $delegate->id,
                    'distance' => number_format($delegate->distance, 1, '.', ''),
                    'lat' => $delegate->lat,
                    'lng' => $delegate->lng,
                ]);
                $type=0;
                if($request->department_id==1){
                    $type=2;
                }
                elseif ($request->department_id==2){
                    $type=5;

                }
                elseif ($request->department_id==3){
                    $type=1;

                }
                else{
                    $type=4;

                }


                $add = new Notification();
                $add->title = $title;
                $add->body = $message;
                $add->user_id = $delegate->id;
                $add->order_id = $order->id;
                $add->type = $type;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                $add->order_type = $request->department_id;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                $add->save();

                // Notification::send($delegate->token, $title,
                //     $message, 0, 1,
                //     "$order", NULL, NULL, $order->id);
                Notification::sendNotifyTo($delegate->jwt,$title,$message,0,$order->id,$order->department_id,'delegates');


//                Notification::send($delegate->token, $title,
//                    $message, 0, 1,
//                    null, NULL, NULL, $order->id,null,null,null,null,$order->department_id);
            }
            $order['department_id']=(int)$order->department_id;
            return $order;
        }
        return "there is no available delegates";
    }

    public function reOrder($request, $user_id, $lang = "ar", $user_country_id,$user_wallet)
    {
        $old_order = Order::whereId($request->order_id)
            ->with(["order_products" => function ($query) {
                $query->with(["order_product_variations" => function ($query) {
                    $query->with('order_product_variation_options');
                }]);
            }])
            ->first();
        $in_lat = isset($request->in_lat) ? $request->in_lat : $old_order->in_lat;
        $in_lng = isset($request->in_lng) ? $request->in_lng : $old_order->in_lng;
        $in_address = isset($request->in_address) ? $request->in_address : (isset($old_order->in_address) ? $request->in_address :null);
        $in_city_name = isset($request->in_city_name) ? $request->in_city_name : (isset($old_order->in_city_name) ? $old_order->in_city_name :null);
        $out_lat = isset($request->out_lat) ? $request->out_lat : $old_order->out_lat;
        $out_lng = isset($request->out_lng) ? $request->out_lng : $old_order->out_lng;
        $out_address = isset($request->out_address) ?str_replace('‬','',$request->out_address)  : (isset($old_order->out_address) ? str_replace('‬','',$request->out_address) :null);
        $out_city_name = isset($request->out_city_name) ? $request->out_city_name : (isset($old_order->out_city_name) ? $old_order->out_city_name :null);
        $last_lat = isset($request->last_lat) ? $request->last_lat : $old_order->last_lat;
        $last_lng = isset($request->last_lng) ? $request->last_lng : $old_order->last_lng;
        $last_address = isset($request->last_address) ? $request->last_address : (isset($old_order->last_address) ? $request->last_address :null);
        $last_city_name = isset($request->last_city_name) ? $request->last_city_name : (isset($old_order->last_city_name) ? $old_order->last_city_name :null);
        $title = isset($request->title) ? $request->title : (isset($old_order->title) ? $old_order->title :null);
        $notes = isset($request->notes) ? $request->notes : (isset($old_order->notes) ? $old_order->notes :null);
        //dd($request->all());
        $delegates = Order::filterDelegates($in_lat, $in_lng, 50, 'delegates');
        //dd($delegates);
        if ($delegates) {

            $order = Order::create([
                'order_number' => $user_id . rand(100000, 999999),
                'department_id' => $old_order->department_id,
                'sub_department_id' => $old_order->sub_department_id,
                'user_id' => $user_id,
                'country_id' => $user_country_id,
                'shop_id' => (isset($request->shop_id) && is_int($request->shop_id)) ? $request->shop_id : NULL,
                'confirm_code' => rand(100000, 999999) . $user_id,
                'title' => $title,
                'notes' => $notes,
                //'date' => $request->date,
                'in_lat' => $in_lat,
                'in_lng' => $in_lng,
                'in_address' => $in_address,
                'in_city_name' => $in_city_name,
                'out_lat' => $out_lat,
                'out_lng' => $out_lng,
                'out_address' => $out_address,
                'out_city_name' => $out_city_name,
                'last_lat' => $last_lat,
                'last_lng' => $last_lng,
                'last_address' => $last_address,
                'last_city_name' => $last_city_name,
                'promo_id' => isset($old_order->promo_id) && $old_order->promo_id > 0 ? $old_order->promo_id : null,
                'delivery_time' => isset($old_order->delivery_time) ? $old_order->delivery_time : null,
                'total_cost' => $request->department_id == 2 ? (isset($request->total_cost) ? $request->total_cost : -1) : null,
                //'distance' => number_format($delegates->distance, 1, '.', ''),
                'sub_total' => $request->department_id == 2 ? (isset($request->sub_total) ? $request->sub_total : -1) : null,
                'tax' => $request->department_id == 2 ? (isset($request->tax) ? $request->tax : -1) : null,

            ]);
            if(!$order->distance){
                $result = getDistanceAndTime($order->in_lat,$order->in_lng,$order->out_lat,$order->out_lng);
                if(isset($result->rows[0]) && isset($result->rows[0]->elements[0]) && isset($result->rows[0]->elements[0]->distance)){
                    $distance_val_in_miles = $result->rows[0]->elements[0]->distance->value;
                    $distance_val_in_km = ($distance_val_in_miles * 1.609344)/1000;
                    $distance_val_in_km = number_format($distance_val_in_km, 2, '.', '');
                }else{
                    $distance_val_in_km = 0;
                }
                if($order->last_lat){
                    //
                    $result2 = getDistanceAndTime($order->out_lat,$order->out_lng,$order->last_lat,$order->last_lng);
                    if(isset($result2->rows[0]) && isset($result2->rows[0]->elements[0]) && isset($result2->rows[0]->elements[0]->distance)){
                        $distance_val_in_miles2 = $result2->rows[0]->elements[0]->distance->value;
                        $distance_val_in_km2 = ($distance_val_in_miles2 * 1.609344)/1000;
                        $distance_val_in_km2 = number_format($distance_val_in_km2, 2, '.', '');
                    }else{
                        $distance_val_in_km2 = 0;
                    }
                    //
                }else{
                    $distance_val_in_km2 = 0;
                }
                $distance = $distance_val_in_km + $distance_val_in_km2;
            }else{
                $distance = $order->distance;
            }
            Order::whereId($order->id)->update(['distance' => $distance]);

            $order_images = DB::table('order_images')->where('order_id', $request->order_id)->get();
            if (isset($order_images)) {
                foreach ($order_images as $image)
                    DB::table('order_images')->insert([
                        'order_id' => $order->id,
                        'image' => $image->image
                    ]);
            }
            OrderStatus::create(['order_id' => $order->id, 'user_id' => $user_id]);
            if (isset($old_order->order_products)) {
                foreach ($old_order->order_products as $product) {
                    //
                    $OrderProduct = OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $product['product_id'],
                        'quantity' => isset($product['quantity']) ? $product['quantity'] : 1,
                        'description' => $product['description'],
                    ]);
                    if (isset($product['order_product_variations'])) {
                        foreach ($product['order_product_variations'] as $variation) {
                            $OrderProductVariation = OrderProductVariation::create([
                                'order_product_id' => $OrderProduct->id,
                                'variation_id' => $variation['variation_id'],
                            ]);
                            if (isset($variation['order_product_variation_options'])) {
                                foreach ($variation['order_product_variation_options'] as $option) {
                                    OrderProductVariationOption::create([
                                        'order_product_var_id' => $OrderProductVariation->id,
                                        'option_id' => $option['option_id'],
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            if($request->department_id == 4){
                GoogleShop::create([
                    'order_id' => $order->id,
                    'name' => $request->shop_name,
                    'cover_image' => $request->shop_cover_image,
                    'category_id' => $request->category_id,
                    'shop_id' => $request->shop_id,
                    'image' => $request->shop_image,
                    'lat' => $request->in_lat,
                    'lng' => $request->in_lng,
                    'address' => $request->in_address,
                    'city_name' => $request->city_name,
                    'description' => $request->description,
                ]);
            }

            if ($lang == 'ar') {
                $title = ' طلب جديد ';
                $message = 'لديك طلب خدمة جديد,الرجاء الإستجابة';
            } else {
                $title = 'New Order';
                $message = 'You have a new order ,please respond';
            }
            foreach ($delegates as $delegate) {
                OrderRequest::create([
                    'order_id' => $order->id,
                    'delegate_id' => $delegate->id,
                    'distance' => number_format($delegate->distance, 1, '.', ''),
                    'lat' => $delegate->lat,
                    'lng' => $delegate->lng,
                ]);

//                Notification::send($delegate->token, $title,
//                    $message, 0, 1,
//                    $order, NULL, NULL, $order->id);
                Notification::sendNotifyTo($delegate->jwt,$title,$message,0,$order->id,$order->department_id,'delegates');

            }
            return $order;
        }
        return "there is no available delegates";


    }
    //chat event
    public function acceptOffer($request, $user_id, $lang = 'ar')
    {

        $order_offer = OrderOffer::whereId($request->order_offer_id)->with('delegate')->first();
        if ($order_offer) {
            $order = Order::whereId($order_offer->order_id)
                ->where('user_id', $user_id)
//                ->whereNull('delegate_id')
                ->first();

            if ($order) {

                Order::whereId($order_offer->order_id)
                    ->where('user_id', $user_id)
                    ->update([
                        'delegate_id' => $order_offer->delegate_id,
                        'offer_id' => $request->order_offer_id,
                    ]);
                if($order->department_id != 5){
                    Delegate::whereId($order_offer->delegate_id)->update(['busy' => 1 ]);
                }


                OrderStatus::where('order_id', $order_offer->order_id)
                    ->where('user_id', $user_id)
                    ->update([
                        'accept' => Carbon::now(),
                    ]);
                Notification::where('order_id',$order->id)->where('order_type','>',1)->delete();
                if ($lang == 'ar') {
                    $title = ' وافق المستخدم علي عرضك ';
                    $message = 'قيمة التوصيل: ' . $order_offer->offer;
                } else {
                    $title = 'User accept your offer';
                    $message = 'offer value: '. $order_offer->offer;
                }
                $type=0;
                $orderType=0;
                if($order->department_id==1){
                    $type=2;
                    $orderType=1;
                }
                elseif ($order->department_id==2){
                    $type=5;
                    $orderType=2;

                }
                elseif ($order->department_id==3){
                    $type=1;
                    $orderType=3;

                }
                else{
                    $type=4;
                    $orderType=5;


                }
                $add = new Notification();
                $add->title = $title;
                $add->body = $title;
                $add->user_id = $order_offer->delegate->id;
                $add->order_id = $order->id;
                $add->type = $type;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                $add->order_type = $orderType;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                $add->save();
//                Broadcast(new OrdersEvent($order,$order_offer->delegate->jwt,$order->user));
               Notification::sendNotifyTo($order_offer->delegate->jwt,$title,$message,2,$order->id,$order->department_id,'delegates');

//                Notification::send($order_offer->delegate->token, "$title",
//                    "$message", 1, 1,
//                    "$order_offer", NULL, NULL, $order_offer->order_id);
                //send notification
                if($order->shop_id){
                    $shop = Admin::where('shop_id',$order->shop_id)->first();
                    if ($lang == 'ar') {
                        $title = 'تطبيق أمرك';
                        $message = 'لديك طلب جديد' ;
                    } else {
                        $title = 'New Order';
                        $message = 'You have a new order';
                    }
                    $add = new Notification();
                    $add->title = $title;
                    $add->body = $message;
                    $add->user_id = $order->shop_id;
                    $add->order_id = $order->id;
                    $add->type = 5;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                    $add->order_type = 2;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                    $add->save();


//                    Notification::send($shop->token, "$title",
//                        "$message", 1, 1,
//                        "$order_offer", NULL, NULL, $order_offer->order_id);
                    //send notification
                }
            }
        }

    }
    //redirect delegate to home

    public function cancelOrder($request, $user_id, $lang = 'ar')
    {
        $order = Order::whereId($request->order_id)
            ->where('user_id', $user_id)
            ->first();
        $delegate = Delegate::whereId($order->delegate_id)->first();
        if ($order)
            OrderStatus::where('order_id', $request->order_id)
                ->where('user_id', $user_id)
                ->update([
                    'cancelled' => Carbon::now(),
                    'cancel_by' => 0,
                    'cancel_reason' => isset($request->cancel_reason) ? $request->cancel_reason : "",
                ]);
        if ($delegate) {
            $message = [];
            $message['confirm_accept_order']=-1;
            $message['order_id']=(int)$request->order_id;
            if ($lang == 'ar') {
                $title = ' قام المستخدم بإلغاء الطلب ';
            } else {
                $title = 'User cancelled the order ';
            }
            Message::where([
                'order_id'=>$request->order_id
            ])->delete();
            Delegate::whereId($order->delegate_id)->update(['busy' => 0 ]);
//            Notification::send($delegate->token, "$title",
//                "$title", 8, 1,
//                "", (object)$message, NULL, $request->order_id);
            Broadcast(new OrdersChatEvent($message,$delegate->jwt,$delegate->image,"",$order->department_id,-1));

            Notification::sendNotifyTo($delegate->jwt,$title,$title,3,$order->id,$order->department_id,'delegates');

            //order_type   = 2;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
        }
        Notification::where('order_id',$request->order_id)
            ->where('order_type','>',1)->delete();


    }
    //chat event

    public function acceptConfirmRequest($request, $user_id, $lang = 'ar')
    {
        $order = Order::whereId($request->order_id)
            ->where('user_id', $user_id)
            ->first();
        $lastOffer=OrderOffer::where('order_id',$request->order_id)->latest()->first();
//        dd($order);
        if ($order) {
            $delegate = Delegate::whereId($order->delegate_id)->first();

            if ($request->type == 0) { // reject
                Order::whereId($request->order_id)->update([
                    'confirm_accept' => 0,
                    'offer_id' => null,
                ]);
                if ($lang == 'ar') {
                    $title = 'رسالة جديدة';
                    $message = 'تم إلغاء تأكيد الطلب من العميل';
                } else {
                    $title = 'Amrrek App';
                    $message = 'The order confirmation has been canceled from the customer';
                }

                Message::where('order_id', $request->order_id)
                    ->where('sender_type', 1)
                    ->where('receiver_type', 0)
                    ->where('sender_id', $delegate->id)
                    ->where('receiver_id', $user_id)
                    ->update(['confirm_accept_order' => 0]);
                Message::where('order_id', $request->order_id)
                    ->where('sender_type', 0)
                    ->where('receiver_type', 1)
                    ->where('sender_id', $user_id)
                    ->where('receiver_id', $delegate->id)
                    ->update(['confirm_accept_order' => 0]);
                $message = Message::create([
                    'sender_id' => $user_id,
                    'sender_type' => 0,//0=>user , 1=>delegate
                    'receiver_id' => $delegate->id,
                    'receiver_type' => 1,//0=>user , 1=>delegate
                    'message' => $message,
                    'confirm_accept_order' => -1,
                    'type' => 0,//0=>orders , 1=>trips
                    'order_id' => $request->order_id
                ]);
                $message['order_id']=(int)$message->order_id;
                $message->confirm_accept_order = -1;
                //
                $order->update(['delegate_id' => null]);
                Delegate::whereId($delegate->id)->update(['busy' => 0 ]);

                OrderStatus::where('order_id',$request->order_id)->update(['accept' => null]);
                //
                Broadcast(new OrdersChatEvent($message,$delegate->jwt,$delegate->image,"",$order->department_id,-1));
                Notification::sendNotifyTo($delegate->jwt,$title,$title,3,$order->id,$order->department_id,'delegates');

//                Notification::send($delegate->token, $title,
//                    $title, 1, 1,
//                    "", "$message", NULL, $request->order_id, 0);
                //send order again to all delegates
                Notification::where('order_id',$order->id)->where('type',1)
                    ->where('order_type','>',1)->delete();

                $delegates = Order::filterDelegates($order->in_lat, $order->in_lng, 50, 'delegates',
                    null,null,null,188,$order->department_id);
                if ($lang == 'ar') {
                    $title = ' طلب جديد ';
                    $body = 'لديك طلب خدمة جديد,الرجاء الإستجابة';
                } else {
                    $title = 'Amrrek App';
                    $body = 'You have a new order ,please respond';
                }
                foreach ($delegates as $delegate) {
//                    Notification::send("$delegate->token", $title,
//                        $body, 0, 1,
//                        "$order", NULL, NULL, $order->id);
                    Notification::sendNotifyTo($delegate->jwt,$title,$body,0,$order->id,$order->department_id,'delegates');

                }
                //

                return $message;
            }
            elseif ($request->type == 2) { // accept
                Order::whereId($request->order_id)->update(['confirm_accept' => 2]);
// dd('test');
//dd($delegate);
                if ($lang == 'ar') {
                    $title = 'الموافقة علي تأكيد الطلب';
                    $message = 'تم الموافقة علي تأكيد الطلب';
                    $titleShop = 'لديك طلب جديد';
                    $messageShop = 'لديك طلب جديد';
                } else {
                    $title = 'Agree to confirm the order';
                    $message = 'Agree to confirm the order';
                    $titleShop='You have a new order';
                    $messageShop='You have a new order';
                }
                Message::where('order_id', $request->order_id)
                    ->where('sender_type', 1)
                    ->where('receiver_type', 0)
                    ->where('sender_id', $delegate->id)
                    ->where('receiver_id', $user_id)
                    ->update(['confirm_accept_order' => 2]);
                Message::where('order_id', $request->order_id)
                    ->where('sender_type', 0)
                    ->where('receiver_type', 1)
                    ->where('sender_id', $user_id)
                    ->where('receiver_id', $delegate->id)
                    ->update(['confirm_accept_order' => 2]);
                $message = Message::create([
                    'sender_id' => $user_id,
                    'sender_type' => 0,//0=>user , 1=>delegate
                    'receiver_id' => $delegate->id,
                    'receiver_type' => 1,//0=>user , 1=>delegate
                    'message' => $message,
                    'confirm_accept_order' => 2,
                    'type' => 0,//0=>orders , 1=>trips
                    'order_id' => $request->order_id
                ]);
                $message['order_id']=(int)$message->order_id;

                Delegate::whereId($delegate->id)->update(['busy' => 1 ]);
                $shop=Admin::where(['shop_id'=>$order->shop_id])->first();
                Broadcast(new OrdersChatEvent($message,$delegate->jwt,$delegate->image,"",$order->department_id,2));
                Notification::sendNotifyTo($delegate->jwt,$title,$title,2,$order->id,$order->department_id,'delegates');

//                Notification::send($delegate->token, $title,
//                    $title, 1, 1,
//                    "", "$message", NULL, $request->order_id, 2);
                $orderType=0;
                if($order->department_id==1){
                    $type=2;
                    $orderType=1;
                }
                elseif ($order->department_id==2){
                    $type=5;
                    $orderType=2;

                }
                elseif ($order->department_id==3){
                    $type=1;
                    $orderType=3;

                }
                else{
                    $type=4;
                    $orderType=5;


                }
                if($shop){
                    $add = new Notification();

                    $add->title = $titleShop;
                    $add->body = $titleShop;
                    $add->user_id = $shop->shop_id;
                    $add->order_id = $request->order_id;
                    $add->type = 5;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                    $add->order_type = $orderType;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                    $add->save();
//                    Notification::sendNotifyTo($delegate->jwt,$title,$title,2,$order->id,$order->department_id,'delegates');

                    $pdfFilePath = public_path('invoices/orders-'.$order->order_number.'.pdf');
                    $input = "https://chart.googleapis.com/chart?cht=qr&chs=150x150&chl=".route('shop.orders.pdf',$order->id);
                    $output = public_path($order->order_number.'.png');
//                    dd($order);


                    file_put_contents($output, file_get_contents($input));
                    $pdf=PDF::loadView('cp_shop.orders.invoice-pdf', compact('order','output','delegate','lastOffer'));
                    $pdf->save($pdfFilePath);
                    Broadcast(new OrdersEvent($order,$shop->jwt,$order->user,2));
//                    dd($shop->jwt);

                    Notification::sendNotifyTo($shop->jwt,$titleShop,$titleShop,0,$order->id,$order->department_id,'shops');
                }
                return $message;

            }

        }

    }

    public function getOrdersOffers($request, $user_id, $lang = 'ar')
    {
//        dd('test');
        $department_id = $request->department_id;
        $orders_array = OrderStatus::where('user_id', $user_id)
            ->where('cancelled', NULL)
            ->where('accept',  NULL)
            ->pluck('order_id');
        if($department_id == 5){
            $orders = Order::orderBy('id', 'desc')
                ->where('department_id', $department_id)
                ->whereIn('id',$orders_array)
                ->where('user_id', $user_id)
                ->where('delegate_id', null)
                ->where('offer_id', null)
                ->with('shop')
//                ->withCount('order_offer')
                ->with(["order_status" => function ($query) {
                    $query->where('accept', null)
                        ->where('on_way', null)
                        ->where('finished', null)
                        ->where('cancelled', null);
                }])
                ->with(["order_image" => function ($query) {
                }])
                ->with(["sub_department" => function ($query)use($lang) {
                    $query->select('id',"name_".$lang." as name");
                }])
                ->select('id', 'order_number', 'title', 'notes', 'delivery_time',
                    'shop_id','department_id','sub_department_id','offer_id')
                ->where( 'created_at', '>', Carbon::now()->subDay())

                ->get();
        }
        else{
            $orders = Order::orderBy('id', 'desc')
                ->where('department_id',"!=", 5)
                ->where('department_id',"!=", 4)
                ->whereIn('id',$orders_array)
                ->where('user_id', $user_id)
                ->where('delegate_id', null)
                ->where('offer_id', null)
                ->with('shop')
//                ->withCount('order_offer')
                ->with(["order_status" => function ($query) {
                    $query->where('accept', null)
                        ->where('on_way', null)
                        ->where('finished', null)
                        ->where('cancelled', null);
                }])
                ->whereHas("order_status" , function ($query) {
                    $query->where('accept', null)
                        ->where('on_way', null)
                        ->where('finished', null)
                        ->where('cancelled', null);
                })
                ->with(["order_image" => function ($query) {
                }])
                ->with(["sub_department" => function ($query)use($lang) {
                    $query->select('id',"name_".$lang." as name")->first();
                }])
                ->select('id', 'order_number', 'title', 'notes', 'delivery_time',
                    'shop_id','department_id','sub_department_id','offer_id')
                ->where( 'created_at', '>', Carbon::now()->subDay())

                ->get();
        }
        foreach ($orders as $order) {
//            foreach ($orders as $order){
//
//            }
            $order->delivery_time = $order->department_id == 5 ? Carbon::parse($order->delivery_time)->translatedFormat('l jS F, h:i a') : $order->delivery_time;
            $order_offers = OrderOffer::where('order_id', $order->id)
                ->count();
            $order->has_offers = $order_offers > 0 ? 1 : 0;
            //$order->order_images = (object) $order->order_images ? $order->order_images[0] : null;
        }
        return $orders;
    }

    public function getOffers($request, $user_id, $lang = 'ar')
    {
//        dd('test');
        $setting = Admin::where('email','admin@admin.com')->first();

        $order = Order::whereId($request->order_id)->select('counter', 'delivery_time','department_id')->first();
        $department_id = $order->department_id;
        $order->department_id == 5 ? $items=10 : $items=3;
        $delegates_id=OrderChangedDelegate::where([
            'order_id' => $request->order_id,
        ])->pluck('delegate_id')->toArray();
//dd($delegates_id);
        $order_offers = OrderOffer::orderBy('offer', 'asc')
            ->where('order_id', $request->order_id)
            ->where('status', 0)
            ->with('delegate')
//            ->whereHas('delegate',function($query)use($delegates_id){
//                $query->whereNotIn('delegates.id',$delegates_id);
//            })
//            ->where( 'created_at', '>', Carbon::now()->subDays(2))

            ->take($items)
            ->where( 'created_at', '>', Carbon::now()->subDay())

            ->get();
//        dd($order_offers);
        if($order->department_id != 5){
            if (sizeof($order_offers) > 0) {
//dd($order_offers);

                $lower_offers_counter = $order->counter;
                $delivery_time = $order->delivery_time;

                $orders_count = Order::where('delegate_id', $order_offers[0]->delegate_id)->count();
                $comments_count = Order::where('delegate_id', $order_offers[0]->delegate_id)
                    ->where('delegate_rate', '!=', NULL)
                    ->where('delegate_comment', '!=', NULL)
                    ->count();

                $order_offers[0]->delegate->orders_count = $orders_count;
                $order_offers[0]->delegate->comments_count = $comments_count;
                $order_offers[0]->lower_offers_counter = $lower_offers_counter;
                $order_offers[0]->delivery_time = $delivery_time;
                $order_offers[0]->department_id =   $department_id;

                $date = Carbon::parse($order->updated_at);
                $now = Carbon::now();
                $diff = $date->diffInHours($now);
                //dd($diff);
                $cancel_warning = "";
//                dd($diff > $setting->user_cancel_time);
                if($diff > $setting->user_cancel_time){
                    $money = $setting->user_cancel_fee;
                    $hours = $setting->user_cancel_time;
                    if(request()->header('lang') == "ar")
                        $cancel_warning = "عند إلغاء الحجز سيتم دفع غرامة مالية قدرها " . $money ." ريال لسبب تعدي الحجز " .$hours. " ساعة ";
                    else
                        $cancel_warning = "When canceling the reservation,".$money." Riyal a fine will be charged  for the reason of booking infringement" .$hours. " hours ";
                }
                $order_offers[0]->cancel_warning =   $cancel_warning;

            }
        }
        else{
            if (sizeof($order_offers) > 0) {
                foreach ($order_offers as $order_offer){
                    $lower_offers_counter = $order->counter;
                    $delivery_time = $order->delivery_time;

                    $orders_count = Order::where('delegate_id', $order_offer->delegate_id)->count();
                    $comments_count = Order::where('delegate_id', $order_offer->delegate_id)
                        ->where('delegate_rate', '!=', NULL)
                        ->where('delegate_comment', '!=', NULL)
                        ->count();

                    $order_offer->delegate->orders_count = $orders_count;
                    $order_offer->delegate->comments_count = $comments_count;
                    $order_offer->lower_offers_counter = $lower_offers_counter;
                    $order_offer->delivery_time = $delivery_time;
                    $order_offer->department_id =   $department_id;


                    $date = Carbon::parse($order->updated_at);
                    $now = Carbon::now();
                    $diff = $date->diffInHours($now);
                    //dd($diff);
                    $cancel_warning = "";
//                    dd($diff > $setting->user_cancel_time);
                    if($diff > $setting->user_cancel_time){
                        $money = $setting->user_cancel_fee;
                        $hours = $setting->user_cancel_time;
                        if(request()->header('lang') == "ar")
                            $cancel_warning = "عند إلغاء الحجز سيتم دفع غرامة مالية قدرها " . $money ." ريال لسبب تعدي الحجز " .$hours. " ساعة ";
                        else
                            $cancel_warning = "When canceling the reservation,".$money." Riyal a fine will be charged  for the reason of booking infringement" .$hours. " hours ";                    }

                    $order->cancel_warning =   $cancel_warning;
                }
            }
        }



//        $data=[
//            'orders_count' => $orders_count,
//            'order_offers' => $order_offers,
//        ];
        return $order_offers;
    }

    public function getDelegateOrdersRates($request, $user_id, $lang = 'ar')
    {
        $orders_count = Order::where('delegate_id', $request->delegate_id)->count();
        $orders_rate = Order::where('delegate_id', $request->delegate_id)
            ->where('delegate_rate', '!=', null)
            ->where('delegate_comment', '!=', null)
            ->select('id', 'delegate_rate as rate', 'delegate_comment as comment', 'user_id', 'created_at')
            ->with('user')
            ->get();
        $data = [
            'orders_count' => $orders_count,
            'rates_count' => sizeof($orders_rate),
            'rates' => $orders_rate,
        ];
        return $data;

    }

    public function getLowerOffer($request, $user_id, $lang = 'ar')
    {
        $order = Order::whereId($request->order_id)->where('user_id', $user_id)->first();

        if ($order) {
            if ($order->counter > 2)
                return false;
            Order::whereId($request->order_id)
                ->where('user_id', $user_id)
                ->update(['counter' => $order->counter + 1]);
            $delegates = Order::filterDelegates($order->in_lat, $order->in_lng, 50, 'delegates', null, null, null, "$order->country_id",$order->department_id,$order->sub_department_id);
// killo_offer_price ;
            $delegateIds=OrderOffer::where('order_id',$request->order_id)->pluck('delegate_id')->toArray();
            $delegates=Delegate::
                where('online',1)
                ->where('busy',0)
                ->where('notification_flag',1)
                ->select('id','lat','lng','token','car_level','near_orders','jwt')->

               whereIn('id',$delegateIds)->get();
            if ($lang == 'ar') {
                $title = ' طلب عرض سعر  اقل ';
                $message = 'لديك طلب خدمة جديد,الرجاء الإستجابة';
            } else {
                $title = 'Request a lower quote';
                $message = 'You have a new order ,please respond';
            }
            foreach ($delegates as $delegate) {
                $type=0;
                if($order->department_id==1){
                    $type=2;
                }
                elseif ($order->department_id==2){
                    $type=5;

                }
                elseif ($order->department_id==3){
                    $type=1;

                }
                else{
                    $type=4;

                }

                $add            = new Notification();
                $add->title     = $title;
                $add->body      = $message;
                $add->user_id   = $delegate->id;
                $add->type      = $type;//type >> 0=>user, 1=>delegate, 2=>driver, 3=>all, 4=>delegateHeavey, 5=>shop
                $add->order_type   = $order->department_id;//0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey
                $add->order_id   = $order->id;
                $add->save();
                Notification::sendNotifyTo($delegate->jwt,$title,$message,0,$order->id,$order->department_id,'delegates');

//                Notification::send("$delegate->token", $title,
//                    $message, 0, 1,
//                    NULL, NULL, NULL, $order->id,NULL,NULL,NULL,NULL,$order->department_id);

            }
            return true;
        }
        return $order;

    }

    public function changeMyDelegate($request, $user_id, $lang = 'ar')
    {
        $order = Order::whereId($request->order_id)->with('order_status')->where('user_id', $user_id)->first();
        if ($order) {
            $order_status = OrderStatus::where('order_id', $order->id)->first();
            if ($order->counter > 2)
                return false;
            //
            if ($order_status->on_way != NULL or
                $order_status->finished != NULL or
                $order_status->cancelled != NULL)
                return "cant_change_delegate";
            //

            //==
            $old_delegate = Delegate::whereId($order->delegate_id)->first();
            Delegate::whereId($order->delegate_id)->update(['busy' => 0 ]);
            OrderChangedDelegate::create([
                'order_id' => $request->order_id,
                'delegate_id' => $order->delegate_id,
            ]);
            $old_delegates_array = OrderChangedDelegate::where('order_id',$request->order_id)
                ->pluck('delegate_id')->toArray();
            if ($lang == 'ar') {
                $title = ' طلب تغيير المندوب ';
                $message = 'قام المستخدم بتغيير المندوب';
            } else {
                $title = 'Request a change delegate';
                $message = 'Customer request to change delegate';
            }
            $chat = [];
            $chat['confirm_accept_order']=-1;
            $chat['order_id']=$order->id;
            Notification::where('order_id',$order->id)
                ->where('order_type','>',1)->where('type',1)->delete();
            Notification::sendNotifyTo($old_delegate->jwt,$title,$message,3,$order->id,$order->department_id,'delegates');
            Broadcast(new OrdersChatEvent($chat,$old_delegate->jwt,$old_delegate->image,"",$order->department_id,-1));

//            Notification::send("$old_delegate->token", $title,
//                $message, 22, 1,
//                "", $chat, NULL, $order->id);
            //==
            OrderOffer::where('id',$order->offer_id)
                ->update(['status' => 0]);
            Order::whereId($request->order_id)
                ->where('user_id', $user_id)
                ->update([
                    'counter' => $order->counter + 1,
                    'delegate_id' => NULL,
                    'offer_id' => NULL,
                ]);
            OrderStatus::where('order_id', $request->order_id)
                ->update([
                    'accept' => NULL
                ]);
            $delegates = Order::filterDelegates($order->in_lat, $order->in_lng, 50, 'delegates', null, null, null, "$order->country_id");

            if ($lang == 'ar') {
                $title = ' طلب جديد ';
                $body = 'لديك طلب خدمة جديد,الرجاء الإستجابة';
            } else {
                $title = 'New Order';
                $body = 'You have a new order ,please respond';
            }
            $message = [];
            $message['confirm_accept_order'] = -1;

            foreach ($delegates as $delegate) {
                if(!in_array($delegate->id,$old_delegates_array)){
                    Notification::sendNotifyTo($delegate->jwt,$title,$body,0,$order->id,$order->department_id,'delegates');

//                    Notification::send("$delegate->token", $title,
//                        $body, 8, 1,
//                        "$order", NULL, NULL, $order->id);
                }



            }
            return "delegate_changed";
        }

    }

    public function checkPromo($request, $country_id = null, $lang = 'ar')
    {
        $codeCheck = PromoCode::where("code", $request->code)
            //->where('department_id', $request->department_id)
            ->select('id', 'department_id', 'code', 'value', 'type',
                'country_ids', 'car_level_ids', 'expire_times',
                'expire_at', $lang . '_desc as description')
            ->first();
        if ($codeCheck && $codeCheck->shop_id==null) {
            if ((int)strtotime($codeCheck->expire_at) < (int)strtotime(Carbon::now()->format('d F Y')))
                return "code_expired";

            if ($request->department_id == 1 || $request->department_id == 2 || $request->department_id == 5) {
                $orders = Order::where("promo_id", $codeCheck->id)->count();
                if ($orders >= $codeCheck->expire_times)
                    return "code_expired";
            }

            else {
                $car_level_ids = explode(',', $codeCheck->car_level_ids);
                if (!(in_array($request->car_level_id, $car_level_ids)))
                    return "invalid_code.";
                $trips = Trip::where("promo_id", $codeCheck->id)->count();
                if ($trips >= $codeCheck->expire_times)
                    return "code_expired";
            }


            $country_ids = explode(',', $codeCheck->country_ids);
            if (!(in_array($country_id, $country_ids)))
                return "invalid_code_";

            unset($codeCheck->country_ids, $codeCheck->expire_times,
                $codeCheck->expire_at, $codeCheck->created_at, $codeCheck->updated_at);

            $codeCheck->type = (int)$codeCheck->type;
            return $codeCheck;


        }
        elseif($codeCheck && $codeCheck->shop_id!=null){
            if ((int)strtotime($codeCheck->expire_at) < (int)strtotime(Carbon::now()->format('d F Y')))
                return "code_expired";
            $orders = Order::where("promo_id", $codeCheck->id)->count();
            if ($orders >= $codeCheck->expire_times)
                return "code_expired";
            if ($codeCheck->shop_id != $request->shop_id)
                return "invalid_code";




            $country_ids = explode(',', $codeCheck->country_ids);
            if (!(in_array($country_id, $country_ids)))
                return "invalid_code_";

            unset($codeCheck->country_ids, $codeCheck->expire_times,
                $codeCheck->expire_at, $codeCheck->created_at, $codeCheck->updated_at);

            $codeCheck->type = (int)$codeCheck->type;
            return $codeCheck;

        }
        else {
            return "invalid_code";
        }
    }

    public function savedLocations($request, $user_id, $lang = 'ar')
    {
        global $locations;
        $locations = new Order();
        if ($request->type == 0) { //pick-up
            return $locations->where('user_id', $user_id)
                ->where('in_lat', '!=', NULL)
                ->where('in_lng', '!=', NULL)
                ->where('in_address', '!=', NULL)
                ->where('in_city_name', '!=', NULL)
                ->select('in_lat as lat', 'in_lng as lng',
                    'in_address as address', 'in_city_name as city_name')
                ->get();
        } else { //
            return $locations->where('user_id', $user_id)
                ->where('out_lat', '!=', NULL)
                ->where('out_lng', '!=', NULL)
                ->where('out_address', '!=', NULL)
                ->where('out_city_name', '!=', NULL)
                ->select('out_lat as lat', 'out_lng as lng',
                    'out_address as address', 'out_city_name as city_name')
                ->get();
        }
    }

    public function getNotifications($request, $user_id, $lang = 'ar')
    {
        //0=>user, 1=>delegate, 2=>driver
        return Notification::where('user_id', $user_id)
//            ->select('title','body','created_at')
            ->get();
    }

    public function getHistoryOrders($request, $user_id, $lang = 'ar')
    {
        ////////////////////
        app()->setLocale($lang);

        if ($request->type == 0)//current_orders
            $orders_array = OrderStatus::where('user_id', $user_id)
                ->where('cancelled', NULL)
                ->where('finished', NULL)
                ->where( 'created_at', '>', Carbon::now()->subDay())
                ->pluck('order_id');
        if ($request->type == 1)//finished_orders
            $orders_array = OrderStatus::where('user_id', $user_id)
                ->where('cancelled', NULL)
                ->where('finished', '!=', NULL)
                ->pluck('order_id');
        ////////////////////

        //1=>uber, 2=>shops, 3=>from any to any
        if ($request->department_id == 1) {
//            return Order::where('user_id',$user_id)
//                    ->where('department_id',$request->department_id)
//                    ->get();
        } elseif ($request->department_id == 2) {
            $orders = Order::orderBy('id', 'desc')
                ->whereIn('id', $orders_array)
                ->where('department_id', $request->department_id)
                ->with(["sub_department" => function ($query)use($lang) {
                    $query->select('id',"name_".$lang." as name");
                }])
                ->with('order_status')
                ->with('order_images')
                ->with('shop')
                ->with('delegate')
//                ->with('offer')
                ->with('offer')
                ->with(["order_products" => function ($query) use ($lang) {
                    $query->with(["order_product_variations" => function ($query) use ($lang) {
                        $query->with('order_product_variation_options');
                    }]);
                }])
                ->where( 'created_at', '>', Carbon::now()->subDay())

                ->get();
        } elseif ($request->department_id == 3) {
            $orders = Order::orderBy('id', 'desc')
                ->whereIn('id', $orders_array)
                ->where('department_id', $request->department_id)
                ->with(["sub_department" => function ($query)use($lang) {
                    $query->select('id',"name_".$lang." as name");
                }])
                ->with('order_status')
                ->with('order_images')
                ->with('shop')
                ->with('delegate')
                ->with('offer')
                ->with(["order_products" => function ($query) use ($lang) {
                    $query->with(["order_product_variations" => function ($query) use ($lang) {
                        $query->with('order_product_variation_options');
                    }]);
                }])
                ->where( 'created_at', '>', Carbon::now()->subDay())

                ->get();
        } elseif ($request->department_id == 5) {
            $orders = Order::orderBy('id', 'desc')
                ->whereIn('id', $orders_array)
                ->where('department_id', $request->department_id)
                ->with(["sub_department" => function ($query)use($lang) {
                    $query->select('id',"name_".$lang." as name");
                }])
                ->with('order_status')
                ->with('order_images')
                ->with('shop')
                ->with('delegate')
                ->with('offer')
                ->with(["order_products" => function ($query) use ($lang) {
                    $query->with(["order_product_variations" => function ($query) use ($lang) {
                        $query->with('order_product_variation_options');
                    }]);
                }])
                ->where( 'created_at', '>', Carbon::now()->subDay())

                ->get();
        }
        $setting = Admin::where('email','admin@admin.com')->first();
        foreach ($orders as $order){
//            if(!$order->distance || $order->distance == ""){
//                $result = getDistanceAndTime($order->in_lat,$order->in_lng,$order->out_lat,$order->out_lng);
//                if(isset($result->rows[0]) && isset($result->rows[0]->elements[0]) && isset($result->rows[0]->elements[0]->distance)){
//                    $distance_val_in_miles = $result->rows[0]->elements[0]->distance->value;
//                    $distance_val_in_km = ($distance_val_in_miles * 1.609344)/1000;
//                    $distance_val_in_km = number_format($distance_val_in_km, 2, '.', '');
//                }else{
//                    $distance_val_in_km = 0;
//                }
//                if($order->last_lat){
//                    //
//                    $result2 = getDistanceAndTime($order->out_lat,$order->out_lng,$order->last_lat,$order->last_lng);
//                    if(isset($result2->rows[0]) && isset($result2->rows[0]->elements[0]) && isset($result2->rows[0]->elements[0]->distance)){
//                        $distance_val_in_miles2 = $result2->rows[0]->elements[0]->distance->value;
//                        $distance_val_in_km2 = ($distance_val_in_miles2 * 1.609344)/1000;
//                        $distance_val_in_km2 = number_format($distance_val_in_km2, 2, '.', '');
//                    }else{
//                        $distance_val_in_km2 = 0;
//                    }
//                    //
//                }else{
//                    $distance_val_in_km2 = 0;
//                }
//                $distance = $distance_val_in_km + $distance_val_in_km2;
//                Order::whereId($order->id)->update(['distance' => $order->distance]);
//            }else{
//                $distance = $order->distance;
//            }
            $distance = $order->distance;
            //dd($distance);
            if(request()->header('lang') == "en"){
                $order->distance = round($distance) ." "."km";
            }else{
                $order->distance = round($distance)  ." "."كم";
            }
            ////
            $order->delivery_time =
                $order->department_id == 5 ? Carbon::parse($order->delivery_time)->translatedFormat('l jS F, h:i a') : $order->delivery_time;

            $date = Carbon::parse($order->updated_at);
            $now = Carbon::now();
            $diff = $date->diffInHours($now);
            //dd($diff);
            $cancel_warning = "";
            if($diff > $setting->user_cancel_time){
                $money = $setting->user_cancel_fee;
                $hours = $setting->user_cancel_time;
                if(request()->header('lang') == "ar")
                    $cancel_warning = "عند إلغاء الحجز سيتم دفع غرامة مالية قدرها " . $money ." ريال لسبب تعدي الحجز " .$hours. " ساعة ";
                else
                    $cancel_warning = "When canceling the reservation,".$money." Riyal a fine will be charged  for the reason of booking infringement" .$hours. " hours ";            }
            $order->cancel_warning =   $cancel_warning;
        }



//"عند إلغاء الحجز سيتم دفع غرامة مالية قدرها ٢٠ ريال لسبب تعدي الحجز ٢٤ ساعة"
//        $data=[
//            'cancel_warning' =>[
//                'user_cancel_fee' =>
//                'user_cancel_time' =>
//            ],
//            'cancel_warning' => "عند إلغاء الحجز سيتم دفع غرامة مالية قدرها " . $money ."ريال لسبب تعدي الحجز " .$hours. "ساعة",
//            'orders' => $orders
//        ];
        return $orders;

    }

    public function getReplacedPoints($request, $user_id, $lang)
    {
        //type =>>>> 0=>user, 1=>delegate, 2=>driver
        $data = [
            'user_points' => User::whereId($user_id)->select('points')->first()->points,
            'user_replaced_points' => UserReplacedPoint::where('user_id', $user_id)
                ->where('type', 0)->get(),
        ];
        return $data;
    }

    public function getofferPoints($request, $user_id, $lang)
    {
        $data = [
            'user_points' => User::whereId($user_id)->select('points')->first()->points,
            'user_offer_points' => OfferPoint::where('used', 0)->get(),
        ];
        return $data;
    }

    public function replacePoints($request, $user_id, $lang)
    {
        $offer = OfferPoint::whereId($request->offer_point_id)->first();
        if (isset($offer) && $offer->used == 0) {
            $user_points = User::whereId($user_id)->select('points')->first()->points;
            if ($offer->points <= $user_points) {
                UserReplacedPoint::create([
                    'user_id' => $user_id,
                    'offer_point_id' => $request->offer_point_id,
                ]);
                User::whereId($user_id)->update(['points' => $user_points - $offer->points]);
                OfferPoint::whereId($request->offer_point_id)->update(['used' => 1]);
                return true;
            }
            return false;
        }

    }

    public function getUserWalletRecharges($request, $user_id, $lang)
    {
        return User::whereId($user_id)->select('id','wallet')->with('transactions')->first();
    }

    public function raiseUserWallet($request, $user_id, $lang)
    {
        $data = WalletRecharge::create([
            'payment_id' => rand(100000, 999999),
            'user_id' => $user_id,
            'amount' => $request->amount,
        ]);
        $user = User::whereId($user_id)->first();
        $user->wallet = $user->wallet + $request->amount;
        $user->save();
        return WalletRecharge::whereId($data->id)->first();
    }

    public function getUserAdminMessages($request, $user_id, $lang)
    {
        return UserAdminMessage::where('user_id', $user_id)
            ->with('user_admin_messages_image')
            ->get();
    }

    public function chatWithAdmin($request, $user_id, $lang)
    {
        $message = UserAdminMessage::create([
            'user_id' => $user_id,
            'message' => $request->message,
            'sender_type' => 0 //0=>user , 1=>admin
        ]);
        if (isset($request->image)) {
            UserAdminMessageImage::create([
                'message_id' => $message->id,
                'image' => $request->image,
            ]);
        }

        return UserAdminMessage::whereId($message->id)
            ->with('user_admin_messages_image')
            ->first();
    }

    public function getUserMessages($request, $user_id, $user_image, $lang)
    {
        $order_id = $request->order_id;
        $order = Order::whereId($order_id)->first();
        $department_id = $order->department_id;
        $receiver_id = $order->delegate_id; //delegate
        $order = Order::whereId($order_id)->select('id', 'delegate_id')->first();
//       dd($order);
        if ($order) {
            $delegate_info = Delegate::whereId($order->delegate_id)->select('id', 'f_name', 'l_name', 'phone','supplier_code')->first();
//        dd($delegate_info);
        } else {
            $delegate_info = (object)[];
        }

        $messages = DB::select("
                SELECT id,message,order_id,created_at,
                    sender_id,sender_type,receiver_id,receiver_type,confirm_accept_order
                FROM messages
                Where
                (
                (sender_id = $user_id and sender_type = 0 AND receiver_id = $receiver_id and receiver_type = 1)
                OR
                (sender_id = $receiver_id and sender_type = 1 AND receiver_id = $user_id and receiver_type = 0)
                )
                AND (order_id = $order_id)
                AND (type = 0)
                Order By id asc
            ");
        if ($messages) {
            if ($messages[0]->sender_id == $user_id && $messages[0]->sender_type == 0) {
                $delegate = Delegate::whereId($messages[0]->receiver_id)->select('image')->first();
            } else {
                $delegate = Delegate::whereId($messages[0]->sender_id)->select('image')->first();
            }

            foreach ($messages as $message) {
                if (isset($message)) {
                    if ($message->message == NULL  ) {
                        $message->message = "";
                        $message->messages_image['image'] = MessageImage::where('message_id', $message->id)->first()?MessageImage::where('message_id', $message->id)->first()->image:null;
                    }
                    elseif ($message->message!=null&&$message->confirm_accept_order==4 ){
                        $message->messages_image = null;
                        $message->invoice= OrderInvoice::where('order_id', $order_id)->first();

                    }
                    else {
                        $message->messages_image = null;
                    }
                    $message->created_at = Carbon::parse($message->created_at)->format('d F Y H:i A');
                    $message->user_image = $user_image;
                    $message->delegate_image = $delegate?$delegate->image:'';
                }

            }
        }
        $setting = Admin::where('email','admin@admin.com')->first();
        $date = Carbon::parse($order->updated_at);
        $now = Carbon::now();
        $diff = $date->diffInHours($now);
        //dd($diff);
        $cancel_warning = "";
        if($diff > $setting->user_cancel_time){
            $money = $setting->user_cancel_fee;
            $hours = $setting->user_cancel_time;
            if(request()->header('lang') == "ar")
                $cancel_warning = "عند إلغاء الحجز سيتم دفع غرامة مالية قدرها " . $money ." ريال لسبب تعدي الحجز " .$hours. " ساعة ";
            else
                $cancel_warning = "When canceling the reservation,".$money." SAR a fine will be charged  for the reason of booking infringement" .$hours. " hours ";
        }
        return [
            'cancel_warning' => $cancel_warning,
            'department_id' => $department_id,
            'confirm_accept_order' => $order->confirm_accept,
            'order_status' => OrderStatus::where('order_id', $order_id)->first(),
            'delegate_info' => $delegate_info,
            'messages' => $messages,
        ];
    }
//chat event
    public function sendMessage($request, $user_id, $user_image, $lang)
    {
        $order = Order::whereId($request->order_id)->first();
        $message = Message::create([
            'sender_id' => $user_id,
            'sender_type' => 0,//0=>user , 1=>delegate
            'receiver_id' => $request->receiver_id,
            'receiver_type' => 1,//0=>user , 1=>delegate
            'message' => $request->message,
            'type' => 0,//0=>orders , 1=>trips
            'order_id' => $request->order_id,
        ]);
        if (isset($request->image)) {
            MessageImage::create([
                'message_id' => $message->id,
                'image' => $request->image,
            ]);
        }
        $delegate = Delegate::whereId($request->receiver_id)->select('id','token','image','jwt')->first();
        $data = Message::whereId($message->id)
            ->with('messages_image')
            ->first();
        $data->delegate_image = $delegate->image;
        $data->user_image = $user_image;
        $data->department_id = $order->department_id;
        $data->confirm_accept_order = $order->confirm_accept;
        if ($lang == 'ar') {
            $title = 'رسالة جديدة';
        } else {
            $title = 'New Message';
        }
        Broadcast(new OrdersChatEvent($data,$delegate->jwt,$delegate->image,$user_image,$order->department_id,$order->confirm_accept));
//       dd($delegate->id);
        Notification::sendNotifyTo($delegate->jwt,$title,$title,2,$order->id,$order->department_id,'delegates');

//        Notification::send($delegate->token, $title,
//            $title, 1, 1,
//            "", $data, NULL, $request->order_id,$order->confirm_accept,
//            null,null,null,$order->department_id);

        return $data;
    }

    public function shopSliders($request, $lang)
    {
        return Slider::inRandomOrder()
            ->with('shop')
            ->take(10)
            ->get();
    }

    public function departments($request, $lang)
    {
        return Department::with(['sub_departments' => function($q) use ($lang){
            $q->where('active',1)
                ->select('id','name_'.$lang.' as name')
            ;

        }])
            ->select('id','name_'.$lang.' as name','image','active')
            ->get();
    }

    public function searchShops($request, $user_id, $lang, $user_country_id)
    {
        $shop = new Shop();
        return $shop->filterbylatlngbySearchKeyForUser($request->lat, $request->lng, "50",
            "shops", null, $request->name, $user_country_id);
//        return Shop::where('name','LIKE','%' . $request->name . '%')
//            ->orWhere('description','LIKE','%' . $request->name . '%')
//            ->where('suspend',0)
//            ->select('id','name','image','department_id','category_id','description')
//            ->get();
    }

    public function getOrderStatus($request, $lang)
    {
        $order = Order::whereId($request->order_id)
            ->with('order_status')
            ->with('delegate')
            ->select('id', 'delegate_id','department_id','sub_department_id',
                'in_lat', 'in_lng', 'in_address',
                'out_lat', 'out_lng', 'out_address',
                'last_lat','last_lng','last_address',
                'country_id')
            ->first();
        return $order;
        return [
            'order_status' => $order->order_status,
            'delegate_info' => $order->delegate,
        ];
    }

    public function rateOrder($logged_user_id,$request, $lang, $user_type)
    {
        $order =  Order::whereId($request->order_id)->first();
        if ($user_type == 0) { //user
            Order::whereId($request->order_id)
                ->update([
                    "delegate_rate" => $request->rate,
                    "delegate_comment" => $request->comment
                ]);
            $avg_rate = Order::where('delegate_id',$order->delegate_id)
                ->whereNotNull('delegate_rate')
                ->avg('delegate_rate');
            Delegate::whereId($order->delegate_id)->update([
                'rate' =>$avg_rate
            ]);
        }
        elseif ($user_type == 1) { //delegate
            Order::whereId($request->order_id)
                ->update([
                    "user_rate" => $request->rate,
                    "user_comment" => $request->comment
                ]);
            $avg_rate = Order::where('user_id',$order->user_id)
                ->whereNotNull('user_rate')
                ->avg('user_rate');
            $avg_trip_rate = Trip::where('user_id',$order->user_id)
                ->whereNotNull('user_rate')
                ->avg('user_rate');
            $avg = ($avg_rate + $avg_trip_rate) / 2;
            User::whereId($order->user_id)->update([
                'rate' =>$avg
            ]);
        }

        return true;
    }
    public function confirmInvoice($request)
    {
//        dd($request->header('lang'));
        $order = Order::whereId($request->order_id)->first();
//        $orderStatus = OrderStatus::where('order_id', $request->order_id)->first();
//        $orderStatus->update([
//            'finished' => Carbon::now()
//        ]);
        $user = User::whereId($order->user_id)->first();
        $delegate = Delegate::whereId($order->delegate_id)->first();
        $order->update(['payment_method' => $request->payment_method,'confirm_accept'=>5]);
        if ($request->header('lang') == 'ar') {
            $title = 'تم الدفع بنجاح';
        } else {
            $title = 'Payment done successfully';
        }
        $message = Message::firstOrCreate([
            'sender_id' => $user->id, 'sender_type' => 0, 'receiver_id' => $order->delegate_id, 'receiver_type' => 1,
            'message' => $title, 'type' => 0, 'order_id' => $request->order_id, 'confirm_accept_order' => 5

        ]);

        Broadcast(new OrdersChatEvent($message, $user->jwt, $delegate->image, $user->image, $order->department_id, 5));
        Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

        Broadcast(new OrdersChatEvent($message, $delegate->jwt, $user->image, $delegate->image, $order->department_id, 5));
        Notification::sendNotifyTo($delegate->jwt, $title, $title, 2, $order->id, $order->department_id, 'delegates');


        OrderInvoice::where('order_id', $request->order_id)->update([
            'confirmed' => 1
        ]);
    }

}
