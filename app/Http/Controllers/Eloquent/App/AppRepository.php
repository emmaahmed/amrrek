<?php
/**
 * Created by PhpStorm.
 * User: Al Mohands
 * Date: 18/06/2019
 * Time: 03:52 م
 */

namespace App\Http\Controllers\Eloquent\App;


use App\Events\OrdersChatEvent;
use App\Http\Controllers\Interfaces\App\AppRepositoryInterface;
use App\Models\AboutUs;
use App\Models\Admin;
use App\Models\AppExplanation;
use App\Models\AppSetting;
use App\Models\CarLevel;
use App\Models\CarType;
use App\Models\ComplainSuggests;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\City;
use App\Models\CriedtCard;
use App\Models\Delegate;
use App\Models\Message;
use App\Models\NationalType;
use App\Models\Notification;
use App\Models\OrderInvoice;
use App\Models\OrderStatus;
use App\Models\Payment;
use App\Models\Period;
use App\Models\Shop;
use App\Models\Subscription;
use App\Models\SubscriptionsType;
use App\Models\TermCondition;
use App\Models\Term;
use App\Models\Trip;
use App\Models\Order;
use App\Models\User;
use App\Models\Driver;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppRepository implements AppRepositoryInterface
{
    public function getFeePercent()
    {
        return Admin::where('email',"admin@admin.com")->select('fee_percent')->first()->fee_percent;
//         dd($country);
//          if(is_array($country))
//              return "aaa";
//        dd($country);
//        $country = implode(",", $country);
//        return $country;
    }

    public function countries()
    {
        return Country::where('active',1)->get();
    }

    public function countriesCodes()
    {
        return $country = Country::where('active',1)->pluck('code_name');
//         dd($country);
//          if(is_array($country))
//              return "aaa";
//        dd($country);
//        $country = implode(",", $country);
//        return $country;
    }

    public function carTpes()
    {
        $lang = request()->header('lang');
        if(request()->type == 2){
            return CarLevel::whereActive(1)
                ->select('id','name_'.$lang.' as name')
                ->get();
        }
        return CarType::where('active',1)
            ->where('type',request()->type)
            ->select('id','name_'.$lang.' as name')
            ->get();
    }

    public function nationalTypes($type)
    {
        $lang = request()->header('lang');
        return NationalType::where('active',1)
            ->where('type',$type)

            ->select('id',"name_".$lang." as name","num")
            ->get();
    }

    public function subscriptionsTypes()
    {
        $lang = request()->header('lang');
        return SubscriptionsType::where('active',1)->select('id','name_'.$lang.' as name')->get();
    }

    public function cities($input)
    {
        return City::where('country_id',$input->country_id)
            ->where('active',1)->get();
    }

    public function complainAndSuggestion($input)
    {
        $array = array(
            'type'=> $input->type,
            'user_id' => $input->user_id,
            'title' => $input->title,
            'description' => $input->description,
        );
        ComplainSuggests::create($array);
        return true;
    }

    public function aboutUs()
    {
        return new AboutUs;
    }

    public function termCondition()
    {
        return new Term();
    }

    public function appExplanation()
    {
        return new AppExplanation();
    }

    public function contactUs($request)
    {
        ContactUs::create($request->all());
    }

    public function getNotifications($request,$user_id, $lang = 'ar'){
        //type =>>>0=>user, 1=>delegate, 2=>driver, 3=>all, 5=>shop
        //order_type =>>> 0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order ,5=>heavey

        if($request->type == 0){
                           $user=User::whereId($user_id)->first();

            $nots = Notification::orderBy('id','desc')
                ->where('user_id',$user_id)
                ->orwhere('user_id',0)
                ->where('type',$request->type)
                                ->where('created_at','>=',$user->created_at)

//            ->where('order_type',$request->order_type)
                // ->Where('order_type',0)
                ->take(50)
                ->get();
        }
        if($request->type == 1){
                           $user=Delegate::whereId($user_id)->first();

            $nots = Notification::orderBy('id','desc')
                ->where('user_id',$user_id)
                ->orwhere('user_id',0)

                ->where('type',$request->type)
//            ->where('order_type',$request->order_type)
                ->whereIn('order_type',[0,2,3])
                                                ->where('created_at','>=',$user->created_at)

//                ->orWhere('order_type',3)
                ->take(50)
                ->get();
        }
        if($request->type == 2){
                                       $user=Driver::whereId($user_id)->first();

            $nots = Notification::orderBy('id','desc')
                ->where('user_id',$user_id)
                ->where('type',$request->type)
                                                                ->where('created_at','>=',$user->created_at)

//            ->where('order_type',$request->order_type)
                ->WhereIn('order_type',[0,1])
                ->take(50)
                ->get();
        }
        if($request->type == 3){
            return [];
        }
        if($request->type == 4){
                           $user=Delegate::whereId($user_id)->first();
                           if($user->accept==0){
                               return [];
                           }

             $nots = Notification::orderBy('id','desc')
                ->whereIn('user_id',[$user_id,0])

                ->where('type',$request->type)
//            ->where('order_type',$request->order_type)
                                                ->where('created_at','>=',$user->created_at)

                ->whereIn('order_type',[0,5])

                ->take(50)
                ->get();
        }
        if($request->type == 5){
                                       $user=Shop::whereId($user_id)->first();

            $nots = Notification::orderBy('id','desc')
                ->where('user_id',$user_id)
                ->where('type',$request->type)
                ->orwhere('user_id',0)
                 ->where('created_at','>=',$user->created_at)

//            ->where('order_type',$request->order_type)
                ->whereIn('order_type',[0,2])
                ->take(50)
                ->get();
            // dd($nots);
        }


        foreach ($nots as $not){
            if($not->order_type == 1){
                $trip = $not->order_id ? Trip::whereId($not->order_id)->select('status')->first() : null ;
                $not->trip_status = isset($trip->status) ? $trip->status : 0 ;
                $not->order_status = null ;
                $not->delegate_id = 0 ;
            }elseif($not->order_type > 1){
                $order = $not->order_id ? OrderStatus::where('order_id',$not->order_id)->first() : null;
                $delegate = $not->order_id && Order::whereId($not->order_id)->select('delegate_id')->first()? Order::whereId($not->order_id)->select('delegate_id')->first()->delegate_id : 0 ;
                $not->trip_status = "" ;
                $not->order_status = $order ;
                $not->delegate_id =  $delegate ;
            }else{
                $not->trip_status = "" ;
                $not->order_status =  null ;
                $not->delegate_id = 0 ;
            }
        }
        return $nots;
        return Notification::orderBy('id','desc')
            ->where('user_id',$user_id)
            ->where('type',$request->type)
//            ->where('order_type',$request->order_type)
//            ->orWhere('order_type',0)
            ->take(50)
            ->get();
    }

    public function makeSubscription($request){
        //make subscribtion here with payment
    }

    public function getSubscription($request,$user_id){
        //type  >>> 1=>delegate, 2=>driver, 3=>heaveyDelegate
        $data = Admin::where('email','admin@admin.com')->first();

        $checkSubscription = Subscription::orderBy('id','desc')
            ->where('type',$request->type)
            ->where('user_id',$user_id)->first();
        $date="";
        if($checkSubscription){
            $date = $checkSubscription->expire_at;
        }

        if($request->type == 1){
            $data=AppSetting::where('key_name','delegate_monthly_subscribtion')->first();
            $monthley_subscribtion = $data->val;
        }
        elseif($request->type == 2){
            $data=AppSetting::where('key_name','driver_monthly_subscribtion')->first();
            $monthley_subscribtion = $data->val;

        }elseif($request->type == 3){
            $data=AppSetting::where('key_name','heavey_delegate_monthly_subscribtion')->first();
            $monthley_subscribtion = $data->val;

        }
        $tax=AppSetting::where('key_name','subscription_tax')->first();

        $tax = $tax->val;
        $total = ($monthley_subscribtion * ($tax / 100)) + $monthley_subscribtion;
        // return $date;
        $now =  Carbon::now();



        return [
            'monthley_subscribtion' => (int)$monthley_subscribtion,
            'tax' => (int) $tax,
            'total' => number_format($total, 1, '.', ''),
            'date' => $now->diffInDays($date),
        ];
    }

    //getPeriods
    public function getPeriods($request,$lang){
        return Period::select('id','name_'.$lang.' as name')->get();
    }
    public function rechargeWallet($request,$lang,$jwt){
        if($request->type==0){
            if(checkJWT($jwt)){
                $user=checkJWT($jwt);
                Payment::create([
                    'attribute_id'=>$user->id,
                    'attribute_type'=>'App\Models\User',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,

                ]);
                if($request->status==1) {
                    $user->update([
                        'wallet' => ($user->wallet + $request->amount),
                        'cc_token' => $request->cc_token ? $request->cc_token : null,

                    ]);
                }
                if($request->card_num) {
                    CriedtCard::create([
                        'card_num' => $request->card_num,
                        'cc_token' => $request->cc_token,

                    ]);
                }

                return $user;


            }
            if(checkDriverJWT($jwt)){
                $driver=checkDriverJWT($jwt);

                Payment::create([
                    'attribute_id'=>$driver->id,
                    'attribute_type'=>'App\Models\Driver',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,

                ]);
                if($request->status==1) {

                    $driver->update([
                        'wallet' => ($driver->wallet + $request->amount),
                        'cc_token' => $request->cc_token ? $request->cc_token : null,
                    ]);
                }
                return $driver;

            }

        }
        elseif($request->type==1){
            if(checkDelegateJWT($jwt)){
                $delegate=checkDelegateJWT($jwt);
                Payment::create([
                    'attribute_id'=>$delegate->id,
                    'attribute_type'=>'App\Models\Delegate',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,

                ]);
                if($request->status==1) {

                    $subscription = Subscription::where(['user_id' => $delegate->id, 'type' => $delegate->type == 0 ? 1 : 3])->first();
                    if ($subscription) {
                        $subscription->update([
                            'expire_at' => Carbon::parse($subscription->expire_at)->addDays(30),
                            'is_subscribed'=>1
                        ]);
                    } else {
                        Subscription::create([
                            'user_id' => $delegate->id,
                            'type' => $delegate->type == 0 ? 1 : 3,
                            'expire_at' => Carbon::now()->addMonth()
                        ]);
                    }
                }
                $delegate->update([
                    'cc_token'=>$request->cc_token?$request->cc_token:null,
                ]);

                return $delegate;


            }
            if(checkDriverJWT($jwt)){
                $driver=checkDriverJWT($jwt);
                Payment::create([
                    'attribute_id'=>$driver->id,
                    'attribute_type'=>'App\Models\Driver',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,

                ]);
                if($request->status==1) {

                    $subscription = Subscription::where(['user_id' => $driver->id, 'type' => 2])->first();
                    if ($subscription) {
                        $subscription->update([
                            'expire_at' => Carbon::parse($subscription->expire_at)->addDays(30)
                        ]);
                    } else {
                        Subscription::create([
                            'user_id' => $driver->id,
                            'type' => 2,
                            'expire_at' => Carbon::now()->addMonth()
                        ]);
                    }
                }
                $driver->update([
                    'cc_token'=>$request->cc_token?$request->cc_token:null,
                ]);

                return $driver;


            }

        }
        elseif($request->type==2){
            if(checkDelegateJWT($jwt)){
                $delegate=checkDelegateJWT($jwt);
                Payment::create([
                    'attribute_id'=>$delegate->id,
                    'attribute_type'=>'App\Models\Delegate',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,
                ]);
                $debit=0;
                $wallet=0;
                if($delegate->app_total>$request->amount){
                    $debit=($delegate->app_total-$request->amount);
                }
                else{
                    $debit=$request->amount-$delegate->app_total;
                    $wallet=$delegate->wallet+$wallet;

                }
                if($request->status==1) {

                    $delegate->update([
                        'app_total' => $debit,
                        'wallet' => $wallet,
                        'cc_token' => $request->cc_token ? $request->cc_token : null,
                    ]);
                }
                return $delegate;


            }
            if(checkDriverJWT($jwt)){
                $driver=checkDriverJWT($jwt);
                Payment::create([
                    'attribute_id'=>$driver->id,
                    'attribute_type'=>'App\Models\Delegate',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,
                ]);
                $debit=0;
                $wallet=0;
                if($driver->app_total>$request->amount){
                    $debit=($driver->app_total-$request->amount);
                }
                else{
                    $debit=$request->amount-$driver->app_total;
                    $wallet=$driver->wallet+$wallet;

                }
                if($request->status==1) {

                    $driver->update([
                        'app_total' => $debit,
                        'wallet' => $wallet,
                        'cc_token' => $request->cc_token ? $request->cc_token : null,
                    ]);
                }

                return $driver;


            }


        }
        else{
            if(checkJWT($jwt)){
                $user=checkJWT($jwt);
                Payment::create([
                    'attribute_id'=>$user->id,
                    'attribute_type'=>'App\Models\User',
                    'transaction_id'=>$request->transaction_id,
                    'status'=>$request->status,
                    'type'=>$request->type,
                    'amount'=>$request->amount,

                ]);
                if($request->card_num) {
                    CriedtCard::create([
                        'card_num' => $request->card_num,
                        'cc_token' => $request->cc_token,

                    ]);
                }
                $order = Order::whereId($request->order_id)->first();
                $orderStatus = OrderStatus::where('order_id', $request->order_id)->first();
                $orderStatus->update([
                    'finished' => Carbon::now()
                ]);
                $user = User::whereId($order->user_id)->first();
                $delegate = Delegate::whereId($order->delegate_id)->first();
                $delegate->update(['busy'=>0]);
                $order->update(['payment_method' => 1,'confirm_accept'=>5]);
                if ($request->lang == 'ar') {
                    $title = 'تم الدفع بنجاح';
                } else {
                    $title = 'Payment done successfully';
                }
                $message = Message::firstOrCreate([
                    'sender_id' => $user->id, 'sender_type' => 0, 'receiver_id' => $order->delegate_id, 'receiver_type' => 1,
                    'message' => $title, 'type' => 0, 'order_id' => $request->order_id, 'confirm_accept_order' => 5

                ]);

                Broadcast(new OrdersChatEvent($message, $user->jwt, $delegate->image, $user->image, $order->department_id, 6));
                Notification::sendNotifyTo($user->jwt, $title, $title, 2, $order->id, $order->department_id, 'users');

                Broadcast(new OrdersChatEvent($message, $delegate->jwt, $user->image, $delegate->image, $order->department_id, 6));
                Notification::sendNotifyTo($delegate->jwt, $title, $title, 2, $order->id, $order->department_id, 'delegates');


                OrderInvoice::where('order_id', $request->order_id)->update([
                    'confirmed' => 1
                ]);

                return $user;


            }

        }

    }
    public function getTransactions($user_id){
        return User::whereId($user_id)->select('id','wallet')->with('transactions')->first();
//        return Payment::where(['attribute_id'=>$user_id,'attribute_type'=>'App\Models\User','status'=>1])->get();
    }

}
