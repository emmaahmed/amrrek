<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {

        if (! $request->expectsJson()) {
                if($request->is('admin')||$request->is('admin/*') ){
                    return route('login');

                }
                elseif($request->is('website-admin')||$request->is('website-admin/*') )
                    {
                        return route('website-admin.login-page');
                    }
                else{
                    return route('login');

                }

        }
    }

}
