<?php

namespace App\Exports;

use App\Models\Shop;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ShopsExport implements FromView,WithStyles,WithColumnWidths,WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $accept)
    {
        $this->accept = $accept;
    }

    public function view(): View
    {
        if ($this->accept == 2) {
            return view('cp.shops.excel-view', [
                'shops' => Shop::orderBy('id','desc')
                    ->with('country')
                    ->withCount('branches')
                    ->withCount('allOrders')
                    ->get()

            ]);
        } else {
            return view('cp.shops.excel-view', [
                'shops' => Shop::orderBy('id','desc')
                    ->with('country')
                    ->withCount('branches')
                    ->withCount('allOrders')
                    ->where('suspend',$this->accept)
                    ->get()
            ]);

        }
    }

    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
        ;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }

    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic');

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 30,
            'D' => 20,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 30,
            'I' => 90,
            'J' => 30,
            'K' => 30,
            'L' => 30,
            'M' => 30,
            'N' => 30,
        ];
    }
}
