<?php

namespace App\Exports;

use App\Models\Delegate;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromView;


class DelegatesExport implements FromView,WithStyles,WithColumnWidths,WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int  $accept,int $type)
    {
        $this->type =   $type;
        $this->accept =   $accept;
    }

    public function view(): View
    {
//        dd());
//        $delegates=Delegate::where(['type'=>$this->type,'accept'=>$this->accept])->with(['country','car_type','delegate_documents'])->withCount('orders')->get();
//         foreach ($delegates as $delegate){
//             is_file(public_path('/uploads/delegates/images/'.$delegate->getAttributes()['image']))? :public_path('/uploads/no-image.png');
//         }
        //            dd(file_exists(public_path('/uploads/delegates/images/'.$delegates[0]->getAttributes()['image'])));
//        dd(Delegate::where(['type'=>$this->type,'accept'=>$this->accept])->with(['country','car_type','delegate_documents'])->withCount('orders')->get());
        return view('cp.delegates.excel-view', [
            'delegates' => Delegate::where(['type'=>$this->type,'accept'=>$this->accept])->with(['country','car_type','delegate_documents'])->withCount('orders')->get()
        ]);
    }
    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }
    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic' );

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 30,
            'D' => 20,
            'E' => 15,
            'F' => 15,
            'G' => 5,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,
            'N' => 20,
        ];
    }


}

