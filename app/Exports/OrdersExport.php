<?php

namespace App\Exports;

use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Spatie\Permission\Models\Permission;

class OrdersExport implements FromView,WithStyles,WithColumnWidths,WithEvents
{
    public function __construct(int $shop_id,$request)
    {
        $this->shop_id = $shop_id;
        $this->request = $request;
    }

    public function view(): View
    {
        if ($this->shop_id != 0) {
            if ($this->request->from != null && $this->request->to ) {
                $from = date('Y-m-d', strtotime($this->request->from));
                $to = date('Y-m-d', strtotime($this->request->to));

                $orders = Order::orderBy('id', 'desc')
                    ->where('shop_id', $this->shop_id)
                    ->with('user')
                    ->with('delegate')
                    ->with(["order_products" => function ($query) {
                        $query->with(["order_product_variations" => function ($query) {
                            $query->with('order_product_variation_options');
                        }]);
                    }])
                    ->whereBetween('created_at',[$from,$to])
                    ->with("order_imagess")
                    ->get();
            } else {
                $orders = Order::orderBy('id', 'desc')
                    ->where('shop_id', $this->shop_id)
                    ->with('user')
                    ->with('delegate')
                    ->with(["order_products" => function ($query) {
                        $query->with(["order_product_variations" => function ($query) {
                            $query->with('order_product_variation_options');
                        }]);
                    }])
                    ->with("order_imagess")
                    ->get();

            }
        }



        else{
            $orders = Order::orderBy('id', 'desc')
                ->with('user')
                ->with('delegate')
                ->with(["order_products" => function ($query) {
                    $query->with(["order_product_variations" => function ($query) {
                        $query->with('order_product_variation_options');
                    }]);
                }])
                ->with("order_imagess")
                ->get();

        }
        foreach($orders as $order) {
            $order->order_status = OrderStatus::where('order_id', $order->id)->first();
        }
            return view('cp.orders.excel-view', [
            'orders' => $orders


        ]);


    }

    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
        ;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }

    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic');

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 15,
            'D' => 30,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,
            'N' => 20,
            'O' => 20,
            'P' => 20,
            'Q' => 20,
            'R' => 20,
            'S' => 20,
            'T' => 20,
            'U' => 20,
            'V' => 20,
            'W' => 20,
            'X' => 20,
            'Y' => 20,
            'z' => 20,
        ];
    }
}
