<?php

namespace App\Exports;

use App\Models\BranchProduct;
use App\Models\Menu;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProductsExport implements FromView,WithStyles,WithColumnWidths,WithEvents,WithProperties
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        $shop_id = Auth::guard('shop')->user()->shop_id;
        $shop = Shop::whereId($shop_id)->first();
        if($shop->parent_id == null){
            $menus = Menu::where('shop_id',$shop->id)->get();
            $products = Product::orderBy('id','desc')
                ->where('shop_id',$shop->id)
                ->with('menue')
                ->with(["variations" => function ($query) {
                    $query->with(["options"]);
                }])
                ->with('variations')
                ->get();
        }
        else{
            $menus = Menu::where('shop_id',$shop->parent_id)->get();
            $products = Product::orderBy('id','desc')
                ->where('shop_id',$shop_id)
                ->orWhere('shop_id',$shop->parent_id)
                ->with('menue')
                ->with(["variations" => function ($query) {
                    $query->with(["options"]);
                }])
                ->with('variations')
                ->get();

            foreach ($products as $c){
                $branch_product = BranchProduct::where('product_id',$c->id)
                    ->where('branch_id',$shop_id)->first();
                if($branch_product)
                    $c->active = $branch_product->status;
            }
        }

        return view('cp_shop.products.excel-view', [
            'products'=>$products,
            'menus'=>$menus,

            ]);
    }
    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
        ;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }
    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic' );

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 15,
            'D' => 20,
            'E' => 20,
            'F' => 20,
            'G' => 20,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,
            'N' => 20,
            'O' => 20,
            'P' => 20,
            'Q' => 20,
            'R' => 20,
            'S' => 20,
            'T' => 20,
            'U' => 20,
            'V' => 20,
            'W' => 20,
            'X' => 20,
            'Y' => 20,
            'z' => 20,
        ];
    }
    public function properties(): array
    {
        return [
            'creator'        => 'أمرك',
            'lastModifiedBy' => 'أمرك',
            'title'          => 'إكسيل المنتجات',
            'description'    => 'Latest Invoices',
            'subject'        => 'المنتجات',
            'category'       => 'المنتجات',
            'manager'        => 'أمرك',
            'company'        => 'أمرك',
        ];
    }


}
