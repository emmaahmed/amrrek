<?php

namespace App\Exports;

use App\Models\ComplainSuggests;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ComplainsExport implements FromView,WithStyles,WithColumnWidths,WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct(int $accept,int $type)
    {
        $this->accept = $accept;
        $this->type = $type;
    }

    public function view(): View
    {
        $complains = ComplainSuggests::orderBy('id','desc')
            ->with('user')
//            ->with('issue')
//            ->with('lost')
//            ->with('trip')
            ->where('type',$this->type)
            ->where('status',$this->accept)
            ->get();
        foreach ($complains as $c){
            if(isset($c->trip)){
                if(isset($c->trip->driver_id)){
                    $c->driver = User::whereId($c->trip->driver_id)
                        ->select('id','name','phone','email')->first();
                }
            }
        }

        return view('cp.complains_suggestions.excel-view', [
                'complains' => $complains
            ]);


    }

    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
        ;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }

    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic');

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 30,
            'D' => 20,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,
            'N' => 20,
        ];
    }
}
