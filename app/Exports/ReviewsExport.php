<?php

namespace App\Exports;

use App\Models\ShopRate;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReviewsExport implements FromView,WithStyles,WithColumnWidths,WithEvents
{
    public function __construct(int $department_id)
    {
        $this->department_id = $department_id;
    }

    public function view(): View
    {
        $rates = ShopRate::orderBy('id','desc')
            ->with('user')
            ->with('shop')
            ->where('department_id',$this->department_id)
            ->get();
        return view('cp.shop_rates.excel-view', [
            'rates' => $rates


        ]);


    }

    public function styles(Worksheet $sheet)
    {

//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true,'italic' => true,'size' => 12]],
//
//            // Styling a specific cell by coordinate.
//        ];
//        $sheet->getStyle(1)->getFont()->setBold(true);
//        $sheet->getStyle(1)->getAlignment()->setHorizontal('center');
//        $sheet->getStyle(1)->getFont()->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color( \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKRED ));
//        $sheet->getStyle(1) ->getFill() ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_PATTERN_DARKGRAY)
//            ;;
//        $sheet->getStyle(1)->getFont()->setSize(12);
//        $sheet->getStyle(1)->getFont()->setUnderline(true);
        ;
//        $sheet->getStyle('B')->getFont()->setSize(12);
//        $sheet->getStyle('C')->getFont()->setSize(12);
//        $sheet->getStyle('D')->getFont()->setSize(12);


    }

    public function registerEvents(): array

    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('MS P Gothic');

            },

        ];

    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 15,
            'C' => 15,
            'D' => 30,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 20,
            'L' => 20,
            'M' => 20,
            'N' => 20,
            'O' => 20,
            'P' => 20,
            'Q' => 20,
            'R' => 20,
            'S' => 20,
            'T' => 20,
            'U' => 20,
            'V' => 20,
            'W' => 20,
            'X' => 20,
            'Y' => 20,
            'z' => 20,
        ];
    }
}

