<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'website-admin'], function () {
        Route::group(['middleware' => 'locale'], function () {

        Route::group(['middleware' => 'auth:website-admin'], function () {

    Route::get('/dashboard', 'DashboardController@dashboard')->name('website-admin.dashboard');
    Route::get('/logout', 'DashboardController@logout')->name('website-admin.logout');

    Route::resource('slider', 'SliderController');
    Route::resource('under-slider', 'UnderSliderController');
    Route::resource('about', 'AboutController');
    Route::resource('services', 'ServicesController');
    Route::resource('under-services', 'UnderServicesController');
    Route::resource('download-section', 'DownloadSectionController');
    Route::resource('how-to-use', 'HowToUseController');
    Route::resource('client-section', 'ClientsSectionController');
    Route::resource('opinions', 'OpinionsController');
    Route::resource('clients', 'ClientsController');
    Route::resource('faq-category', 'FaqsCategoriesController');
    Route::resource('faqs', 'FaqsController');
    Route::resource('website-settings', 'SettingsController');
    Route::get('brief-about', 'AboutController@briefAbout')->name('brief-about.index');
    Route::get('news-letter', 'ContactUsController@newsletter')->name('news-letter.index');
    Route::patch('update-brief-about/{id}', 'AboutController@updateBriefAbout')->name('brief-about.update');
});
            
        });
    Route::get('/', 'DashboardController@loginPage')->name('website-admin.login-page');
    Route::post('/', 'DashboardController@login')->name('website-admin.login');
Route::get('/changelanguage/{langkey}', 'DashboardController@ChangeLang')->name('change.lang');

});
