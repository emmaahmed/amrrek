<?php

namespace App\Modules\Websiteadmin\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        // app()->setLocale('ar');
        $this->loadTranslationsFrom(module_path('websiteadmin', 'Resources/Lang', 'app'), 'websiteadmin');
        $this->loadViewsFrom(module_path('websiteadmin', 'Resources/Views', 'app'), 'websiteadmin');
        $this->loadMigrationsFrom(module_path('websiteadmin', 'Database/Migrations', 'app'), 'websiteadmin');
        $this->loadConfigsFrom(module_path('websiteadmin', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('websiteadmin', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
