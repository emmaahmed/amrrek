<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <title>Amrk - امرك</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/fonts/flaticon/font/flaticon.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets-login/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets-login/css/skins/default.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/style-spider.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets-login/css/animate.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">



</head>
<body id="top" >

<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<!-- <div class="loader-color">
<div class="cssload-dots">
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>
    <div class="cssload-dot"></div>

</div>
</div> -->
<!-- Login 11 start -->
@yield('content')


<!-- Login 11 end -->

<!-- External JS libraries -->

<script src="{{asset('assets-login/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('assets-login/js/popper.min.js')}}"></script>
<script src="{{asset('assets-login/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> <!-- stats.js lib -->
<script src="{{asset('assets-login/js/script-spider.js')}}"></script>
<script src="{{asset('assets-login/js/wow.min.js')}}"></script>
<script>

    new WOW().init();

</script>

<!-- Custom JS Script -->

<!-- <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <filter id="goo">
            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12">
            </feGaussianBlur>
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7" result="goo">
            </feColorMatrix>
        </filter>
    </defs>
</svg>
<script>



       $(".loader-color").show();
       setTimeout(function () {

          $('.loader-color').hide();
       }, 2000);


    </script> -->

</body>
</html>