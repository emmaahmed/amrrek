<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper">
            <a style="display: table;margin: 0 auto;" href="{{route('website-admin.dashboard')}}">

                <img src="{{asset('logo-svg.svg')}}" alt="" ></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div style="padding-top: 7%;">
                <img class="img-60 rounded-circle" src=" @if(Auth::guard('website-admin')->user())  {{Auth::guard('website-admin')->user()->image }} @endif" alt="#">
                <div class="profile-edit">


                </div>
            </div>
            <h6 class="mt-3 f-14"></h6>
            <p> @if(Auth::guard('website-admin')->user()) {{ Auth::guard('website-admin')->user()->name }} @endif</p>
        </div>

        <ul class="sidebar-menu">

            <li><a class="sidebar-header" href="{{route('website-settings.index')}}"><i data-feather="settings"></i><span> {{__('admin.settings')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('slider.index')}}"><i data-feather="repeat"></i><span> {{__('admin.slider')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('under-slider.index')}}"><i data-feather="image"></i><span> {{__('admin.under-slider')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('about.index')}}"><i data-feather="thumbs-up"></i><span> {{__('admin.about')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('services.index')}}"><i data-feather="list"></i><span> {{__('admin.services')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('under-services.index')}}"><i data-feather="file-text"></i><span> {{__('admin.under-services')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('download-section.index')}}"><i data-feather="download"></i><span> {{__('admin.download-section')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('how-to-use.index')}}"><i data-feather="help-circle"></i><span> {{__('admin.how-to-use')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('client-section.index')}}"><i data-feather="image"></i><span> {{__('admin.client-section')}} </span></a>

            </li>
            <li><a class="sidebar-header" href="{{route('brief-about.index')}}"><i data-feather="file-text"></i><span> {{__('admin.brief-about')}} </span></a>
            </li>
            <li>
                <a class="sidebar-header"  ><i data-feather="message-circle"></i>
                    <span>{{__('admin.opinions')}}</span>
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    <i class="fa fa-angle-right pull-right" style="transform: rotate(180deg)"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('opinions.index')}}"><i class="fa fa-circle"></i><span>{{__('admin.all-opinions')}}</span></a></li>
                    <li><a href="{{route('opinions.create')}}"><i class="fa fa-circle"></i><span>{{__('admin.create-opinion')}}</span></a></li>
                </ul>
            </li>
            <li>
                <a class="sidebar-header"  ><i data-feather="image"></i>
                    <span>{{__('admin.clients')}}</span>
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    <i class="fa fa-angle-right pull-right" style="transform: rotate(180deg)"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('clients.index')}}"><i class="fa fa-circle"></i><span>{{__('admin.all-clients')}}</span></a></li>
                    <li><a href="{{route('clients.create')}}"><i class="fa fa-circle"></i><span>{{__('admin.create-client')}}</span></a></li>
                </ul>
            </li>
            <li>
                <a class="sidebar-header"  ><i data-feather="layers"></i>
                    <span>{{__('admin.faq-category')}}</span>
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    <i class="fa fa-angle-right pull-right" style="transform: rotate(180deg)"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('faq-category.index')}}"><i class="fa fa-circle"></i><span>{{__('admin.all-faq-category')}}</span></a></li>
                    <li><a href="{{route('faq-category.create')}}"><i class="fa fa-circle"></i><span>{{__('admin.create-faq-category')}}</span></a></li>
                </ul>
            </li>
            <li>
                <a class="sidebar-header"  ><i data-feather="minus"></i>
                    <span>{{__('admin.faqs')}}</span>
                    {{--                            <i class="fa fa-angle-right pull-right"></i>--}}
                    <i class="fa fa-angle-right pull-right" style="transform: rotate(180deg)"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('faqs.index')}}"><i class="fa fa-circle"></i><span>{{__('admin.all-faqs')}}</span></a></li>
                    <li><a href="{{route('faqs.create')}}"><i class="fa fa-circle"></i><span>{{__('admin.create-faqs')}}</span></a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href="{{route('news-letter.index')}}"><i data-feather="file-text"></i><span> {{__('admin.news-letter')}} </span></a>

            </li>




        </ul>

    </div>
</div>
