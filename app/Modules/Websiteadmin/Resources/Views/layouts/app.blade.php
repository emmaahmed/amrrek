<!DOCTYPE html>
<html @if(app()->getLocale()=='ar') lang="ar" @else lang="en" @endif >
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Juha Admin-Panel.">
    <meta name="keywords" content="Juha Admin-Panel Admin shop">
    <meta name="author" content="Juha">
    <link rel="icon" href="{{asset('default.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('default.png')}}" type="image/x-icon">
    <title>Amrk - أمرك</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/datatables.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/select2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/date-picker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/owlcarousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/prism.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/whether-icon.css')}}">
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('front/admin/assets/css/light-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front/admin/assets/css/timepicker.css')}}">

    <link href="https://fonts.googleapis.com/css?family=Tajawal&display=swap" rel="stylesheet">

    <style>
        .pd-row{
            padding-left: 1em;
            padding-bottom: 2em;
            float: left;
        }
        .card-header h5,.card-header {
            text-align: center;
            font-weight: bold;
            color: #06215b;


        }
        .card-header.b-l-primary h5{
            text-align: right;


        }
    </style>
    <style>
        @font-face {
            font-family: 'din';
            src: url({{asset('din.otf')}}) format('opentype');
        }

        body, h1, h2, h3, h4, h5, h6, * {
            font-family: 'din';
            font-size: small;
        }
        th{
            text-align: center;
        }
        td{
            text-align: center;
        }

        element.style {
        }
        .selling-update svg path {
            color: #C32F45;
        }
        .selling-update svg path,
        .selling-update svg line,
        .selling-update svg polyline,
        .selling-update svg polygon,
        .selling-update svg rect,
        .selling-update svg circle,
        .mb-0,
        .f-18,
        .align-abjust,
        h6,h4,h3{

            color:#C32F45;
        }
        .page-wrapper .page-body-wrapper .page-sidebar {
            background: #b5293c;
        }
        .loader ,.whirly-loader{
            color : red !important;
        }
        button{
            margin: 2px;
        }
        .tt-menu{
            display: none!important;
        }

    </style>

</head>
<body @if(app()->getLocale()=='ar') main-theme-layout="rtl" dir="rtl" @endif >
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-right row">
            <div class="main-header-left d-lg-none">
                <div class="logo-wrapper">
{{--                    <a href="{{route('website-admin.dashboard')}}">--}}
{{--                        <img src="{{asset('logo-svg.svg')}}" alt=""></a>--}}
                </div>
            </div>
            <div class="mobile-sidebar d-block">
                <div class="media-body text-right switch-sm">
                    <label class="switch"><a ><i id="sidebar-toggle"
                                                 data-feather="align-left"></i></a></label>
                </div>
            </div>

            <div class="nav-right col p-0">
                <ul class="nav-menus" >
                    <li style="text-align: center;">
                    </li>
                    <li><a class="text-dark"  onclick="javascript:toggleFullScreen()"><i
                                    data-feather="maximize"></i></a></li>
              <li class="onhover-dropdown"><a class="txt-dark" >
                  <h6>@if(app()->getLocale()=='ar') {{__('admin.arabic')}} @else {{__('admin.english')}}  @endif</h6></a>
                <ul class="language-dropdown onhover-show-div p-20">
                  <li><a href="{{route('change.lang','ar')}}" data-lng="en"><i class="flag-icon flag-icon-sa"></i>{{__('admin.arabic')}}</a></li>
                  <li><a href="{{route('change.lang','en')}}" data-lng="es"><i class="flag-icon flag-icon-us"></i> {{__('admin.english')}}</a></li>
                </ul>
              </li>

                    <li class="onhover-dropdown" >
                        <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="@if(Auth::guard('website-admin')->user()!==null) {{Auth::guard('website-admin')->user()->image }}  @endif " alt="header-user">
                            <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div p-20" style="width: 200px;">
                            {{--                            @if(\Illuminate\Support\Facades\Auth::guard('shop')->user())--}}
                            {{--                            <li><a href="{{route('shops.editprofile')}}"><i data-feather="user"></i> تعديل اللف الشخصي</a></li>--}}
                            {{--                            @endif--}}
                            <li><a href="{{route('website-admin.logout')}}"><i data-feather="log-out"></i> {{__('admin.logout')}}</a></li>
                        </ul>
                    </li>


                </ul>
                <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
            </div>
            <script id="result-template" type="text/x-handlebars-template">
                <div class="ProfileCard u-cf">
                    <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                    <div class="ProfileCard-details">
                        <div class="ProfileCard-realName"></div>
                    </div>
                </div>
            </script>
            <script id="empty-template" type="text/x-handlebars-template">
                <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>

            </script>
        </div>
    </div>
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
    @include('websiteadmin::layouts.includes.sidebar')

    <!-- Page Sidebar Ends-->

        @yield('content')
        <footer class="footer">
            <div class="container-fluid">
{{--                <div class="row">--}}

{{--                    <a class="pull-right mb-0" href="http://2grand.net" target="_blank" style="margin: auto">--}}
{{--                        <img src="{{asset('grand.png')}}" style="width:100px;">--}}
{{--                    </a>--}}
                    <div class="col-md-12 footer-copyright" style="text-align: center">
                        <a href="https://amrk.my-staff.net/" target="_blank">
                            <b class="mb-0" style="color: #C32F45;">                                    جميع الحقوق  محفوظة - لتطبيق أمرك 2021
                            </b>
                        </a>

                    </div>
{{--                    <div class="col-md-6">--}}
{{--                      <p class="pull-right mb-0">Hand crafted & made with<i class="fa fa-heart"></i></p>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </footer>



    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- latest jquery-->
<script src="{{asset('front/admin/assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/time-picker/jquery-clockpicker.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/time-picker/highlight.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/time-picker/clockpicker.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{asset('front/admin/assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/bootstrap/bootstrap.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('front/admin/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('front/admin/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('front/admin/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->



<script src="{{asset('front/admin/assets/js/prism/prism.min.js')}}"></script>

<script src="{{asset('front/admin/assets/js/chart/knob/knob.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/chart/knob/knob-chart.js')}}"></script>
<script src="{{asset('front/admin/assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('front/admin/assets/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('front/admin/assets/js/owlcarousel/owl.carousel.js')}}"></script>
<script src="{{asset('front/admin/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('front/admin/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('front/admin/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('front/admin/assets/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/chat-menu.js')}}"></script>
<script src="{{asset('front/admin/assets/js/general-widget.js')}}"></script>
<script src="{{asset('front/admin/assets/js/height-equal.js')}}"></script>
<script src="{{asset('front/admin/assets/js/tooltip-init.js')}}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('front/admin/assets/js/script.js')}}"></script>
<!-- Plugin used-->
<script src="{{asset('front/admin/assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('front/admin/assets/js/datatable/datatables/datatable.custom.js')}}"></script>
<script src="{{asset('front/admin/assets/js/jquery.printPage.js') }}"></script>

@yield('extra-js')
<script>
        $('#advance-1').dataTable({
        "language": {
            "sProcessing": "{{__('admin.loading')}}...",
            "sLengthMenu": "{{__('admin.show')}} _MENU_ {{__('admin.entries')}}",
            "sZeroRecords": "{{__('admin.no-entries')}}",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sSearch": "{{__('admin.search')}}:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "{{__('admin.first')}}",
                "sPrevious": "{{__('admin.previous')}}",
                "sNext": "{{__('admin.next')}}",
                "sLast": "{{__('admin.last')}}"
            }
        }
    });

</script>
</body>
</html>
