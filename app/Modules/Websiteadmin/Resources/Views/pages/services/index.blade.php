@extends('websiteadmin::layouts.app')
@section('content')
<style>
    .btn {
        padding: 0 1rem !important;
    }
    .btn-lg {
        font-size: 16px !important;
        margin-right: 4px !important;
        border-radius: 0px !important;
    }
</style>


    <div class="page-body">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.dashboard')}} </a></li>
                            <li class="breadcrumb-item active">{{__('admin.all')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(Session::has('success'))

        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
            <p></p>
            <hr>
            <p class="mb-0">{{Session::get('success')}}</p>
        </div>
        @endif
        <div class="container-fluid">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display dataTable" id="advance-1">
                            <thead>

                            <tr>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.img')}}</th>
                                <th>{{__('admin.title')}}</th>
                                <th>{{__('admin.desc')}}</th>
                                <th>{{__('admin.created_at')}}</th>
                                <th>{{__('admin.more')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($services as $service)
                                    <tr>
                                        <td>{{$service->id}}</td>
                                        <td><img src="{{$service->image}}" style="width:85px; height: 85px"></td>

                                        <td>{{$service->name}}</td>
                                        <td>{{$service->websiteDepartment?$service->websiteDepartment->desc:''}}</td>
                                        <td>{{$service->created_at}}</td>



                                    <td>
                                        <div class="row">
                                        <a href="{{route('services.edit',$service->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                        </div>
                                    </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->

@endsection
