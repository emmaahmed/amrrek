@extends('websiteadmin::layouts.app')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.home')}} </a></li>
{{--                                <li class="breadcrumb-item active"><a href="{{route('slider.index')}}">  {{__('admin.all')}} </a> </li>--}}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif
            @if(Session::has('success'))

                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
                    <p></p>
                    <hr>
                    <p class="mb-0">{{Session::get('success')}}</p>
                </div>
            @endif

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h5>{{__('admin.services')}}</h5><span> <code>{{$service->name}}</code> </span>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-material nav-secondary nav-left" id="danger-tab" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" onclick="openCity('home','profile')" id="danger-home-tab" data-toggle="tab" href="tab-material.html#danger-home" role="tab" aria-controls="danger-home" aria-selected="true" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.arabic')}}</a>
                                <div class="material-border"></div>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="profile-danger-tab" onclick="openCity('profile','home')" data-toggle="tab" href="tab-material.html#danger-profile" role="tab" aria-controls="danger-profile" aria-selected="false" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.english')}}</a>
                                <div class="material-border"></div>
                            </li>
                        </ul>

                        <div class="tab-content" id="danger-tabContent">
                            <form  action="{{route('services.update',$service->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="update" value="1">

                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="danger-home-tab">


                                    <!-- Form Name -->
                                    <!-- Text input-->

                                    <!-- Text input-->
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="brief_desc_ar">{{__('admin.brief_desc_ar')}}</label>
                                            <textarea id="brief_desc_ar" required  name="brief_desc_ar"   class="form-control" rows="3">{{$service->websiteDepartment?$service->websiteDepartment->brief_desc_ar:''}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="desc_ar">{{__('admin.desc_ar')}}</label>
                                            <textarea id="desc_ar" required  name="desc_ar"   class="form-control" rows="3">{{$service->websiteDepartment?$service->websiteDepartment->desc_ar:''}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="offer-text-ar">{{__('admin.offer-text-ar')}}</label>
                                             <input id="offer-text-ar" required name="offer_text_ar" value="{{$service->websiteDepartment?$service->websiteDepartment->offer_text_ar:''}}" type="text" class="form-control btn-square input-md">

                                        </div>

                                        <!-- Text input-->
                                    </div>


                                </div>


                                <div class="tab-pane fade  " id="profile" role="tabpanel" aria-labelledby="profile-danger-tab">

                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="desc_en">{{__('admin.brief_desc_en')}}</label>
                                            <textarea id="brief_desc_en" required  name="brief_desc_en"   class="form-control" rows="3">{{$service->websiteDepartment?$service->websiteDepartment->brief_desc_en:''}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="desc_en">{{__('admin.desc_en')}}</label>
                                            <textarea id="desc_en" required  name="desc_en"   class="form-control" rows="3">{{$service->websiteDepartment?$service->websiteDepartment->desc_en:''}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>

                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="offer-text-ar">{{__('admin.offer-text-en')}}</label>
                                             <input id="offer-text-en" required name="offer_text_en" value="{{$service->websiteDepartment?$service->websiteDepartment->offer_text_en:''}}" type="text" class="form-control btn-square input-md">


                                        </div>

                                        <!-- Text input-->
                                    </div>



                                </div>

                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="offer-text-ar">{{__('admin.offer_discount')}}</label>
                                             <input id="offer_discount" required name="offer_discount" value="{{$service->websiteDepartment?$service->websiteDepartment->offer_discount:''}}" type="text" class="form-control btn-square input-md">


                                        </div>

                                        <!-- Text input-->
                                    </div>


                                <div class="form-group row">
                                    <img style="    height: 150px;" src="{{$service->websiteDepartment?$service->websiteDepartment->ad_img:''}}">

                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="ad_img">{{__('admin.ad_img')}} </label>
                                        <input id="ad_img"  name="ad_img"  class="input-file" type="file">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <img style="    height: 150px;" src="{{$service->websiteDepartment?$service->websiteDepartment->about_img:''}}">

                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="about_img">{{__('admin.about_img')}} </label>
                                        <input id="about_img"  name="about_img"  class="input-file" type="file">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <img style="    height: 150px;" src="{{$service->websiteDepartment?$service->websiteDepartment->service_details_img:''}}">

                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="service_details_img">{{__('admin.service_details_img')}} </label>
                                        <input id="service_details_img"  name="service_details_img"  class="input-file" type="file">
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-success btn-lg active" value="{{__('admin.update')}}">

                            </form>
                        </div>

                    </div>
                </div>



            </div>
            <!-- Container-fluid Ends-->
        </div>

        <!-- Modal -->
    </div>
@endsection
@section('extra-js')
    <script>
        function openCity(id,id1) {
            $('#'+id).addClass('active');
            $('#'+id).addClass('show');
            $('#'+id1).removeClass('active');
            $('#'+id1).removeClass('show');
            console.log('#'+id);
        }


    </script>

@endsection
