@extends('websiteadmin::layouts.app')
@section('content')
    <style>
        .btn {
            padding: 0 1rem !important;
        }
        .btn-lg {
            font-size: 16px !important;
            margin-right: 4px !important;
            border-radius: 0px !important;
        }
    </style>


    <div class="page-body">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.dashboard')}} </a></li>
                            <li class="breadcrumb-item active">{{__('admin.all')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(Session::has('success'))

            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
                <p></p>
                <hr>
                <p class="mb-0">{{Session::get('success')}}</p>
            </div>
        @endif
        <div class="container-fluid">

            <div class="card">
                <div class="card-body">
                    <li class="table-responsive">
                        <table class="display dataTable" id="advance-1">
                            <thead>

                            <tr>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.question')}}</th>
                                <th>{{__('admin.answer')}}</th>
                                <th>{{__('admin.category')}}</th>
                                <th>{{__('admin.created_at')}}</th>
                                <th>{{__('admin.more')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faqs as $faq)
                                <tr>
                                    <td>{{$faq->id}}</td>
                                    <td>{{$faq->question}}</td>
                                    <td>{{$faq->answer}}</td>
                                    <td>{{$faq->category?$faq->category->name:''}}</td>

                                    <td>{{$faq->created_at}}</td>



                                    <td>
                                        <ul style="display: flex">
                                            <li>
                                                <a href="{{route('faqs.edit',$faq->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                            </li>
                                            <li>

                                                <button title="حذف" type="button" class="btn btn-success btn-lg active" data-toggle="modal"

                                                        data-target="#delete_{{$faq->id}}">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </li>
                                        </ul>

                                    </td>
                                </tr>
                                <div class="modal animated fadeIn" id="delete_{{$faq->id}}" tabindex="-1"
                                     style="text-align: right"
                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header btn-danger">
                                                <h5 class="modal-title" id="exampleModalLabel">حذف الشاشة الترحيبية</h5>
                                                {{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                                {{--                                                <span aria-hidden="true">&times;</span>--}}
                                                {{--                                            </button>--}}
                                            </div>
                                            <form method="post" action="{{route('faqs.destroy',$faq->id)}}" class="buttons">
                                                {{csrf_field()}}
                                                @method('DELETE')
                                                <div class="modal-body">
                                                    <h4>هل انت متأكد ؟</h4>
                                                    <h6>
                                                        انت علي وشك الحذف

                                                    </h6>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="hidden" name="model_id" value="{{$faq->id}}">
                                                    <button class="btn btn-dark" type="button" data-dismiss="modal">
                                                        إغلاق
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">تأكيد</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->

@endsection
