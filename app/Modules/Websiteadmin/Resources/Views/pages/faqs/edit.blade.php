@extends('websiteadmin::layouts.app')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.home')}} </a></li>
{{--                                <li class="breadcrumb-item active"><a href="{{route('slider.index')}}">  {{__('admin.all')}} </a> </li>--}}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif
            @if(Session::has('success'))

                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
                    <p></p>
                    <hr>
                    <p class="mb-0">{{Session::get('success')}}</p>
                </div>
            @endif

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h5>{{__('admin.faq-category')}}</h5><span> <code>{{$faq->question}}</code> </span>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-material nav-secondary nav-left" id="danger-tab" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" onclick="openCity('home','profile')" id="danger-home-tab" data-toggle="tab" href="tab-material.html#danger-home" role="tab" aria-controls="danger-home" aria-selected="true" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.arabic')}}</a>
                                <div class="material-border"></div>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="profile-danger-tab" onclick="openCity('profile','home')" data-toggle="tab" href="tab-material.html#danger-profile" role="tab" aria-controls="danger-profile" aria-selected="false" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.english')}}</a>
                                <div class="material-border"></div>
                            </li>
                        </ul>

                        <div class="tab-content" id="danger-tabContent">
                            <form  action="{{route('faqs.update',$faq->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="update" value="1">

                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="danger-home-tab">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="question_en">{{__('admin.question_en')}}</label>

                                            <input id="question_en" required name="question_en" value="{{$faq->question_en}}" type="text" class="form-control btn-square input-md">

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="answer_en">{{__('admin.answer_en')}}</label>
                                            <textarea id="answer_en" required  name="answer_en"   class="form-control" rows="3">{{$faq->answer_en}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>


                                </div>


                                <div class="tab-pane fade  " id="profile" role="tabpanel" aria-labelledby="profile-danger-tab">

                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="question_ar">{{__('admin.question_ar')}}</label>

                                            <input id="question_ar" required name="question_ar" value="{{$faq->question_ar}}" type="text" class="form-control btn-square input-md">

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="answer_ar">{{__('admin.answer_ar')}}</label>
                                            <textarea id="answer_ar" required  name="answer_ar"   class="form-control" rows="3">{{$faq->answer_ar}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>




                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-left" for="faq_category_id">{{__('admin.choose-category')}}</label>

                                        <select id="faq_category_id" name="faq_category_id" class="form-control btn-square">
                                            <option label="{{__('admin.choose-category')}}"></option>
                                            @foreach($faqsCategories as $faqCategory)
                                                <option value="{{$faqCategory->id}}" @if($faqCategory->id==$faq->faq_category_id) selected @endif >{{$faqCategory->name}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <!-- Select Basic -->
                                </div>



                                <input type="submit" class="btn btn-success btn-lg active" value="{{__('admin.update')}}">

                            </form>
                        </div>

                    </div>
                </div>



            </div>
            <!-- Container-fluid Ends-->
        </div>

        <!-- Modal -->
    </div>
@endsection
@section('extra-js')
    <script>
        function openCity(id,id1) {
            $('#'+id).addClass('active');
            $('#'+id).addClass('show');
            $('#'+id1).removeClass('active');
            $('#'+id1).removeClass('show');
            console.log('#'+id);
        }


    </script>

@endsection
