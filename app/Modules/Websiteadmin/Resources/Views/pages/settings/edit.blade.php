@extends('websiteadmin::layouts.app')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.home')}} </a></li>
{{--                                <li class="breadcrumb-item active"><a href="{{route('slider.index')}}">  {{__('admin.all')}} </a> </li>--}}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif
            @if(Session::has('success'))

                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
                    <p></p>
                    <hr>
                    <p class="mb-0">{{Session::get('success')}}</p>
                </div>
            @endif

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h5>{{__('admin.settings')}}</h5><span> <code>{{$settings->title}}</code> </span>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-material nav-secondary nav-left" id="danger-tab" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" onclick="openCity('home','profile')" id="danger-home-tab" data-toggle="tab" href="tab-material.html#danger-home" role="tab" aria-controls="danger-home" aria-selected="true" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.arabic')}}</a>
                                <div class="material-border"></div>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="profile-danger-tab" onclick="openCity('profile','home')" data-toggle="tab" href="tab-material.html#danger-profile" role="tab" aria-controls="danger-profile" aria-selected="false" data-original-title="" title=""><i class="icofont icofont-throne"></i>{{__('admin.english')}}</a>
                                <div class="material-border"></div>
                            </li>
                        </ul>

                        <div class="tab-content" id="danger-tabContent">
                            <form  action="{{route('website-settings.update',$settings->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="update" value="1">

                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="danger-home-tab">


                                    <!-- Form Name -->
                                    <!-- Text input-->
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="address1_ar">{{__('admin.address1_ar')}}</label>

                                            <input id="address1_ar" required name="address1_ar" value="{{$settings->address1_ar	}}" type="text" class="form-control btn-square input-md">

                                        </div>

                                        <!-- Text input-->
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="address2_ar">{{__('admin.address2_ar')}}</label>
                                            <textarea id="address2_ar" required  name="address2_ar"   class="form-control" rows="3">{{$settings->address2_ar}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="footer_desc_ar">{{__('admin.footer_desc_ar')}}</label>
                                            <textarea id="footer_desc_ar" required  name="footer_desc_ar"   class="form-control" rows="3">{{$settings->footer_desc_ar}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>


                                </div>


                                <div class="tab-pane fade  " id="profile" role="tabpanel" aria-labelledby="profile-danger-tab">

                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="address1_ar">{{__('admin.address1_en')}}</label>

                                            <input id="address1_en" required name="address1_en" value="{{$settings->address1_en	}}" type="text" class="form-control btn-square input-md">

                                        </div>

                                        <!-- Text input-->
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="address2_en">{{__('admin.address2_en')}}</label>
                                            <textarea id="address2_en" required  name="address2_en"   class="form-control" rows="3">{{$settings->address2_en}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-lg-6">
                                            <label class="col-lg-12 control-label text-lg-right" for="footer_desc_en">{{__('admin.footer_desc_en')}}</label>
                                            <textarea id="footer_desc_en" required  name="footer_desc_en"   class="form-control" rows="3">{{$settings->footer_desc_en}}</textarea>

                                        </div>

                                        <!-- Text input-->
                                    </div>




                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="app_store_link">{{__('admin.app_store_link')}}</label>

                                        <input id="app_store_link" required name="app_store_link" value="{{$settings->app_store_link}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="google_store_link">{{__('admin.google_store_link')}}</label>

                                        <input id="google_store_link" required name="google_store_link" value="{{$settings->google_store_link}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="google_store_link">{{__('admin.phone1')}}</label>

                                        <input id="phone1" required name="phone1" value="{{$settings->phone1}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="phone2">{{__('admin.phone2')}}</label>

                                        <input id="phone2" required name="phone2" value="{{$settings->phone2}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="email1">{{__('admin.email1')}}</label>

                                        <input id="email1" required name="email1" value="{{$settings->email1}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="email2">{{__('admin.email2')}}</label>

                                        <input id="email2" required name="email2" value="{{$settings->email2}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="facebook">{{__('admin.facebook')}}</label>

                                        <input id="facebook" required name="facebook" value="{{$settings->facebook}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="twitter">{{__('admin.twitter')}}</label>

                                        <input id="twitter" required name="twitter" value="{{$settings->twitter}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="twitter">{{__('admin.youtube')}}</label>

                                        <input id="linkedin" required name="linkedin" value="{{$settings->linkedin}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right" for="twitter">{{__('admin.instagram')}}</label>

                                        <input id="instagram" required name="instagram" value="{{$settings->instagram}}" type="text" class="form-control btn-square input-md">

                                    </div>

                                    <!-- Text input-->
                                </div>



                                <input type="submit" class="btn btn-success btn-lg active" value="{{__('admin.update')}}">

                            </form>
                        </div>

                    </div>
                </div>



            </div>
            <!-- Container-fluid Ends-->
        </div>

        <!-- Modal -->
    </div>
@endsection
@section('extra-js')
    <script>
        function openCity(id,id1) {
            $('#'+id).addClass('active');
            $('#'+id).addClass('show');
            $('#'+id1).removeClass('active');
            $('#'+id1).removeClass('show');
            console.log('#'+id);
        }


    </script>

@endsection
