@extends('websiteadmin::layouts.app')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.home')}} </a></li>
{{--                                <li class="breadcrumb-item active"><a href="{{route('slider.index')}}">  {{__('admin.all')}} </a> </li>--}}
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif
            @if(Session::has('success'))

                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
                    <p></p>
                    <hr>
                    <p class="mb-0">{{Session::get('success')}}</p>
                </div>
            @endif

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h5>{{__('admin.slider-section')}}</h5><span> <code>{{$underSliderSection->title}}</code> </span>
                    </div>
                    <div class="card-body">

                        <div class="tab-content" id="danger-tabContent">
                            <form  action="{{route('under-slider.update',$underSliderSection->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="update" value="1">



                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img1')}} </label>
                                        <input id="filebutton1"  name="img1"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img1)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img2')}} </label>
                                        <input id="filebutton1"  name="img2"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img2)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img3')}} </label>
                                        <input id="filebutton1"  name="img3"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img3)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img4')}} </label>
                                        <input id="filebutton1"  name="img4"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img4)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img5')}} </label>
                                        <input id="filebutton1"  name="img5"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img5)}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label class="col-lg-12 control-label text-lg-right"
                                               for="filebutton1">{{__('admin.img6')}} </label>
                                        <input id="filebutton1"  name="img6"  class="input-file" type="file">
                                        <img style="    height: 150px;" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img6)}}">
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-success btn-lg active" value="{{__('admin.update')}}">

                            </form>
                        </div>

                    </div>
                </div>



            </div>
            <!-- Container-fluid Ends-->
        </div>

        <!-- Modal -->
    </div>
@endsection
@section('extra-js')
    <script>
        function openCity(id,id1) {
            $('#'+id).addClass('active');
            $('#'+id).addClass('show');
            $('#'+id1).removeClass('active');
            $('#'+id1).removeClass('show');
            console.log('#'+id);
        }


    </script>

@endsection
