@extends('websiteadmin::layouts.app')
@section('content')
<style>
    .btn {
        padding: 0 1rem !important;
    }
    .btn-lg {
        font-size: 16px !important;
        margin-right: 4px !important;
        border-radius: 0px !important;
    }
</style>


    <div class="page-body">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('website-admin.dashboard')}}"><i data-feather="home"> </i> {{__('admin.dashboard')}} </a></li>
                            <li class="breadcrumb-item active">{{__('admin.all')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(Session::has('success'))

        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{__('admin.done-successfully')}}</h4>
            <p></p>
            <hr>
            <p class="mb-0">{{Session::get('success')}}</p>
        </div>
        @endif
        <div class="container-fluid">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display dataTable" id="advance-1">
                            <thead>

                            <tr>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.email')}}</th>
                                <th>{{__('admin.created_at')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($emails as $email)
                                    <tr>
                                        <td>{{$email->id}}</td>

                                        <td>{{$email->email}}</td>
                                        <td>{{$email->created_at}}</td>



                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->

@endsection
