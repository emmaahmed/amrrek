@extends('websiteadmin::layouts.login-app')
@section('content')

    <div class="login-11">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-12 col-pad-0 bg-color-11">
                    <div class="form-section">
                        <div class="logo">
                            <a >
                                <img src="{{asset('assets-login/img/logos/logo.png')}}" alt="logo">
                            </a>
                        </div>
                        <h3>تسجيل الدخول إلي لوحة تحكم الموقع</h3>
                        <div class="text-center" style="color: red;">
                            <strong>{{Session::get('error')}}</strong>
                        </div>

                        <div class="login-inner-form">
                            <form action="{{route('website-admin.login')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group clearfix">
                                    <label>البريد الإلكتروني</label>
                                    <div class="form-box">
                                        <input type="email" name="email" class="input-text" placeholder="البريد الإلكتروني">
                                        <i class="flaticon-mail-2"></i>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label>كلمة المرور</label>
                                    <div class="form-box">
                                        <input type="password" name="password" class="input-text" placeholder="كلمة المرور">
                                        <i class="flaticon-password"></i>
                                    </div>
                                </div>
                                <div class="checkbox clearfix">
                                    <div class="form-check checkbox-theme">
                                        <input class="form-check-input" type="checkbox" value="" id="rememberMe">
                                        <label class="form-check-label" for="rememberMe">
                                            تذكر كلمة المرور
                                        </label>
                                    </div>
                                    <a href="{{route('password.forget')}}"> <label class="form-check-label">
                                            نسيت كلمة المرور؟
                                        </label></a>


                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn-md btn-theme btn-block">تسجيل الدخول</button>
                                </div>
                            </form>


                        </div>

                    </div>
                </div>
                <div class="col-xl-8 col-lg-7 col-md-12 col-pad-0 bg-img bg-img2 none-992">

                    <div class="info wow fadeInLeft" data-wow-duration="2s">
                        <h1 class="black"><span>مرحبا بك في </span> أمرك</h1>
                        <p>أمرك هي منصة التوصيل العملاقة في المملكة. أمرك هي تجربة فريدة عند الطلب تحصل على أعلى تقييمات المستخدمين بين جميع تطبيقات التوصيل الأخرى. إنه ليس الأول ولكنه يعتبر أفضل تطبيق يقدم كل ما يمكنك تخيله من جميع أنواع المتاجر والمطاعم ويغطي جميع مجالات الأعمال في المملكة العربية السعودية ودول مجلس التعاون الخليجي</p>
                        <img src="{{asset('assets-login/img/mockup.png')}}" class="img-fluid  mockup">
                    </div>
                    <div id="particles-js">
                        <div class="bg-photo">
                            <img src="{{asset('assets-login/img/bg-image-51.png')}}" alt="bg" class="img-fluid wow fadeInUp " data-wow-duration="2s">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
