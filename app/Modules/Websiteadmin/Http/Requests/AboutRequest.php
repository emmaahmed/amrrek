<?php

namespace App\Modules\Websiteadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ar'=>'required',
            'desc_ar'=>'required',
            'button1_ar'=>'required',
            'title_en'=>'required',
            'desc_en'=>'required',
            'button1_en'=>'required',
            'button1_link'=>'required',
            'img1'=>'',
            'img2'=>'',
        ];
    }
    public function messages()
    {
        return[
            'title_ar.required'=>__('admin.title_ar-required'),
            'title_en.required'=>__('admin.title_en-required'),
            'desc_ar.required'=>__('admin.desc_ar-required'),
            'desc_en.required'=>__('admin.desc_en-required'),
            'button1_en.required'=>__('admin.button1_en-required'),
            'button1_ar.required'=>__('admin.button1_ar-required'),
            'button1_link.required'=>__('admin.button1_link-required'),
        ];

    }

}
