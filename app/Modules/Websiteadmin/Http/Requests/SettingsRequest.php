<?php

namespace App\Modules\Websiteadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address1_ar'=>'required',
            'address2_ar'=>'required',
            'footer_desc_ar'=>'required',
            'address1_en'=>'required',
            'address2_en'=>'required',
            'footer_desc_en'=>'required',
            'app_store_link'=>'required',
            'google_store_link'=>'required',
            'phone1'=>'required',
            'phone2'=>'required',
            'email1'=>'required',
            'email2'=>'required',
            'facebook'=>'required',
            'twitter'=>'required',
            'linkedin'=>'required',
            'instagram'=>'required',

        ];
    }
    public function messages()
    {
        return[
            'address1_ar.required'=>__('admin.address1_ar-required'),
            'address2_ar.required'=>__('admin.address2_ar-required'),
            'footer_desc_ar.required'=>__('admin.footer_desc_ar-required'),
            'address1_en.required'=>__('admin.address1_en-required'),
            'address2_en.required'=>__('admin.address2_en-required'),
            'footer_desc_en.required'=>__('admin.footer_desc_en-required'),
            'app_store_link.required'=>__('admin.app_store_link-required'),
            'google_store_link.required'=>__('admin.google_store_link-required'),
            'phone1.required'=>__('admin.phone1-required'),
            'phone2.required'=>__('admin.phone2-required'),
            'email1.required'=>__('admin.email1-required'),
            'email2.required'=>__('admin.email2-required'),
            'facebook.required'=>__('admin.facebook-required'),
            'twitter.required'=>__('admin.twitter-required'),
            'linkedin.required'=>__('admin.linkedin-required'),
            'instagram.required'=>__('admin.instagram-required'),
        ];

    }

}
