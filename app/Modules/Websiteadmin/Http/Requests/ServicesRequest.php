<?php

namespace App\Modules\Websiteadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->update==1) {
            return [
                'brief_desc_ar' => 'required',
                'desc_ar' => 'required',
                'brief_desc_en' => 'required',
                'desc_en' => 'required',
                 'offer_text_en'=>'required',
                 'offer_text_ar'=>'required',
                 'offer_discount'=>'required',

                'ad_img' => '',
                'about_img' => '',
                'service_details_img' => '',
            ];
        }else{
            return [
                'brief_desc_ar' => 'required',
                'desc_ar' => 'required',
                'brief_desc_en' => 'required',
                                 'offer_text_en'=>'required',
                 'offer_text_ar'=>'required',
                 'offer_discount'=>'required',

                'desc_en' => 'required',
                'ad_img' => 'required',
                'about_img' => 'required',
                'service_details_img' => 'required',
            ];

        }
    }
    public function messages()
    {
        return[
            'brief_desc_ar.required'=>__('admin.brief_desc_ar-required'),
            'desc_en.required'=>__('admin.desc_en-required'),
            'desc_ar.required'=>__('admin.desc_ar-required'),
            'offer_text_ar.required'=>__('admin.offer_text_ar-required'),
            'offer_text_en.required'=>__('admin.offer_text_en-required'),
            'offer_discount.required'=>__('admin.offer_discount-required'),
            
            'brief_desc_en.required'=>__('admin.brief_desc_en-required'),
            'ad_img.required'=>__('admin.ad_img-required'),
            'about_img.required'=>__('admin.about_img-required'),
            'service_details_img.required'=>__('admin.service_details_img-required'),
        ];

    }

}
