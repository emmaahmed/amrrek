<?php

namespace App\Modules\Websiteadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_ar'=>'required',
            'question_en'=>'required',
            'answer_ar'=>'required',
            'answer_en'=>'required',
            'faq_category_id'=>'required',

        ];
    }
    public function messages()
    {
        return[
            'question_ar.required'=>__('admin.question_ar-required'),
            'question_en.required'=>__('admin.question_en-required'),
            'answer_ar.required'=>__('admin.answer_ar-required'),
            'answer_en.required'=>__('admin.answer_en-required'),
            'faq_category_id.required'=>__('admin.faq_category_id-required'),
        ];

    }

}
