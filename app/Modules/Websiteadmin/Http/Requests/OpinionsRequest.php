<?php

namespace App\Modules\Websiteadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OpinionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar'=>'required',
            'desc_ar'=>'required',
            'name_en'=>'required',
            'desc_en'=>'required',
            'img'=>'',

        ];
    }
    public function messages()
    {
        return[
            'name_ar.required'=>__('admin.name_ar-required'),
            'name_en.required'=>__('admin.name_en-required'),
            'desc_ar.required'=>__('admin.desc_ar-required'),
            'desc_en.required'=>__('admin.desc_en-required'),
        ];

    }

}
