<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\Section;
use App\Modules\Websiteadmin\Http\Requests\HowToUseRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class HowToUseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $howToUseSection=Section::whereId(6)->first();
        return view('websiteadmin::pages.how-to-use.edit',compact('howToUseSection'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HowToUseRequest $request, $id)
    {
        $howToUseSection=Section::whereId($id)->first();
        $howToUseSection->update($request->validated());
        if($request->img1){
            $img_name = time().uniqid().'.jpg';
            $request->img1->move(public_path('front/uploads/how-to-use/'),$img_name);

            $howToUseSection->img1=$img_name;
            $howToUseSection->save();

        }
        return redirect()->back()->with('success','Edited');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
