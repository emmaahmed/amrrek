<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\Section;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class UnderSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $underSliderSection=Section::whereId(2)->first();

        return view('websiteadmin::pages.under-slider-images.edit',compact('underSliderSection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $underSliderSection=Section::whereId($id)->first();
        if($request->img1){
            $img_name = time().uniqid().'.jpg';
            $request->img1->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img1=$img_name;

        }
        if($request->img2){
            $img_name = time().uniqid().'.jpg';
            $request->img2->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img2=$img_name;
        }
        if($request->img3){
            $img_name = time().uniqid().'.jpg';
            $request->img3->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img3=$img_name;
        }
        if($request->img4){
            $img_name = time().uniqid().'.jpg';
            $request->img4->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img4=$img_name;
        }
        if($request->img5){
            $img_name = time().uniqid().'.jpg';
            $request->img5->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img5=$img_name;
        }
        if($request->img6){
            $img_name = time().uniqid().'.jpg';
            $request->img6->move(public_path('front/uploads/under-slider/'),$img_name);

            $underSliderSection->img6=$img_name;
        }
        $underSliderSection->save();
        return redirect()->back()->with('success','Edited');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
