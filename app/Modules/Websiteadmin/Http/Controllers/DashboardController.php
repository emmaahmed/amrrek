<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function loginPage(){
        return view('websiteadmin::login.login');
    }
    public function login(Request $request){

        $credentials = array(
            'email' => $request->email,
            'password' =>$request->password
        );

        if (Auth::guard('website-admin')->attempt($credentials)) {
            if(Auth::guard('website-admin')->user()->type==5)
            return redirect()->route('website-admin.dashboard');
            else
                return redirect()->route('website-admin.login-page')->with('error','Email Or Password not correct');

        }
        else{

            return redirect()->route('website-admin.login-page')->with('error','Email Or Password not correct');

        }





    }
    public function dashboard(){
        $jsonurlryad = "https://api.openweathermap.org/data/2.5/weather?q=Riyadh&appid=c1631dca74fa5dc3468b78c9995a7b3a";
        $jsonurljadah = "https://api.openweathermap.org/data/2.5/weather?q=Medina &appid=c1631dca74fa5dc3468b78c9995a7b3a";
        $jsonurlmakka = "https://api.openweathermap.org/data/2.5/weather?q=Jeddah&appid=c1631dca74fa5dc3468b78c9995a7b3a";

        $jsonryad = file_get_contents($jsonurlryad);
        $jsonrjadah = file_get_contents($jsonurljadah);
        $jsonrmakka = file_get_contents($jsonurlmakka);

        $weatherryad = json_decode($jsonryad,TRUE);
        $kelvinryad = $weatherryad['main']['temp'];
        $celciusryad = $kelvinryad - 273.15;

        $weatherjadah = json_decode($jsonrjadah,TRUE);
        $kelvinjadah = $weatherjadah['main']['temp'];
        $celciusjadah = $kelvinjadah - 273.15;

        $weathermakka = json_decode($jsonrmakka,TRUE);
        $kelvinmakka = $weathermakka['main']['temp'];
        $celciusmakka = $kelvinmakka - 273.15;
        $contacts5=ContactUs::where('type',5)->orderBy('created_at')->take(5)->get();
        return view('websiteadmin::pages.index',compact('celciusryad','celciusjadah','celciusmakka',
          'contacts5'

        ));


    }
    public function logout(){
        Auth::logout();
        return redirect()->route('website-admin.login-page');

    }
        public function ChangeLang($lang){

         app()->setLocale($lang);  
        //  session()->put('locale',$lang);
        //           $locale = session()->get('locale');

            session(['locale' => $lang]);

        //  dd(app()->getLocale());
         return redirect()->back();


    }

}
