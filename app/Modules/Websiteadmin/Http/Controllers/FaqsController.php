<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\Faq;
use App\Modules\Website\Models\FaqCategory;
use App\Modules\Websiteadmin\Http\Requests\FaqsRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs=Faq::with(['category'=>function($query){
           $query->select('id','name_'.app()->getLocale().' as name');
    }])->get();
        return view('websiteadmin::pages.faqs.index',compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faqsCategories=FaqCategory::select('id','name_'.app()->getLocale().' as name')->get();
        return view('websiteadmin::pages.faqs.create',compact('faqsCategories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqsRequest $request)
    {
//        return $request->validated();
        Faq::create($request->validated());
        return redirect()->route('faqs.index')->with('success','Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq=Faq::whereId($id)->first();
        $faqsCategories=FaqCategory::select('id','name_'.app()->getLocale().' as name')->get();

        return view('websiteadmin::pages.faqs.edit',compact('faq','faqsCategories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqsRequest $request, $id)
    {
        $faq=Faq::whereId($id)->first();
//        return$request->validated();
        $faq->update($request->validated());
        return redirect()->route('faqs.index')->with('success','Edited');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq=Faq::whereId($id)->first();
        $faq->delete();
        return redirect()->route('faqs.index')->with('success','Deleted');

    }
}
