<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\Opinion;
use App\Modules\Websiteadmin\Http\Requests\OpinionsRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class OpinionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opinions=Opinion::all();
        return view('websiteadmin::pages.opinions.index',compact('opinions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('websiteadmin::pages.opinions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpinionsRequest $request)
    {
        Opinion::create($request->validated());
        return redirect()->back()->with('success','Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opinion=Opinion::whereId($id)->first();
        return view('websiteadmin::pages.opinions.edit',compact('opinion'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OpinionsRequest $request, $id)
    {
        $opinion=Opinion::whereId($id)->first();
        $opinion->update($request->validated());
        if($request->img)
        {
            $opinion->img=$request->img;
            $opinion->save();
        }
        return redirect()->route('opinions.index')->with('success','Edited');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $opinion=Opinion::whereId($id)->first();
        // dd($opinion);
        $opinion->delete();
        return redirect()->route('opinions.index')->with('success','Deleted');

    }
}
