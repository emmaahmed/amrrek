<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\FaqCategory;
use App\Modules\Websiteadmin\Http\Requests\FaqCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class FaqsCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqCategories=FaqCategory::select('id','name_'.app()->getLocale().' as name')->get();
        return view('websiteadmin::pages.faq-categories.index',compact('faqCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('websiteadmin::pages.faq-categories.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqCategoryRequest $request)
    {
        FaqCategory::create($request->validated());
        return redirect()->route('faq-category.index')->with('success','Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faqCategory=FaqCategory::whereId($id)->first();
        return view('websiteadmin::pages.faq-categories.edit',compact('faqCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqCategoryRequest $request, $id)
    {
        $faqCategory=FaqCategory::whereId($id)->first();
        $faqCategory->update($request->validated());
        return redirect()->route('faq-category.index')->with('success','Added');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faqCategory=FaqCategory::whereId($id)->first();
        $faqCategory->delete();
        return redirect()->route('faq-category.index')->with('success','Added');

    }
}
