<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Modules\Website\Models\Section;
use App\Modules\Websiteadmin\Http\Requests\AboutRequest;
use App\Modules\Websiteadmin\Http\Requests\BriefAboutRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutSection=Section::whereId(3)->first();
        return view('websiteadmin::pages.about.edit',compact('aboutSection'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutRequest $request, $id)
    {
        $aboutSection=Section::whereId($id)->first();
        $aboutSection->update($request->validated());
        if($request->img1){
            $img_name = time().uniqid().'.jpg';
            $request->img1->move(public_path('front/uploads/about/'),$img_name);

            $aboutSection->img1=$img_name;


        }
        if($request->img2){
            $img_name = time().uniqid().'.jpg';
            $request->img2->move(public_path('front/uploads/about/'),$img_name);

            $aboutSection->img2=$img_name;


        }
        $aboutSection->save();
        return redirect()->back()->with('success','Edited');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function briefAbout(){
        $aboutPage=Section::whereId(8)->first();
        return view('websiteadmin::pages.brief-about.edit',compact('aboutPage'));


    }
    public function updateBriefAbout(BriefAboutRequest $request,$id){

        $aboutPage=Section::whereId($id)->first();
        $aboutPage->update($request->validated());
        if($request->img1){
            $img_name = time().uniqid().'.jpg';
            $request->img1->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img1=$img_name;

        }
        if($request->img2){
            $img_name = time().uniqid().'.jpg';
            $request->img2->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img2=$img_name;

        }
        if($request->img3){
            $img_name = time().uniqid().'.jpg';
            $request->img3->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img3=$img_name;

        }
        if($request->img4){
            $img_name = time().uniqid().'.jpg';
            $request->img4->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img4=$img_name;

        }
        if($request->img5){
            $img_name = time().uniqid().'.jpg';
            $request->img5->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img5=$img_name;

        }
        if($request->img6){
            $img_name = time().uniqid().'.jpg';
            $request->img6->move(public_path('front/uploads/sidebar/'),$img_name);

            $aboutPage->img6=$img_name;

        }
        $aboutPage->save();
        return redirect()->back()->with('success','Edited');

    }
}
