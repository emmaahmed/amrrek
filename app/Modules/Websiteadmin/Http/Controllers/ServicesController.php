<?php

namespace App\Modules\Websiteadmin\Http\Controllers;

use App\Models\Department;
use App\Modules\Website\Models\Section;
use App\Modules\Website\Models\WebsiteDepartment;
use App\Modules\Websiteadmin\Http\Requests\ServicesRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services=Department::with('websiteDepartment')->select('id','name_'.app()->getLocale().' as name')->get();
        return view('websiteadmin::pages.services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service=Department::whereId($id)->with('websiteDepartment')->first();
        return view('websiteadmin::pages.services.edit',compact('service'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesRequest $request, $id)
    {
              $websiteService=WebsiteDepartment::where('department_id',$id)->first();
//return $request->validated();
        if($websiteService){
              $websiteService->update($request->validated());
            }
        else{
            WebsiteDepartment::create($request->validated());
        }
        return redirect()->back()->with('success','Edited');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
