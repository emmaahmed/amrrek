<?php

namespace App\Modules\Website\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('website', 'Resources/Lang', 'app'), 'website');
        $this->loadViewsFrom(module_path('website', 'Resources/Views', 'app'), 'website');
        $this->loadMigrationsFrom(module_path('website', 'Database/Migrations', 'app'), 'website');
        $this->loadConfigsFrom(module_path('website', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('website', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
