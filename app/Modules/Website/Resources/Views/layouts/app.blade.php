<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="mainUrl" content="{{Illuminate\Support\Facades\URL::current()}}">

     @yield('about-css')
    <link href="{{asset('front/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    @if(app()->getLocale()=='en')
        <link href="{{asset('front/assets/css/meanmenu.css')}}" rel="stylesheet">

    @else
        <link href="{{asset('front/assets/css/meanmenu-rtl.css')}}" rel="stylesheet">

    @endif


    <link href="{{asset('front/assets/css/boxicons.min.css')}}" rel="stylesheet">

    <link href="{{asset('front/assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/owl.theme.default.min.css')}}" rel="stylesheet">

    <link href="{{asset('front/assets/css/slick.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/slick-theme.min.css')}}" rel="stylesheet">

    <link href="{{asset('front/assets/css/magnific-popup.min.css')}}" rel="stylesheet">
    @if(app()->getLocale()=='en')
    <link href="{{asset('front/assets/css/style.css')}}" rel="stylesheet">
    @else
        <link href="{{asset('front/assets/css/style-rtl.css')}}" rel="stylesheet">

    @endif
    <link href="{{asset('front/assets/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" >
    <title>{{__('website.project-title')}}</title>
    <link href="{{asset('front/assets/img/favicon.png')}}" rel="icon" type="image/png">

</head>
<body>

@include('website::layouts.includes.header')
@yield('sub-header')
@yield('content')

@include('website::layouts.includes.footer')



<script src="{{asset('front/assets/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('front/assets/js/popper.min.js')}}"></script>
<script src="{{asset('front/assets/js/bootstrap.min.js')}}"></script>

<script src="{{asset('front/assets/js/jquery.meanmenu.js')}}"></script>

<script src="{{asset('front/assets/js/owl.carousel.min.js')}}"></script>

<script src="{{asset('front/assets/js/jquery.mixitup.min.js')}}"></script>

<script src="{{asset('front/assets/js/slick.min.js')}}"></script>

<script src="{{asset('front/assets/js/jquery.ajaxchimp.min.js')}}"></script>

<script src="{{asset('front/assets/js/form-validator.min.js')}}"></script>

<script src="{{asset('front/assets/js/contact-form-script.js')}}"></script>

<script src="{{asset('front/assets/js/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('front/assets/js/custom.js')}}"></script>
<script src="{{asset('front/assets/js/wow.min.js')}}"></script>
@yield('extra-js')
<script>


    new WOW().init();

</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){

var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/60a389ab185beb22b30e4987/1f5vd3i2l';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
Tawk_API = Tawk_API || {};
Tawk_API.onLoad = function(){
    // Tawk_API.maximize();
};
</script>
<!--End of Tawk.to Script-->


</body>
</html>