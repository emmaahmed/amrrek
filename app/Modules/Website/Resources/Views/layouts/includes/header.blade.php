<div class="loader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="spinner"></div>
        </div>
    </div>
</div>


<div class="navbar-area fixed-top">

    <div class="mobile-nav">
        <a class="logo" href="{{route('pages.home')}}">
            <img alt="Logo" src="{{asset('front/assets/img/logo-two.svg')}}">
        </a>
    </div>

    <div class="main-nav  @if(empty(request()->segment(1))) main-nav-two @else main-nav-three @endif">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{route('pages.home')}}">
                    @if(empty(request()->segment(1)))
                    <img alt="Logo" class="nav-two-logo-one" src="{{asset('front/assets/img/logo-two.svg')}}">
                    <img alt="Logo" class="nav-two-logo-two" src="{{asset('front/assets/img/logo.svg')}}">
                    @else
                        <img alt="Logo" src="{{asset('front/assets/img/logo.png')}}">
                    @endif
                </a>
                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link  @if(empty(request()->segment(1))) active active-white2 @endif {{request()->segment(1)}} " href="{{route('pages.home')}}">{{__('website.home')}} </a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->segment(1)=='about') active active-white @endif " href="{{route('pages.about')}}">{{__('website.about-amrk')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->segment(1)=='service-details') active active-white @endif " href="#">{{__('website.services')}} <i class='bx bx-chevron-down'></i></a>
                            <ul class="dropdown-menu">
                                @if(count($allServices)>0)
                                    @foreach($allServices as $service)

                                    <li class="nav-item">
                                    <a class="nav-link @if(request()->segment(count(request()->segments()))==$service->slug) active active-white2 @endif nav-link2" href="{{route('pages.service-details',$service->slug)}}">{{$service->name}} </a>
                                </li>
                                    @endforeach
                                    @endif

                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->segment(1)=='stores') active active-white @endif" href="{{route('pages.stores',['category_id'=>'all'])}}">{{__('website.stores')}}</a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link @if(request()->segment(1)=='help') active active-white @endif" href="{{route('pages.help')}}">{{__('website.help')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->segment(1)=='contact-us') active active-white @endif" href="{{route('pages.contact-us')}}">{{__('website.contact-us')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{route('shops.login-page')}}">{{__('website.shop-login')}}</a>
                        </li>
                        <li class="nav-item">
                            @if(app()->getLocale()=='en')
                            <a class="nav-link " href="{{route('change.lang',['langkey'=>'ar','segment'=>request()->segment(1)==''?'home':request()->segment(1)])}}"> عربي <i class="fas fa-globe"></i></a>
                            @else
                            <a class="nav-link " href="{{route('change.lang',['langkey'=>'en','segment'=>request()->segment(1)==''?'home':request()->segment(1)])}}"> English <i class="fas fa-globe"></i></a>
                              @endif
                        </li>
                    </ul>
                    <div class="side-nav">
                        <a class="nav-tel" href="{{route('shop-form')}}" target="_blank">
                            <i class="fas fa-store"></i>
                            {{__('website.be-a-shop')}}
                        </a>
                        <button class="btn modal-btn" data-bs-target="#myModalRight" data-bs-toggle="modal"
                                type="button">
                            <i class='bx bx-menu-alt-right'></i>
                        </button>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>


<div class="modal fade modal-right" id="myModalRight" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img alt="Logo" src="{{asset('front/assets/img/logo.png')}}">
                <button aria-label="Close" class="btn-close" data-bs-dismiss="modal" type="button"></button>
            </div>
            <div class="modal-body">
                <h2>{{$about->title}} </h2>
                <p>{{$about->desc}}</p>
                <div class="image-area">
                    <h2>{{__('website.gallery')}}</h2>
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img1)}}">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img2)}}">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img3)}}">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img4)}}">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img5)}}">
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a href="#">
                                <img alt="Instagram" src="{{asset('front/uploads/sidebar/'.$about->img6)}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="social-area">
                    <h3><span class="">{{__('website.project-title')}}</span> {{__('website.social-contacts')}}</h3>
                    <ul>
                        <li>
                            <a href="{{$settings->facebook}}" target="_blank">
                                <i class='bx bxl-facebook'></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$settings->twitter}}" target="_blank">
                                <i class='bx bxl-twitter'></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$settings->linkedin}}" target="_blank">
                                <i class='bx bxl-youtube'></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$settings->instagram}}" target="_blank">
                                <i class='bx bxl-instagram'></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
