<footer class="footer-area-two pt-100 pb-30 footer-wrap" id="footer">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <div class="footer-logo">
                        <a href="{{route('pages.home')}}">
                            <img alt="Logo" src="{{asset('front/assets/img/logo-two.svg')}}">
                        </a>
                        <p>{{$settings->footer_desc}}</p>
                        <div class="footer-subscribe footer-subscriber-two">
                            <form action="{{route('newsletter.submit')}}" method="post">
                                @csrf
                            <input class="form-control" placeholder="{{__('website.enter-your-email')}}" name="email" type="email">
                            <button class="btn footer-btn" type="submit">
                                <i class='bx bxs-send bx-flashing'></i>
                            </button>
                               @if(Session::has('success'))

                             <div class="h3 text-center " id="msgSubmit" style="padding-top: 17px; color: #fff;">{{__('website.submitted-successfully')}}</div>
                                @endif
                               @if(Session::has('error'))

                             <div class="h3 text-center " id="msgSubmit" style="padding-top: 17px; color: #fff;">{{__('website.already-sent')}}</div>
                                @endif

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @if(count($allServices)>0)
            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <div class="footer-service">
                        <h3>{{__('website.services')}}</h3>
                        <ul>
                            @foreach($allServices as $service)
                            <li>
                                <a href="{{route('pages.service-details',$service->slug)}}">
                                    <i class='bx bx-chevron-right'></i>
                                    {{$service->name}}
                                </a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <div class="footer-service">
                        <h3>{{__('website.quick-links')}}</h3>
                        <ul>
                            <li>

                            </li>
                            <li>
                                <a href="{{route('shop-form')}}">
                                    <i class='bx bx-chevron-right'></i>
                                    {{__('website.be-shop')}}
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class='bx bx-chevron-right'></i>
                                    {{__('website.be-delivery')}}                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class='bx bx-chevron-right'></i>
                                    {{__('website.be-driver')}}
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class='bx bx-chevron-right'></i>
                                    {{__('website.be-heavy-driver')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('pages.terms')}}">
                                    <i class='bx bx-chevron-right'></i>
                                    {{__('website.terms-and-conditions')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <div class="footer-service">
                        <h3>{{__('website.contact-us')}}</h3>
                        <ul>
                            <li>
                                <a href="tel:{{$settings->phone1}}" style="direction: ltr;">
                                    <i class='bx bx-phone-call'></i>
{{$settings->phone1}}                                </a>
                            </li>
                            <li>
                                <a href="tel:{{$settings->phone2}}" style="direction: ltr;">
                                    <i class='bx bx-phone-call'></i>
                                    {{$settings->phone2}}
                                </a>
                            </li>
                            <li>
                                <a href="mailto:{{$settings->email1}}">
                                    <i class='bx bx-message-detail'></i>
                                    <span class="__cf_email__" data-cfemail="0b62656d644b796e787f6a657f25686466">{{$settings->email1}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:{{$settings->email2}}">
                                    <i class='bx bx-message-detail'></i>
                                    <span class="__cf_email__" data-cfemail="91f9f4fdfdfed1e3f4e2e5f0ffe5bff2fefc">{{$settings->email2}}</span>
                                </a>
                            </li>
                            <li>
                                <i class='bx bx-location-plus'></i>
                                {{$settings->address1}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area copyright-area-two">
        <div class="container">
            <div class="copyright-item">
                <p>{{__('website.copy-right')}} <a href="{{route('pages.home')}}" target="_blank">{{__('website.project-title')}}</a></p>
            </div>
        </div>
    </div>

</footer>
