@extends('website::layouts.app')
@section('sub-header')
    <div class="page-title-area page-title-img-three">
        <div class="container">
            <div class="page-title-item">
                <h2>{{__('website.contact-us')}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{__('website.contact-us')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <div class="contact-location-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="location-item">
                        <img alt="Location" src="{{asset('front/assets/img/home-one/service-shape.png')}}">
                        <i class='bx bx-message-detail'></i>
                        <ul>
                            <li>{{$settings->email1}}</li>
                            <li>{{$settings->email2}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="location-item active">
                        <img alt="Location" src="{{asset('front/assets/img/home-one/service-shape.png')}}">
                        <i class='bx bxs-location-plus'></i>
                        <ul>
                            <li>{{$settings->address1}}</li>
                            <li>{{$settings->address2}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
                    <div class="location-item">
                        <img alt="Location" src="{{asset('front/assets/img/home-one/service-shape.png')}}">
                        <i class='bx bxs-phone-call'></i>
                        <ul>
                            <li>
                                <a href="tel:{{$settings->phone1}}" style="direction: ltr;">{{$settings->phone1}}</a>
                            </li>
                            <li>
                                <a href="tel:+{{$settings->phone2}}" style="direction: ltr;">{{$settings->phone2}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="contact-form-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-img">
                        <h3>{{__('website.contact-us')}}</h3>
                        <p>
                            {{__('website.contact-us-desc')}}


                        </p>
                        <div class="footer-service">
                            <h3>{{__('website.contact-information')}}</h3>
                            <ul class="contact-list">
                                <div class="phone">
                                    <h5><i class="fas fa-phone-volume"></i> {{__('website.phone')}} </h5>
                                    <li>
                                        <a href="tel:{{$settings->phone1}}" style="direction: ltr;">
                                            <i class="bx bx-phone-call"></i>
                                            {{$settings->phone1}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="tel:{{$settings->phone2}}" style="direction: ltr;">
                                            <i class="bx bx-phone-call"></i>
                                            {{$settings->phone2}}
                                        </a>
                                    </li>
                                </div>
                                <div class="phone">
                                    <h5><i class="far fa-envelope-open"></i> {{__('website.email')}} </h5>
                                    <li>
                                        <a href="mailto:{{$settings->email1}}">
                                            <i class="bx bx-message-detail"></i>
                                            <span class="__cf_email__" data-cfemail="0b62656d644b796e787f6a657f25686466">{{$settings->email1}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="mailto:{{$settings->email2}}">
                                            <i class="bx bx-message-detail"></i>
                                            <span class="__cf_email__" data-cfemail="0b62656d644b796e787f6a657f25686466">{{$settings->email2}}</span>
                                        </a>
                                    </li>
                                </div>
                                <div class="Location">
                                    <h5><i class="fas fa-map-marker-alt"></i> {{__('website.location')}} </h5>

                                    <li>
                                        <i class="bx bx-location-plus"></i>
                                        <a href="">
                                            {{$settings->address1}}</a>

                                    </li>
                                    <li>

                                        <i class="bx bx-location-plus"></i>
                                        <a href="">
                                            {{$settings->address2}}
                                        </a>
                                    </li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-item">

                        <form id="contactForm" method="post" action="{{route('contact-us.submit')}}"  >
                            @csrf
                            <input type="hidden" name="type" value="5">
                            <h3 class="center mb-10">{{__('website.contact-form')}} </h3>
                            <div class="row">
                                <div class="col-sm-6 col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control" data-error="{{__('website.please-enter-your-name')}}" id="name" name="name" placeholder="{{__('website.name')}}"
                                               required type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control" data-error="{{__('website.please-enter-your-email')}}" id="email" name="email" placeholder="{{__('website.email')}}"
                                               required type="email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control" data-error="{{__('website.please-enter-your-number')}}" id="phone_number" name="phone"
                                               placeholder="{{__('website.phone')}}" required
                                               type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-12">
                                    <div class="form-group">
                                        <select class="form-control form-control2" name="subject" >

                                            <option disabled selected hidden>
                                               {{__('website.select-subject')}} 
                                            </option>
                                            <option value="{{__('website.payment')}}">
                                               {{__('website.payment')}} 
                                            </option>
                                            <option value="{{__('website.delivery')}}">
                                                
                                                {{__('website.delivery')}} 
                                            </option>
                                            <option value="{{__('website.delivery-payment')}}">
                                               {{__('website.delivery-payment')}} 
                                                
                                            </option>
                                            <option value="{{__('website.website')}}">
                                               {{__('website.website')}} 
                                                
                                            </option>
                                            <option value="{{__('website.application')}}">
                                               {{__('website.application')}} 
                                                
                                            </option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                    <textarea class="form-control" cols="30" data-error="{{__('website.write-your-message')}}" id="message" name="message"
                                              placeholder="{{__('website.message')}}" required rows="6"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <button class="cmn-btn btn" type="submit">
                                        {{__('website.send-message')}}
                                    </button>
                                    <div class="h3 text-center hidden" id="msgSubmit"></div>
                                    @if(Session::has('success'))
                                        <div class="h3 text-center " id="msgSubmit">{{__('website.submitted-successfully')}}</div>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="grayscale  " style="width: 100%">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14301.81008309927!2d50.144781129143574!3d26.34422186326727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e49e5c53f261fa1%3A0x1b3a7426528ea24!2sAl%20Qusur%2C%20Dhahran%2034247%2C%20Saudi%20Arabia!5e0!3m2!1sen!2seg!4v1631004049234!5m2!1sen!2seg" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <!--<iframe frameborder="0" height="600" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"-->
        <!--        width="100%"></iframe>-->
        <a href="https://www.maps.ie/draw-radius-circle-map/"></a></div>

@endsection