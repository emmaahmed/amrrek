@extends('website::layouts.app')
@section('sub-header')
    <div class="page-title-area page-title-img-one3">
        <div class="container">
            <div class="page-title-item page-title-item2">
                <h2>{{__('website.stores')}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{__('website.stores')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection
@section('content')
    <section class="collection-area pb-100 ptb-100">
        <div class="container">
            <div class="section-title">
                <h2><span class="primary-color">{{__('website.project-title')}}</span> {{__('website.shops-service')}}</h2>
                <p>{{__('website.shops-service-desc')}}</p>
            </div>
            <div class="banner-area search-area ">

                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-12">
                                    <div class="banner-content">

                                        <form method="get" action="{{route('pages.stores')}}">
                                            <input class="form-control" name="search_key" placeholder="{{__('website.enter-shop-name')}}" type="text">
                                            <input class="form-control" value="all" name="category_id"  type="hidden">
                                            <button class="btn banner-form-btn"  type="submit">{{__('website.search-now')}}</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sorting-menu">
                <ul>
                    <li class="filter active" onclick="addMixClass('all',@if(isset(request()->category_id)) '{{request()->category_id}}' @else 0 @endif)" data-filter=".all">{{__('website.all')}}</li>
                    @if(count($categories)>0)
                        @foreach($categories as $category)
                    <li class="filter" onclick="addMixClass({{$category->id}} , @if(isset(request()->category_id)) '{{request()->category_id}}' @else 0 @endif )"  data-filter=".{{$category->id}}">{{$category->name}}</li>
                            @endforeach
                    @endif
                </ul>
            </div>
            <div class="row" id="Container">
                @if(count($categories))
                    @foreach($categories as $category)
                    @foreach($category['shops'] as $shop)

                <div class="col-sm-6 col-lg-4  web {{$category->id}}" @if(isset(request()->category_id) && $category->id ==request()->category_id) style="display: inline-block;" @else style="display: none;" @endif>
                    <div class="blog-item">
                        <div class="blog-top">
                            <a >
                                <img alt="{{$shop->name}}" src="{{$shop->cover_image}}">
                            </a>
                        </div>
                        <img class="store" src="{{$shop->image}}">
                        <div class="blog-bottom">
                            <h3>
                                <a >{{$shop->name}} </a>
                            </h3>
                            <p>{{$shop->desc}}</p>

                        </div>
                    </div>
                </div>

                        @endforeach
                        <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-4 col-lg-4">
                        </div>
                        <div class="col-sm-4 col-lg-4">
                        <div class="pagination center web {{$category->id}}" style="display: none;">

                            {{$category['shops']->appends(['category_id'=>$category->id])->links()}}
                        </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                        </div>

                    </div>
                    @endforeach
                @endif
                @if(count($shops)>0)
                    @foreach($shops as $shop)
                            <div class="col-sm-6 col-lg-4  web all" @if(isset(request()->category_id) && request()->category_id=='all') style="display: inline-block;" @else style="display: none;" @endif>
                                <div class="blog-item">
                                    <div class="blog-top">
                                        <a >
                                            <img alt="{{$shop->name}}" src="{{$shop->cover_image}}">
                                        </a>
                                    </div>
                                    <img class="store" src="{{$shop->image}}">
                                    <div class="blog-bottom">
                                        <h3>
                                            <a >{{$shop->name}} </a>
                                        </h3>
                                        <p>{{$shop->desc}}</p>

                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-4 col-lg-4">
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="pagination center web all" >

                                {{$shops->appends(['category_id'=>'all'])->links()}}
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                        </div>

                    </div>

            </div>


{{--        <div class="pagination center">--}}
{{--            <a href="#">&laquo;</a>--}}
{{--            <a href="#">1</a>--}}
{{--            <a class="active" href="#">2</a>--}}
{{--            <a href="#">3</a>--}}
{{--            <a href="#">4</a>--}}
{{--            <a href="#">5</a>--}}
{{--            <a href="#">6</a>--}}
{{--            <a href="#">&raquo;</a>--}}
{{--        </div>--}}
    </section>

@endsection
@section('extra-js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js'></script>
    <script src="{{asset('front/assets/js/script-counter.js')}}"></script>
<script>
   {{-- var oldUrl = "{{route('pages.stores')}}";--}}
   {{-- function addCategoryId(id){--}}
   {{--    $('a.page-link').each(function(index){--}}
   {{--        console.log( index + ": " + $( this ).text() );--}}
   {{--        var newUrl = oldUrl+'?page='+ $( this ).text()+'&category_id='+id;--}}
   {{--        $(this).attr("href", newUrl); // Set herf value--}}
   {{--    });--}}

   {{--    // var newURLString = window.location.href +--}}
   {{--    //     "?category="+id;--}}

   {{--    // window.location.href = newURLString;--}}


   {{--}--}}
   function addMixClass(id,requested_id){
       $('.web.'+id).addClass('mix');
       if(requested_id!=0){
           $('.web.'+requested_id).addClass('mix');

       }
       // $('.pagination.'+id).css('display','block');
   }
</script>
@endsection