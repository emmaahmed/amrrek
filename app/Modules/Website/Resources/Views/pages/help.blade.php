@extends('website::layouts.app')
@section('sub-header')
    <div class="page-title-area page-title-img-one2">
        <div class="container">
            <div class="page-title-item">
                <h2>{{__('website.help')}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{__('website.help')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection
@section('content')
    <section class="faq-area pt-100 pb-70">
        <div class="container">
@if(count($categories)>0)
    @foreach($categories as $category)
            <div class="row faq-wrap">
                <div class="col-lg-12">
                    <div class="faq-head">
                        <h2>{{$category->name}}</h2>
                    </div>
                    <div class="faq-item">
                        <ul class="accordion">
                            @if(count($category->faqs))
                                @foreach($category->faqs as $faq)
                            <li>
                                <a>{{$faq->question}}</a>
                                <p> {{$faq->answer}}</p>
                            </li>
                                @endforeach
                                @endif
                        </ul>
                    </div>
                </div>
            </div>
                @endforeach
    @endif
        </div>
    </section>

@endsection
