@extends('website::layouts.app')
@section('sub-header')
    <div class="page-title-area page-title-img-one2">
        <div class="container">
            <div class="page-title-item page-title-item2">
                <h2>{{$service->name}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{$service->name}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@section('content')
    <div class="service-details-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="service-details-item">
                        <div class="service-details-more">
                            <h3>{{__('website.more-services')}}</h3>
                            <ul>
                                @if(count($services))
                                    @foreach($services as $oneService)
                                <li>
                                    <a href="{{route('pages.service-details',$oneService->slug )}}">
                                        {{$oneService->name}}
                                        <i class='bx bx-plus'></i>
                                    </a>
                                </li>
                                    @endforeach
                                    @endif


                            </ul>
                        </div>
                        <div class="service-details-order">
                            <h3>{{$service->websiteDepartment->offer_text}}</h3>
{{--                            <span>09:00am - 12:00am</span>--}}
                            <img alt="Service" src="{{$service->websiteDepartment->ad_img}}">
                            <div class="offer-off">
                               <span class="white">{{$service->websiteDepartment->offer_discount}}</span>
                               <span class="white">{{__('website.off')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="service-details-item">
                        <div class="service-details-fresh">
                            <h2>{{$service->name}}</h2>
                            <img alt="Service" src="{{$service->websiteDepartment->service_details_img}}">
                                                        <p>{{$service->websiteDepartment->desc}}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection