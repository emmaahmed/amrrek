@extends('website::layouts.app')
@section('sub-header')
    <div class="page-title-area page-title-img-one2">
        <div class="container">
            <div class="page-title-item">
                <h2>{{__('website.terms-and-conditions')}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{__('website.terms-and-conditions')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection
@section('content')
    <section class="privacy-area pt-100">
        <div class="container">
            <div class="privacy-item">
                {!! $terms->term !!}
            </div>
        </div>
    </section>

@endsection
