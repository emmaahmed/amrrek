@extends('website::layouts.app')
@section('content')
    <div class="banner-area-two">

        <div class="container">
            <div class="banner-content">
                <h1> {{$sliderSection->title}}</h1>
                <p>{{$sliderSection->desc}}</p>
                <div class="banner-btn-wrap">
                    <a class="cmn-btn black-btn " href="{{$sliderSection->button1_link}}">{{$sliderSection->button1}}</a>
                    <a class="banner-btn-two" href="{{$sliderSection->button2_link}}">{{$sliderSection->button2}}</a>
                </div>
            </div>
            <div class="banner-img">
                <img alt="Banner" class="wow fadeInUp" data-wow-duration="2s"
                     src="{{asset('front/uploads/slider/'.$sliderSection->img1)}}">
            </div>
        </div>
    </div>


    <div class="food-img-area pb-70 ">
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img1)}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img2)}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img3)}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img4)}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img5)}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4 col-lg-2">
                    <div class="food-img-item">
                        <img alt="Food" src="{{asset('front/uploads/under-slider/'.$underSliderSection->img6)}}">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="about-area">
        <div class="about-shape">
            <img alt="Shape" src="{{asset('front/assets/img/home-two/about3.png')}}">
            <img alt="Shape" class="wow rotateIn" data-wow-delay="1" data-wow-duration="4s"
                 src="{{asset('front/assets/img/home-two/about4.png')}}">
            <img alt="Shape" class="wow rotateIn" data-wow-delay="1.2" data-wow-duration="4s"
                 src="{{asset('front/assets/img/home-two/about5.png')}}">
            <img alt="Shape" class="wow rotateIn" data-wow-delay="1.4" data-wow-duration="4s"
                 src="{{asset('front/assets/img/home-two/about6.png')}}">
            <img alt="Shape" class="wow rotateIn" data-wow-delay="1.6" data-wow-duration="4s"
                 src="{{asset('front/assets/img/home-two/about7.png')}}">
        </div>
        <div class="container-fluid p-0">
            <div class="row m-0 align-items-center">
                <div class="col-lg-6 p-0">
                    <div class="about-img">
                        <img alt="About" class="wow fadeInLeft" data-wow-delay="0.2s"
                             data-wow-duration="1.5s" src="{{asset('front/uploads/about/'.$aboutSection->img1)}}">
                        <img alt="About" class="wow fadeInLeft" data-wow-delay="1.2s" data-wow-duration="2s"
                             src="{{asset('front/uploads/about/'.$aboutSection->img2)}}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-content">
                        <div class="section-title">
                            <span class="sub-title">{{__('website.about')}}</span>
                            <h2>{{$aboutSection->title}}</h2>
                            <p>{{$aboutSection->desc}}</p>
                        </div>
                        <a class="cmn-btn" href="{{$aboutSection->button1_link}}">{{$aboutSection->button1}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="service-area service-area-two ptb-100  @if(app()->getLocale()=='ar') ltr @endif">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">{{__('website.services')}}</span>
                <h2> {{__('website.amrk-services')}}</h2>
                <p>{{__('website.services-desc')}}</p>
            </div>
            <div class="service-slider owl-theme owl-carousel">
                @if(count($services)>0)
                @foreach($services as $service)
                <div class="service-item">
                    <a href="{{route('pages.service-details',$service->slug)}}">
                        <img alt="Service" src="{{$service->image}}">
                        <img alt="Service" class="service-shape" src="{{asset('front/assets/img/home-one/service-shape.png')}}">
                        <h3>{{$service->name}}</h3>
                        <p>{{$service->websiteDepartment?$service->websiteDepartment->brief_desc:''}}</p>
                    </a>
                </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <div class="restant-area menu-area pt-100 pb-70">
        <div class="restant-shape">
            <img alt="Shape"   src="{{asset('front/assets/img/home-one/service-shape2.png')}}">
        </div>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="restant-img">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img1)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img2)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img3)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img4)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img5)}}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="restant-content">
                        <div class="section-title">
                            <h2>{{$underServiceSection->title}}</h2>
                            <p>{{$underServiceSection->desc}}</p>
                        </div>
                        <a class="cmn-btn" href="{{$underServiceSection->button1_link}}">{{$underServiceSection->button1}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="download-area pt-100 pb-70" id="download-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="download-content ">
                        <div class="section-title">
                            <h2>{{$downloadSection->title}}</h2>
                            <p>{{$downloadSection->desc}}</p>
                        </div>
                        <ul>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                            data-wow-delay="1s" >
                            <span>01</span>
                                {{$downloadSection->button1}}
                            </li>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                             data-wow-delay="1.4s">
                            <span>02</span>
                                {{$downloadSection->button2}}
                            </li>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                             data-wow-delay="1.8s">
                            <span>03</span>
                                {{$downloadSection->button3}}
                            </li>
                        </ul>
                        <div class="app-wrap wow fadeInUp" data-wow-duration="2s"
                         data-wow-delay="1.8s">
                        <a href="{{$settings->google_store_link}}">
                            <img alt="Google" src="{{asset('front/assets/img/home-two/google-store.png')}}">
                        </a>
                        <a href="{{$settings->app_store_link}}">
                            <img alt="App" src="{{asset('front/assets/img/home-two/app-store.png')}}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="download-img wow fadeInRight" data-wow-duration="2s">
                    <img alt="Download" src="{{asset('front/uploads/download/'.$downloadSection->img1)}}">
                </div>
            </div>
        </div>
        </div>
    </section>

    <div class="about-area-two pt-100 pb-70">
        <div class="about-shape">
            <img alt="Shape" src="{{asset('front/assets/img/home-three/about3.png')}}">
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-img">
                        <img alt="About" src="{{asset('front/uploads/how-to-use/'.$howToUseSection->img1)}}">
                        <img alt="About" src="{{asset('front/assets/img/home-three/about2.png')}}">
                        <div class="video-wrap">
                            <a class="popup-youtube" href="{{$howToUseSection->button2_link}}">
                                <i class='bx bx-play'></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-content">
                        <div class="section-title">
                            <h2>{{$howToUseSection->title}} <span class="primary">{{__('website.project-title')}}</span></h2>
                            <p>{{$howToUseSection->desc}} </p>
                        </div>
                        <a class="cmn-btn" href="{{$howToUseSection->button1_link}}">{{$howToUseSection->button1}} </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="review-area">
        <div class="container-fluid p-0">
            <div class="row m-0 align-items-center">
                <div class="col-lg-6 p-0">
                    <div class="review-img">
                        <img alt="Review" src="{{asset('front/assets/img/home-one/review1.png')}}">
                        <img alt="Review" src="{{asset('front/uploads/opinions/'.$opinionsSection->img1)}}">
                    </div>
                </div>
                <div class="col-lg-6 p-0">
                    <div class="review-item @if(app()->getLocale()=='ar') ltr @endif">
                        <div class="section-title">
                            <h2>{{$opinionsSection->title}}</h2>
                            <p>{{$opinionsSection->desc}}</p>
                        </div>
                        <div class="slider-nav">
                            @if(count($opinionsSection['opinions'])>0)
                                @foreach($opinionsSection['opinions'] as $opinion)
                            <div class="item">
                                <img alt="{{$opinion->name}}" draggable="false" src="{{$opinion->img}}"/>
                            </div>
                                @endforeach
                                @endif
                        </div>

                        <div class="slider-for">
                            @if(count($opinionsSection['opinions'])>0)
                                @foreach($opinionsSection['opinions'] as $opinion)

                                <div class="item">
                                <h3>{{$opinion->name}}</h3>
                                <p>{{$opinion->desc}}</p>
                            </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="blog-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">{{__('website.stores')}}</span>
                <h2>{{__('website.top-stores')}} </h2>
                <p>{{__('website.top-stores-desc')}}</p>
            </div>
            <div class="row">
                @if(count($stores)>0)
                    @foreach($stores as $store)
                <div class="col-sm-6 col-lg-4">
                    <div class="blog-item">
                        <div class="blog-top">
                            <a class="nohover" href="blog-details.html">
                                <img alt="Blog" src="{{$store->cover_image}}">
                            </a>
                        </div>
                        <img class="store" src="{{$store->image}}">
                        <div class="blog-bottom">
                            <h3>
                                <a class="nohover" href="#">{{$store->name}} </a>
                            </h3>
                            <p>{{$store->desc}}</p>

                        </div>
                    </div>
                </div>
                    @endforeach
                @endif
            </div>
            <div class="text-center">
                <a class="nav-tel primary-btn" href="{{route('pages.stores',['category_id'=>'all'])}}">
                    <i class="fas fa-store"></i>
                    {{__('website.show-all-stores')}}
                </a>
            </div>
        </div>
    </section>

@endsection