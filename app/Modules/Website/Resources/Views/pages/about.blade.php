@extends('website::layouts.app')
@section('about-css')
    <link href="{{asset('front/assets/css/style-counter.css')}}" rel="stylesheet">
    <link href="{{asset('front/assets/css/style-brand.css')}}" rel="stylesheet">
<style>
    .owl-stage-outer
{
    height: 160px !important;
}
</style>
@endsection
@section('sub-header')
    <div class="page-title-area page-title-img-one2">
        <div class="container">
            <div class="page-title-item">
                <h2>{{__('website.about-us')}}</h2>
                <ul>
                    <li>
                        <a href="{{route('pages.home')}}">{{__('website.home')}}</a>
                    </li>
                    <li>
                        <i class='bx bx-chevron-right'></i>
                    </li>
                    <li>{{__('website.about')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection
@section('content')
    <section class="story-area pt-100 pb-70">
        <div class="story-shape">
            <img alt="Shape" src="{{asset('front/assets/img/about/story3.png')}}">
        </div>
        <div class="container">
            <div class="story-head">
                <h2>{{$aboutPage->title}}</h2>
                <p>{{$aboutPage->desc}}</p>
            </div>
            <div class="row">
                @if(count($services)>0)
                    @foreach($services as $service)
                <div class="col-sm-6 col-lg-6">
                    <div class="story-item">
                        <img alt="Story" src="{{$service->websiteDepartment->about_img}}">
                        <h3><a href="{{route('pages.service-details',$service->slug)}}">{{$service->name}}</a></h3>
                    </div>
                </div>
                    @endforeach
                @endif
            </div>
{{--            <div class="row">--}}
{{--                <div class="col-sm-6 col-lg-6">--}}
{{--                    <div class="story-item">--}}
{{--                        <img alt="Story" src="assets/img/about/story1.jpg">--}}
{{--                        <h3><a href="amrk-heavy.html">Heavy Transport Service</a></h3>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-6">--}}
{{--                    <div class="story-item">--}}
{{--                        <img alt="Story" src="assets/img/about/story2.jpg">--}}
{{--                        <h3><a href="amrk-any-service.html">Order Any Service</a></h3>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </section>
    <div class="restant-area menu-area pt-100 pb-70">
        <div class="restant-shape">
            <img alt="Shape" class="wow fadeInRight" data-wow-duration="2s" src="{{asset('front/assets/img/home-one/service-shape2.png')}}">
        </div>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="restant-img">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img1)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img2)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img3)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img4)}}">
                        <img alt="Restant" src="{{asset('front/uploads/under-service/'.$underServiceSection->img5)}}">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="restant-content">
                        <div class="section-title">
                            <h2>{{$underServiceSection->title}}</h2>
                            <p>{{$underServiceSection->desc}}</p>
                        </div>
                        <a class="cmn-btn" href="{{$underServiceSection->button1_link}}">{{$underServiceSection->button1}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="download-area pt-100 pb-70" id="download-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="download-content ">
                        <div class="section-title">
                            <h2>{{$downloadSection->title}}</h2>
                            <p>{{$downloadSection->desc}}</p>
                        </div>
                        <ul>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                                data-wow-delay="1s" >
                                <span>01</span>
                                {{$downloadSection->button1}}
                            </li>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                                data-wow-delay="1.4s">
                                <span>02</span>
                                {{$downloadSection->button2}}
                            </li>
                            <li class="wow fadeInLeft" data-wow-duration="1.5s"
                                data-wow-delay="1.8s">
                                <span>03</span>
                                {{$downloadSection->button3}}
                            </li>
                        </ul>
                        <div class="app-wrap wow fadeInUp" data-wow-duration="2s"
                             data-wow-delay="1.8s">
                            <a href="{{$settings->google_store_link}}">
                                <img alt="Google" src="{{asset('front/assets/img/home-two/google-store.png')}}">
                            </a>
                            <a href="{{$settings->app_store_link}}">
                                <img alt="App" src="{{asset('front/assets/img/home-two/app-store.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="download-img wow fadeInRight" data-wow-duration="2s">
                        <img alt="Download" src="{{asset('front/uploads/download/'.$downloadSection->img1)}}">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="statistic-section one-page-section" id="statistic">
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12 col-md-3">
                    <div class="counter">
                        <i class="fas fa-user-friends fa-2x stats-icon"></i>
                        <h2 class="timer count-title count-number">{{$users}}</h2>
                        <div class="stats-line-black"></div>
                        <p class="stats-text">{{__('website.users')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="counter">
                        <i class="fas fa-map-marked-alt fa-2x stats-icon"></i>
                        <h2 class="timer count-title count-number">{{$cities}}
                        </h2>
                        <div class="stats-line-black"></div>
                        <p class="stats-text">{{__('website.cities')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="counter">
                        <i class="fas fa-cloud-download-alt fa-2x stats-icon"></i>
                        <h2 class="timer count-title count-number">{{$shops}}</h2>
                        <div class="stats-line-black"></div>
                        <p class="stats-text">{{__('website.shops')}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="counter">
                        <i class="fas fa-car fa-2x stats-icon"></i>
                        <h2 class="timer count-title count-number">{{$drivers}}</h2>
                        <div class="stats-line-black"></div>
                        <p class="stats-text">{{__('website.drivers')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(count($clients)>0)

        <h3 class="center mt-40"><span class="primary-color">{{__('website.project-title')}}</span> {{__('website.success-partner')}}</h3>
    <div class="brand-carousel section-padding owl-carousel carousel-style ltr ">
        @foreach($clients as $client)
        <div class="single-logo">
            <img alt="" src="{{$client->img}}">
        </div>
        @endforeach
    </div>
    @endif
@endsection
@section('extra-js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js'></script>
    <script src="{{asset('front/assets/js/script-counter.js')}}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
    <script src="{{asset('front/assets/js/script-brand.js')}}"></script>

@endsection