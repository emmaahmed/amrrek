<?php

namespace App\Modules\Website\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\ContactUs;
use App\Models\Department;
use App\Models\Driver;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\Term;
use App\Models\User;
use App\Modules\Website\Http\Requests\ContactUsRequest;
use App\Modules\Website\Models\Client;
use App\Modules\Website\Models\FaqCategory;
use App\Modules\Website\Models\Opinion;
use App\Modules\Website\Models\Section;
use App\Modules\Website\Models\WebsiteSetting;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LandingController extends Controller
{
    public function home(){
        $sliderSection=Section::whereId(1)->first();
        $underSliderSection=Section::whereId(2)->first();
        $aboutSection=Section::whereId(3)->first();
        $services=Department::with('websiteDepartment')->select('id','name as slug','name_'.app()->getLocale().' as name','image')->get();
        $underServiceSection=Section::whereId(4)->first();
        $downloadSection=Section::whereId(5)->first();
        $settings=WebsiteSetting::first();
        $howToUseSection=Section::whereId(6)->first();
        $opinionsSection=Section::whereId(7)->first();
        $opinionsSection['opinions']=Opinion::all();
        $stores=Shop::withCount('allOrders')->select('*','description_'.app()->getLocale().' as desc')->get()->sortByDesc('all_orders_count')->take(6);

        return view('website::pages.home',compact('sliderSection','underServiceSection','underSliderSection','aboutSection'
        ,'services','downloadSection','howToUseSection','opinionsSection','stores','settings'));

    }
    public function about(){
        $underServiceSection=Section::whereId(4)->first();
        $downloadSection=Section::whereId(5)->first();
        $services=Department::with('websiteDepartment')->select('id','name as slug','name_'.app()->getLocale().' as name')->get();
        $settings=WebsiteSetting::first();
        $aboutPage=Section::whereId(8)->first();
        $users=User::count();
        $shops=Shop::count();
        $cities=City::count();
        $drivers=Driver::count();
        $clients=Client::all();

        return view('website::pages.about',compact('underServiceSection','downloadSection','services','settings','aboutPage','users','shops','cities','drivers','clients'));

    }
    public function serviceDetails($name){
        $service=Department::where('name',$name)->select('id','name as slug','name_'.app()->getLocale().' as name')->with('websiteDepartment')->first();
        $services=Department::where('name','!=',$name)->select('id','name as slug','name_'.app()->getLocale().' as name')->get();
        return view('website::pages.service-details',compact('service','services'));
    }
    public function help(){
        $categories=FaqCategory::select('id','name_'.app()->getLocale().' as name')->with('faqs')->get();
        return view('website::pages.help',compact('categories'));


    }
    public function contactUs(){
        $settings=WebsiteSetting::first();
        return view('website::pages.contact-us',compact('settings'));

    }
    public function submitContact(ContactUsRequest $request){
        // $request['type']=5;
        // dd($request->validated());
        ContactUs::create($request->validated());
        return redirect()->back()->with('success','done');

    }
    public function submitNewsLetter(Request $request){
        // $request['type']=5;
        // dd($request->validated());
        $emailExist=ContactUs::where(['type'=>6,'email'=>$request->email])->first();
        if($emailExist){
                    return redirect(route('pages.home').'#footer')->with('error','done');

        }
        ContactUs::create(['type'=>6,'email'=>$request->email]);
        return redirect(route('pages.home').'#footer')->with('success','done');

    }
    public function stores(Request $request){

        $categories=Category::select('id','name_'.app()->getLocale().' as name')->whereHas('shops')->get();
        foreach ($categories as $category){
            if($request->search_key) {
                $category['shops'] = $category->shops()->where('name','like','%'.$request->search_key.'%')->paginate(6);
            }
            else{
                $category['shops'] = $category->shops()->select('*','description_'.app()->getLocale().' as desc')->paginate(6);

            }
        }
        if($request->search_key) {

            $shops = Shop::select('*','description_'.app()->getLocale().' as desc')->where('name','like','%'.$request->search_key.'%')->paginate(6);
        }else{
            $shops = Shop::select('*' ,'description_'.app()->getLocale().' as desc')->paginate(6);

        }
            //        return $categories;
        return view('website::pages.stores',compact('categories','shops'));
    }
    public function terms(){
        $terms=Term::where('type',0)->select('id','term_'.app()->getLocale().' as term')->first();
        return view('website::pages.terms-and-conditions',compact('terms'));


    }
        public function ChangeLang($lang,$segment=null){

         app()->setLocale($lang);  
        //  session()->put('locale',$lang);
        //           $locale = session()->get('locale');

            session(['locale' => $lang]);

         if($segment=='home'){
            //  dd(app()->getLocale());
            // dd(route($segment));
         return redirect()->back();
         }
        //  dd(app()->getLocale());
         return redirect()->back();


    }

}
