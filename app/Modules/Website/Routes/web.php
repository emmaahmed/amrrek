<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Route::get('/website', function () {
//     return  redirect('website/'.app()->getLocale());
// });

    Route::group(['middleware' => 'locale'], function () {
    // Route::group(['prefix' => app()->getLocale()], function () {

        Route::get('/', 'LandingController@home')->name('pages.home');
    Route::get('/about', 'LandingController@about')->name('pages.about');
    Route::get('/service-details/{name}', 'LandingController@serviceDetails')->name('pages.service-details');
    Route::get('/help', 'LandingController@help')->name('pages.help');
    Route::get('/contact-us', 'LandingController@contactUs')->name('pages.contact-us');
    Route::post('/contact-us', 'LandingController@submitContact')->name('contact-us.submit');
    Route::post('/news-letter', 'LandingController@submitNewsLetter')->name('newsletter.submit');
    
    Route::get('/stores', 'LandingController@stores')->name('pages.stores');
    Route::get('/terms-and-conditions', 'LandingController@terms')->name('pages.terms');
});
Route::get('/changelanguage/{langkey}/{segment}', 'LandingController@ChangeLang')->name('change.lang');

// });
