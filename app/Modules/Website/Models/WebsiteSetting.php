<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteSetting extends Model
{
    protected $table='website_settings';
    protected $fillable=['app_store_link', 'google_store_link', 'phone1', 'phone2', 'email1', 'email2', 'address1_en', 'address1_ar', 'address2_en', 'address2_ar', 'map_iframe','footer_desc_en','footer_desc_ar','facebook', 'twitter', 'linkedin', 'instagram'];
    protected $hidden=['created_at','updated_at'];
    protected $appends=['address1','address2','footer_desc'];
    public function getAddress1Attribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['address1_'.$lang];

    }
    public function getAddress2Attribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['address2_'.$lang];

    }
    public function getFooterDescAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['footer_desc_'.$lang];

    }

}
