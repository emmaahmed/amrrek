<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    protected $table = 'opinions';
    protected $fillable = ['name_en', 'name_ar', 'desc_en', 'desc_ar', 'img'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['name', 'desc'];

    public function getNameAttribute($key)
    {
        $lang = app()->getLocale();
        return $this->attributes['name_' . $lang];

    }

    public function getDescAttribute($key)
    {
        $lang = app()->getLocale();
        return $this->attributes['desc_' . $lang];

    }

    public function getImgAttribute($key)
    {
        if ($key) {
            return asset('front/uploads/opinions/' . $key);
        } else {
            return asset('front/uploads/opinions/no-image.jpg');
        }


    }
    public function setImgAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.jpg';
            if($value->move(public_path('front/uploads/opinions/'),$img_name)){
            $this->attributes['img'] = $img_name ;
            }
        }

    }

}

