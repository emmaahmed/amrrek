<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table='faqs';
    protected $fillable=['faq_category_id', 'question_en', 'question_ar', 'answer_en', 'answer_ar'];
    protected $hidden=['created_at','updated_at'];
    protected $appends=['question','answer'];

    public function getQuestionAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['question_'.$lang];

    }
    public function getAnswerAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['answer_'.$lang];

    }
    public function category(){
        return $this->belongsTo(FaqCategory::class,'faq_category_id','id');
}

}
