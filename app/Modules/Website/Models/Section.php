<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table='sections';
    protected $fillable=['title_en', 'title_ar', 'desc_en', 'desc_ar', 'button1_en', 'button1_ar', 'button1_link', 'button2_en', 'button2_ar', 'button2_link', 'button3_en', 'button3_ar', 'button3_link', 'img1', 'img2', 'img3', 'img4', 'img5', 'img6'];
    protected $hidden=['created_at','updated_at'];
    protected $appends=['title','desc','button1'];
    public function getTitleAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['title_'.$lang];

    }
    public function getDescAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['desc_'.$lang];

    }
    public function getButton1Attribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['button1_'.$lang];

    }
    public function getButton2Attribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['button2_'.$lang];

    }
    public function getButton3Attribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['button3_'.$lang];

    }

}
