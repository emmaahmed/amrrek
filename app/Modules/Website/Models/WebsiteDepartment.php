<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteDepartment extends Model
{
    protected $table='website_departments';
    protected $fillable=['department_id', 'title_en', 'title_ar', 'brief_desc_en', 'brief_desc_ar', 'desc_en', 'desc_ar', 'img', 'offer_text_en','offer_text_ar','offer_discount', 'ad_img','about_img','service_details_img'];
    protected $hidden=['created_at','updated_at'];
    protected $appends=['title','desc','brief_desc','offer_text'];
    public function getTitleAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['title_'.$lang];

    }
    public function getDescAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['desc_'.$lang];

    }
    public function getBriefDescAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['brief_desc_'.$lang];

    }
    public function getOfferTextAttribute($key)
    {
        $lang=app()->getLocale();
        return $this->attributes['offer_text_'.$lang];

    }
    public function getServiceDetailsImgAttribute($key)
    {
        if ($key) {
            return asset('front/uploads/services/service-details/' . $key);
        } else {
            return asset('front/uploads/services/service-details/no-image.jpg');
        }
    }
    public function setServiceDetailsImgAttribute($value){
         if($value) {
             $img_name = time() . uniqid() . '.jpg';
             $value->move(public_path('front/uploads/services/service-details/'), $img_name);

             $this->attributes['service_details_img'] = $img_name;
         }


    }
    public function setAdImgAttribute($value){
         if($value) {
             $img_name = time() . uniqid() . '.jpg';
             $value->move(public_path('front/uploads/services/ads/'), $img_name);

             $this->attributes['ad_img'] = $img_name;
         }


    }
    public function setAboutImgAttribute($value){
         if($value) {
             $img_name = time() . uniqid() . '.jpg';
             $value->move(public_path('front/uploads/about-imgs/'), $img_name);

             $this->attributes['about_img'] = $img_name;
         }


    }
    public function getAdImgAttribute($key)
    {
        if($key){
            return asset('front/uploads/services/ads/'.$key);
        }
        else{
            return asset('front/uploads/services/ads/no-image.jpg');
        }
    }
    public function getAboutImgAttribute($key)
    {
        if($key){
            return asset('front/uploads/about-imgs/'.$key);
        }
        else{
            return asset('front/uploads/services/ads/no-image.jpg');
        }
    }



}
