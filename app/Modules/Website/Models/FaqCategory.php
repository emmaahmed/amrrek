<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    protected $table='faq_categories';
    protected $fillable=['name_en', 'name_ar'];
    protected $hidden=['created_at','updated_at'];
    public function faqs(){
        return $this->hasMany(Faq::class,'faq_category_id','id');
    }

}
