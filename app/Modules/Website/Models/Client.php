<?php

namespace App\Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table='clients';
    protected $fillable=['img'];
    protected $hidden=['created_at','updated_at'];
    public function getImgAttribute($key)
    {
        if($key){
            return asset('front/uploads/clients/'.$key);
        }
        else{
            return asset('front/uploads/clients/no-image.jpg');
        }
    }
    public function setImgAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('front/uploads/clients/'),$img_name);
            $this->attributes['img'] = $img_name ;

        }

    }

}
