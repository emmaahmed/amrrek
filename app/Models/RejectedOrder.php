<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class RejectedOrder extends Model
{
    use Notifiable;


    protected $table = 'rejected_orders';

    protected $fillable = [
        'order_id','delegate_id'
    ];




}
