<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class UserLocation extends Model
{
    use Notifiable;
    use SoftDeletes;


    protected $table = 'user_locations';

    protected $fillable = [
        'user_id','lat','lng','name','address','description','city_name'
    ];

    protected $hidden = [
        'updated_at','deleted_at',
    ];

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('d F Y H:i A');
    }

    public function getNameAttribute($val)
    {
        if($val)
            return $val;
        else
            return "";
    }

    public function getAddressAttribute($val)
    {
        if($val)
            return $val;
        else
            return "";
    }

    public function getDescriptionAttribute($val)
    {
        if($val)
            return $val;
        else
            return "";
    }

    public function getLatAttribute($val)
    {
        if($val)
            return $val;
        else
            return "0";
    }

    public function getLngAttribute($val)
    {
        if($val)
            return $val;
        else
            return "0";
    }

    public function getCityNameAttribute($val)
    {
        if($val)
            return $val;
        else
            return "";
    }


}
