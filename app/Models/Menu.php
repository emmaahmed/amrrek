<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Menu extends Model
{
    use Notifiable;
    use SoftDeletes;


    protected $table = 'menus';

    protected $fillable = [
        'shop_id','name_ar','name_en'
    ];

    protected $hidden = [
        'active', 'deleted_at','created_at', 'updated_at','shop_id'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'menu_id');
    }





}
