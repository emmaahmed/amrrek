<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Trip extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    //payment >> 0=>cash, 1=>online, 2=>wallet
    /*status: 1=waiting_captin , 2=trip_started , 3=trip_finished , 4=trip_cancelled*/
    protected $fillable = [
        "paid",
        'status','cancel_id','canceled_by','cancel_reason','user_id','driver_id','','cancel_reason',
        'start_address','start_lat','start_lng',
        'end_address','end_lat','end_lng','date','time',
        'description','country_id','pay_status','driver_arrive_date','start_date','end_date'
    ];

    protected $appends = ['currency','created_at_time','car_model'];
    public function promo(){
        return $this->belongsTo(PromoCode::class,'promo_id','id');
    }
    function getCurrencyAttribute()
    {
        // if(isset($this->attributes['country_id'])){
        //     return (string)Country::whereId($this->attributes['country_id'])->first()->currency;
        // }
        //$country_id = $this->attributes['country_id'];
        // if(!isset($this->attributes['country_id'])){
            $lang = request()->header('lang');
            if(!$lang)
                $lang = 'ar';
            if($lang == 'ar')
                return "ريال";
            else
                return "Riyal";
        // }
//            $country_id=2;
//

    }

    function getCarModelAttribute()
    {
        if(isset($this->attributes['car_level_id'])){
            return (string)CarLevel::whereId($this->attributes['car_level_id'])->first()->name;
        }
        return "normal";

    }

    public function trip_paths()
    {
        return $this->hasMany(TripPath::class, 'trip_id');
    }
    public function lastPath()
    {
        return $this->hasMany(TripPath::class, 'trip_id')->orderBy('id','DESC')->whereIn('status',[1,2]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select('id','name','phone','lat','lng','image','rate','country_id','promo_code','no_of_trips');
    }

    public function driver()
    {
        $data = $this->belongsTo(Driver::class, 'driver_id');
//           ->select('id','f_name','l_name','phone','lat','lng','image','rate','country_id','promo_code');
        return $data;
    }

    public static function filterbylatlng($mylat,$mylng,$radius,$model,$country_id=188,$car_level_id=0,$driver_id=0)
    {
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        if($driver_id!=0) {


            $datainradiusrange = Driver::orderBy('id', 'desc')
                ->select('drivers.id', 'f_name', 'l_name', 'lat', 'lng', 'image', 'car_color', 'car_num', 'car_model', 'token', 'jwt')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->where('files_completed', 1)
                ->where('car_level', request()->car_level_id ? request()->car_level_id : $car_level_id)
                // ->join('driver_car_levels','drivers.id','driver_car_levels.driver_id')
                // ->where(['driver_car_levels.car_level_id'=>request()->car_level_id,'flag'=>1])
                ->where('active', 1)
                ->where('suspend', 0)
                ->where('accept', 1)//captin_infos
                ->where('busy', 1)//captin_infos
                ->where('online', 1)//captin_infos
                ->where('id','!=',$driver_id);
//            ->where('country_id', $country_id)
//                ->take(5)
            $data=$datainradiusrange->get();
            $datainradiusrange->update(['busy'=> 0]);
        }
        else{
            $datainradiusrange = Driver::orderBy('id', 'desc')
                ->select('drivers.id', 'f_name', 'l_name', 'lat', 'lng', 'image', 'car_color', 'car_num', 'car_model', 'token', 'jwt')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->where('files_completed', 1)
                ->where('car_level', request()->car_level_id ? request()->car_level_id : $car_level_id)
                // ->join('driver_car_levels','drivers.id','driver_car_levels.driver_id')
                // ->where(['driver_car_levels.car_level_id'=>request()->car_level_id,'flag'=>1])
                ->where('active', 1)
                ->where('suspend', 0)
                ->where('accept', 1)//captin_infos
                ->where('busy', 0)//captin_infos
                ->where('online', 1);//captin_infos
//            ->where('country_id', $country_id)
//                ->take(5)
//            $datainradiusrange->update(['busy'=> 1]);
            $data=$datainradiusrange->get();
            $datainradiusrange->update(['busy'=> 1]);

        }

//dd($datainradiusrange);
        return $data;
    }

    public static function calc_distance($lat_1,$lng_1,$lat_2,$lng_2)
    {

        $latitudeFrom=$lat_1;
        $longitudeFrom=$lng_1;
        $latitudeTo=$lat_2;
        $longitudeTo=$lng_2;
        $earthRadius=6371;
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle2= $angle * $earthRadius;

    }

    function getTripDistanceAttribute($value)
    {
        if($value)
            return  "$value";
        return  "";
    }

    function getCreatedAtAttribute()
    {
    
            if(request()->header('lang') == "ar"){

        return  Carbon::parse($this->attributes['created_at'])->locale('ar')
            ->translatedFormat('Y-m-d , H:i a');
            }
            else{
                        return  Carbon::parse($this->attributes['created_at'])
            ->format('Y-m-d , H:i a');

            }
    }

    function getCreatedAtTimeAttribute()
    {
        if(isset($this->attributes['created_at']))
            return  Carbon::parse($this->attributes['created_at'])
                ->format('Y-m-d H:i');
        return  Carbon::now()
            ->format('Y-m-d H:i');
    }

    function getUserCommentAttribute($val){
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getDriverCommentAttribute($val){
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getCanceledByAttribute($val){
        if($val == null){
            return  "0";
        }else{
            return $val;
        }
    }

    function getCancelIdAttribute($val){
        if($val == null){
            return  0;
        }else{
            return $val;
        }
    }

    function getCancelReasonAttribute($val){
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getPromoIdAttribute($val){
        if($val == null){
            return  0;
        }else{
            return $val;
        }
    }

    function getDescriptionAttribute($val){
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getDriverIdAttribute($val){
        if($val == null){
            return  0;
        }else{
            return $val;
        }
    }

    function getStatusAttribute($val){
        //
        if($val == null){
            return  0;
        }else{
            return $val;
        }
    }
//$date = Carbon::parse($details->reservation_date->dateA)->locale('ar');
//$place_item['id'] = $details->id;
//$place_item['reservation_number'] = '#' . $details->reservation_number;
//$place_item['reservation_date'] = $date->translatedFormat('l jS F Y');

    function getDateAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            if(request()->header('lang') == "ar"){
                $date = Carbon::parse($val)->locale('ar');
                return $date->translatedFormat('l j F');
            }else{
                return Carbon::parse($val)->format('d F Y');
            }
        }
    }

    function getTimeAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            if(request()->header('lang') == "ar"){
                return  Carbon::parse($val)->isoFormat('h:mm a');
            }else{
                return  Carbon::parse($val)->isoFormat('h:mm A');
            }
        }
    }

    function getPromoCodeAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getEndAddressAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getEndLatAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    function getEndLngAttribute($val){
        //
        if($val == null){
            return  '';
        }else{
            return $val;
        }
    }

    // function getTripTotalAttribute($val){
    //     if($val!=null){
    //     return (int)$val;
    //     }else{
    //         return 0 ;
    //     }

    // }



}
