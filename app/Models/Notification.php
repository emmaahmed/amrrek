<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Pusher\PushNotifications\PushNotifications;

class Notification extends Model
{
    use Notifiable;
    use SoftDeletes;


    protected $table = 'notifications';
    protected $fillable = ['title', 'body', 'user_id', 'order_id', 'department_id', 'type', 'image', 'order_type'];
    protected $hidden = ['deleted_at', 'updated_at'];
//type =>>>0=>user, 1=>delegate, 2=>driver, 3=>all
//0=>user, 1=>delegate, 2=>driver , 3=>mandobHeavey , 4=>shop

    public static function send($tokens, $title = "hello", $msg = "helo msg", $type = 1,
                                $is_captin = 0, $new_offer = null, $chat = null, $wallet = null,
                                $order_id = null, $confirm_accept_order = null, $trip = null,
                                $uber_chat = null, $trip_id = null, $department_id = null)
    {
//type >>>> 0=>new order , get lower offer  >notifications go for delegate
//type >>>> 1=>change status , chat     >notifications go for user & delegate
//type >>>> 3=>user accept offer    >notifications go for delegate

//type >>>> 0=>new offer    >notifications go for user
//type >>>> 4=>chat with admin    >notifications go for user

//type >>>> 5 for user uber chat

        //$key = 'AAAAoYnj12I:APA91bGgL-NA8vEODyDSXd1tXvU6YAmmD6O1UHbtftGhA6wwzT4AymX9bS498PenGGyVevnMZ3xqmc5UzPzKQVNKE-RcIYrE_M_cFsJcwzZlKZsoy0sGPbKbLUeG5p8H9HRhd3Maq-PR';
        $key = 'AAAA-FOe9TU:APA91bEuN67TuM0VMlmTNk0E_tKEYFcDN_ka0Dj1CnKrctFz5NpxJVQqb3gO69Wi53HIjE-hLUVwXgGNXhoIOcohgfJztP-k4lsYSYJmK6VmihJds9ocdtOnleqBRtxt2MVsJ2M3UWAV';

        $fields = array
        (
            "registration_ids" => (array)$tokens,
            "priority" => 10,
            'data' => [
                'title' => $title,
                'body' => $msg,
                'new_offer' => $new_offer,
                'department_id' => $department_id,
                'chat' => $chat,
                'uber_chat' => $uber_chat,
                'wallet' => $wallet,
                'type' => (string)$type,
                'order_id' => $order_id,
                'trip_details' => $trip,
                'trip_id' => $trip_id,
                'confirm_accept_order' => $confirm_accept_order,
                'icon' => 'myIcon',
                "sound" => "notificaitonSound.mpeg"]
        ,
            'notification' => [
                'title' => $title,
                'body' => $msg,
                'new_offer' => $new_offer,
                'department_id' => $department_id,

                'chat' => $chat,
                'uber_chat' => $uber_chat,
                'wallet' => $wallet,
                'type' => (string)$type,
                'order_id' => $order_id,
                'trip_details' => $trip,
                'trip_id' => $trip_id,
                'confirm_accept_order' => $confirm_accept_order,
                'icon' => 'myIcon',
                "sound" => "notificaitonSound.mpeg"],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' . $key
        );
        //////
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // $result = curl_exec($ch);
        // curl_close($ch);
        // return $result;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        // dd($result);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public static function sendNotify($tokens, $title = "hello", $msg = "helo msg", $type = 1,
                                      $is_captin = 0, $new_offer = null, $chat = null, $wallet = null,
                                      $order_id = null, $confirm_accept_order = null, $trip = null,
                                      $uber_chat = null, $trip_id = null)
    {
//type >>>> 0=>new order , get lower offer  >notifications go for delegate
//type >>>> 1=>change status , chat     >notifications go for user & delegate
//type >>>> 3=>user accept offer    >notifications go for delegate

//type >>>> 0=>new offer    >notifications go for user
//type >>>> 4=>chat with admin    >notifications go for user

//type >>>> 5 for user uber chat

        //$key = 'AAAAoYnj12I:APA91bGgL-NA8vEODyDSXd1tXvU6YAmmD6O1UHbtftGhA6wwzT4AymX9bS498PenGGyVevnMZ3xqmc5UzPzKQVNKE-RcIYrE_M_cFsJcwzZlKZsoy0sGPbKbLUeG5p8H9HRhd3Maq-PR';
        $key = 'AAAA-FOe9TU:APA91bEuN67TuM0VMlmTNk0E_tKEYFcDN_ka0Dj1CnKrctFz5NpxJVQqb3gO69Wi53HIjE-hLUVwXgGNXhoIOcohgfJztP-k4lsYSYJmK6VmihJds9ocdtOnleqBRtxt2MVsJ2M3UWAV';

        $fields = array
        (
            "registration_ids" => (array)$tokens,
            "priority" => 10,
            'data' => [
                'title' => $title,
                'body' => $msg,
                // 'new_offer' => $new_offer,
                // 'chat' => $chat,
                // 'uber_chat' => $uber_chat,
                // 'wallet' => $wallet,
                // 'type' => (string)$type,
                'order_id' => $order_id,
                // 'trip_details' => $trip,
                // 'trip_id' => $trip_id,
                // 'confirm_accept_order' => $confirm_accept_order,
                'icon' => 'myIcon',
                'sound' => 'mySound'
            ]
        ,
            'notification' => [
                'title' => $title,
                'body' => $msg,
                // 'new_offer' => $new_offer,
                // 'chat' => $chat,
                // 'uber_chat' => $uber_chat,
                // 'wallet' => $wallet,
                // 'type' => (string)$type,
                'order_id' => $order_id,
                // 'trip_details' => $trip,
                // 'trip_id' => $trip_id,
                // 'confirm_accept_order' => $confirm_accept_order,
                'icon' => 'myIcon',
                'sound' => 'mySound'
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        // dd($fields);
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' . $key
        );
        //////
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // $result = curl_exec($ch);
        // curl_close($ch);
        // return $result;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        // dd($result);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public static function sendNotifyTo($jwt, $title, $body, $type, $attribute_id,$department_id, $userType)
    {
        /*
        0=>offer
          1=>trip
          2=>chat-delegates
          3=>home redirect if user or delegate cancelled
          4=>admin notifications
          5=> chat uber
        */
        if($userType=='shops'){
            $shop=Admin::where('jwt',$jwt)->select('notification_flag')->first();
           if($shop->notification_flag==1) {
               $notificationFlag=false;
                }
           else{
               $notificationFlag=true;

           }
        }
        $beamsClient = new PushNotifications(array(

//            "instanceId" => "4b9224fd-ac88-4e17-86bf-65c347bc0fbd",
//            "secretKey" => "9ADC639AFE236794A06B8927D5FA6B28FA2E8A4E968E13A8556A5C9F8CF9548B",
            "instanceId" => config('services.Beams.Beams_Instance_Id'),
            "secretKey" => config('services.Beams.Beams_Secret_key')
        ));
//        if($userType=='user'){
//            $userID=array("users-".$jwt);
//        }
//        else{
//            $userID=array("drivers-".$jwt);
//
//        }
        $userID = $userType . '-' . $jwt;
//        $userID='drivers'.'-'.'0dOsZcVVRQzXYg6rTVz7QwOI71636377221';
        $publishResponse = $beamsClient->publishToUsers(
            array($userID),
            array(
                "fcm" => array(
                    "data" => array(
                        "title" => $title,
                        "body" => $body,
                        "type" => $type,
                        "attribute_id" => $attribute_id,
                        "department_id" =>(int) $department_id
                    ),

                ),
                "apns" => array("aps" => array(
                    "alert" => array(
                        "title" => $title,
                        "body" => $body,


                    ),
                    "sound" => "default",
                    "badge" => 0,

                    "type" => $type,
                    "attribute_id" => $attribute_id,
                    "department_id" => (int)$department_id



                )),
                "web" => array(
                    "time_to_live"=>3600,
                    "notification" => array(
                    "title" => $title,
                    "body" => $body,
                    "attribute_id" => $attribute_id,
                    "department_id" => (int)$department_id,
                    "icon"=>"https://amrk1.my-staff.net/front/assets/img/favicon.png",
                    "badge"=>"https://amrk1.my-staff.net/front/assets/img/favicon.png",
                    "deep_link"=>route('orders.index'),
                    "sound"=>"default",
                    "hide_notification_if_site_has_focus"=>isset($notificationFlag)?$notificationFlag:false

                ))
            ));
        return $publishResponse;

    }

    public static function sendNotifyFor($title, $body, $type, $interest)
    {
        $beamsClient = new PushNotifications(array(

            "instanceId" => config('services.Beams.Beams_Instance_Id'),
            "secretKey" => config('services.Beams.Beams_Secret_key')
        ));
        $publishResponse = $beamsClient->publishToInterests(
            array($interest),
            array(
                "fcm" => array(
                    "data" => array(
                        "title" => $title,
                        "body" => $body,
                        "type" => $type,
                    ),

                ),
                "apns" => array("aps" => array(
                    "alert" => array(
                        "title" => $title,
                        "body" => $body,


                    ),
                    "sound" => "default",
                    "badge" => 0,

                    "type" => $type,


                )),
            ));
        return $publishResponse;

    }

    function getCreatedAtAttribute()
    {
        //return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
        return Carbon::parse($this->attributes['created_at'])->format('d M Y g:i A');
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $img_name = time() . uniqid() . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('/uploads/notifications/'), $img_name);
            $this->attributes['image'] = $img_name;
        }
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('/uploads/notifications/' . $value);
        } else {
            return asset('/default.png');
        }
    }

    public function user($user_id)
    {
        return User::whereId($user_id)->first();
    }

    public function delegate($user_id)
    {
        return Delegate::whereId($user_id)->first();
    }

    public function driver($user_id)
    {
        return Driver::whereId($user_id)->first();
    }

    public function shop($user_id)
    {
        return Shop::whereId($user_id)->first();
    }
//    public static function sendNotifyToUsers($jwt,$title,$body,$type,$trip_id){
//        $beamsClient = new \Pusher\PushNotifications\PushNotifications(array(
//
////            "instanceId" => "4b9224fd-ac88-4e17-86bf-65c347bc0fbd",
////            "secretKey" => "9ADC639AFE236794A06B8927D5FA6B28FA2E8A4E968E13A8556A5C9F8CF9548B",
//            "instanceId" => config('services.Beams.Beams_Instance_Id'),
//            "secretKey" => config('services.Beams.Beams_Secret_key')
//        ));
//
//        $publishResponse = $beamsClient->publishToUsers(
//            array("users-".$jwt),
//            array(
//                "fcm" => array(
//                    "data" => array(
//                        "title" => $title,
//                        "body" => $body,
//                        "type"=>$type,
//                        "trip_id"=>$trip_id
//                    ),
//
//                ),
//                "apns" => array("aps" => array(
//                    "alert" => array(
//                        "title" => $title,
//                        "body" => $body,
//
//
//
//                    ),
//                    "sound" => "default",
//
//                    "type"=>$type,
//                    "trip_id"=>$trip_id
//
//
//                )),
//            ));
//        return $publishResponse;
//
//    }

}
