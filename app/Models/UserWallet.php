<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class UserWallet extends Model
{
//    use Notifiable;
//    use SoftDeletes;


    protected $table = 'user_wallets';

    protected $fillable = [
        'user_id','old_value','new_value','description'
    ];

    protected $hidden = [
        'updated_at','deleted_at',
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    

}
