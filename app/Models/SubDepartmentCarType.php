<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubDepartmentCarType extends Model
{
    use Notifiable;


    protected $table = 'sub_departments_car_types';

    protected $fillable = [
        'car_type_id','sub_department_id'
    ];
    protected $hidden = [
        'updated_at','deleted_at',
    ];

//    public function departemnt(){
//        return $this->belongsTo(Department::class , "department_id");
//    }

//    public function setImageAttribute($value)
//    {
//        if($value) {
//            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
//            $value->move(public_path('/uploads/sub_departments/'),$img_name);
//            $this->attributes['image'] = $img_name ;
//        }
//
//    }
//
//    public function getImageAttribute($value)
//    {
//        if($value)
//        {
//            return asset('/uploads/sub_departments/'.$value);
//        }else{
//            return asset('/default.png');
//        }
//    }



}
