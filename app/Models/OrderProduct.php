<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class OrderProduct extends Model
{
    use Notifiable;
    //use SoftDeletes;


    protected $table = 'order_products';

    protected $fillable = [
        'order_id','product_id','quantity','description'
    ];

    protected $hidden = [
        'deleted_at', 'updated_at'
    ];
    protected $appends = ['product_name','product_name_en','product_image','price_after'];

    public function getProductNameAttribute()
    {
        if(request()->header('lang')){
            if(request()->header('lang') == "ar"){
                return $this->product()->first()->name_ar;
            }else{
                return $this->product()->first()->name_en;
            }
        }
//        dd($this->product()->first());
        return $this->product()->first()?$this->product()->first()->name_en:"";
    }
    public function getProductNameEnAttribute()
    {
                return $this->product()->first()->name_en;

    }
    public function getProductImageAttribute()
    {
        return $this->product()->first()? $this->product()->first()->image:"";
    }
    public function getPriceAfterAttribute()
    {
        $price=0;
        if(count($this->variations)>0)
            foreach ($this->variations as $variation ) {
                foreach ($variation->options as $option) {
                    $price += $option->price;
                }
            }
        if($price>0){
            return "$price";
        }
        else{
          return $this->product()->first()?$this->product()->first()->price_after:"";
        }

    }
    /////

    public function order_product_variations()
    {
        return $this->hasMany(OrderProductVariation::class, 'order_product_id');
    }
    public function variations()
    {
        return $this->hasMany(OrderProductVariation::class, 'order_product_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
        function getDescriptionAttribute()
    {
        $lang=app()->getLocale();
       return $this->product()->first()?$this->product()->select('description_'.$lang.' as description')->first()->description:'';
    }


//    public function order()
//    {
//        return $this->belongsTo(Order::class, 'user_id');
//    }
//
//    function getCreatedAtAttribute()
//    {
//        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
//    }
//
//    public function getAcceptAttribute($value)
//    {
//        //return  Carbon::parse($this->attributes['accept'])->format('D,d M ,h:m a');
//        if($value)
//            return Carbon::parse($this->attributes['accept'])->translatedFormat('l jS F, h:m a');
//        else
//            return "";
//    }
//
//    public function getOnWayAttribute($value)
//    {
//        //return Carbon::parse($this->attributes['on_way'])->format('D,d M ,h:m a');
//        if($value)
//            return Carbon::parse($this->attributes['on_way'])->translatedFormat('l jS F, h:m a');
//        else
//            return "";
//    }
//
//    public function getFinishedAttribute($value)
//    {
//        if($value)
//            return Carbon::parse($this->attributes['finished'])->translatedFormat('l jS F, h:m a');
//        else
//            return "";
//    }

}
