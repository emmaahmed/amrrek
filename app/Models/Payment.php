<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=['attribute_id', 'attribute_type', 'amount','transaction_id', 'status', 'type'];
    public function attribute()
    {
        return $this->morphTo('attribute_type');
    }

}
