<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ShopDay extends Model
{
    use Notifiable;


    protected $table = 'shops_days';

    protected $fillable = [
        'shop_id','day_id','from','to'
    ];

    protected $hidden = [
         'deleted_at','created_at', 'updated_at','shop_id','user_id'
    ];

    protected $appends = ['day','name_en'];

    public function shop()
    {
        return $this->belongsTo(shop::class,"shop_id");
    }

    public function day()
    {
        return $this->belongsTo(Day::class,"day_id");
    }

    public function getDayAttribute()
    {
        if(request()->header('lang') == 'ar'){
            $data = $this->day()->first()->name;
        }else{
            $data = $this->day()->first()->name_en;
        }
        //unset($this->day);
        return $data;
    }

//    public function getIsOpenAttribute()
//    {
//        $data = $this->getNameEnAttribute() == Carbon::now()->format('l') ? $this->is_open = true : $this->is_open= false;
//        //unset($this->day);
//        return $data;
//    }

    public function getNameEnAttribute()
    {
        return $this->day()->first()->name_en;
    }

    public function getTodayAttribute()
    {
        $data = $this->getNameEnAttribute() == Carbon::now()->format('l') ? $this->today = true : $this->today= false;

        return $data;
        //$dayOfWeek = date("l", strtotime($_GET['b_date']));
    }

    public function getFromAttribute()
    {
        if(request()->lang == "ar")
            return  Carbon::parse($this->attributes['from'])->isoFormat('h:i a');
        return  Carbon::parse($this->attributes['from'])->format('h:i a');

    }

    public function getToAttribute()
    {
        if(request()->lang == "ar")
            return  Carbon::parse($this->attributes['to'])->isoFormat('h:i a');
        return  Carbon::parse($this->attributes['to'])->format('h:i a');
    }




}
