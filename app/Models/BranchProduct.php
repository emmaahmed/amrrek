<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BranchProduct extends Model
{
    use Notifiable;


    protected $table = 'branches_products';

    protected $fillable = [
        //0=>not_active, 1=>active
        'status',
        'branch_id','product_id'
    ];


    protected $hidden = [
         'deleted_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select('id','name','image');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }


}
