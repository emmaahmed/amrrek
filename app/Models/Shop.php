<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class Shop extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    protected $table = 'shops';

    protected $fillable = [
        'commercial_register','comercial_number',
        'bank_name',
        'bank_account',
        'parent_id','department_id','name','name_en','email','phone','cover_image','category_id','country_id',
        'jwt','rate','lat','lng','address','city_name','password','token', 'image',
//        'description',
        'expiry_date',
        'description_ar',
        'description_en','deleted_at'
    ];

    protected $hidden = [
        'active', 'deleted_at','created_at', 'updated_at','password'
    ];

    public function moderator(){
        return $this->hasOne(Admin::class,'shop_id');
    }
    public function branches(){
        return $this->hasMany(Shop::class,'parent_id','id');
    }
    public function parentShop(){
        return $this->belongsTo(Shop::class,'parent_id','id');

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function rates()
    {
        return $this->hasMany(ShopRate::class, 'shop_id');
    }

    public function menus()
    {
        return $this->hasMany(Menu::class, 'shop_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'shop_id');
    }
    public function allOrders(){
        return $this->hasMany(Order::class);
    }

    public function days()
    {
        return $this->hasMany(ShopDay::class,"shop_id");
    }
    public function promoCode(){
        return $this->hasOne(PromoCode::class,'shop_id','id');
    }

    public function setPasswordAttribute($value)
    {
        if($value) {
            $this->attributes['password'] = Hash::make($value);
        }else{
            $this->attributes['password'] = Hash::make('123456');
        }

    }

    public function setImageAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/images/'),$img_name);
            $this->attributes['image'] = $img_name ;
        }

    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/images/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function getRateAttribute($value)
    {
        if($value)
        {
            return $value;
        }else{
            return "";
        }
    }

    public function setCoverImageAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/coverimages/'),$img_name);
            $this->attributes['cover_image'] = $img_name ;
        }

    }

    public function getCoverImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/coverimages/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function getDescriptionArAttribute($value)
    {
        if($value)
        {
            return $value;
        }else{
            return "";
        }
    }

    public function getQuantityAttribute($value)
    {
        if($value)
        {
            return (int)$value;
        }else{
            return 0;
        }
    }
    public static function getDistance($mylat,$mylng,$radius,$model,$shop_id)
    {
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = Shop::whereId($shop_id)->

        selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [5000])

            //            ->with('delegate_count')
            ->first();

        if($datainradiusrange) {
            return number_format($datainradiusrange->distance, 2, '.', '') ;
        }else{
            return "0";
        }
    }

    public static function filterbylatlng($mylat,$mylng,$radius,$model,$delegate_id=null,$user_country_id=188)
    {
        $lang = request()->header('lang');
        if(!$lang)
            $lang="ar";
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = Shop::select('id','parent_id','name','lat','lng','address','image','cover_image',
            'description_'.$lang.' as description','rate','country_id')
                 //       ->whereHas('products')
                 ->where(function ($query){
                     $query->whereHas('products')
                         ->orWhereHas('parentShop',function($q){
                             $q->whereHas('products');
                         });

                 })

            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])

            ->where('active', 1)
            ->where('country_id', $user_country_id)
            ->where('suspend', 0)
            ->orderBy('distance')




            //            ->with('delegate_count')
            ->paginate(20)
        ;
        $dist = request()->header('lang') == "ar" ? "كم" : "Km";
$data = [];
        foreach ($datainradiusrange as $a){

            $a->distance = number_format($a->distance, 2, '.', '') . " "  . $dist;
            $a->delegate_count = $a->delegate_count();
            $a->orders_count = $a->orders_count();
            if(isset($delegate_id)){
                $is_subscribed = ShopDelegate::where('delegate_id',$delegate_id)
                    ->where('status',1)
                    ->where('shop_id',$a->id)
                    ->first();
                $a->is_delegate_subscribe = isset($is_subscribed) ? 1 : 0;
            }
            $parent_products_count = 0;
            $parent_shop = Shop::whereId($a->parent_id)->first();
            if(isset($parent_shop)){
                $parent_products_count = Product::where('shop_id',$parent_shop->id)->count();
            }
            if($a->products_count > 0 || $parent_products_count > 0){
                array_push($data,$a);
            }
        }
//        return $data;
        return $datainradiusrange;
    }

    public static function filterbylatlngbySearchKey($mylat,$mylng,$radius,$model,$delegate_id=null,$searchKey=null,$user_country_id=2)
    {
        $lang = request()->header('lang');
        if(!$lang)
            $lang="ar";
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = Shop::select('id','parent_id','name','lat','lng','address','image','cover_image',
            'description_'.$lang.' as description','rate','country_id')
            ->selectRaw("{$haversine} AS distance")
            ->orderBy('distance','asc')
            ->whereRaw("{$haversine} < ?", [500000000000000000000])
            ->where('active', 1)
            ->where('country_id', $user_country_id)
            ->where('name', 'like', '%'.$searchKey.'%')
            //->orWhere('description', 'like', '%'.$searchKey.'%')
            //->where('verified', 1)
            ->where('suspend', 0)
            ->orderBy('distance')
            ->where(function ($query){
                $query->whereHas('products')
                    ->orWhereHas('parentShop',function($q){
                        $q->whereHas('products');
                    });

            })

            ->paginate(10);
        $dist = request()->header('lang') == "ar" ? "كم" : "Km";
$data = [];
        foreach ($datainradiusrange as $a){
            $a->distance = number_format($a->distance, 2, '.', '') . " "  . $dist;
            $a->delegate_count = $a->delegate_count();
            $a->orders_count = $a->orders_count();
            if(isset($delegate_id)){
                $is_subscribed = ShopDelegate::where('delegate_id',$delegate_id)
                    ->where('shop_id',$a->id)
                    ->where('status',1)
                    ->first();
                $a->is_delegate_subscribe = isset($is_subscribed) ? 1 : 0;
            }
            $parent_products_count = 0;
            $parent_shop = Shop::whereId($a->parent_id)->first();
            if(isset($parent_shop)){
                $parent_products_count = Product::where('shop_id',$parent_shop->id)->count();
            }
            if($a->products_count > 0 || $parent_products_count > 0){
                array_push($data,$a);
            }
        }
//        return $data;
        return $datainradiusrange;
    }

    public function category()
    {
        return $this->belongsTo(Category::class,"category_id");
    }

    public function delegate_count()
    {
        return $this->hasMany(ShopDelegate::class,"shop_id")
            ->count();
    }

    public function orders_count()
    {
        return $this->hasMany(Order::class,"shop_id")
            ->where('delegate_id',NULL)
            ->count();
    }

    public function waiting_orders_count()
    {
        $shop_id = request()->shop_id;
        $orders = Order::where('shop_id',$shop_id)
            ->where('delegate_id',NULL)
            ->pluck('id');
        return sizeof($orders);
//        $Waiting_order = OrderStatus::whereIn('delegate_id',$orders)
//        return $this->hasMany(Order::class,"shop_id")
//            ->where('delegate_id',NULL)
//            ->whereIn('delegate_id',$orders)
//            ->count();
    }

    public static function filterbylatlngByCatId($mylat,$mylng,$radius,$model,$cat_id,$user_country_id=2)
    {
        $lang = request()->header('lang');
        if(!$lang)
            $lang="ar";
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = Shop::select('id','parent_id','name','lat','lng','address','image','cover_image','category_id',
            'description_'.$lang.' as description','rate','country_id')
            ->selectRaw("{$haversine} AS distance")
            ->orderBy('distance','asc')
            ->whereRaw("{$haversine} < ?", [$radius])
            ->where('category_id',$cat_id)
            ->where('country_id', $user_country_id)
            ->where('active', 1)
            //->where('verified', 1)
            ->where('suspend', 0)
            ->orderBy('distance')
            ->where(function ($query){
                $query->whereHas('products')
                    ->orWhereHas('parentShop',function($q){
                        $q->whereHas('products');
                    });

            })

            ->paginate(20);
        $dist = request()->header('lang') == "ar" ? "كم" : "Km";
$data = [];
        foreach ($datainradiusrange as $a){
            $a->distance = number_format($a->distance, 2, '.', '') . " " . $dist;

            $parent_products_count = 0;
            $parent_shop = Shop::whereId($a->parent_id)->first();
            if(isset($parent_shop)){
                $parent_products_count = Product::where('shop_id',$parent_shop->id)->count();
            }
            if($a->products_count > 0 || $parent_products_count > 0){
                array_push($data,$a);
            }
        }
//        return $data;
        return $datainradiusrange;
    }

    public static function filterbylatlngbySearchKeyForUser($mylat,$mylng,$radius,$model,$delegate_id=null,$searchKey=null,$user_country_id=2)
    {
        $lang = request()->header('lang');
        if(!$lang)
            $lang="ar";
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";
        $datainradiusrange = Shop::orderBy('distance')
            ->select('id','parent_id','name','lat','lng','address','image','cover_image',
                'description_'.$lang.' as description','rate','country_id')
            ->selectRaw("{$haversine} AS distance")
            ->orderBy('distance','asc')
            ->whereRaw("{$haversine} < ?", [$radius])
            ->where('active', 1)
            ->where('country_id', $user_country_id)

            ->where('name', 'like', '%'.$searchKey.'%')
            //->where('verified', 1)
            ->where('suspend', 0)
            ->withCount('products')


//            ->with('delegate_count')
            ->paginate(20);
        $dist = request()->header('lang') == "ar" ? "كم" : "Km";
$data = [];
        foreach ($datainradiusrange as $a){
            $a->distance = number_format($a->distance, 2, '.', '') . " "  . $dist;
            $a->delegate_count = $a->delegate_count();
            $a->orders_count = $a->orders_count();
            if(isset($delegate_id)){
                $is_subscribed = ShopDelegate::where('delegate_id',$delegate_id)
                    ->where('shop_id',$a->id)
                    ->where('status',1)
                    ->first();
                $a->is_delegate_subscribe = isset($is_subscribed) ? 1 : 0;
            }
            if($a->products_count > 0){
                array_push($data,$a);
            }
        }
        return $data;
        return $datainradiusrange;
    }



}
