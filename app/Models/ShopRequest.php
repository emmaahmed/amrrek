<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class ShopRequest extends Model
{
    use Notifiable;
    //use SoftDeletes;


    protected $table = 'shop_requests';

    protected $fillable = [
        'active','accept',
        'name','commercial_register','email',
        'phone','category_id','tax_num',
        'tax_img','city','moderator_name',
        'moderator_email','moderator_phone','moderator_title',
        'bank_name','bank_account',
        'lat','lng','address','password',
        'description_ar','description_en',
        'cover_image','image'
    ];

    protected $hidden = [
        'active','created_at', 'updated_at'
    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }


    public function setTaxImgAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/images/'),$img_name);
            $this->attributes['tax_img'] = $img_name ;
        }

    }

    public function getTaxImgAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/images/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/images/'),$img_name);
            $this->attributes['image'] = $img_name ;
        }

    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/images/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function setCoverImageAttribute($value)
    {
        if(is_file($value)) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/coverimages/'),$img_name);
            $this->attributes['cover_image'] = $img_name ;
        }

    }

    public function getCoverImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/coverimages/'.$value);
        }else{
            return asset('/default.png');
        }
    }

    public function setPasswordAttribute($value)
    {
        if($value) {
            $this->attributes['password'] = Hash::make($value);
        }else{
            $this->attributes['password'] = Hash::make('123456');
        }

    }



}
