<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInvoice extends Model
{
    protected $table = 'order_invoices';

    protected $fillable = [
        'order_id', 'total', 'sub_total', 'tax', 'offer','tax_percentage', 'confirmed'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
