<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class ShopModerator extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;


    protected $table = 'shop_moderators';

    protected $fillable = [
        'name','email','phone','type','shop_id',
        'jwt','image','password','active','token'
    ];

    protected $hidden = [
        'active', 'deleted_at','created_at', 'updated_at'
    ];

    public function shop(){
        return $this->belongsTo(Shop::class,"shop_id");
    }



    public function setPasswordAttribute($value)
    {
        if($value) {
            $this->attributes['password'] = Hash::make($value);
        }else{
            $this->attributes['password'] = Hash::make('123456');
        }

    }

    public function setImageAttribute($value)
    {
        if($value) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/shops/images/'),$img_name);
            $this->attributes['image'] = $img_name ;
        }

    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/shops/images/'.$value);
        }else{
            return asset('/default.png');
        }
    }


}
