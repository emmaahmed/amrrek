<?php

namespace App\Models;

use App\Modules\Website\Models\WebsiteDepartment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Department extends Model
{
    use Notifiable;


    protected $table = 'departments';

    protected $fillable = [
        'name', 'image','name_ar','name_en'
    ];
    protected $hidden = [
        'updated_at','deleted_at',
    ];
//protected $appends=['name'];
    public function sub_departments(){
//        return $this->hasMany(SubDepartment::class , "department_id");
        $lang = request()->header('lang');
        if(!$lang)
            $lang="ar";

        return $this->hasMany(SubDepartment::class , "department_id");
    }


    public function setImageAttribute($value)
    {
        if($value) {
            $img_name = time().uniqid().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('/uploads/departments/'),$img_name);
            $this->attributes['image'] = $img_name ;
        }

    }

    public function getImageAttribute($value)
    {
        if($value)
        {
            return asset('/uploads/departments/'.$value);
        }else{
            return asset('/default.png');
        }
    }
    public function websiteDepartment(){
        return $this->hasOne(WebsiteDepartment::class,'department_id','id');

    }
//    public function getNameAttribute($key)
//    {
//        return $this->attributes['name_'.app()->getLocale()];
//    }


}
