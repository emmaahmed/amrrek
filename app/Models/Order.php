<?php

namespace App\Models;

use App\Http\Controllers\Admin\OrderController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;
    //use SoftDeletes;


    protected $table = 'orders';

    protected $fillable = [
        'counter','order_number','department_id','user_id','title','notes',
        'in_lat','in_lng','in_address','out_lat','out_lng','out_address',
        'last_lat','last_lng','last_address','last_city_name',
        'delivery_time','promo_id','distance','confirm_code','delegate_id',
        'in_city_name','out_city_name','counter','offer_id','shop_id','confirm_accept',
        'country_id','total_cost' ,'sub_department_id','sub_total','tax','confirm_order_count','payment_method'
    ];

    protected $hidden = [
        'active', 'deleted_at', 'updated_at','user_id'
    ];

    protected $appends = ['currency'];
    public function promo(){
        return $this->belongsTo(PromoCode::class,'promo_id','id');
    }
    public function sub_department(){
//        $lang = request()->header('lang');
//        if(!$lang)
//            $lang="ar";
        return $this->belongsTo(SubDepartment::class , "sub_department_id",'id');
    }

    public function order_products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id','id');
    }
    public function branches()
    {
        return $this->hasMany(Shop::class, 'parent_id','id');
    }
    public function invoice()
    {
        return $this->hasOne(OrderInvoice::class);
    }

    public function order_status()
    {
        return $this->hasOne(OrderStatus::class, 'order_id','id');
    }

    public function order_imagess()
    {
        return $this->hasMany(OrderImage::class, 'order_id');
    }

    public function order_images()
    {
        return $this->hasMany(OrderImage::class, 'order_id');
    }

    public function order_image()
    {
        return $this->hasOne(OrderImage::class, 'order_id');
    }

    public function finished_orders()
    {
        return $this->hasOne(OrderStatus::class, 'order_id')
            ->where('accept','!=', "")
            ->where('on_way','!=', "")
            ->where('finished','!=', "");
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select('id','name','image','rate','token','email','phone','jwt');
    }

    public function delegate()
    {
        return $this->belongsTo(Delegate::class, 'delegate_id')
            ->select('id','f_name','l_name','image','lat','lng','email','phone','rate','no_of_trips','jwt','national_id');
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id')
            ->select('id','name','image','lat','lng','name_en');
    }

    public function google_shop(){
        return $this->hasOne(GoogleShop::class, 'order_id');
    }

    public function offer()
    {
        return $this->hasOne(OrderOffer::class, 'order_id');
    }

    public function order_offer()
    {
        return $this->hasMany(OrderOffer::class, 'order_id')
            ->select('offer','distance');
    }

    function getCreatedAtAttribute()
    {
        // return Carbon::parse($this->attributes['created_at'])->translatedFormat('jS F Y, h:i a');
        $date = Carbon::parse($this->attributes['created_at'])->locale(app()->getLocale());
        return $date->translatedFormat('jS F Y, h:i a');
    }

    public static function filterbylatlng($mylat,$mylng,
                                          $radius,$model,
                                          $delegate_id=null,$near_orders=null,
                                          $order_status=null,$country_id,$refuded_delegate_id=null){

        $subscribed_shops=null;
        if($near_orders != null && $near_orders == 1){
            $subscribed_shops = ShopDelegate::where('delegate_id',$delegate_id)->pluck('shop_id');
        }

        //$order_status==>> 0=privious , 1=current
        if(isset(request()->shop_id) && request()->shop_id !=null ){
          $orders_ids=OrderChangedDelegate::where([
                'delegate_id' => $refuded_delegate_id,
            ])->pluck('order_id')->toArray();
// dd($orders_ids);
            $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.in_lat))
                           * cos(radians($model.in_lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.in_lat))))";
            $datainradiusrange = Order::orderBy('id','desc')
                            ->whereNotIn('id',$orders_ids)

                ->where('shop_id',request()->shop_id)
                ->where('delegate_id',$delegate_id)
                ->where('country_id',$country_id)
                ->select('id','order_number','total_cost','title','notes','delivery_time','user_id','country_id','shop_id')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->with(["order_status" => function ($query) use($order_status) {
                    if($order_status == 0){
                        $query->where('finished','!=',NULL);
                    } elseif($order_status == 1){
                        $query->where('on_way','!=',NULL);
                    }

                    }])
                ->with('order_status')
                ->with('user')
                ->with('offer')
                ->with('shop')
            //                 ->whereHas('delegate',function($query)use($delegates_id){
            //     $query->whereNotIn('delegates.id',$delegates_id);
            // })
            ->where( 'created_at', '>', Carbon::now()->subDay())

                ->orderBy('created_at')
                ->get();
        }
        if(isset(request()->department_id) && request()->department_id !=null ){
            $haversine = "(6371 * acos(cos(radians($model.in_lat))
                           * cos(radians($model.in_lat))
                           * cos(radians($model.in_lng)
                           - radians($model.in_lng))
                           + sin(radians($model.in_lat))
                           * sin(radians($model.in_lat))))";
            if(!empty($subscribed_shops)){
                $datainradiusrange = Order::orderBy('id','desc')
                    ->where('department_id',request()->department_id)
                    ->where('delegate_id',$delegate_id)
                    ->whereIn('shop_id',$subscribed_shops)
                    ->select('id','order_number','total_cost','title','notes','delivery_time','user_id','shop_id','country_id')
                    ->selectRaw("{$haversine} AS distance")
                    ->whereRaw("{$haversine} < ?", [$radius])
                    ->with(["order_status" => function ($query) use($order_status) {
//                        if($order_status == 0){
//                            $query->where('finished','!=',NULL);
//                        } elseif($order_status == 1){
//                            $query->where('accept',NULL);
//                        }

                    }])
                    ->with('user')
                    ->with('offer')
//                    ->with('order_status')
                    ->where( 'created_at', '>', Carbon::now()->subDay())

                    ->orderBy('created_at')

                    ->get();
            }else{
                $datainradiusrange = Order::orderBy('id','desc')
                    ->where('department_id',request()->department_id)
                    ->where('delegate_id',$delegate_id)
                    ->select('id','order_number','total_cost','title','notes','delivery_time','user_id','shop_id','country_id')
                    ->selectRaw("{$haversine} AS distance")
                    ->whereRaw("{$haversine} < ?", [$radius])
                    ->with(["order_status" => function ($query) use($order_status) {
//                        if($order_status == 0){
//                            $query->where('finished','!=',NULL);
//                        } elseif($order_status == 1){
//                            $query->where('accept',NULL);
//                        }

                    }])
                    ->with('user')
                    ->with('offer')
                    ->with('order_status')
                    ->where( 'created_at', '>', Carbon::now()->subDay())

                    ->orderBy('created_at')

                    ->get();
            }


        }

        return $datainradiusrange;
    }

    public static function filterDelegates($mylat,$mylng,$radius,$model,$delegate_id=null,$near_orders=null,$order_status=null,$user_country_id=188,$department_id=null,$sub_department_id=null)
    {
        global $type;
        global $data;
        $data = [];
        if($department_id == 5){
            //heavey machens
            $type = 1;
        }else {
            //normal delegates
            $type = 0;
        }

        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.lat))
                           * cos(radians($model.lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.lat))))";

//        $haversine = "(6371 * acos(cos(radians($model.lat))
//                           * cos(radians($model.lat))
//                           * cos(radians($model.lng)
//                           - radians($model.lng))
//                           + sin(radians($model.lat))
//                           * sin(radians($model.lat))))";
        $data = [];
        $datainradiusrange = Delegate::orderBy('id','desc')
            ->where('online',1)
            ->where('busy',0)
            ->where('type',$type)
            ->where('notification_flag',1)
            ->where('country_id',$user_country_id)
//            ->whereHas('subscription',function ($query){
//                $query->where('expire_at','>',Carbon::now()->toTimeString());
//            })
//            ->where('department_id',request()->department_id)
//            ->where('delegate_id',$delegate_id)
//            ->whereIn('shop_id',$subscribed_shops)
            ->select('id','lat','lng','token','car_level','near_orders','jwt')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
//            ->with('order_status')
//            ->with('user')
            ->get();
        if($department_id == 5 &&
           $sub_department_id &&
            $sub_department_id > 0){
              $SubDepartmentCarType = SubDepartmentCarType::where('sub_department_id',$sub_department_id)
                ->pluck('car_type_id')->toArray();

            $datainradiusrange->whereIn('car_level',$SubDepartmentCarType);
            $data = $datainradiusrange;
        }
        if($department_id == 2 ){
            foreach ($datainradiusrange as $d){
                if($d->near_orders == 0){
                    $shop_delegates = ShopDelegate::where('shop_id',request()->shop_id)
                        ->pluck('delegate_id')->toArray();
                    if(in_array($d->id, $shop_delegates)){
                        array_push($data,$d);
                    }
                }else{
                    array_push($data,$d);
                }
            }
        }
        else{$data=$datainradiusrange;}

        //--start near orders
//        foreach ($datainradiusrange as $d){
//            if(isset(request()->shop_id)){
//                if($d->near_orders = 0){
//                    $shop_delegate = ShopDelegate::where('shop_id',request()->shop_id)
//                        ->where('delegate_id',$d->id)->first();
//                    if($shop_delegate)
//                        array_push($data,$d);
//                }
//            }else{
//                array_push($data,$d);
//            }
//
//        }
        return $data;
        //--end near orders
        //return $datainradiusrange;
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
//
//    function getCountryIdAttribute()
//    {
//        return  $this->country()->first()->name;
//    }


    function getTotalCostAttribute($value)
    {
        return  (string)$value;
    }

    function getCurrencyAttribute()
    {
        $lang = request()->header('lang');
        if(!$lang)
            $lang = 'ar';
        if($lang == 'ar')
            return "ريال";
        else
            return "Riyal";

        if(isset($this->attributes['country_id'])){
            return (string)Country::whereId($this->attributes['country_id'])->first()->currency;
        }
        //$country_id = $this->attributes['country_id'];
        $lang = request()->header('lang');
        if(!$lang)
            $lang = 'ar';
        if($lang == 'ar')
            return "ريال";
        else
            return "Riyal";
//            $country_id=2;
//

    }

    function getPromoIdAttribute($value)
    {
        return  (int)$value;
    }

    function getShopRateAttribute($value)
    {
        return  (string)$value;
    }

    function getShopCommentAttribute($value)
    {
        return  (string)$value;
    }

    function getDelegateRateAttribute($value)
    {
        return  (string)$value;
    }

    function getDelegateCommentAttribute($value)
    {
        return  (string)$value;
    }

    function getUserRateAttribute($value)
    {
        return  (string)$value;
    }

    function getUserCommentAttribute($value)
    {
        return  (string)$value;
    }

    function getOrderStatusAttribute($value)
    {
        return  (object)$value;
    }

//    function getOrderImagesAttribute($value)
//    {
//        return  (array)$value;
//    }

    public function getOrderImagesAttribute()
    {
        if ( ! array_key_exists('order_images', $this->relations)) $this->load('order_images');

        $data = ($this->getRelation('order_images')) ? [] : [];

        return $data;
    }

    public function getShopIdAttribute($value)
    {
        if($value)
            return $value;
        else
            return 0;
    }
    public function getSubDepartmentIdAttribute($value)
    {
        if($value!=null)
            return $value;
        else
            return 0;
    }

    public function getOfferIdAttribute($value)
    {
        if($value)
            return $value;
        else
            return 0;
    }

    public function getDelegateIdAttribute($value)
    {
        if($value)
            return $value;
        else
            return 0;
    }

    public function getDeliveryTimeAttribute($value)
    {
        if($value){
//            if($this->attributes['department_id'] == 5)
//                return Carbon::parse($value)->format('Y-m-d');
            return $value;
        }else{
            return "";
        }
    }

    public function getDistanceAttribute($value)
    {
        if($value)
            return $value;
        else
            return "";
            return "";
    }

    public static function filterbylatlngWaitingOrderforDelegates($mylat,$mylng,$radius,$model,$delegate_id=null,$near_orders=null,$order_status=null,$delegate_country_id,$department_id)
    {

        if($department_id!=5){
            $date= Carbon::now()->subDay();

        }
        else{
            $date = Carbon::now()->subDays(15);

        }
        $waitingOrders = OrderStatus::join('orders','orders.id','order_statuses.order_id')
            ->where('orders.created_at','>',$date)
            ->where('accept',NULL)
            ->where('cancelled',NULL)
            ->where('department_id',$department_id)
            ->pluck('order_id');

        $orders_ids=OrderChangedDelegate::where([
                'delegate_id' => $delegate_id,
            ])->pluck('order_id')->toArray();

        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.in_lat))
                           * cos(radians($model.in_lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.in_lat))))";
        $datainradiusrange = Order::orderBy('id','desc')
        ->whereNotIn('id',$orders_ids)
            ->where('department_id',$department_id)
//            ->where('country_id',$delegate_country_id)
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->whereIn('id',$waitingOrders)
            ->with('shop')
            ->with('order_status')
            ->with('user')
            ->orderBy('created_at')

            ->where('orders.created_at','>',$date)

            ->get();
//        dd($datainradiusrange);

        return $datainradiusrange;
    }

    public static function filterbylatlngWaitingOrderforDelegatesHeavy($mylat,$mylng,$radius,$model,$delegate_id=null,$near_orders=null,$order_status=null,$delegate_country_id,$department_id)
    {
        $lang = request()->header('lang');
        $haversine = "(6371 * acos(cos(radians($mylat))
                           * cos(radians($model.in_lat))
                           * cos(radians($model.in_lng)
                           - radians($mylng))
                           + sin(radians($mylat))
                           * sin(radians($model.in_lat))))";
        if(request()->type == 0){
            //current orders
            $date = Carbon::now()->subDays(15);
            $waitingOrders = OrderStatus::join('orders','orders.id','order_statuses.order_id')
                ->where('orders.created_at','>',$date)
                ->where('department_id',$department_id)
                ->pluck('order_id');

            $datainradiusrange = Order::orderBy('id','desc')
                ->where('department_id',$department_id)
                ->where('delegate_id',$delegate_id)
//            ->where('country_id',$delegate_country_id)
                ->select('*')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->whereIn('id',$waitingOrders)
                ->with('shop')
                ->with('order_status')
                ->with('order_images')
                ->with('user')
                ->with('offer')
                ->with(['sub_department'=> function($q)use($lang){
                    $q->select('id','name_'.$lang.' as name');
                }])
                ->where('created_at',Carbon::now())

                ->get();
        }
        elseif(request()->type == 1){
            //waiting orders
            $date = Carbon::now()->subDays(15);
            $waitingOrders = OrderStatus::join('orders','orders.id','order_statuses.order_id')
                ->where('orders.created_at','>',$date)
                ->whereNull('accept')
                ->where('department_id',$department_id)
                ->pluck('order_id');

            $datainradiusrange = Order::orderBy('id','desc')
                ->where('department_id',$department_id)
                ->whereNull('delegate_id')
//            ->where('country_id',$delegate_country_id)
                ->select('*')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->whereIn('id',$waitingOrders)
                ->with('shop')
                ->with('order_status')
                ->with('order_images')
                ->with('user')
                ->with('offer')
                ->with(['sub_department'=> function($q)use($lang){
                    $q->select('id','name_'.$lang.' as name');
                }])
                ->where('created_at',Carbon::now())

                ->get();
        }
        elseif(request()->type == 2){
            //previos orders
            $waitingOrders = OrderStatus::join('orders','orders.id','order_statuses.order_id')
                ->whereNotNull('finished')
                ->where('department_id',$department_id)
                ->pluck('order_id');

            $datainradiusrange = Order::orderBy('id','desc')
                ->where('department_id',$department_id)
                ->where('delegate_id',$delegate_id)
//            ->where('country_id',$delegate_country_id)
                ->select('*')
                ->selectRaw("{$haversine} AS distance")
                ->whereRaw("{$haversine} < ?", [$radius])
                ->whereIn('id',$waitingOrders)
                ->with('shop')
                ->with('order_status')
                ->with('order_images')
                ->with('user')
                ->with('offer')
                ->with(['sub_department'=> function($q)use($lang){
                    $q->select('id','name_'.$lang.' as name');
                }])
                ->where('created_at',Carbon::now())

                ->get();
        }

        $waitingOrders = OrderStatus::join('orders','orders.id','order_statuses.order_id')
            ->whereNull('finished')
            ->whereNull('accept')
            ->whereNull('cancelled')
            ->where('department_id',$department_id)
            ->pluck('order_id');
        $datainradiusrange = Order::orderBy('id','desc')
            ->where('department_id',$department_id)
            ->whereNull('delegate_id')
//            ->where('country_id',$delegate_country_id)
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->whereIn('id',$waitingOrders)
            //->with('shop')
            ->with('order_status')
            ->with('order_images')
            ->with(['sub_department'=> function($q)use($lang){
                $q->select('id','name_'.$lang.' as name');
            }])
            ->with('user')
            //->with('offer')
            ->where('created_at','>=',Carbon::now()->toTimeString())

            ->get();
//dd($datainradiusrange);
        return $datainradiusrange;
    }


}
