<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class AppSetting extends Model
{
    use Notifiable;
    //use SoftDeletes;


    protected $table = 'app_settings';

    protected $fillable = [
        'name_ar','name_en','key_name','val','department_id'
    ];

    protected $hidden = [
        'updated_at','created_at'
    ];

//    protected $appends = ['order_status','order_images'];

//    public function products()
//    {
//        return $this->hasMany(Product::class, 'menu_id');
//    }


}
