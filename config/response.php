<?php

return
    [
        'name' =>
            [
                'ar' => 'البريد الإلكتروني موجود من قبل',
                'en' => 'Name is Required',
            ],
        'email' =>
            [
                'ar' => 'البريد الإلكتروني مطلوب',
                'en' => 'Email is Required',
            ],
        'phone' =>
            [
                'ar' => 'رقم الهاتف مطلوب',
                'en' => 'Phone is Required',
            ],
        'email_exist' =>
            [
                'ar' => 'البريد الإلكتروني موجود من قبل',
                'en' => 'Email already exists',
            ],
        'phone_not_exist' =>
            [
                'ar' => 'الهاتف غير مسجل لدينا',
                'en' => 'Phone not registered',
            ],
        'phone_checked' =>
            [
                'ar' => 'تم التحقق من الهاتف, يرجي ادخال الرقم السرى',
                'en' => 'Phone verified ,please type your password',
            ],
        'phone_exist' =>
            [
                'ar' => 'الهاتف موجود من قبل',
                'en' => 'Phone already exists',
            ],
        'not_active' =>
            [
                'ar' => 'عفواً,الحساب غير مفعل بعد',
                'en' => 'Sorry,the account is not active yet',
            ],
        'waiting_admin' =>
            [
                'ar' => 'عفواً,فى انتظار موافقة الأدمن',
                'en' => 'Sorry,waiting admin to accept',
            ],
        'registered' =>
            [
                'ar' => 'تم التسجيل بنجاح,الرجاء التفعيل',
                'en' => 'Registered Successfully,please activate',
            ],
        'register_success' =>
            [
                'ar' => 'تم التسجيل بنجاح',
                'en' => 'Registered Successfully',
            ],
        'activated' =>
            [
                'ar' => 'تم التفعيل بنجاح',
                'en' => 'Activated Successfully',
            ],
        'activated_waiting' =>
            [
                'ar' => 'الهاتف مفعل الان, فى انتظار موافقة الأدمن',
                'en' => 'Phone Activated , waiting admin to accept',
            ],
        'password_changed' =>
            [
                'ar' => 'تم إعادة تعيين كلمة المرور بنجاح',
                'en' => 'Password has been reset successfully',
            ],
        'old_password' =>
            [
                'ar' => 'كلمة المرور القديمة غير صحيحة',
                'en' => 'Old Password incorrect',
            ],
        'invalid_code' =>
            [
                'ar' => 'كود غير صحيح',
                'en' => 'Invalid Code',
            ],
        'code_expired' =>
            [
                'ar' => 'انتهاء صلاحية الكود',
                'en' => 'Code Expired',
            ],
        'logged_in' =>
            [
                'ar' => 'تم الدخول',
                'en' => 'Logged In',
            ],
        'code_sent' =>
            [
                'ar' => 'تم إرسال الكود',
                'en' => 'Code Sent Successfully',
            ],
        'code_expire' =>
            [
                'ar' => 'انتهاء صلاحية الكود',
                'en' => 'Code Expire',
            ],
        'invalid_data' =>
            [
                'ar' => 'بيانات خاطئة',
                'en' => 'Invalid Data',
            ],
        'success' =>
            [
                'ar' => 'تمت العملية بنجاح',
                'en' => 'Done Successfully',
            ],
        'send' =>
            [
                'ar' => 'تم الإرسال.',
                'en' => 'Send successfully.',
            ],
        'user_accept' =>
            [
                'ar' => 'وافق المستخدم على عرضك.',
                'en' => 'User accept your offer.',
            ],
        'user_decline' =>
            [
                'ar' => 'رفض المستخدم عرضك.',
                'en' => 'User decline your offer.',
            ],
        'new_message' =>
            [
                'ar' => 'رسالة جديدة.',
                'en' => 'New Message.',
            ],
        'rate' =>
            [
                'ar' => 'تم التقيم بنجاح',
                'en' => 'Rate done successfully.',
            ],
        'already_rate' =>
            [
                'ar' => 'لا يمكن التقيم اكثر من مره.',
                'en' => 'Can not rate more than one.',
            ],
        'canceled' =>
            [
                'ar' => 'تم إلغاء الاوردر.',
                'en' => 'Order canceled.',
            ],
        'not_cancel' =>
            [
                'ar' => 'لا يمكن إلغاء الاوردر الفني فى الطريق.',
                'en' => 'Can not cancel order, worker on way.',
            ],
        'no_available_worker' =>
            [
                'ar' => 'لا يوجد فنين فى هذا النطاق.',
                'en' => 'No available worker in same area.',
            ],
        'canot_cancel' =>
            [
                'ar' => 'لا يمكن إلغاء الاوردر    .',
                'en' => 'Can not cancel order,.',
            ],
        'worker_cancel_order' =>
            [
                'ar' => 'تم إلغاء الطلب للخدمة .',
                'en' => 'Request service cancelled.',
            ],
        'worker_not_cancel' =>
            [
                'ar' => 'لا يمكنك إلغاء الطلب .',
                'en' => 'Request service canot cancelled.',
            ],
        'order_status_changed' =>
            [
                'ar' => 'لقد تم إلغاء حالة الطلب من قبل المستخدم',
                'en' => 'Sorry,‘Order status cancelled by user.',
            ],
        'worker_not_available' =>
            [
                'ar' => '  عذرا.. العامل الذي قمت بإختياره غير متاح حاليا',
                'en' => 'Sorry,, the worker is not available right now',
            ],
        'order_in_10' =>
            [
                'ar' => ' يمكنك انهاء الطلب بعد مرور عشرة دقائق من وقت الطلب مع التوااجد في موقع الطلب',
                'en' => 'You can finish after ten minutes of order time and be in order location',
            ],
        'outOfArea' =>
            [
                'ar' => '   من فضلك تأكد من تواجدك في موقع الطلب  ',
                'en' => 'Please,Check that you are at the order location',
            ],
        'no_available_in_city' =>
            [
                'ar' => 'عفوا هذه الخدمة غير متاحة في المدينة ',
                'en' => 'Sorry ,service not available in city',
            ],
        'order_before_now' =>
            [
                'ar' => 'عذرا توقيت الطلب قبل الوقت الحالي',
                'en' => 'Sorry ,Your order time before time now',
            ],
        'delegate_subscribed' =>
            [
                'ar' => 'تم تسجيلك بنجاح لهذا المتجر',
                'en' => 'You have subscribed successfully to this shop',
            ],
        'delegate_unsubscribed' =>
            [
                'ar' => 'تم إلغاء تسجيلك لهذا المتجر',
                'en' => 'You have cancelled subscribion to this shop',
            ],
        'points_invalid' =>
            [
                'ar' => 'خطأ في النقاط للتحويل',
                'en' => 'Invalid points to transfer',
            ],
        'max_offers_limit' =>
            [
                'ar' => 'لقد وصلت للحد الأقصي للعروض',
                'en' => 'You reached max sliders limit',
            ],
        'points_replaced_success' =>
            [
                'ar' => 'لقد تم استبدال نقاطك بنجاح',
                'en' => 'Your points replaced successfully',
            ],
        'not_enough_points' =>
            [
                'ar' => 'عذرا, ليس لديك نقاط كافية',
                'en' => "You haven't enough points",
            ],

        'cant_follow' =>
            [
                'ar' => 'لا يمكنك المتابعة.',
                'en' => 'You Can not follow.',
            ],
        'cant_review' =>
            [
                'ar' => 'لا يمكنك التقيم.',
                'en' => 'You Can not review.',
            ],
        'unfollowed' =>
            [
                'ar' => 'تم إلغاء المتابعة بنجاح.',
                'en' => 'Unfollowed Captin successfully.',
            ],
        'failed' =>
            [
                'ar' => 'بيانات غير متطابقة.',
                'en' => 'Data Error.',
            ],
        'unblocked' =>
            [
                'ar' => ' تم إلغاء الحذر بنجاح .',
                'en' => ' Unblocked successfully.',
            ],
        'no_drivers' =>
            [
                'ar' => 'عذرا, لا يوجد سائقين متاحين الان',
                'en' => ' Sorry, No drivers available now.',
            ],
        'trip_go_to_other' =>
            [
                'ar' => 'عذرا, تم قبول الرحلة من سائق اخر    ',
                'en' => ' Sorry, trip accepted by another driver.',
            ],
        'empty_wallet' =>
            [
                'ar' => 'عذرا, الرصيد غير كافي برجاء تغيير وسيلة الدفع ',
                'en' => ' Sorry, Wallet is empty, please change payment way
            .',
            ],
        'payment_success' =>
            [
                'ar' => 'تمت عملية الدفع بنجاح ',
                'en' => ' Payment success.',
            ],
        'payment_failed' =>
            [
                'ar' => 'فشلت عملية الدفع ',
                'en' => ' Payment failed.',
            ],
        'pay_your_debit' =>
            [
                'ar' => 'يرجي سداد المديونية حتي تتمكن من اكمال الطلب',
                'en' => ' Please pay off the debt to complete the order.',
            ],
        'cant_reject_order' =>
            [
                'ar' => 'لا يمكنك رفض الطلب',
                'en' => 'You can\'t reject order.',
            ],
        'user_cancel_order' =>
            [
                'ar' => 'قام العميل بإلغاء الطلب',
                'en' => 'User has cancelled the order.',
            ],
        'no_delegates_available' =>
            [
                'ar' => 'لا يوجد مناديب متاحين',
                'en' => 'No delegates available.',
            ],
        'promo-code-added' =>
            [
                'ar' => 'تم إضافة كود الخصم بنجاح',
                'en' => 'Promo Code Added Successfully.',
            ],
        'cant_change_delegate' =>
            [
                'ar' => 'لا يمكنك تغيير المندوب',
                'en' => 'you can\'t change delegate.',
            ],
        'order-not-ready' =>
            [
                'ar' => 'لم يتم تجهيز الطلب',
                'en' => 'order is not ready yet.',
            ],
        'exceeded_confirm_order_count' =>
            [
                'ar' => ' تم تخطي العدد المسموح بيه لتأكيد الطلب',
                'en' => 'Confirm order count is exceeded.',
            ],
        'has-no-trip' =>
            [
                'ar' => ' لا يوجد رحلة',
                'en' => 'No trip found.',
            ],
        'ready_to_receive_trips' =>
            [
                'ar' => 'جاهز لاستقبال الرحلات',
                'en' => 'Ready to receive trips.',
            ],
        'pending-approval' =>
            [
                'ar' => 'يتم مراجعة المندوب من المتجر',
                'en' => 'Delegate is under review.',
            ],
        'invoice-not-confirmed' =>
            [
                'ar' => 'لم يتم تأكيد الفاتورة',
                'en' => 'Invoice not confirmed yet.',
            ],

    ];
